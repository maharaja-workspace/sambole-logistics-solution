
function geetQuestionUI(question) {
    let mainDivClass = "col-xs-12";
    try {
        mainDivClass = catQnMainDivClass;
    } catch (error) {
        
    }
    returnData = '<div class="' + mainDivClass + '" >';
    returnData += '<label>' + question.question + '</label>';

    if (question.checkbox == 1 && question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
        returnData += '<div class="col-xs-12" >';
        let ind = 0;
        $.each(question.answer_options, function (index, option) {
            let checked = "" ;
            if (question.hasOwnProperty('answers')){
                $.each(question.answers, function (answerIndex, answer){
                    
                    if (answer.answer_option_id == option.id){
                        checked = "checked";
                    }
                });
            }
            returnData += '<div class="col-md-4 min-height-38"><label><input type="checkbox" ' + checked + ' value="' + option.id + '" name=ansOptions[' + question.id + '][' + ind + ']> ' + option.option + '</label></div>';
            ind++;
        });
        returnData += '</div><div id="ansOptions_' + question.id + '_error' +'"></div>';

    } else if (question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
        let plcaeHolder = "";
        if (question.hint) {
            plcaeHolder = '(Hint : ' + question.hint + ' )';
        }
        returnData += '<select  placeholder="Select an answer ' + plcaeHolder + '" class="form-control answer_options" name="ansOptions[' + question.id + '][0]">';

        returnData += '<option value="" >Select an answer</option>';
        $.each(question.answer_options, function (index, option) {
            let selected = "";
            if (question.hasOwnProperty('answers')) {
                $.each(question.answers, function (answerIndex, answer) {
                    if (answer.answer_option_id == option.id) {
                        selected = "selected";
                    }
                });
            }
            returnData += '<option ' + selected + ' value=' + option.id + '> ' + option.option + '</option>';
        });
        returnData += '</select><div id="ansOptions_' + question.id + '_error'+'"></div>';
    } else {
        let plcaeHolder = "";
        if (question.hint) {
            plcaeHolder = question.hint;
        }
        let userAnswer = "";

        if (question.hasOwnProperty('answers')) {
            $.each(question.answers, function (answerIndex, answer) {
                
                if (answer.category_question_id == question.id) {
                    console.log(answer.answer);
                    userAnswer = answer.answer;
                }
            });
        }
        returnData += '<input class="form-control" placeholder="' + plcaeHolder + '" name="ansOptions[' + question.id + '][0]" value="' + userAnswer +'" >';

    }
    returnData += '</div>';
    return returnData;
}
var questionCheckBoxes = [];
var questionSelects = [];
function setQuestionValidation(question){
    
    if (question.validations){
        let validations = [];
        $.each(question.validations, function(index, validation){
            if (validation.validation){
                let element ;
                if (question.checkbox == 1 && question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
                    if (validation.validation.rule_tag == "required") {
                        
                        validations.push({
                            required :  true
                        });
                    } 
                    
                } if (question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
                    if (validation.validation.rule_tag == "required") {
                        validations.push({
                            required: true
                        });
                    } 
                }else{
                    
                        element = $("input[name='ansOptions[" + question.id + "][0]'");
                    

                    if (validation.validation.rule_tag == "max") {
                        element.rules("add", {
                            max: validation.value
                        });
                    } else if (validation.validation.rule_tag == "min") {
                        element.rules("add", {
                            min: validation.value
                        });
                    } else if (validation.validation.rule_tag == "required") {
                        element.rules("add", {
                            required: true
                        });
                    } else if (validation.validation.rule_tag == "int") {
                        element.rules("add", {
                            number: true
                        });
                    } else if (validation.validation.rule_tag == "price") {
                        element.rules("add", {
                            pattern: /^(\d+(,\d{1,2})?)?$/
                        });
                    } 
                } 
                
               
            }
        });

        if (question.checkbox == 1 && question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
            questionCheckBoxes.push({
                name: 'ansOptions[' + question.id + ']',
                errorDiv: 'ansOptions_' + question.id + '_error',
                validations: validations
            });
        } if (question.hasOwnProperty('answer_options') && question.answer_options.length > 0) {
            questionSelects.push({
                name: 'ansOptions[' + question.id + ']',
                errorDiv: 'ansOptions_' + question.id + '_error',
                validations: validations
            });
        }
        
    }
}

function validateQuestionCheckboxes(){
    let valid = true;
    $.each(questionCheckBoxes, function(index, validations){
        $.each(validations.validations, function (tag, validation){
            if (validation.hasOwnProperty("required")){
                if($('input[name^="' + validations.name+'"]:checked').length == 0){
                    $("#" + validations.errorDiv).html('<label style="color: #cc5965; display: inline-block; margin-left: 5px;" class="'+validations.name+'_error col-xs-12">This field is required.</label>');
                    valid =  false;
                }else{
                    $("#" + validations.errorDiv).html("");
                }
            }
        });
    });

    return valid;
}

function validateQuestionsSelects() {
    let valid = true;
    $.each(questionSelects, function (index, validations) {
        $.each(validations.validations, function (tag, validation) {
            if (validation.hasOwnProperty("required")) {
                if ($('select[name^="' + validations.name + '"]').val() == "" ) {
                        
                    $("#" + validations.errorDiv).html('<label style="color: #cc5965; display: inline-block; margin-left: 5px;" class="' + validations.name + '_error col-xs-12">This field is required.</label>');

                    
                    valid = false;
                } else {
                    $("#" + validations.errorDiv).html("");
                }
            }
        });
    });

    return valid;
}