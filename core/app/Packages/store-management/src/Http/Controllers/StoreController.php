<?php
namespace StoreManage\Http\Controllers;

use App\Http\Controllers\Controller;
use StoreManage\Models\Store;
use App\Models\Bin;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;


class ZoneController extends Controller {

    function index(){
        return view( 'ZoneManage::list');
    }

    public function jsonList(Request $request)
    {
            $jsonList = array();
            $i=1;
            foreach (Area::get() as $area) {

                $dd = array();
                array_push($dd, $area->name);
                array_push($dd, $i);

                array_push($dd, $area->description);

                $permissions = Permission::whereIn('name',['areas.edit','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('areas/'.$area->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit Area"><i class="fa fa-pencil"></i></a></center>');
                }

                $permissions = Permission::whereIn('name',['areas.delete','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){
                    array_push($dd, '<center><a href="#" class="area-delete" data-id="'.$area->id.'" data-toggle="tooltip" data-placement="top" title="Delete Area"><i class="fa fa-trash-o"></i></a></center>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data'=>$jsonList));

    }

    function addZone(){
        return view('ZoneManage::add');
    }

    function storeZone(Request $request){
        dd($request->all());
       $zone =  Zone::create([
           'name' => $request->zone_name,
           'description' => $request->description,
           'description' => $request->area,
        ]);

       $store = Store::class;

       if ($zone){
           return redirect('zone/add')->with([ 'success' => true,
               'success.message'=> 'Zone Created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('zone/add')->with([ 'error' => true,
            'error.message'=> 'Zone did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $area = Area::find($id);
        return view('AreaManage::edit')->with('area', $area);
    }

    function update(Request $request, $id){
        $area = Area::find($id);
        if (!$area){
            return redirect('areas/')->with([ 'error' => true,
                'error.message'=> 'Area did not found ',
                'error.title' => 'Ooops !']);
        }

        $area->name = $request->area_name;
        $area->description = $request->description;

        if ($area->save()){
            return redirect('areas')->with([ 'success' => true,
                'success.message'=> 'Area updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('areas/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Area did not updated ',
            'error.title' => 'Ooops !']);
    }
}