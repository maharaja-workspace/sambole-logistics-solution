<?php
namespace StoreManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use StockManage\Models\Stock;
use VehicleManage\Models\Vehicle;
use ZoneManage\Models\Zone;

class Store extends Model{
    use SoftDeletes;
    protected $fillable = ['related_id', 'store_type', 'status'];

    function zone(){
        return $this->belongsTo(Zone::class, 'related_id');
    }

    function vehicle(){
        return $this->belongsTo(Vehicle::class, 'related_id');
    }

    function stock()
    {
        return $this->hasMany(Stock::class, "store_id");
    }
}