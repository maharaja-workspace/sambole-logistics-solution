@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Package Weights</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Package Weight </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form" action="{{url("package-weights/".$packageWeight->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Package Weight Code <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled  name="package_max_weight" class="form-control" value="{{$packageWeight->id}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Package Min Weight <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="number" min="0" id="package_min_weight" name="package_min_weight" class="form-control" value="{{$packageWeight->min_weight}}"></div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Package Max Weight(in grams) <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="number" min="0" name="package_max_weight" class="form-control" value="{{$packageWeight->max_weight}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Package Price <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="number" min="0" name="package_price" class="form-control" value="{{$packageWeight->price}}"></div>
                </div>
               <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button class="btn btn-danger" type="button" style="float: right;" onclick="location.reload();">Cancel</button>
                    </div>
                </div>

                

            </form>

        </div>
    </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();
           $.validator.addMethod('minStrict', function (value, el, param) {
               return value > param;
           });



           $("#form").validate({
            rules: {
                package_min_weight: {
                    required: true,
                    number : true,
                    minStrict : 0
                },
                package_max_weight: {
                    required: true,
                    number : true,
                    min : function(element){
                        return parseFloat($('#package_min_weight').val()) ;
                    }
                },
                package_price : {
                    required: true,
                    number : true,
                    minStrict : 0
                }
            },
               messages: {
                   package_price: {
                       minStrict: "Please enter value greater than 0",
                   },
                   package_min_weight: {
                       minStrict: "Please enter value greater than 0",
                   },
               },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop