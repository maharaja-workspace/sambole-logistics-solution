<?php
namespace PackageWeightManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageWeightManage\Models\PackageWeight;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use Yajra\Datatables\Datatables;


class PackageWeightController extends Controller {

    function index(){
        return view( 'PackageWeightManage::package-weights');
    }


    public function listData(Request $request)
    {
        $permissions = Permission::whereIn('name',['package-weights.edit','admin'])->where('status','=',1)->lists('name');

        return Datatables::of(PackageWeight::query())
            ->addColumn("edit", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<a href="#" class="blue" onclick="window.location.href=\''.url('package-weights/'.$filteredData->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit package size"><i class="fa fa-pencil"></i></a>';
                }
            })
            ->addColumn("status", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    if($filteredData->status == 1){
                        return '<a href="javascript:void(0)" form="noForm" class="blue package-weights-status-toggle " data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></>';
                    }
                    else if($filteredData->status == 0){
                        return  '<a href="javascript:void(0)" class="blue package-weights-status-toggle " data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></>';
                    }
                }
            })
            ->make(true);
    }

    function create(){
        return view('PackageWeightManage::package-weight-add');
    }

    function store(Request $request){
        $packageWeight =  PackageWeight::create([
           'min_weight' => $request->package_min_weight,
           'max_weight' => $request->package_max_weight,
           'price' => $request->package_price,
        ]);

       if ($packageWeight){
           return redirect('package-weights/create')->with([ 'success' => true,
               'success.message'=> 'Package weight created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('package-weights/create')->with([ 'error' => true,
            'error.message'=> 'Package weight did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $packageWeight = PackageWeight::find($id);
        return view('PackageWeightManage::package-weight-edit')->with('packageWeight', $packageWeight);
    }

    function update(Request $request, $id){
        $packageWeight = PackageWeight::find($id);
        if (!$packageWeight){
            return redirect('package-weights/')->with([ 'error' => true,
                'error.message'=> 'Package weight did not found ',
                'error.title' => 'Ooops !']);
        }

        $packageWeight->min_weight = $request->package_min_weight;
        $packageWeight->max_weight = $request->package_max_weight;
        $packageWeight->price = $request->package_price;

        if ($packageWeight->save()){
            return redirect('package-weights')->with([ 'success' => true,
                'success.message'=> 'Package weight updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('package-weights/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Package weight size  did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $packageWeight = PackageWeight::find($id);
            if($packageWeight){
                $packageWeight->delete();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    function  toggleStatus(Request $request){
        $weight = PackageWeight::find($request->id);
        $msg = "";
        if ($weight){
            if ($weight->status == 1){
                $weight->status = 0;
                $msg = "You have Deactivated Successfully";
            }else if ($weight->status == 0){
                $weight->status = 1;
                $msg = "You have Activated Successfully";
            }
            $weight->save();

            return $msg;
        }
    }
}