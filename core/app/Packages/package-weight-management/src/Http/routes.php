<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'PackageWeightManage\Http\Controllers'], function(){
        Route::post('package-weights/list/data', 'PackageWeightController@listData')->name("package-weights.index");
        Route::post('package-weights/status-toggle', 'PackageWeightController@toggleStatus')->name("package-weights.update");
        Route::resource('package-weights', 'PackageWeightController');
    });

});


