<?php

namespace PackageWeightManage;

use Illuminate\Support\ServiceProvider;

class PackageWeightServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'PackageWeightManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackageWeightManage', function(){
            return new PackageWeightManage;
        });
    }
}
