<?php
namespace PackageWeightManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageWeight extends Model{
    use SoftDeletes;
    protected $fillable = ['name', 'min_weight', 'max_weight', 'price'];
}