<?php

namespace CityManage\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $table = 'cities';

    protected $fillable = ['name', 'postal_code'];

    public function zones()
    {
        return $this->belongsToMany('ZoneManage\Models\Zone', 'zone_city', 'city_id', 'zone_id')->withTimestamps();
    }

    public function routes()
    {
        return $this->belongsToMany('RouteManage\Models\Routing', 'route_city', 'city_id', 'route_id')->withTimestamps();
    }
}
