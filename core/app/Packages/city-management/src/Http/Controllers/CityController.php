<?php
namespace CityManage\Http\Controllers;

use App\Http\Controllers\Controller;
use CityManage\Models\City;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use Yajra\Datatables\Datatables;


class CityController extends Controller {

    function index(){
        return view( 'CityManage::list');
    }

    public function listData(Request $request)
    {
        $permissions = Permission::whereIn('name',['cities.edit','admin'])->where('status','=',1)->lists('name');

//        return $this->listDataNew($request, $permissions);

        return Datatables::of(City::query())
            ->addColumn("edit", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<a href="#" class="blue" onclick="window.location.href=\''.url('cities/'.$filteredData->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit City"><i class="fa fa-pencil"></i></a></>';
                }
            })
            ->addColumn("status", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    if($filteredData->status == 1){
                        return '<a href="javascript:void(0)" form="noForm" class="blue city-status-toggle " data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></>';
                    }
                    else if($filteredData->status == 0){
                        return  '<a href="javascript:void(0)" class="blue city-status-toggle " data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></>';
                    }
                }
            })
            ->make(true);

    }

    public function listDataNew($request, $permissions)
    {

        $jsonList = array();

        $columns = array(
            0 => 'id',
            1 => 'postal_code',
            4 => 'name'
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $cities = City::query();

        $totalData = $cities->count();
        $searchExists = false;
        if ($request->input('columns.0.search.value')) {
            $searchExists = true;
            $cities = $cities->where('id', $request->input('columns.0.search.value'));
        }
        if ($request->input('columns.1.search.value')) {
            $searchExists = true;
            $cities = $cities->where('postal_code', 'LIKE',"%{$request->input('columns.1.search.value')}%");
        }
        if ($request->input('columns.2.search.value')) {
            $searchExists = true;
            $cities = $cities->where('name', 'LIKE',"%{$request->input('columns.2.search.value')}%");
        }

        if ($limit != -1) {
            $cities = $cities->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir);
        }

        $cities = $cities->get();

        $totalFilteredData = $cities->count();

        if ($cities->count()) {
            foreach ($cities as $city) {
                $dd = array();


                array_push($dd, $city->id);
                array_push($dd, $city->postal_code);
                array_push($dd, $city->name);
                if(Sentinel::hasAnyAccess($permissions)){
                    $edit = '<a href="#" class="blue" onclick="window.location.href=\''.url('cities/'.$city->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit City"><i class="fa fa-pencil"></i></a></>';
                    array_push($dd, $edit);
                }
                if(Sentinel::hasAnyAccess($permissions)){
                    if($city->status == 1){
                        $inactive = '<a href="javascript:void(0)" form="noForm" class="blue city-status-toggle " data-id="'.$city->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></>';
                        array_push($dd, $inactive);
                    }
                    else if($city->status == 0){
                        $active = '<a href="javascript:void(0)" class="blue city-status-toggle " data-id="'.$city->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></>';
                        array_push($dd, $active);
                    }
                }

                array_push($jsonList, $dd);
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFilteredData),
            "data"            => $jsonList
        );

        echo json_encode($json_data);


    }

    function create(){
        return view('CityManage::add');
    }

    function store(Request $request){
       $area =  City::create([
           'name'        => $request->city_name,
           'postal_code' => $request->postal_code,
        ]);

       if ($area){
           return redirect('cities')->with([ 'success' => true,
               'success.message'=> 'City Created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('cities/create')->with([ 'error' => true,
            'error.message'=> 'City did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $city = City::find($id);
        return view('CityManage::edit')->with('city', $city);
    }

    function update(Request $request, $id){
        $city = City::find($id);
        if (!$city){
            return redirect('cities/')->with([ 'error' => true,
                'error.message'=> 'City did not found ',
                'error.title' => 'Ooops !']);
        }

        $city->name = $request->city_name;
        $city->postal_code = $request->postal_code;

        if ($city->save()){
            return redirect('cities')->with([ 'success' => true,
                'success.message'=> 'City updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('cities/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'City did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $city = City::find($id);
            if($city){
                $city->status = -1 ;
                if($city->save()){
                    return response()->json(['status' => 'success']);
                }
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    function verify($verify, Request $request){
        if($verify == "name"){
            if (City::where('name', $request->city_name)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function verifyUpdate($verify, Request $request){
        if($verify == "name"){
            if (City::where('name', $request->city_name)->where('id', "!=", $request->id)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function  toggleStatus(Request $request){
        $city = City::find($request->id);
        $msg = "";
        if ($city){
            if ($city->status == 1){
                $city->status = 0;
                $msg = "You have Deactivated Successfully";
            }else if ($city->status == 0){
                $city->status = 1;
                $msg = "You have Activated Successfully";
            }
            $city->save();
            return $msg;
        }
    }
}