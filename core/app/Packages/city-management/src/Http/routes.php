<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'CityManage\Http\Controllers'], function(){
        Route::post('cities/list/data', 'CityController@listData')->name('cities.index');
        Route::post('cities/verify/{fieldName}', 'CityController@verify')->name('cities.verify');
        Route::post('cities/verify-update/{fieldName}', 'CityController@verifyUpdate')->name('cities.verify');
        Route::post('cities/status-toggle', 'CityController@toggleStatus')->name('cities.update');
        Route::resource('cities', 'CityController');
    });

});


