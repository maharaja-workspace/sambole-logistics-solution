@extends('layouts.back.master') @section('current_title','City Management | Edit City')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>City Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>City Edit</strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form" action="{{url("cities/".$city->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">City Code <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled name="area_code" class="form-control" value="{{$city->id}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Postal Code<span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="postal_code" class="form-control" value="{{$city->postal_code}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">City Name <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="city_name" class="form-control" value="{{$city->name}}"></div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button class="btn btn-danger" type="button" style="float: right;" onclick="location.reload();">Cancel</button>
                    </div>
                </div>

                

            </form>

        </div>
    </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The area name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                city_name: {
                    required: true,
                    maxlength: 20             
                },
                postal_code:{
                    required: true,
                    maxlength: 10,
                    digits: true
                }
            },
            messages:{
                postal_code: {digits:"Please enter valid postal code"}
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop