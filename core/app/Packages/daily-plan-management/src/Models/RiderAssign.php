<?php

namespace DailyPlanManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use StoreManage\Models\Store;

class RiderAssign extends Model
{
    use SoftDeletes;

    protected $fillable = ['rider_id', 'store_id', 'route_id', 'request_id', 'active', 'assigned_by', 'completed'];

    public function getRider()
    {
        return $this->belongsTo('RiderManage\Models\Rider', 'rider_id');
    }

    public function rider()
    {
        return $this->belongsTo('RiderManage\Models\Rider', 'rider_id', 'id');
    }

    public function route()
    {
        return $this->belongsTo('RouteManage\Models\Routing', 'route_id', 'id');
    }

    public function rider_vehicle()
    {
        return $this->belongsTo('DailyPlanManage\Models\RiderVehicle', 'rider_id', 'rider_id');
    }

    function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }
}
