<?php

namespace DailyPlanManage;

use Illuminate\Support\ServiceProvider;

class DailyPlanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'DailyPlanManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DailyPlanManage', function(){
            return new DailyPlanManage();
        });
    }
}
