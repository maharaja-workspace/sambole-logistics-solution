<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'DailyPlanManage\Http\Controllers', 'prefix' => 'daily-plan'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'dailyplan.list', 'uses' => 'DailyPlanController@index'
        ]);

        Route::get('json/pending/list', [
            'as' => 'dailyplan.list', 'uses' => 'DailyPlanController@jsonPendingList'
        ]);

        Route::get('json/pending-deliveries/list', [
            'as' => 'dailyplan.list', 'uses' => 'DailyPlanController@jsonPendingDeliveryList'
        ]);

        Route::post('assign/requests', [
            'as' => 'dailyplan.add', 'uses' => 'DailyPlanController@assignRequests'
        ]);


    });
});


