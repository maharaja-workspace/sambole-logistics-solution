<?php

namespace DailyPlanManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use Carbon\Carbon;
use DailyPlanManage\Models\RiderAssign;
use DailyPlanManage\Models\RiderVehicle;
use DB;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use Illuminate\Http\Request;
use Response;
use RiderManage\Models\Rider;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\Stock;
use StoreManage\Models\Store;
use VehicleManage\Models\Vehicle;

class DailyPlanController extends Controller
{

    public function index()
    {
        $logged_in_user = Sentinel::getUser();

        if ($logged_in_user->inRole('zone-manager')) {

            $routes = Routing::where('zone_id', $logged_in_user->zone['id'])->get();

            $rider_ids = RiderVehicle::where(function ($q) {
                $q->where('assigned_date', Carbon::today())
                    ->orWhere('assigned_date', '!=', Carbon::today());
            })->where('status', 1)->lists('rider_id')->toArray();
            $vehicle_ids = RiderVehicle::where(function ($q) {
                $q->where('assigned_date', Carbon::today())
                    ->orWhere('assigned_date', '!=', Carbon::today());
            })->where('status', 1)->lists('vehicle_id')->toArray();
//            $riders = Rider::whereNotIn('id', $rider_ids)->where('zone', $logged_in_user->zone['id'])->get();
//            $vehicles = Vehicle::whereNotIn('id', $vehicle_ids)->where('zone_id', $logged_in_user->zone['id'])->get();
//            $riders = Rider::where(function ($q){
//                        $q->doesnthave('rider_assign')
//                        ->orWhereHas('rider_assign', function ($q){
//                            $q->latest()->where('active', '!=', 1);
//                        });
//                      })->where('zone', $logged_in_user->zone['id'])->get();

            $riders = Rider::where('zone', $logged_in_user->zone['id'])->lists('id')->toArray();
            foreach ($riders as $k => $rider) {
                $assign = RiderAssign::where('rider_id', $rider)->orderBy('created_at', 'desc')->first();
                if ($assign) {
                    if ($assign->active == 1) {
                        unset($riders[$k]);
                    }
                }
            }
            $riders = Rider::whereIn('id', $riders)->get();

//            $vehicles = Vehicle::where(function ($q){
//                            $q->doesnthave('rider_vehicle')
//                              ->orWhereHas('rider_vehicle', function ($q){
//                                  $q->latest()->where('status', '!=', 1);
//                              });
//                        })
//                        ->where('zone_id', $logged_in_user->zone['id'])->get();
            $vehicles = Vehicle::where('zone_id', $logged_in_user->zone['id'])->lists('id')->toArray();
            foreach ($vehicles as $k => $vehicle) {
                $riderVehicle = RiderVehicle::where('vehicle_id', $vehicle)->orderBy('created_at', 'desc')->first();
                if ($riderVehicle) {
                    if ($riderVehicle->status == 1) {
                        unset($vehicles[$k]);
                    }
                }
            }
            $vehicles = Vehicle::whereIn('id', $vehicles)->where('status', 1)->get();


            return view('DailyPlanManage::list')->with(['routes' => $routes, 'riders' => $riders, 'vehicles' => $vehicles]);
        }

        return redirect('admin');
    }

    public function array_column_recursive(array $haystack, $needle)
    {
        $found = [];
        array_walk_recursive($haystack, function ($value, $key) use (&$found, $needle) {
            if ($key == $needle) {
                $found[] = $value;
            }

        });
        return $found;
    }

    public function jsonPendingList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();
        if ($request->id && $request->id !== null) {
            $cities = Routing::find($request->id)->cities()->lists('city_id')->toArray();

            $customer_requests = CustomerRequest::where('status', 0);
            if ($isCentralWarehouse) {
                $customer_requests = $customer_requests->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 1);
                    });
                });
            } else {
                $customer_requests = $customer_requests->whereIn('pickup_city', $cities)
                    ->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                        });
                    });;
            }
            $customer_requests = $customer_requests->get();

            foreach ($customer_requests as $customer_request) {

                $dd = array();

                $customer = Customer::find($customer_request->customer_id);

                array_push($dd, '<td><input class="check" type="checkbox" value="' . $customer_request->id . '"></td>');
                array_push($dd, $customer_request->order_id);
                array_push($dd, $customer_request->merchant_order_id);
                array_push($dd, $customer->full_name);
                array_push($dd, $customer->mobile);
                array_push($dd, $customer->full_address);
                array_push($dd, $customer_request->pickupCity ? $customer_request->pickupCity->name : "-");
                array_push($dd, $customer_request->buyerCity ? $customer_request->buyerCity->name : "-");
                array_push($dd, $customer_request->vehicle_number);
                array_push($jsonList, $dd);
            }
        }
        return Response::json(array('data' => $jsonList));
    }

    public function jsonPendingDeliveryList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();
        if ($request->id && $request->id !== null) {

            $route = Routing::find($request->id);
            $bins = Bin::where('related_id', $route->id)->where('bin_type', 'route')->lists('id')->toArray();
            $requestIds = Stock::whereIn('bin_id', $bins)->where('availability', 'in')->lists('request_id')->toArray();


            $customer_requests = CustomerRequest::where('status', 3)->whereIn('id', $requestIds);
            if ($isCentralWarehouse) {
                $customer_requests = $customer_requests->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 1);
                    });
                });
            } else {
                $customer_requests = $customer_requests->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                        });
                    });
            }
            $customer_requests = $customer_requests->get();

            foreach ($customer_requests as $customer_request) {
                $dd = array();
                array_push($dd, '<td><input class="checkDeliveries" type="checkbox" value="' . $customer_request->id . '"></td>');
                array_push($dd, $customer_request->order_id);
                array_push($dd, $customer_request->merchant_order_id);
                array_push($dd, $customer_request->buyer_name);
                array_push($dd, $customer_request->buyer_mobile);
                array_push($dd, $customer_request->buyer_address);
                array_push($dd, $customer_request->pickupCity ? $customer_request->pickupCity->name : "-");
                array_push($dd, $customer_request->buyerCity ? $customer_request->buyerCity->name : "-");
                array_push($dd, $customer_request->vehicle_number);
                array_push($jsonList, $dd);
            }

        }

        return Response::json(array('data' => $jsonList));
    }

    public function assignRequests(Request $request)
    {

        $logged_in_user = Sentinel::getUser();

        if ($request->req_ids) {
            foreach ($request->req_ids as $req_id) {
                if ($req_id !== null || $req_id !== [] || $req_id !== '') {
                    $c_request = CustomerRequest::find($req_id)->update(['status' => 1]);
                    $store = Store::where('related_id', $request->vehicle)->where('store_type', 'vehicle')->first();

                    RiderAssign::create([
                        'rider_id' => $request->rider,
                        'store_id' => $store['id'],
                        'route_id' => $request->c_route,
                        'request_id' => $req_id,
                        'assigned_by' => $logged_in_user->id,
                    ]);

                    $riderVehicle = RiderVehicle::where('rider_id', $request->rider)->where('vehicle_id', $request->vehicle)
                        ->where('assigned_date', Carbon::today())->where('status', 1)->first();
                    if (!$riderVehicle) {
                        RiderVehicle::create([
                            'rider_id' => $request->rider,
                            'vehicle_id' => $request->vehicle,
                            'assigned_date' => Carbon::today(),
                            'assigned_by' => $logged_in_user->id,
                            'status' => 1,
                        ]);
                    }

                }
            }
//            return response('Success');
        }
        if ($request->has("delivery_req_ids") && $request->has('rider') && $request->has('vehicle')) {
            foreach ($request->delivery_req_ids as $key => $delivery_req_id) {
                try {
                    DB::beginTransaction();
                    $stock = Stock::where('request_id', $delivery_req_id)->first();

                    if ($stock) {
                        $stock->availability = 'out';
                        $stock->save();

                        $rider_assign = RiderAssign::where('rider_id', $request->rider)
                            ->where('completed', 0)
                            ->orderBy('created_at', 'desc')
                            ->first();

                        $store_id = null;
                        $rider_vehicle = RiderVehicle::where('rider_id', $request->rider)->orderBy('created_at', 'desc')->first();

                        $store = Store::where('store_type', 'vehicle')
                            ->orderBy('created_at', 'desc');

                        if ($rider_vehicle) {
                            $store = $store->where('related_id', $rider_vehicle->vehicle_id);
                        } else {
                            $store = $store->where('related_id', $request->vehicle);
                        }
                        $store = $store->first();

                        if ($store) {
                            $store_id = $store->id;
                        }

                        RiderAssign::create([
                            'rider_id' => $request->rider,
                            'store_id' => $store_id,
                            'route_id' => $request->c_route,
                            'request_id' => $delivery_req_id,
                            'assigned_by' => $logged_in_user->id,
                            'active' => 1,
                            'completed' => 0,
                        ]);

                        CustomerRequest::find($delivery_req_id)->update(['status' => 4]);

                    }
                    DB::commit();
                } catch (\Exception $exception) {
                    \Log::error("DELIVERY REQ FAILED = " . $exception->getMessage());
                    DB::rollBack();
                }

            }
        }

        return response('Success');
    }
}
