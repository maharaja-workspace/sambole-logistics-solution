@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

    /* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    #Wheader {
        margin-left: 10px;
    }

    .align-middle {
        margin-top: 3%;
    }

    .h4 {
        font-size: 14px;
    }

    .balance {
        margin-top: 2%;
        width: 50%;
    }

    .Fill {
        padding-left: 2px;
        padding-right: 2px;
    }


    .clickable {
        cursor: pointer;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Warehouse Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Warehouse View</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2><small>&nbsp;</small></h2>
    <ol class="breadcrumb text-right">



    </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
    onclick="location.href = '{{url('wareHouse/add')}}';">
    <p class="plus">+</p>
</div>

@if(Sentinel::getUser()->inRole('admin'))
<div class="row">

    <div class="col-lg-12">
        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-1">
                    <label class="h4">Zone: </label></div>
                <div class="col-sm-4">
                    <select class="js-source-states form-control" id="zones">
                        <option value="all">All</option>
                        @foreach($zones as $zone)
                        <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>


</div>
@endif

<form method="POST" class="form-horizontal" id="form">
    {!!Form::token()!!}
    <div class="row" style="margin-bottom: 100px;">
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class=" clickable">
                    <h4 class="toggle" type="button" id="Ms" data-toggle="collapse" data-target="#MainStore"><span
                            class="glyphicon glyphicon-plus"></span> &nbsp Zone Store</h4>
                </div>

                <div id="MainStore" class=" collapse">
                    <div class = "table-responsive">
                    <table id="example1" class="table table-striped table-responsive table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Package ID</th>
                                <th>Merchant Order ID</th>
                                <th>Package Type</th>
                                <th>Package Size</th>
                                <th>Bin</th>
                                <th>Delivery Type</th>
                                <th>Pickup Method</th>
                                <th>Payment Type</th>
                                <th>Buyer Name</th>
                                <th>Merchant Name</th>
                                <th>Pickup City</th>
                                <th>Destination City</th>
                                <th>Added Date</th>
                                @if(Sentinel::getUser()->inRole('zone-manager'))
                                    <th>Action</th>
                                @endif
                                @if(Sentinel::getUser()->inRole('admin'))
                                    <th>Zone</th>
                                @endif
                            </tr>
                        </thead>

                    </table>
                    </div>
                </div>


                <!--   </div>
</div>

<div class="row">
  <div class="col-lg-12 margins"> -->
                <div class="clickable">
                    <h4 class="toggle" type="button" id="bs" data-toggle="collapse" data-target="#BikeStore"><span
                            class="glyphicon glyphicon-plus"></span> &nbsp Vehicle Store</h4>
                </div>

                <div id="BikeStore" class=" collapse">
                    <div class = "table-responsive">
                    <table id="example2" class="table table-striped table-responsive table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Package ID</th>
                                <th>Merchant Order ID</th>
                                <th>Vehicle Name</th>
                                <th>Rider Name</th>
                                <th>Delivery Type</th>
                                <th>Pickup Method</th>
                                <th>Payment Type</th>
                                <th>Buyer Name</th>
                                <th>Merchant Name</th>
                                <th>Pickup City</th>
                                <th>Destination City</th>
                                <th>Type</th>
                                <th>Added Date</th>
                                @if(Sentinel::getUser()->inRole('admin'))
                                    <th>Zone</th>
                                @endif
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>

                    </table>
                    </div>
                </div>


                <!--   </div>
</div>

<div class="row">
  <div class="col-lg-12 margins"> -->
                @if(Sentinel::getUser()->inRole('admin'))
                <div class="clickable">
                    <h4 class="toggle" type="button" id="mps" data-toggle="collapse" data-target="#MobilePickup"><span
                            class="glyphicon glyphicon-plus"></span> &nbsp Vehicle Store (zone access)</h4>
                </div>

                <div id="MobilePickup" class=" collapse">
                    <div class = "table-responsive">
                    <table id="example3" class="table table-striped table-responsive table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Package ID</th>
                                <th>Merchant Order ID</th>
                                <th>Vehicle Number</th>
                                <th>Delivery Type</th>
                                <th>Pickup Method</th>
                                <th>Payment Type</th>
                                <th>Buyer Name</th>
                                <th>Merchant Name</th>
                                <th>Pickup City</th>
                                <th>Destination City</th>
                                <th>Pickup Zone</th>
                                <th>Destination Zone</th>
                                <th>Assigned Date</th>
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>

                    </table>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</form>

<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <span style="font-weight: 600;" class="modal-title text-center">Sambole Logistics Invoice</span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="printable">
                @if(!Sentinel::getUser()->inRole('admin'))
                <div id="codes">
                    <div class="row" style="margin-bottom: 3px;" id="customer_qr">
                        <div class="col-xs-5 col-sm-offset-1 Fill">
                            <label class="align-middle">Package QR Code</label>
                        </div>
                        <div class="col-xs-5 Fill">
                            <input type="text" class="form-control check_btn customer_qr model-clear" name="customer_qr" required
                                placeholder="Enter Code">
                            <h5>Press Enter key to confirm</h5>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 3px;" id="package_qr">
                        <div class="col-xs-5 col-sm-offset-1 Fill">
                            <label class="align-middle">Customer's Invoice Code</label>
                        </div>
                        <div class="col-xs-5 Fill">
                            <input type="text" class="form-control check_btn package_qr model-clear" name="package_qr" required
                                placeholder="Enter Package QR Code">
                            <h5>Press Enter key to confirm</h5>
                        </div>
                    </div>
                    <hr>
                </div>
                @endif
                <div style="text-align: center; display: none" id="title">
                    <h3 style="font-weight: 600;">Sambole Logistics
                        Invoice</h3>
                    <hr>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Reference Number</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <input type="text" class="form-control order_id model-clear" name="order_id" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Customer Name</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <input type="text" class="form-control buyer_name model-clear" name="buyer_name" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Mobile Number</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <input type="text" class="form-control buyer_mobile model-clear" name="buyer_mobile" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Package Type</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <!-- <input type="text" class="form-control" name="RefNum"> -->
                        <input type="text" class="form-control package_type model-clear" name="package_type" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Package Size</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <!-- <input type="text" class="form-control" name="RefNum"> -->
                        <input type="text" class="form-control package_size model-clear" name="package_size" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Package Weight</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <!-- <input type="text" class="form-control" name="RefNum"> -->
                        <input type="text" class="form-control package_weight model-clear" name="package_weight" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5  col-sm-offset-1 Fill">
                        <label class="align-middle">Delivery Type</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <!-- <input type="text" class="form-control" name="RefNum"> -->
                        <input type="text" class="form-control delivery_type model-clear" name="delivery_type" readonly>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 3px;">
                    <div class="col-xs-5 col-sm-offset-1 Fill">
                        <label class="align-middle">Payment Type</label>
                    </div>
                    <div class="col-xs-5 Fill">
                        <!-- <input type="text" class="form-control" name="RefNum"> -->
                        <input type="text" class="form-control payment_type model-clear" name="payment_type" readonly>
                    </div>
                </div>

                    <div class="row">
                        <div class="col-xs-5 col-sm-offset-1 Fill">
                            <label class="align-middle">Total Amount</label>
                        </div>
                        <div class="col-xs-5 Fill">
                            <!-- <input type="text" class="form-control" name="RefNum"> -->
                            <input type="text" class="form-control total_amount model-clear" name="total_amount" readonly>
                        </div>
                    </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <div id="printBtn" style="text-align: center; display: none">
                    <button type="button" class="btn btn-info print-btn" style="">PRINT <i
                            class="fa fa-print"></i></button>
                    <hr>
                </div>
                @if(!Sentinel::getUser()->inRole('admin'))
                <button type="button" class="btn btn-primary deliver-btn" disabled>DELIVER</button>
                @endif
                <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            </div>

        </div>
    </div>
</div>


@stop
@section('js')


<script type="text/javascript">
    $(document).ready(function() {
        $('.toggle').click(function(){
            $(this).find('span').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
        });
    });
</script>

<script type='text/javascript' src="{{asset('assets/back/js/plugins/JqueryPrint/jQuery.print.min.js')}}"></script>

<script>
    $(document).ready(function(){
        table = $('#example1').DataTable({
            "autoWidth": false,
            "order": [[0, "asc"]],
            "responsive": true,
            "ajax": '{{url('warehouse/json/zone/list?value=')}}' + 'all',
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Warehouse - Zone Store', className: 'btn-sm'},
                {extend: 'pdf', title: 'Warehouse - Zone Store', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
        });
        table2 = $('#example2').DataTable({
            "autoWidth": false,
            "order": [[0, "asc"]],
            "responsive": true,
             "ajax": '{{url('warehouse/json/vehicle/list?value=')}}' + 'all',
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Warehouse - Vehicle Store', className: 'btn-sm'},
                {extend: 'pdf', title: 'Warehouse - Vehicle Store', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
        });
        table3 = $('#example3').DataTable({
            "autoWidth": false,
            "order": [[0, "asc"]],
            "responsive": true,
             "ajax": '{{url('warehouse/json/zone-vehicle/list?value=')}}' + 'all',
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Warehouse - Vehicle Store(zone access)', className: 'btn-sm'},
                {extend: 'pdf', title: 'Warehouse - Vehicle Store(zone access)', className: 'btn-sm'},
                {extend: 'print', className: 'btn-sm'}
            ],
        });

        $("#zones").change(function(){
            table.ajax.url("{{ url('warehouse/json/zone/list?value=') }}" + $(this).val()).load();
            table2.ajax.url("{{ url('warehouse/json/vehicle/list?value=') }}" + $(this).val()).load();
            table3.ajax.url("{{ url('warehouse/json/zone-vehicle/list?value=') }}" + $(this).val()).load();
        });

        table.on('draw.dt', function () {
            $(".deliver").click(function(){
                $(".model-clear").val("");
                $("#codes").css('display', 'block');
                $("#title").css('display', 'none');
                $("#printBtn").css('display', 'none');
                $(".deliver-btn").prop('disabled', true);
                $(".customer_qr").val('');
                $(".package_qr").val('');
                $.ajax({
                    method: "POST",
                    url: '{{url('warehouse/get/request/data')}}',
                    data: {
                        'order_id': $(this).attr('data-orderid'),
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        $(".order_id").val(data.order_id);
                        // $("input[name='package_qr']").val(data.merchant_order_code);
                        $(".buyer_name").val(data.buyer_name);
                        $(".buyer_mobile").val(data.buyer_mobile);
                        $(".package_type").val(data.package_type);
                        $(".package_size").val(data.package_size);
                        $(".package_weight").val(data.package_weight);
                        $(".delivery_type").val(data.delivery_type);
                        $(".payment_type").val(data.payment_type);
                        $(".total_amount").val(data.total_amount);
                    }
                });
            });
        });

        $(".check_btn").keypress(function(e){
            var key = e.which;
            if(key == 13) {
                e.preventDefault();
                if($(".customer_qr").val() === ''){
                    toastr.warning('Cutromer Invoice code cannot be empty');
                }
                else if($(".package_qr").val() === ''){
                    toastr.warning('Package QR code cannot be empty');
                }
                else{
                    $.ajax({
                        method: "POST",
                        url: '{{url('warehouse/check/request/qr')}}',
                        data: {
                            'order_id': $(".order_id").val(),
                            'package_qr': $(".package_qr").val(),
                            'customer_qr': $(".customer_qr").val(),
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function (data) {
                            if(data == 'success'){
                                toastr.success('Package QR code and Cutromer Invoice code matched successfully');
                                $(".deliver-btn").prop('disabled', false);
                            }
                            else{
                                toastr.warning('Package QR code and Cutromer Invoice code did not match');
                            }
                        }
                    });
                }
            }
        });
        // $(".package_qr").keypress(function(e){
        //     var key = e.which;
        //     if(key == 13) {
        //         e.preventDefault();
        //         if($(this).val() === ''){
        //             toastr.warning('Package QR code cannot be empty');
        //         }
        //         else if($(".customer_qr").val() === ''){
        //             toastr.warning('Cutromer Invoice code cannot be empty');
        //         }
        //         else{
        //             $.ajax({
        //                 method: "POST",
        //                 url: '{{url('warehouse/check/request/qr')}}',
        //                 data: {
        //                     'order_id': $(".order_id").val(),
        //                     'package_qr': $(this).val(),
        //                     'customer_qr': $(".customer_qr").val(),
        //                     '_token': '{{ csrf_token() }}'
        //                 },
        //                 success: function (data) {
        //                     if(data == 'success'){
        //                         toastr.success('Package QR code and Cutromer Invoice code matched successfully');
        //                         $(".deliver-btn").prop('disabled', false);
        //                     }
        //                     else{
        //                         toastr.warning('Package QR code and Cutromer Invoice code did not match');
        //                     }
        //                 }
        //             });
        //         }
        //     }
        // });

        $(".print-btn").click(function(){
            $.print("#printable");
        });

        $(".deliver-btn").click(function(){
            $.ajax({
                method: "POST",
                url: '{{url('warehouse/request/deliver')}}',
                data: {
                    'order_id': $(".order_id").val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function (data) {
                    if(data == 'success'){
                        toastr.success('Successfully Delivered');
                        $("#codes").fadeOut();
                        $("#title").css('display', 'block');
                        $("#printBtn").css('display', 'block');
                        $(".deliver-btn").prop('disabled', false).fadeOut();
                        table.ajax.url("{{ url('warehouse/json/zone/list?value=all') }}").load();
                    }
                    else{
                        toastr.error('Something went wrong');
                    }
                }
            });
        });
    });
</script>


@stop