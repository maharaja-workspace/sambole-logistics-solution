<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'WarehouseManage\Http\Controllers', 'prefix' => 'warehouse'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@index'
        ]);
        Route::get('json/zone/list', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@jsonZoneList'
        ]);
        Route::get('json/vehicle/list', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@jsonVehicleList'
        ]);
        Route::get('json/zone-vehicle/list', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@jsonZoneVehicleList'
        ]);

        /**
         * POST Routes
         */
        Route::post('get/request/data', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@getRequestData'
        ]);
        
        
        Route::post('check/request/qr', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@checkRequestQR'
        ]);
        
        Route::post('request/deliver', [
            'as' => 'warehouse.list', 'uses' => 'WarehouseController@requestDeliver'
        ]);

    });
});


