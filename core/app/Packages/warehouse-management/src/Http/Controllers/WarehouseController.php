<?php

namespace WarehouseManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\ExtraBin;
use DeliveryManage\Models\Customer;
use function GuzzleHttp\Psr7\str;
use ZoneManage\Models\Zone;
use BinManage\Models\Bin;
use PackageTypeManage\Models\PackageType;
use PackageSizeManage\Models\PackageSize;
use PackageWeightManage\Models\PackageWeight;
use DeliveryManage\Models\CustomerRequest;
use DeliveryManage\Models\Stock;
use RouteManage\Models\Routing;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use StockManage\Models\StockTransaction;
use StoreManage\Models\Store;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;


class WarehouseController extends Controller
{

    function index()
    {
        $logged_in_user = Sentinel::getUser();

        $zones = Zone::where('status', 1)->get();

        return view('WarehouseManage::list')->with(['zones' => $zones]);
    }

    public function jsonZoneList(Request $request)
    {
        $jsonList = array();
        $i = 1;

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        if ($logged_in_user->inRole('zone-manager')) {
            $Zone = Zone::where('id', $logged_in_user->zone['id'])->first();
            $stores = Store::where('store_type', 'zone')->where('related_id', $Zone['id'])->get()->lists('id');
            $stocks = Stock::whereIn('store_id', $stores)->get();

            foreach ($stocks as $stock) {
                $dd = array();
                $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5);
                if ($isCentralWarehouse) {
                    $CustomerRequest= $CustomerRequest->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 1);
                        });
                    });
                } else {
                    $CustomerRequest = $CustomerRequest->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                        });
                    });
                }
                $CustomerRequest = $CustomerRequest->first();
                $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
                $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
                $Bin = Bin::where('id', $stock['bin_id'])->first();

                $BinName = '-';
                if ($Bin['bin_type'] == 'zone') {
                    $BinName = Zone::where('id', $Bin['related_id'])->first()->name . ' Bin';
                } elseif ($Bin['bin_type'] == 'route') {
                    $BinName = Routing::where('id', $Bin['related_id'])->first()->name . ' Bin';
                } else if ($Bin['bin_type'] == 'extra_bin') {
                    $BinName = ExtraBin::where('id', $Bin['related_id'])->first()->name;
                }

                if ($CustomerRequest) {

                    $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                    $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                                        ->orderBy('created_at', 'desc')->first();

                    array_push($dd, $CustomerRequest['order_id']);
                    array_push($dd, $CustomerRequest['merchant_order_id']);
                    array_push($dd, $PackageType['name']);
                    array_push($dd, $PackageSize['name']);
                    array_push($dd, $BinName);
                    if ($CustomerRequest['delivery_type'] == 1) {
                        $delivery_type = 'Collection Point';
                    } elseif ($CustomerRequest['delivery_type'] == 2) {
                        $delivery_type = 'Home Delivery';
                    } else {
                        $delivery_type = '-';
                    }
                    array_push($dd, $delivery_type);
                    if ($CustomerRequest['pickup_type'] == 1) {
                        $pickup_type = 'Harrisons Pickup';
                    }  else {
                        $pickup_type = 'Merchants Location';
                    }
                    array_push($dd, $pickup_type);
                    if ($CustomerRequest['payment_type'] == 1) {
                        $payment_type = 'Online Payment';
                    }  else if ($CustomerRequest['payment_type'] == 2) {
                        $payment_type = 'Cash on Delivery';
                    } else {
                        $payment_type = "-";
                    }
                    array_push($dd, $payment_type);
                    array_push($dd, $CustomerRequest['buyer_name']);
                    array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                    array_push($dd, $CustomerRequest->pickupCity->name);
                    array_push($dd, $CustomerRequest->buyerCity->name);
                    array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                    array_push($dd, '<span data-toggle="modal" data-target="#myModal" data-orderid="' . $CustomerRequest['order_id'] . '" class="glyphicon glyphicon-shopping-cart deliver"></span>');

                    array_push($jsonList, $dd);
                }


                $i++;
            }

            return Response::json(array('data' => $jsonList));
        } else {
            if ($request->value == 'all') {

                $stores = Store::where('store_type', 'zone')->get()->lists('id');
                $stocks = Stock::whereIn('store_id', $stores)->get();

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5)->first();
                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
                    $Bin = Bin::where('id', $stock['bin_id'])->first();

                    $BinName = '-';
                    if ($Bin['bin_type'] == 'zone') {
                        $BinName = Zone::where('id', $Bin['related_id'])->first()->name . ' Bin';
                    } elseif ($Bin['bin_type'] == 'route') {
                        $BinName = Routing::where('id', $Bin['related_id'])->first()->name . ' Bin';
                    } else if ($Bin['bin_type'] == 'extra_bin') {
                        $BinName = ExtraBin::where('id', $Bin['related_id'])->first()->name;
                    }

                    if ($CustomerRequest) {

                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $PackageType['name']);
                        array_push($dd, $PackageSize['name']);
                        array_push($dd, $BinName);
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        }  else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        }  else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                        array_push($dd, $stock->store->zone->name);

                        array_push($jsonList, $dd);
                    }
                    $i++;
                }

                return Response::json(array('data' => $jsonList));
            } elseif ($request->value !== 'all') {
                $Zone = Zone::where('id', $request->value)->first();
                $stores = Store::where('store_type', 'zone')->where('related_id', $Zone['id'])->get()->lists('id');
                $stocks = Stock::whereIn('store_id', $stores)->get();

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5)->first();
                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
                    $Bin = Bin::where('id', $stock['bin_id'])->first();

                    $BinName = '-';
                    if ($Bin['bin_type'] == 'zone') {
                        $BinName = Zone::where('id', $Bin['related_id'])->first()->name . ' Bin';
                    } elseif ($Bin['bin_type'] == 'route') {
                        $BinName = Routing::where('id', $Bin['related_id'])->first()->name . ' Bin';
                    } else if ($Bin['bin_type'] == 'extra_bin') {
                        $BinName = ExtraBin::where('id', $Bin['related_id'])->first()->name;
                    }

                    if ($CustomerRequest) {

                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $PackageType['name']);
                        array_push($dd, $PackageSize['name']);
                        array_push($dd, $BinName);
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        }  else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        }  else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                        array_push($dd, $stock->store->zone->name);

                        array_push($jsonList, $dd);
                    }
                    $i++;
                }

                return Response::json(array('data' => $jsonList));
            } else {
                return Response::json(array('data' => $jsonList));
            }
        }
    }

    public function jsonVehicleList(Request $request)
    {
        $jsonList = array();
        $i = 1;

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        if ($logged_in_user->inRole('zone-manager')) {
            $Zone = Zone::where('id', $logged_in_user->zone['id'])->first();
            $vehicles = Vehicle::where('zone_id', $Zone['id'])->whereHas('type', function ($q){
                $q->where('is_bin', 0)->where('zone_access', 1);
            })->get()->lists('id')->toArray();
            $stores = Store::where('store_type', 'vehicle')->whereIn('related_id', $vehicles)->get()->lists('id');
            $stocks = Stock::whereIn('store_id', $stores)->get();

            foreach ($stocks as $stock) {
                $dd = array();
                $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5);
                if ($isCentralWarehouse) {
                    $CustomerRequest = $CustomerRequest->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 1);
                        });
                    });
                } else {
                    $CustomerRequest = $CustomerRequest->where(function ($q){
                        $q->where(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 1)->where('delivery_type', 1);
                        })->orWhere(function ($q){
                            $q->where('pickup_type', 2)->where('delivery_type', 2);
                        })->orWhere(function ($q){
                            $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                        });
                    });
                }
//                if ($isCentralWarehouse) {
//                    $CustomerRequest= $CustomerRequest;
//                }
                $CustomerRequest = $CustomerRequest->first();
//                $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                $Bin = Bin::where('id', $stock['bin_id'])->first();

//                $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                if ($CustomerRequest) {

                    $riderName = '-';
                    if (isset($CustomerRequest->rider_assign)) {
                        $riderName = $CustomerRequest->rider_assign->getRider->user->first_name . " " . $CustomerRequest->rider_assign->getRider->user->last_name;
                    }

                    if ($CustomerRequest) {
                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $stock->store->vehicle->name);
                        array_push($dd, $riderName);
//                    array_push($dd, $BinName ? $BinName['name'] . ' Bin' : "-");
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        } else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        } else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        if ($CustomerRequest->status == 1 || $CustomerRequest->status == 2) {
                            $requestType = "Pick up";
                        } else if ($CustomerRequest->status == 4 || $CustomerRequest->status == 5) {
                            $requestType = "Delivery";
                        } else if ($CustomerRequest->status == 6) {
                            $requestType = "Stock Transferred";
                        } else {
                            $requestType = "-";
                        }
                        array_push($dd, $requestType);
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                        // array_push($dd, '<span data-toggle="modal" data-target="#myModal" data-orderid="'. $CustomerRequest['order_id'] .'" class="glyphicon glyphicon-shopping-cart deliver"></span>');

                        array_push($jsonList, $dd);
                    }
                    $i++;
                }
            }

            return Response::json(array('data' => $jsonList));
        } else {
            if ($request->value == 'all') {
                $stores = Store::where('store_type', 'vehicle')->whereHas('vehicle.type', function ($q){
                    $q->where('is_bin', 0)->where('zone_access', 1);
                })->get()->lists('id');
                $stocks = Stock::whereIn('store_id', $stores)->get();

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5)->first();
//                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                    $Bin = Bin::where('id', $stock['bin_id'])->first();

//                    $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                    if ($CustomerRequest) {
                        $riderName = '-';
                        if (isset($CustomerRequest->rider_assign)) {
                            $riderName = $CustomerRequest->rider_assign->getRider->user->first_name . " " . $CustomerRequest->rider_assign->getRider->user->last_name;
                        }

                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $zoneName = "-";
                        if ($stock->store->store_type == "zone") {
                            $zoneName = $stock->store->zone->name;
                        } else if ($stock->store->store_type = "vehicle") {
                            $zoneName = $stock->store->vehicle->zone->name;
                        }

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $stock->store->vehicle->name);
                        array_push($dd, $riderName);
//                    array_push($dd, $BinName ? $BinName['name'] . ' Bin' : "-");
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        } else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        } else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        if ($CustomerRequest->status == 1 || $CustomerRequest->status == 2) {
                            $requestType = "Pick up";
                        } else if ($CustomerRequest->status == 4 || $CustomerRequest->status == 5) {
                            $requestType = "Delivery";
                        } else if ($CustomerRequest->status == 6) {
                            $requestType = "Stock Transferred";
                        } else {
                            $requestType = "-";
                        }
                        array_push($dd, $requestType);
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                        array_push($dd, $zoneName);
                        // array_push($dd, '<span data-toggle="modal" data-target="#myModal" data-orderid="'. $CustomerRequest['order_id'] .'" class="glyphicon glyphicon-shopping-cart deliver"></span>');

                        array_push($jsonList, $dd);
                        $i++;
                    }
                }

                return Response::json(array('data' => $jsonList));
            } elseif ($request->value !== 'all') {
                $Zone = Zone::where('id', $request->value)->first();
                $vehicles = Vehicle::where('zone_id', $Zone['id'])->get()->lists('id')->toArray();
                $stores = Store::where('store_type', 'vehicle')->whereIn('related_id', $vehicles)->get()->lists('id');
                $stocks = Stock::whereIn('store_id', $stores)->get();

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where('status', '!=', 5)->first();
//                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                    $Bin = Bin::where('id', $stock['bin_id'])->first();

//                    $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                    if ($CustomerRequest) {
                        $riderName = '-';
                        if (isset($CustomerRequest->rider_assign)) {
                            $riderName = $CustomerRequest->rider_assign->getRider->user->first_name . " " . $CustomerRequest->rider_assign->getRider->user->last_name;
                        }

                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $zoneName = "-";
                        if ($stock->store->store_type == "zone") {
                            $zoneName = $stock->store->zone->name;
                        } else if ($stock->store->store_type = "vehicle") {
                            $zoneName = $stock->store->vehicle->zone->name;
                        }

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $stock->store->vehicle->name);
                        array_push($dd, $riderName);
//                    array_push($dd, $BinName['name'] . ' Bin');
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        } else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        } else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name . " " . $merchant->last_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        if ($CustomerRequest->status == 1 || $CustomerRequest->status == 2) {
                            $requestType = "Pick up";
                        } else if ($CustomerRequest->status == 4 || $CustomerRequest->status == 5) {
                            $requestType = "Delivery";
                        } else if ($CustomerRequest->status == 6) {
                            $requestType = "Stock Transferred";
                        } else {
                            $requestType = "-";
                        }
                        array_push($dd, $requestType);
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                        array_push($dd, $zoneName);
                        // array_push($dd, '<span data-toggle="modal" data-target="#myModal" data-orderid="'. $CustomerRequest['order_id'] .'" class="glyphicon glyphicon-shopping-cart deliver"></span>');

                        array_push($jsonList, $dd);
                        $i++;
                    }
                }

                return Response::json(array('data' => $jsonList));
            } else {
                return Response::json(array('data' => $jsonList));
            }
        }
        return Response::json(array('data' => $jsonList));
    }

    public function jsonZoneVehicleList(Request $request)
    {
        $jsonList = array();
        $i = 1;

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        if ($logged_in_user->inRole('zone-manager')) {
            $Zone = Zone::where('id', $logged_in_user->zone['id'])->first();
            $VehicleTypes = VehicleType::where('is_bin', 1)->where('zone_access', 0)->get()->lists('id')->toArray();
            $vehicles = Vehicle::whereIn('vehicle_type_id', $VehicleTypes)->where('zone_id', $Zone['id'])->get()->lists('id')->toArray();
            $stores = Store::where('store_type', 'vehicle')->whereIn('related_id', $vehicles)->get()->lists('id')->toArray();
            $stocks = Stock::whereIn('store_id', $stores)->get();

            foreach ($stocks as $stock) {
                $dd = array();
                $CustomerRequest = CustomerRequest::where('id', $stock->request_id);
                if ($isCentralWarehouse) {
                    $CustomerRequest= $CustomerRequest->where('pickup_type', 1)->where('delivery_type', 2);
                }
                $CustomerRequest = $CustomerRequest->first();
//                $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                $Bin = Bin::where('id', $stock['bin_id'])->first();

//                $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                    ->orderBy('created_at', 'desc')->first();

                array_push($dd, $CustomerRequest['order_id']);
                array_push($dd, $CustomerRequest['merchant_order_id']);
                array_push($dd, $stock->store->vehicle->number);
                if ($CustomerRequest['delivery_type'] == 1) {
                    $delivery_type = 'Collection Point';
                } elseif ($CustomerRequest['delivery_type'] == 2) {
                    $delivery_type = 'Home Delivery';
                } else {
                    $delivery_type = '-';
                }
                array_push($dd, $delivery_type);
                if ($CustomerRequest['pickup_type'] == 1) {
                    $pickup_type = 'Harrisons Pickup';
                }  else {
                    $pickup_type = 'Merchants Location';
                }
                array_push($dd, $pickup_type);
                if ($CustomerRequest['payment_type'] == 1) {
                    $payment_type = 'Online Payment';
                }  else if ($CustomerRequest['payment_type'] == 2) {
                    $payment_type = 'Cash on Delivery';
                } else {
                    $payment_type = "-";
                }
                array_push($dd, $payment_type);
                array_push($dd, $CustomerRequest['buyer_name']);
                array_push($dd, $merchant->first_name);
                array_push($dd, $CustomerRequest->pickupCity->name);
                array_push($dd, $CustomerRequest->buyerCity->name);
                array_push($dd, isset($CustomerRequest->pickupCity->zones[0]->name) ? $CustomerRequest->pickupCity->zones[0]->name : '');
                array_push($dd, isset($CustomerRequest->buyerCity->zones[0]->name) ? $CustomerRequest->buyerCity->zones[0]->name : "-");
                array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->updated_at->timestamp) : "-");
                // array_push($dd, '<span data-toggle="modal" data-target="#myModal" data-orderid="'. $CustomerRequest['order_id'] .'" class="glyphicon glyphicon-shopping-cart deliver"></span>');

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            if ($request->value == 'all') {
//                $VehicleTypes = VehicleType::where('is_bin', 1)->get()->lists('id')->toArray();
                $vehicles = Vehicle::whereHas('type', function ($q){
                    $q->where('is_bin', 1)->where('zone_access', 0);
                })->get()->lists('id')->toArray();
                $stores = Store::where('store_type', 'vehicle')->whereIn('related_id', $vehicles)->get()->lists('id')->toArray();
                $stocks = Stock::whereIn('store_id', $stores)->get();

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->first();
//                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                    $Bin = Bin::where('id', $stock['bin_id'])->first();

//                    $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                    if ($CustomerRequest) {
                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $stock->store->vehicle->number);
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        }  else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        }  else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        array_push($dd, isset($CustomerRequest->pickupCity->zones[0]->name) ? $CustomerRequest->pickupCity->zones[0]->name : '');
                        array_push($dd, isset($CustomerRequest->buyerCity->zones[0]->name) ? $CustomerRequest->buyerCity->zones[0]->name : "-");
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->created_at->timestamp) : "-");

                        array_push($jsonList, $dd);
                        $i++;
                    }
                }

                return Response::json(array('data' => $jsonList));
            } elseif ($request->value !== 'all') {
                $Zone = Zone::where('id', $request->value)->first();
//                $VehicleTypes = VehicleType::where('is_bin', 1)->get()->lists('id')->toArray();
                $vehicles = Vehicle::whereHas('type', function ($q){
                    $q->where('is_bin', 1)->where('zone_access', 0);
                })->get()->lists('id')->toArray();
                $stores = Store::where('store_type', 'vehicle')->whereIn('related_id', $vehicles)->get()->lists('id')->toArray();
                $stocks = Stock::whereIn('store_id', $stores)->get();

                $cities = $Zone->cities->lists('id');

                foreach ($stocks as $stock) {
                    $dd = array();
                    $CustomerRequest = CustomerRequest::where('id', $stock->request_id)->where(function ($q) use ($cities){
                        $q->where(function ($q) use ($cities) {
                            $q->whereIn('pickup_city', $cities);
                        })->orWhere(function ($q) use ($cities) {
                            $q->whereIn('buyer_city', $cities);
                        });
                    })->first();
//                    $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
//                    $PackageSize = PackageSize::where('id', $CustomerRequest['package_type'])->first();
//                    $Bin = Bin::where('id', $stock['bin_id'])->first();

//                    $BinName = Vehicle::where('id', $Bin['related_id'])->first();

                    if ($CustomerRequest) {
                        $merchant = Customer::where('id', $CustomerRequest->customer_id)->first();

                        $stockRecord = Stock::where('request_id', $CustomerRequest->id)->where('store_id', $stock->store->id)
                            ->orderBy('created_at', 'desc')->first();

                        array_push($dd, $CustomerRequest['order_id']);
                        array_push($dd, $CustomerRequest['merchant_order_id']);
                        array_push($dd, $stock->store->vehicle->number);
                        if ($CustomerRequest['delivery_type'] == 1) {
                            $delivery_type = 'Collection Point';
                        } elseif ($CustomerRequest['delivery_type'] == 2) {
                            $delivery_type = 'Home Delivery';
                        } else {
                            $delivery_type = '-';
                        }
                        array_push($dd, $delivery_type);
                        if ($CustomerRequest['pickup_type'] == 1) {
                            $pickup_type = 'Harrisons Pickup';
                        }  else {
                            $pickup_type = 'Merchants Location';
                        }
                        array_push($dd, $pickup_type);
                        if ($CustomerRequest['payment_type'] == 1) {
                            $payment_type = 'Online Payment';
                        }  else if ($CustomerRequest['payment_type'] == 2) {
                            $payment_type = 'Cash on Delivery';
                        } else {
                            $payment_type = "-";
                        }
                        array_push($dd, $payment_type);
                        array_push($dd, $CustomerRequest['buyer_name']);
                        array_push($dd, $merchant->first_name);
                        array_push($dd, $CustomerRequest->pickupCity->name);
                        array_push($dd, $CustomerRequest->buyerCity->name);
                        array_push($dd, isset($CustomerRequest->pickupCity->zones[0]->name) ? $CustomerRequest->pickupCity->zones[0]->name : '');
                        array_push($dd, isset($CustomerRequest->buyerCity->zones[0]->name) ? $CustomerRequest->buyerCity->zones[0]->name : "-");
                        array_push($dd, $stockRecord ? date('Y-m-d', $stockRecord->created_at->timestamp) : "-");

                        array_push($jsonList, $dd);
                        $i++;
                    }
                }
                return Response::json(array('data' => $jsonList));
            } else {
                return Response::json(array('data' => $jsonList));
            }
        }
        return Response::json(array('data' => $jsonList));
    }

    public function getRequestData(Request $request)
    {
        $CustomerRequest = CustomerRequest::where('order_id', $request->order_id)->first();
        $PackageType = PackageType::where('id', $CustomerRequest['package_type'])->first();
        $PackageSize = PackageSize::where('id', $CustomerRequest['package_size'])->first();
        $PackageWeight = PackageWeight::where('id', $CustomerRequest['package_weight'])->first();

        if ($CustomerRequest['delivery_type'] == 0) {
            $delivery_type = 'Pickup from Store';
        } elseif ($CustomerRequest['delivery_type'] == 1) {
            $delivery_type = 'Pickup from Mobile Store';
        } else {
            $delivery_type = 'Door Step Delivery';
        }

        $payment_type = 'Cash on delivery';
        if ($CustomerRequest['payment_type'] == 1) {
            $payment_type = 'paid';
        }

        $data = array(
            'order_id' => $CustomerRequest['order_id'],
            'buyer_name' => $CustomerRequest['buyer_name'],
            'buyer_mobile' => $CustomerRequest['buyer_mobile'],
            'merchant_order_code' => $CustomerRequest['merchant_order_code'],
            'package_type' => $PackageType['name'],
            'package_size' => $PackageSize['name'],
            'package_weight' => $PackageWeight['min_weight'] . ' - ' . $PackageWeight['max_weight'],
            'delivery_type' => $delivery_type,
            'payment_type' => $payment_type,
            'total_amount' => $CustomerRequest['total_amount']
        );

        return response($data);
    }

    public function checkRequestQR(Request $request)
    {
        $CustomerRequest = CustomerRequest::where('order_id', $request->order_id)->first();

        if ($request->has('customer_qr') && $request->has('package_qr')) {
            if ($CustomerRequest['qr_code'] == $request->customer_qr && $CustomerRequest['merchant_order_code'] == $request->package_qr) {
                return response('success');
            } else {
                return response('failed');
            }
        }

        // if ($request->has('package_qr')) {
        //     if ($CustomerRequest['qr_code'] == $request->package_qr) {
        //         return response('success');
        //     } else {
        //         return response('failed');
        //     }
        // }
        return response('error');
    }

    public function requestDeliver(Request $request)
    {
        $CustomerRequest = CustomerRequest::where('order_id', $request->order_id)->first();

        if ($CustomerRequest) {
            $Stock = Stock::where('request_id', $CustomerRequest->id)->first();

            Stock::find($Stock['id'])->update([
                'availability' => 'out'
            ]);

            $StockTransaction = StockTransaction::create([
                'request_id' => $CustomerRequest['id'],
                'user_id' => Sentinel::getUser()->id,
                'bin_id' => $Stock['bin_id'],
                'store_id' => $Stock['store_id'],
                'transaction_type' => 'out',
                'type' => 'bin'
            ]);

            $CustomerRequest = CustomerRequest::where('order_id', $request->order_id)->update([
                'status' => 5
            ]);

            return response('success');
        }

        return response('error');
    }
}
