<?php

namespace UserManage\Http\Controllers;
ini_set('max_execution_time', 0);

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Industry;
use App\Models\SliderImage;
use App\Models\OpeningHour;
use App\Models\LayoutCustomization;
use App\Models\BusinessRegistration;
use Carbon\Carbon;
use RiderManage\Models\Rider;
use UserManage\Models\User;
use BranchManage\Models\Branch;
use UserRoles\Models\UserRole;
use Permissions\Models\Permission;
use UserManage\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Response;
use Sentinel;
use Hash;
use Activation;
use Validator;
use DB;
use ZoneManage\Models\Zone;

class UserController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | User Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the User add screen to the user.
     *
     * @return Response
     */
    public function addView()
    {
        $logged_in_user = Sentinel::getUser();

        $user = User::where('status', '=', 1)->get();
        $roles = UserRole::orderBy('name', 'asc')->get();

        return view('userManage::user.add')->with([
            'users' => $user,
            'roles' => $roles,
            'logged_in_user' => $logged_in_user
        ]);
    }

    /**
     * Add new user data to database
     *
     * @return Redirect to menu add
     */
    public function add(UserRequest $request)
    {
        $logged_in_user = Sentinel::getUser();

        $username_submitted = User::where('username', '=', $request->get('username'))->get();
        if (isset($username_submitted[0])) {
            return redirect('user/add')->with(['error' => true,
                'error.message' => 'Already EXSIST User!',
                'error.title' => 'Try Again!']);
        }

        if ($logged_in_user->inRole('zone-manager')) {
            $supervisor = User::find(1);
            $credentials = [
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('username'),
                'confirmed' => 1,
                'status' => 1,
                'username' => $request->get('username'),
                'password' => $request->get('password')
            ];
            $user = Sentinel::registerAndActivate($credentials);
            $user->app_password = md5($request->get('password'));
            $user->save();

            $user->makeChildOf($supervisor);

            $role = Sentinel::findRoleById(5);
            $role->users()->attach($user);

            Rider::create([
                'user_id' => $user->id,
                'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
                'zone' => $logged_in_user->zone['id']
            ]);

            return redirect('user/add')->with(['success' => true,
                'success.message' => 'User Created successfully!',
                'success.title' => 'Well Done!']);
        } else {
            if (!empty($request->get('roles')) > 0) {
                $supervisor = User::find(1);
                $credentials = [
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('username'),
                    'confirmed' => 1,
                    'status' => 1,
                    'username' => $request->get('username'),
                    'password' => $request->get('password')
                ];
                $user = Sentinel::registerAndActivate($credentials);
                $user->app_password = md5($request->get('password'));
                $user->save();
            
                $user->makeChildOf($supervisor);

                foreach ($request->get('roles') as $key => $value) {
                    $role = Sentinel::findRoleById($value);
                    $role->users()->attach($user);

                    if ($value == 5) {
                        Rider::create([
                            'user_id' => $user->id,
                            'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
                        ]);
                    }
                }

                return redirect('user/add')->with(['success' => true,
                    'success.message' => 'User Created successfully!',
                    'success.title' => 'Well Done!']);
            } else {
                return redirect('user/add')->with(['error' => true,
                    'error.message' => 'ROLE can not be empty!',
                    'error.title' => 'Try Again!'])->withInput($request->input());
            }
        }
    }

    /**
     * Show the user add screen to the user.
     *
     * @return Response
     */
    public
    function listView($type)
    {
        $logged_in_user = Sentinel::getUser();

        $roles = UserRole::where('visible', 1)->get();
        $role_user_count = array();
        foreach ($roles as $key => $role) {
            $role_detail = Sentinel::findRoleById($role->id);
            $count = $role_detail->users()->get()->count();
            array_push($role_user_count, array('name' => $role->name, 'count' => $count, 'slug' => $role->slug));
        }

        return view('userManage::user.list')->with(['role_wise_count' => $role_user_count, 'roles' => $roles, 'type' => $type, 'logged_in_user' => $logged_in_user]);
    }


    /**
     * Show the user add screen to the user.
     *
     * @return Response
     */
    public
    function jsonList(Request $request)
    {
        $logged_user = Sentinel::getUser();
        $jsonList = array();
        $i = 1;

        $riders = Rider::where('zone', $logged_user->zone['id'])->lists('user_id');

        if ($request->ajax()) {
            if ($logged_user->inRole('zone-manager')) {
                $data = User::whereHas('roles', function ($query) {
                    $query->where('slug', 'rider');
                })->whereIn('id', $riders)->with(['roles'])->get();
            } else {
                $data = User::with(['roles'])->get();
            }

            foreach ($data as $key => $user) {
                if ($user->inRole('admin') | $user->inRole('zone-manager') || $user->inRole('rider')) {
                    $roles = '';
                    $isRider = false;
                    foreach ($user->roles as $key_role => $value_role) {
                        $roles .= $value_role->name . ' ';
                        if ($value_role->name == "Rider") {
                            $isRider = true;
                        }
                    }

                    $dd = array();
                    if ($logged_user->id != $user->id) {
                        array_push($dd, '<center><input type="checkbox" value="' . $user->id . '" name="check[]"></input></center>');
                    } else {
                        array_push($dd, '');
                    }

                    array_push($dd, $i);

                    if ($user->first_name !== "" || $user->last_name !== "") {
                        array_push($dd, $user->first_name . ' ' . $user->last_name);
                    } else {
                        array_push($dd, "-");
                    }

                    if ($user->username != "") {
                        array_push($dd, $user->username);
                    } else {
                        array_push($dd, "-");
                    }
                    if (count($user->roles) > 0) {
                        array_push($dd, $roles);
                    } else {
                        array_push($dd, "-");
                    }

                    $permissions = Permission::whereIn('name', ['user.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
                    if (Sentinel::hasAnyAccess($permissions)) {
                        array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('user/' . $user->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Rider"><i class="fa fa-pencil"></i></a></center>');
                    }else{
                        array_push($dd, '');
                    }

                    if (!$isRider) {
                        $permissions = Permission::whereIn('name', ['user.delete', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
                        if (Sentinel::hasAnyAccess($permissions)) {
                            if ($user->status == 1) {
                                array_push($dd, '<center><a href="#" class="blue user-delete " data-id="' . $user->id . '" title="Deactivate User"><i class="fa fa-toggle-on"></i></a></center>');
                            } else if ($user->status == 0) {
                                array_push($dd, '<center><a href="#" class="blue user-activate " data-id="' . $user->id . '" title="Activate User"><i class="fa fa-toggle-off"></i></a></center>');
                            }
                        }else{
                            array_push($dd, '');
                        }
                    } else {
                        array_push($dd, "");
                    }


                    array_push($jsonList, $dd);
                    $i++;
                }
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public
    function changerole(Request $request, $type)
    {
        $role_selected = $request->input('role');
        $users = $request->input('check');
        if (!empty($users)) {

            foreach ($users as $key => $value_1) {
                $user = Sentinel::findById($value_1);
                foreach ($user->roles as $key => $value_2) {
                    $role = Sentinel::findRoleById($value_2->id);
                    $role->users()->detach($user);
                }

                $user->save();
                //attach user for role
                $role = Sentinel::findRoleById($role_selected);
                $role->users()->attach($user);

                if ($user->roles()->where("role_id", 2)->count() > 0 && LayoutCustomization::where('user_id', $user->id)->count() == 0) {
                    LayoutCustomization::create([
                        'user_id' => $user->id,
                        'header_color_code' => '#800000',
                        'header_style_id' => '1',
                        'logo_img' => '',
                        'background_color_code' => '#ffffff',
                    ]);
                }

            }
            return redirect('user/list/' . $type)->with(['success' => true,
                'success.message' => sizeof($users) . ' User Role Changed successfully!',
                'success.title' => 'Well Done!']);
        } else {
            return redirect('user/list/' . $type)->with(['error' => true,
                'error.message' => 'Please Select one or more users!',
                'error.title' => 'Try Again!'])->withInput($request->input());
        }

    }


    /**
     * Activate or Deactivate User
     * @param Request $request user id with status to change
     * @return json object with status of success or failure
     */
    public
    function status(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');

            $user = User::find($id);
            if ($user) {
                $user->status = $status;
                $user->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Delete a User
     * @param Request $request user id
     * @return Json            json object with status of success or failure
     */
    public
    function delete(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $user = User::find($id);
            if ($user) {
                // $user->delete();
                $user->status = 0;
                $user->save();

                $rider = Rider::where('user_id', $user->id)->first();

                if($rider){
//                    Rider::find($rider['id'])->delete();
                    $res = Rider::find($rider['id']);
                    if ($res) {
                        $res->status = 0;
                        $res->save();
                    }
                }

                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public
    function active(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $user = User::find($id);
            if ($user) {
                // $user->delete();
                $user->status = 1;
                $user->save();

                $rider = Rider::where('user_id', $user->id)->first();

                if($rider){
//                    Rider::find($rider['id'])->delete();
                    $res = Rider::find($rider['id']);
                    if ($res) {
                        $res->status = 1;
                        $res->save();
                    }
                }

                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public
    function toggleUser(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');

            $user = User::find($id);
            if ($user) {
                if ($user->status == 6) {
                    $user->status = 1;
                } else {
                    $user->status = 6;
                }

                if ($user->save()) {
                    return response()->json(['status' => 'success']);
                }
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    /**
     * Show the user edit screen to the user.
     *
     * @return Response
     */
    public function editView($id)
    {


        $logged_in_user = Sentinel::getUser();

        $user_data = User::find($id);

        if (!$user_data) {
            return redirect('user/list/all')->with(['error' => true,
                'error.message' => 'User did not found ',
                'error.title' => 'Ooops !']);
        }

        $users = User::where('status', '=', 1)->get();
        $roles = UserRole::orderBy('name', 'asc')->get();

        return view('userManage::user.edit')->with([
            'user_data' => $user_data,
            'users' => $users,
            'roles' => $roles,
            'logged_in_user' => $logged_in_user
        ]);

    }

    public
    function genarate_password()
    {
        return str_random(8);
    }

    /**
     * Add new user data to database
     *
     * @return Redirect to menu add
     */
    public
    function edit(UserRequest $request)
    {
        $logged_in_user = Sentinel::getUser();
        // return $request->get( 'supervisor' );
        $password = $request->get('password');
        $usercount = User::where('id', '!=', $request->id)->where('email', '=', $request->get('username'))->count();
        if ($usercount == 0) {

            if ($logged_in_user->inRole('zone-manager')) {

                $user = User::with(['roles'])->find($request->id);
                $user->first_name = $request->get('first_name');
                $user->last_name = $request->get('last_name');
                $user->username = $request->get('username');
                $user->email = $request->get('username');
                // $user->branch = $request->get('branch');
                // $user->makeChildOf(Sentinel::findById(1));
                $user->save();

                if ($password != '') {
                    Sentinel::update($user, array('password' => $password));
                    $user->app_password = md5($password);
                    $user->save();
            
                }

                Rider::where('user_id', $request->id)->update([
                    'user_id' => $user->id,
                    'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
                    'zone' => $logged_in_user->zone['id']
                ]);

                return redirect('user/list/all')->with(['success' => true,
                    'success.message' => 'User updated successfully!',
                    'success.title' => 'Good Job!']);
            } else {
                if (!empty($request->get('roles')) > 0) {
                    $user = User::with(['roles'])->find($request->id);
                    $user->first_name = $request->get('first_name');
                    $user->last_name = $request->get('last_name');
                    $user->username = $request->get('username');
                    $user->email = $request->get('username');
                    $user->save();
                    // return $user;

                    foreach ($user->roles as $key => $value) {
                        $role = Sentinel::findRoleById($value->id);
                        $role->users()->detach($user);
                    }

                    //attach user for role
                    foreach ($request->get('roles') as $key => $value) {
                        $role = Sentinel::findRoleById($value);
                        $role->users()->attach($user);

                        if ($value == 5) {
                            Rider::where('user_id', $user->id)->update([
                                'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
                            ]);
                        }
                    }
                    if ($password != '') {
                        Sentinel::update($user, array('password' => $password));
                    }

                    return redirect('user/list/all')->with(['success' => true,
                        'success.message' => 'User updated successfully!',
                        'success.title' => 'Good Job!']);
                } else {
                    return redirect('user/' . $request->id . '/edit')->with(['error' => true,
                        'error.message' => 'ROLE can not be empty!',
                        'error.title' => 'Try Again!'])->withInput($request->input());
                }
            }

        } else {
            return redirect('user/' . $request->id . '/edit')->with(['error' => true,
                'error.message' => 'User Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
    }

    public
    function viewUser($id)
    {
        $user = User::find($id);

        return view('userManage::user.view')->with([
            'user' => $user
        ]);
    }
}