@extends('layouts.back.master') @section('current_title','UPDATE USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
 <div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Update User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">
                       
                <form method="POST" class="form-horizontal" id="form">
                    {!!Form::token()!!}
                	<div class="form-group"><label class="col-sm-2 control-label">FIRST NAME</label>
                    	<div class="col-sm-10"><input type="text" name="first_name" class="form-control" value="{{$curUser->first_name}}"></div>
                	</div>
                	<div class="form-group"><label class="col-sm-2 control-label">LAST NAME</label>
                        <div class="col-sm-10"><input type="text" name="last_name" class="form-control" value="{{$curUser->last_name}}"></div>
                    </div>
                    @if($curUser->store)
                    <div class="form-group profile-url"><label class="col-sm-2 control-label ">PROFILE URL</label>
                    	<div class="col-sm-10"><input type="text" name="profile_url" id="profile_url" class="form-control" value="{{$curUser->store->business_page_name}}">
                        <label id="profile_url-error" class="error" for="first_name"></label></div>
                	</div>
                    @endif
                    <!-- <div class="form-group"><label class="col-sm-2 control-label">BRANCH</label>
                    	<div class="col-sm-10">
                    		 <select class="js-source-states" style="width: 100%" name="branch">
                    		 	<?php foreach ($branch as $key => $value): ?>
                    		 		<option value="{{$value->id}}" <?php if($value->id == $curUser->branch ){?> selected="true" <?php } ?> >{{$value->name}}</option>
                    		 		
                    		 	<?php endforeach ?>
		                        
		                    </select>
                    	</div>
                	</div> -->
                <!-- 	<div class="form-group"><label class="col-sm-2 control-label">SUPERVISOR</label>
                    	<div class="col-sm-10">
                    		 <select class="js-source-states" style="width: 100%" name="supervisor">
                    		 	<?php foreach ($users as $key => $value): ?>
                    		 		<option value="{{$value->id}}" <?php if($value->id == $curUser->supervisor_id ){?> selected="true" <?php } ?> >{{$value->first_name.' '.$value->last_name}}</option>
                    		 		
                    		 	<?php endforeach ?>
		                        
		                    </select>
                    	</div>
                	</div> -->
                   
                    <div class="form-group"><label class="col-sm-2 control-label">ROLES</label>
                        <div class="col-sm-10">
                             <select class="js-source-states" style="width: 100%" name="roles[]" multiple="multiple">
                                <?php foreach ($roles as $key => $value): 
                                	echo $value;                                    
                                endforeach ?>
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10"><input type="text" value="{{$curUser->username}}" name="username" class="form-control"></div>
                    </div>
                    <?php 
                    if (Sentinel::hasAnyAccess('sambole.admin')) {
                    ?>
                   <div class="form-group"><label class="col-sm-2 control-label">PASSWORD</label>
                        <div class="col-sm-6"><input type="password" value="" id="password" name="password" placeholder="Note: password will not be changed unless this field is empty" class="form-control" >
                        </div>
                        <!-- <div class="col-sm-4">
                            <button class="btn btn-primary" type="button"  onclick="genarate_password();">Genarate</button> 
                            <button class="btn btn-primary" type="button" onclick="copyText()">Copy text</button>                           
                          

                        </div> -->
                        
                    </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                        <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                        </div>
                    <?php
                        }
                    ?>
                	
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary submit" type="submit">Save Changes</button>
                        </div>
                    </div>
                	
                </form>
       </div>
    </div>
</div>

@stop
@section('js')
{{-- <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script> --}}
<script src="{{asset('assets/back/jquery-validation/jquery.validate.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
        "use strict";

		 $(".js-source-states").select2();

         $("#form").validate({
            rules: {
                first_name: {
                    // required: true                  
                },
                last_name:{
                    required: true
                },
                profile_url:{
                    required: true
                },
                roles:{
                    required: true,

                },
                branch:{
                    required: true
                },
                username:{
                    required: true
                },
                 password:{
                    // required: true,
                    minlength: 6
                },
                password_confirmation:{
                    // required: true,
                    minlength: 6,
                    equalTo: '#password'
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

         $("#profile_url").keyup(function(event) {
             $.ajax({
                 url: '{{url('checkPageName')}}',
                 data: {
                        business_page_name: $('#profile_url').val(),
                        _token: '{{csrf_token()}}'
                },
                 method: 'POST',
                 success: function(data){
                    if(JSON.parse(data)['valid'] !== 'true'){
                       $('#profile_url-error').text('');
                        $('#profile_url-error').text('This url is not available');
                        // $(".submit").prop("disabled", true);
                    }
                    else{
                        $('#profile_url-error').text('');   
                        // $(".submit").prop("disabled", false);
                    }
                 }
             })
             
         });
	});
    function genarate_password() {
         $.ajax({
          method: "POST",
          url: '{{url('user/genarate_password')}}',
          
        })
          .done(function( msg ) {
            $('#password').val(msg);
          });  
    }
    function copyText() {
      /* Get the text field */
      var copyText = document.getElementById("password");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
    }
	
</script>
@stop