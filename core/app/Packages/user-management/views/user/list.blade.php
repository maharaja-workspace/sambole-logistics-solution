@extends('layouts.back.master') @section('current_title','All User')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

        /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>User Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>User List</strong>
            </li>
        </ol>
    </div>
    @if(!$logged_in_user->inRole('zone-manager'))
        <div class="col-lg-7">
            <h2>
                <small>&nbsp;</small>
            </h2>
            <ol class="breadcrumb text-right">
                <?php foreach ($role_wise_count as $key => $value): ?>
                <li>
                    <a href="{{ url('user/list/' . $value['slug']) }}">{{$value['name']}}s
                        <span>(<strong>{{number_format($value['count'])}}</strong>)</span></a>
                </li>
                <?php endforeach ?>


            </ol>
        </div>
    @endif
@stop

@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
         onclick="location.href = '{{url('user/add')}}';">
        <p class="plus">+</p>
    </div>

    <form method="POST" class="form-horizontal" id="form">
        {!!Form::token()!!}
        <div class="row">
            <div class="col-lg-12 margins">

                <div class="ibox-content">
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Designation</th>
                            <th>Edit</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        @if(!$logged_in_user->inRole('zone-manager'))
            <div class="row">
                <div class="col-lg-12 margins">
                    <div class="ibox-content">

                        <div class="form-group"><label class="col-sm-1 control-label">Designations</label>
                            <div class="col-sm-1 align-middle">
                                <input type="checkbox" name="checkAll" id="checkAll"> All-
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="role">
                                    <?php foreach ($roles as $key => $value): ?>
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-primary" type="button" onclick="confirmation_role_change()">
                                    Change
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
    </form>
@stop
@section('js')





    <script type="text/javascript">

        var table;


        function getValues(status) {
            table.api().ajax.url("{{url('user/json/list/' . $type)}}?status=" + status).load();
        }


        $(document).ready(function () {

            table = $('#example1').dataTable({
                "ajax": '{{url('user/json/list/' . $type)}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'User List', className: 'btn-sm', exportOptions: {
                            columns: [1, 2, 3, 4]
                        }
                    },
                    {extend: 'pdf', title: 'User List', className: 'btn-sm', exportOptions: {
                            columns: [1, 2, 3, 4]
                        }
                    },
                    {extend: 'print', className: 'btn-sm'}
                ],
                "autoWidth": true,
                "order": [[1, "desc"]],
                "columnDefs": [
                    {"targets": [0], "orderable": false}]

            });

            table.on('draw.dt', function () {
                $('.user-delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAction(id);

                });

                $('.user-activate').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmActivate(id);

                });

                $('.user-status-toggle').click(function (e) { // use for activae inactive user
                    e.preventDefault();
                    id = $(this).data('id');

                    if (confirm("Are you sure ?")) {
                        $.ajax({
                            method: "POST",
                            url: '{{url('user/status-toggle')}}',
                            data: {'id': id}
                        })
                            .done(function (msg) {
                                table.fnReloadAjax();
                            });
                    }

                });

            });

            $("#status").change(function (event) {
                //getting fields & values
                status = this.value;

                getValues(status);
            });


        });

        function confirmAction(id) {

            swal({
                title: "Are you sure?",
                text: "Deactivate User?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes"
            }).then(function (isConfirm) {
                if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('user/delete')}}',
                        data: {'id': id},
                    })
                        .done(function (msg) {
                            // table.fnReloadAjax();
                            swal({ title : "Success!", text : "Successfully Deactivated!", type : "success"}).then(function (button) {
                                if (button.hasOwnProperty('value') && button.value === true) {
                                    window.location.reload();
                                }
                            });
                        });
                }


            });
        }

        function confirmActivate(id) {

            swal({
                title: "Are you sure?",
                text: "Activate User?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes"
            }).then(function (isConfirm) {
                if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('user/active')}}',
                        data: {'id': id},
                    })
                        .done(function (msg) {
                            // table.fnReloadAjax();
                            swal({ title : "Success!", text : "Successfully Activated!", type : "success"}).then(function (button) {
                                if (button.hasOwnProperty('value') && button.value === true) {
                                    window.location.reload();
                                }
                            });
                        });
                }


            });
        }

        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        function confirmation_role_change() {
            swal({
                    title: "Are you sure?",
                    text: "Your want to change ROLE !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Change it!"
                }
                // function (isConfirm) {
                //     if (isConfirm) {
                //         alert("CONFIRMED");
                //         $('#form').submit();
                //     } else {
                //         // swal("Cancelled", "Your imaginary file is safe :)", "error");
                //     }
                // }
                ).then(function (isConfirm) {
                if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                    $('#form').submit();
                }


            });
        }


    </script>

@stop