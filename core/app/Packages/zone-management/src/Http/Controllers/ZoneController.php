<?php

namespace ZoneManage\Http\Controllers;

use App\Http\Controllers\Controller;
use AreaManage\Models\Area;
use BinManage\Models\Bin;
use CityManage\Models\City;
use DeliveryManage\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use StoreManage\Models\Store;
use UserManage\Models\User;
use UserRoles\Models\UserRole;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use Yajra\Datatables\Datatables;
use ZoneManage\Models\Zone;
use ZoneManage\Models\ZoneCity;
use ZoneManage\Models\ZonePrice;

class ZoneController extends Controller
{

    public function index()
    {
        return view('ZoneManage::list');
    }

    public function addZone()
    {
        $users = [];
        $cities = [];
        foreach (User::get() as $user) {
            if ($user->inRole('zone-manager')) {
                if ($user->zone == null) {
                    $users[] = $user;
                }
            }
        }

//        foreach (City::where('status', 1)->get() as $city) {
        //            if ($city->zones()->first() == null) {
        //                $cities[] = $city;
        //            }
        //        }

        $districts = City::whereNull('distric_id')->where('status', 1)->get();
        foreach ($districts as $district) {
            $citiesOfDistrict = City::where('distric_id', $district->id)->where('status', 1)->get();
            $district->cities = $citiesOfDistrict;
        }

        $areas = Area::whereStatus(1)->get();

        return view('ZoneManage::add')->with(['areas' => $areas, 'zones' => Zone::all()->count() + 1, 'users' => $users, 'districts' => $districts]);
    }

    public function storeZone(Request $request)
    {

        $centralWarehouse = $request->central_warehouse ? $request->central_warehouse : 0;
        if ($centralWarehouse == 1) {
            $zoneType = 3;
        } else {
            $zoneType = $request->zone_type;
        }

        //        dd($request->all());
        $zone = Zone::create([
            'name' => $request->zone_name,
            'address' => $request->address,
            'description' => $request->description,
            'area_id' => $request->area,
            'manager_id' => $request->zone_manager,
            'code' => $request->zone_code,
            'is_central_warehouse' => $centralWarehouse,
            'zone_type' => $zoneType
        ]);

        $createdZone = $zone;

        if (value($request->cities)) {
            if (is_array($request->cities)) {
                $zone->cities()->sync($request->cities);
            }
        }

        Store::create([
            'related_id' => $zone->id,
            'store_type' => 'zone',
            'status' => 1,
        ]);

        $stores_zone = Store::where('store_type', 'zone')->get();
        $zones = Zone::all();

        foreach ($stores_zone as $store) {
            foreach ($zones as $currentZone) {
                $check_bin = Bin::where('store_id', $store->id)->where('related_id', $currentZone->id)->where('bin_type', 'zone')->first();
                if ($check_bin == null) {
                    Bin::create([
                        'store_id' => $store->id,
                        'related_id' => $currentZone->id,
                        'bin_type' => 'zone',
                        'status' => $store->status,
                    ]);
                }
            }
        }
        $vehicleType = VehicleType::where('is_bin', 1)->first();
        if ($vehicleType) {
            $vehiclesIds = Vehicle::where('vehicle_type_id', $vehicleType->id)->get()->pluck('id')->toArray();

            if (value($vehiclesIds)) {
                $stores = Store::whereIn('related_id', $vehiclesIds)->where('store_type', 'vehicle')->where('status', 1)->get();
                if ($stores->count()) {
                    foreach ($stores as $key => $store) {
                        $check_bin = Bin::where('store_id', $store->id)->where('related_id', $createdZone->id)->where('bin_type', 'vehicle')->first();
                        if ($check_bin == null) {
                            Bin::create([
                                'store_id' => $store->id,
                                'related_id' => $createdZone->id,
                                'bin_type' => 'vehicle',
                                'status' => $store->status,
                            ]);
                        }
                    }
                }
            }
        }

        if ($zone) {
            return redirect('zone/add')->with([
                'success' => true,
                'success.message' => 'Zone Created successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('zone/add')->with([
            'error' => true,
            'error.message' => 'Zone did not created ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function listDataOld(Request $request)
    {
        $queryBuild = Zone::with('user', 'cities', 'area');
        foreach ($request->get('columns') as $column) {
            if ($column['name'] == "cities" && $column['search']['value'] != "") {
                $queryBuild = Zone::with('user', 'cities', 'area')->whereHas('cities', function ($q) use ($column) {
                    $q->where("name", "LIKE", '%' . $column['search']['value'] . '%');
                });
                break;
            }
            if ($column['name'] == "zone_type" && $column['search']['value'] != "") {
                $value = null;
                if (strtolower($column['search']['value']) == "collection point") {
                    $value = 1;
                } else if (strtolower($column['search']['value']) == "home delivery") {
                    $value = 2;
                }
                if ($value != null) {
                    $queryBuild = Zone::with('user', 'cities', 'area')->where('zone_type', '=', $value);
                    break;
                }
            }
        }
        $permissions = Permission::whereIn('name', ['zone.edit', 'zone-manager', 'admin'])->where('status', '=', 1)->lists('name');

        return Datatables::of($queryBuild)
            ->addColumn('is_central_warehouse', function ($filteredData) {
                $isCentral = $filteredData->is_central_warehouse;
                if ($isCentral == 1) {
                    return "true";
                }
                return "false";
            })
            ->addColumn('cities', function ($filteredData) {
                $cities = null;
                if (isset($filteredData->cities)) {
                    foreach ($filteredData->cities as $city) {
                        if ($cities) {
                            $cities .= ", " . $city->name;
                        } else {
                            $cities = $city->name;
                        }
                    }
                }
                return $cities;
            })
            ->addColumn('zone_type', function ($filteredData) {
                $zoneType = $filteredData->zone_type;
                if ($zoneType == 1) {
                    return "Collection Point";
                } else if ($zoneType == 2) {
                    return "Home Delivery";
                } else {
                    return "-";
                }
            })
            ->addColumn("edit", function ($filteredData) use ($permissions) {
                if (Sentinel::hasAnyAccess($permissions)) {
                    return '<a href="#!" class="blue" onclick="window.location.href=\'' . url('zone/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Zone"><i class="fa fa-pencil"></i></a> ';
                }
            })
            ->addColumn("status", function ($filteredData) use ($permissions) {
                if (Sentinel::hasAnyAccess($permissions)) {
                    if ($filteredData->status == 1) {
                        return '<a href="#!" class="blue zone-deactivate " data-id="' . $filteredData->id . '" title="Deactivate Zone"><i class="fa fa-toggle-on"></i></a>';
                    } else if ($filteredData->status == 0) {
                        return '<a href="#!" class="blue zone-activate " data-id="' . $filteredData->id . '" title="Activate Zone"><i class="fa fa-toggle-off"></i></a>';
                    }
                }
            })
            ->make(true);

    }

    public function listData(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'code',
            2 => 'name',
            3 => 'description',
            4 => 'buyer_mobile',
            5 => 'buyer_name',
            6 => 'rider',
            7 => 'area.name',
            8 => 'zone_type',
            9 => 'is_central_warehouse'
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $jsonList = array();

        $zoneList = Zone::with('user', 'cities', 'area');
        if ($request->input('columns.0.search.value')) {
            $zoneList = $zoneList->where('id', $request->input('columns.0.search.value'));
        }
        if ($request->input('columns.1.search.value')) {
            $zoneList = $zoneList->where('code', 'LIKE',"%{$request->input('columns.1.search.value')}%");
        }
        if ($request->input('columns.2.search.value')) {
            $zoneList = $zoneList->where('name', 'LIKE',"%{$request->input('columns.2.search.value')}%");
        }
        if ($request->input('columns.3.search.value')) {
            $zoneList = $zoneList->where('description', 'LIKE',"%{$request->input('columns.3.search.value')}%");
        }
        if ($request->input('columns.4.search.value')) {
            $search = $request->input('columns.4.search.value');
            $zoneList = $zoneList->whereHas('user', function ($q) use ($search){
                $q->where('first_name', 'LIKE',"%{$search}%");
            });
        }
        if ($request->input('columns.5.search.value')) {
            $search = $request->input('columns.5.search.value');
            $zoneList = $zoneList->whereHas('cities', function ($q) use ($search){
                $q->where('name', 'LIKE',"%{$search}%");
            });
        }
        if ($request->input('columns.6.search.value')) {
            $zoneList = $zoneList->where('address', 'LIKE',"%{$request->input('columns.6.search.value')}%");
        }
        if ($request->input('columns.7.search.value')) {
            $search = $request->input('columns.7.search.value');
            $zoneList = $zoneList->whereHas('area', function ($q) use ($search){
                $q->where('name', 'LIKE',"%{$search}%");
            });
        }
        if ($request->input('columns.8.search.value')) {
            $value = null;
            if (strtolower($request->input('columns.8.search.value')) == "collection point") {
                $value = 1;
            } else if (strtolower($request->input('columns.8.search.value')) == "home delivery") {
                $value = 2;
            }
            $zoneList = $zoneList->where('zone_type', $value);
        }

        $totalData = $zoneList->count();

        if ($limit != -1) {
            $zoneList = $zoneList->offset($start)
                ->limit($limit);
            if ($orderNo == 0 || $orderNo == 1 || $orderNo == 2 || $orderNo == 3 || $orderNo == 6 || $orderNo == 8 || $orderNo == 9) {
                $zoneList = $zoneList->orderBy($order,$dir);
            }
        }

        $zoneList = $zoneList->get();

        if ($request->input('order.0.column') == 4) {
            if ($request->input('order.0.dir') == "asc") {
                $zoneList = $zoneList->sortBy(function ($filteredData) {
                    return $filteredData->user ? $filteredData->user->first_name : "-";
                })->values()->all();
            } else {
                $zoneList = $zoneList->sortByDesc(function ($filteredData) {
                    return $filteredData->user ? $filteredData->user->first_name : "-";
                })->values()->all();
            }

            $zoneList = collect($zoneList);
        }

        if ($request->input('order.0.column') == 5) {
            if ($request->input('order.0.dir') == "asc") {
                $zoneList = $zoneList->sortBy(function ($filteredData) {
                    return $filteredData->cities ? $filteredData->cities[0]->name : "-";
                })->values()->all();
            } else {
                $zoneList = $zoneList->sortByDesc(function ($filteredData) {
                    return $filteredData->cities ? $filteredData->cities[0]->name : "-";
                })->values()->all();
            }

            $zoneList = collect($zoneList);
        }

        if ($request->input('order.0.column') == 7) {
            if ($request->input('order.0.dir') == "asc") {
                $zoneList = $zoneList->sortBy(function ($filteredData) {
                    return $filteredData->area ? $filteredData->area->name : "-";
                })->values()->all();
            } else {
                $zoneList = $zoneList->sortByDesc(function ($filteredData) {
                    return $filteredData->area ? $filteredData->area->name : "-";
                })->values()->all();
            }

            $zoneList = collect($zoneList);
        }

        $permissions = Permission::whereIn('name', ['zone.edit', 'zone-manager', 'admin'])->where('status', '=', 1)->lists('name');

        foreach ($zoneList as $k => $zone) {
            $dd = array();

            $cities = null;
            if (isset($zone->cities)) {
                foreach ($zone->cities as $city) {
                    if ($cities) {
                        $cities .= ", " . $city->name;
                    } else {
                        $cities = $city->name;
                    }
                }
            }

            $isCentral = $zone->is_central_warehouse;
            if ($isCentral == 1) {
                $isCentral = "true";
            } else {
                $isCentral = "false";
            }

            $edit = "-";
            if (Sentinel::hasAnyAccess($permissions)) {
                $edit = '<a href="#!" class="blue" onclick="window.location.href=\'' . url('zone/' . $zone->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Zone"><i class="fa fa-pencil"></i></a> ';
            }

            $status = "-";
            if ($zone->status == 1) {
                $status = '<a href="#!" class="blue zone-deactivate " data-id="' . $zone->id . '" title="Deactivate Zone"><i class="fa fa-toggle-on"></i></a>';
            } else if ($zone->status == 0) {
                $status = '<a href="#!" class="blue zone-activate " data-id="' . $zone->id . '" title="Activate Zone"><i class="fa fa-toggle-off"></i></a>';
            }

            array_push($dd, $zone->id);
            array_push($dd, $zone->code);
            array_push($dd, $zone->name);
            array_push($dd, $zone->description);
            array_push($dd, $zone->user ? $zone->user->first_name : "-");
            array_push($dd, $cities);
            array_push($dd, $zone->address);
            array_push($dd, $zone->area->name);
            array_push($dd, $zone->zone_type);
            array_push($dd, $isCentral);
            array_push($dd, $edit);
            array_push($dd, $status);

            array_push($jsonList, $dd);
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $jsonList,
        );

        return Response::json($json_data);
    }

    public function editZone($id)
    {
        $zone = Zone::find($id);

        if (!$zone) {
            return redirect('zone/list')->with([
                'error' => true,
                'error.message' => 'Zone did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        $zone_cities = ZoneCity::select('city_id')->where('zone_id', $id)->lists('city_id')->toArray();
        $areas = Area::whereStatus(1)->get();

        $users = [];
        /**
         * ToDo need to update this to select role and get user
         * Role with user relation not working....
         */
        /*$zoneManager = UserRole::with('users')
        ->where('slug', 'zone-manager')
        ->get();*/
        $role = UserRole::where('slug', 'zone-manager')->first();
        if ($role) {
            $userIds = DB::table('role_users')->where('role_id', $role->id)->lists('user_id');
            foreach (User::whereIn('id', $userIds)->get() as $user) {
                if ($user->zone == null || $user->id == $zone->manager_id) {
                    $users[] = $user;
                }
            }
        }

        $districts = City::whereNull('distric_id')->where('status', 1)->get();
        foreach ($districts as $district) {
            $citiesOfDistrict = City::where('distric_id', $district->id)->where('status', 1)->get();
            $district->cities = $citiesOfDistrict;
        }

        return view('ZoneManage::edit')->with(['zone' => $zone, 'zone_cities' => $zone_cities, 'areas' => $areas, 'users' => $users, 'districts' => $districts]);
    }

    public function updateZone(Request $request)
    {
        $zone = Zone::find($request->id);
        if (!$zone) {
            return redirect('zone/list')->with([
                'error' => true,
                'error.message' => 'Zone did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        $centralWarehouse = $request->central_warehouse ? $request->central_warehouse : 0;

        if ($centralWarehouse == 1) {
            $zoneType = 3;
        } else {
            $zoneType = $request->zone_type;
        }

        $zone->name = $request->zone_name;
        $zone->description = $request->description;
        $zone->manager_id = $request->zone_manager;
        $zone->area_id = $request->area;
        $zone->is_central_warehouse = $centralWarehouse;
        $zone->zone_type = $zoneType;
        $zone->address = $request->address;

        if (value($request->cities)) {
            if (is_array($request->cities)) {
                $zone->cities()->sync($request->cities);
            }
        }

        if ($zone->save()) {
            return redirect('zone/list')->with([
                'success' => true,
                'success.message' => 'Zone updated successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('zone/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Zone did not updated ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function deleteZone(Request $request)
    {
        if ($request->ajax()) {
            $zone = Zone::find($request->id);
            if ($zone) {
                $zone->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function priceList()
    {
        // return view('ZoneManage::price-list')->with(['zones' => Zone::all()]);
        return view('ZoneManage::price-list');
    }

    public function priceJsonList()
    {
        $jsonList = array();
        $i = 1;
        foreach (ZonePrice::orderBy('zone_id', 'asc')->groupBy('zone_id')->get() as $zone_price) {

            $dd = array();

            array_push($dd, $zone_price->id);

            array_push($dd, $zone_price->main_zone->area->name);

            array_push($dd, $zone_price->main_zone['name']);

            $cities = null;
            if (isset($zone_price->main_zone->cities)) {
                foreach ($zone_price->main_zone->cities as $city) {
                    if ($cities) {
                        $cities .= ", " . $city->name;
                    } else {
                        $cities = $city->name;
                    }
                }
            }

            if ($cities == null) {
                array_push($dd, "-");
            } else {
                array_push($dd, $cities);
            }

            $zoneType = "Central Warehouse";
            if ($zone_price->main_zone->zone_type == 1) {
                $zoneType = "Collection Point";
            } else if ($zone_price->main_zone->zone_type == 2) {
                $zoneType = "Home Delivery";
            }

            array_push($dd, $zoneType);

            // array_push($dd, $zone_price->sub_zone['name']);

            // array_push($dd, $zone_price->price);

            $permissions = Permission::whereIn('name', ['zone.edit', 'admin'])->where('status', '=', 1)->lists('name');
            if (Sentinel::hasAnyAccess($permissions)) {
                array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('zone/price/' . $zone_price->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Zone Price"><i class="fa fa-pencil"></i></a></center>');
            }

//            if ($zone_price->status == 1) {
            //                array_push($dd, '<center><a href="#" class="blue zone-deactivate" data-id="' . $zone_price->id . '" data-toggle="tooltip" data-placement="top" title="Deactivate Zone Price"><i class="fa fa-toggle-on"></i></a></center>');
            //            } else {
            //                array_push($dd, '<center><a href="#" class="blue zone-activate" data-id="' . $zone_price->id . '" data-toggle="tooltip" data-placement="top" title="Activate Zone Price"><i class="fa fa-toggle-off"></i></a></center>');
            //            }

            array_push($jsonList, $dd);
            $i++;
        }
        return Response::json(array('data' => $jsonList));
    }

    public function addZonePrice()
    {
        $zone_ids = ZonePrice::lists('zone_id');
        $zones = Zone::whereNotIn('id', $zone_ids)->get();
        return view('ZoneManage::add-price')->with(['zones' => $zones]);
    }

    public function storeZonePrice(Request $request)
    {
        $zone_price = false;

        foreach ($request->price as $key => $data) {

            $zone_price = ZonePrice::create([
                'zone_id' => $request->main_zone,
                'destination_zone_id' => $request->sub_zone[$key],
                'price' => $request->price[$key],
                'per_kilo_price' => $request->per_kilo_price[$key],
                'starting_slab_price' => $request->starting_slab_price[$key],
            ]);
        }

        if ($zone_price) {
            return redirect('zone/price/list')->with([
                'success' => true,
                'success.message' => 'Zone Prices added successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('zone/price/add' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Zone Prices did not added ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function editZonePrice($id)
    {
        $zone_price = ZonePrice::find($id);

        $zoneType = null;
        if ($zone_price->main_zone['zone_type'] == 1) {
            $zoneType = "Collection Point";
        } else if ($zone_price->main_zone['zone_type'] == 2) {
            $zoneType = "Home Delivery";
        }

        $zone_price->zone_type = $zoneType;

        return view('ZoneManage::edit-price')->with(['zone_price' => $zone_price]);
    }

    public function updateZonePrice(Request $request)
    {
        foreach ($request->sub_zone as $key => $data) {

            $zone_price = ZonePrice::where('destination_zone_id', $data)
                ->where('zone_id', $request->main_zone)
                ->whereStatus(1)
                ->first();

            if ($zone_price) {
                $zone_price->price = $request->price[$key];
                $zone_price->per_kilo_price = $request->per_kilo_price[$key];
                $zone_price->starting_slab_price = $request->starting_slab_price[$key];
            } else {
                $zone_price = new ZonePrice;
                $zone_price->price = $request->price[$key];
                $zone_price->per_kilo_price = $request->per_kilo_price[$key];
                $zone_price->starting_slab_price = $request->starting_slab_price[$key];
                $zone_price->zone_id = $request->main_zone;
                $zone_price->destination_zone_id = $data;
                $zone_price->status = 1;
            }
            $zone_price->save();
        }

        if ($zone_price) {
            return redirect('zone/price/list')->with([
                'success' => true,
                'success.message' => 'Zone Prices updated successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('zone/price/' . $request->id . '/edit')->with([
            'error' => true,
            'error.message' => 'Zone Prices did not updated ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function getZone(Request $request)
    {
        $store = Store::where('related_id', $request->id)->where('store_type', 'zone')->first();
        //        $zone_ids = ZonePrice::where('zone_id', '!=', $request->id)->get()
        $bins = Bin::where('store_id', $store['id'])->where('related_id', '!=', $store['related_id'])->where('bin_type', 'zone')->lists('related_id');

        $zones = Zone::whereIn('id', $bins)->get();

        $zone_prices = array();
        foreach ($zones as $key => $zone) {
            $zone_prices[$key]['id'] = $zone->id;
            $zone_prices[$key]['name'] = $zone->name;
        }
        return response()->json($zone_prices);
    }

    public function getZonesByType(Request $request)
    {
        $zones = Zone::whereIn('zone_type', [3, $request->zone_id])->doesntHave('price')->get();

        $zone_list = array();
        foreach ($zones as $key => $zone) {
            $zone_list[$key]['id'] = $zone->id;
            $zone_list[$key]['name'] = $zone->name;
        }

        return response()->json($zone_list);
    }

    public function getStoredZone(Request $request)
    {
        $zoneType = $request->zone_type;
        if ($zoneType != 3) {
            $zones = Zone::whereIn('zone_type', [3, $zoneType])->get();
        } else {
            $zones = Zone::all();
        }

        $zone_prices = array();
        foreach ($zones as $key => $zone) {
            $zone_price = ZonePrice::where('zone_id', $request->id)
                ->where('destination_zone_id', $zone->id)
                ->whereStatus(1)
                ->first();
            $zone_prices[$key]['id'] = $zone->id;
            $zone_prices[$key]['name'] = $zone->name;
            $zone_prices[$key]['price'] = $zone_price ? number_format($zone_price->price, 2, ".", "") : "";
            $zone_prices[$key]['per_kilo_price'] = $zone_price ? number_format($zone_price->per_kilo_price, 2, ".", "") : "";
            $zone_prices[$key]['starting_slab_price'] = $zone_price ? number_format($zone_price->starting_slab_price, 2, ".", "") : "";
        }

        return response()->json($zone_prices);
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {
            $Zone = Zone::find($request->id);
            if ($Zone) {

                try {

                    \DB::beginTransaction();

                    $store = Store::where('store_type', 'zone')->where('related_id', $request->id)->first();

                    // Deactivate other zone bins
                    Bin::where('bin_type', 'zone')->where('related_id', $request->id)->update([
                        'status' => 1,
                    ]);

                    // Zone bn deactivation
                    Bin::where('store_id', $store->id)->update([
                        'status' => 1,
                    ]);

                    // Vehicle bin deactivation
                    Bin::where('bin_type', 'vehicle')->where('related_id', $request->id)->update([
                        'status' => 1,
                    ]);

                    $store->status = 1;
                    $store->save();

                    $Zone->update([
                        'status' => 1,
                    ]);

                    \DB::commit();

                } catch (\PDOException $exception) {
                    \Log::error("ZONE ACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response()->json('Something went wrong');
                }
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function deactivate(Request $request)
    {
        if ($request->ajax()) {
            $Zone = Zone::find($request->id);
            if ($Zone) {

                $store = Store::where('store_type', 'zone')->where('related_id', $request->id)->first();
                if ($store) {
                    $bins = Bin::where('store_id', $store->id)->get()->pluck('id')->toArray();
                    $stock = Stock::whereIn('bin_id', $bins)->where('availability', 'in')->count();
                    if ($stock > 0) {
                        return response()->json('Please clear the zone bins to deactivate the zone');
                    }

                    $bins2 = Bin::where('related_id', $Zone->id)->whereIn('bin_type', ['zone', 'vehicle'])->get()->pluck('id')->toArray();
                    $stock2 = Stock::whereIn('bin_id', $bins2)->where('availability', 'in')->count();
                    if ($stock2 > 0) {
                        return response()->json('Please clear the zone bins to deactivate the zone');
                    }
                }

                try {

                    \DB::beginTransaction();

                    // Deactivate other zone bins
                    Bin::where('bin_type', 'zone')->where('related_id', $request->id)->update([
                        'status' => 0,
                    ]);

                    // Zone bn deactivation
                    Bin::where('store_id', $store->id)->update([
                        'status' => 0,
                    ]);

                    // Vehicle bin deactivation
                    Bin::where('bin_type', 'vehicle')->where('related_id', $request->id)->update([
                        'status' => 0,
                    ]);

                    if ($store) {
                        $store->status = 0;
                        $store->save();
                    }

                    $Zone->update([
                        'status' => 0,
                    ]);

                    \DB::commit();

                } catch (\PDOException $exception) {
                    \Log::error("ZONE DEACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response()->json('Something went wrong');
                }

                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    // Zone Price

    public function activatePrice(Request $request)
    {
        if ($request->ajax()) {
            $ZonePrice = ZonePrice::find($request->id);
            if ($ZonePrice) {
                $ZonePrice->update([
                    'status' => 1,
                ]);
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function deactivatePrice(Request $request)
    {
        if ($request->ajax()) {
            $ZonePrice = ZonePrice::find($request->id);
            if ($ZonePrice) {
                $ZonePrice->update([
                    'status' => 0,
                ]);
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
