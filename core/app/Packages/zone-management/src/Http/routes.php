<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'ZoneManage\Http\Controllers', 'prefix' => 'zone'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'zone.list', 'uses' => 'ZoneController@index'
        ]);

        Route::get('json/list', [
            'as' => 'zone.list', 'uses' => 'ZoneController@jsonList'
        ]);

        Route::get('add', [
            'as' => 'zone.add', 'uses' => 'ZoneController@addZone'
        ]);

        Route::get('{id}/edit', [
            'as' => 'zone.edit', 'uses' => 'ZoneController@editZone'
        ]);

        Route::get('price/list', [
            'as' => 'zone.price.list', 'uses' => 'ZoneController@priceList'
        ]);

        Route::get('price/json/list', [
            'as' => 'zone.list', 'uses' => 'ZoneController@priceJsonList'
        ]);

        Route::get('price/add', [
            'as' => 'zone.price.add', 'uses' => 'ZoneController@addZonePrice'
        ]);

        Route::post('price/store', [
            'as' => 'zone.price.store', 'uses' => 'ZoneController@storeZonePrice'
        ]);

        Route::get('price/{zone_price}/edit', [
            'as' => 'zone.price.edit', 'uses' => 'ZoneController@editZonePrice'
        ]);

        Route::post('price/update', [
            'as' => 'zone.price.update', 'uses' => 'ZoneController@updateZonePrice'
        ]);

        Route::get('get/zones', [
            'as' => 'zone.get.zones', 'uses' => 'ZoneController@getZone'
        ]);

        Route::get('get/zones/type/data', [
            'as' => 'zone.get.zones', 'uses' => 'ZoneController@getZonesByType'
        ]);

        Route::get('get/stored/zones', [
            'as' => 'zone.get.stored.zones', 'uses' => 'ZoneController@getStoredZone'
        ]);
        

        /**
         * POST Routes
         */

        Route::post('list/data', [
            'as' => 'zone.list', 'uses' => 'ZoneController@listData'
        ]);

        Route::post('store', [
            'as' => 'zone.store', 'uses' => 'ZoneController@storeZone'
        ]);

        Route::post('update', [
            'as' => 'zone.update', 'uses' => 'ZoneController@updateZone'
        ]);

        Route::post('delete', [
            'as' => 'zone.delete', 'uses' => 'ZoneController@deleteZone'
        ]);

        Route::post('activate', [
            'as' => 'zone.update', 'uses' => 'ZoneController@activate'
        ]);
        
        Route::post('deactivate', [
            'as' => 'zone.update', 'uses' => 'ZoneController@deactivate'
        ]);
        
        // Zone price
        Route::post('price/activate', [
            'as' => 'zone.price.update', 'uses' => 'ZoneController@activatePrice'
        ]);
        
        Route::post('price/deactivate', [
            'as' => 'zone.price.update', 'uses' => 'ZoneController@deactivatePrice'
        ]);

    });
});


