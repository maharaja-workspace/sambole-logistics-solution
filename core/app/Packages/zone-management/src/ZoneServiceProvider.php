<?php

namespace ZoneManage;

use Illuminate\Support\ServiceProvider;

class ZoneServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'ZoneManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ZoneManage', function(){
            return new ZoneManage;
        });
    }
}
