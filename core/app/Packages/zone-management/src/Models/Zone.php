<?php
namespace ZoneManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model{
    use SoftDeletes;
    protected $fillable = ['name', 'description', 'area_id', 'address', 'code', 'manager_id', 'status', 'is_central_warehouse', 'zone_type'];

    public function cities()
    {
        return $this->belongsToMany( 'CityManage\Models\City', 'zone_city', 'zone_id', 'city_id' )->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo('UserManage\Models\user', 'manager_id','id');
    }

    public function price()
    {
        return $this->hasOne('ZoneManage\Models\ZonePrice', 'zone_id');
    }

    public function stores()
    {
        return $this->hasMany( 'StoreManage\Models\Store', 'related_id')->where('store_type', 'zone');
    }

    public function routings()
    {
        return $this->hasMany( 'RouteManage\Models\Routing', 'zone_id');
    }

    public function area()
    {
        return $this->belongsTo('AreaManage\Models\Area')->whereStatus(1);
    }
    
}