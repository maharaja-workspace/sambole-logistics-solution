<?php
namespace ZoneManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZonePrice extends Model{
    use SoftDeletes;

    protected $table = 'zone_to_zone_prices';
    protected $fillable = ['zone_id', 'destination_zone_id', 'price', 'per_kilo_price', 'starting_slab_price', 'status'];

    public function main_zone()
    {
        return $this->belongsTo('ZoneManage\Models\Zone','zone_id');
    }

    public function sub_zone()
    {
        return $this->belongsTo('ZoneManage\Models\Zone','destination_zone_id');
    }
}