<?php
namespace ZoneManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ZoneCity extends Model{
    use SoftDeletes;

    protected $table = 'zone_city';
    protected $fillable = ['zone_id', 'city_id', 'status'];


    public function zone()
    {
        return $this->belongsTo('ZoneManage\Models\Zone', 'zone_id', 'id');
    }

}