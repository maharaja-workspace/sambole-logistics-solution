@extends('layouts.back.master') @section('current_title','ADD ZONE PRICE')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<style>
    .loader {
        /*display: none;*/
    }
</style>
@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Zone Price Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Zone Price </strong>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row" style="margin-bottom: 100px;">
    <div class="col-lg-12" style="padding-top: 20px;">
        {{-- <h3>Zone to Zone Price</h3> --}}
        <form method="POST" class="form-horizontal" id="form2" action="{{ url('zone/price/update') }}">
            {!!Form::token()!!}

            <div class="form-group">
                <label class="col-sm-2 control-label">Zone Type </label>
                <div class="col-sm-3">
                    <input type="text" readonly name="zoneType" class="form-control"
                           value="{{ $zone_price->zone_type }}">
                    <input type="hidden" class="zone-type" name="main_zone_type" value="{{ $zone_price->zone_type }}">
                </div>
                {{--                        <button class="btn btn-info" type="submit">Select</button>--}}
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Zone Name </label>
                <div class="col-sm-3">

                    <input type="text" readonly name="zone" class="form-control"
                        value="{{ $zone_price->main_zone['name'] }}">
                    <input type="hidden" class="zone-name" name="main_zone" value="{{ $zone_price->zone_id }}">
                </div>
                <span class="loader" style="color: #198c6f; font-size: 20px; padding: 10px 5px;"><i
                        class="fa fa-refresh fa-spin text-center" style="font-size:24px"></i></span>
                {{-- <button class="btn btn-info" type="submit">Select</button>--}}
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label" style="text-align: center">Zone <!--<span class="required">*</span>--></label>
                <label class="col-sm-2 control-label">Delivery Charge <!--<span class="required">*</span>--></label>
                <label class="col-sm-2 control-label">Per kilo Price <!--<span class="required">*</span>--></label>
                <label class="col-sm-2 control-label">Starting Price <!--<span class="required">*</span>--></label>
            </div>
            <div class="hr-line-dashed"></div>
            <div id="zones">

            </div>

            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                    <button class="btn btn-primary" type="submit">Done</button>
                </div>
            </div>
        </form>
    </div>
</div>

@stop
@section('js')
{{-- <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>--}}
{{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

<script type="text/javascript">


    $(document).ready(function() {
        getZones();
        $(".js-source-states").select2();

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore");


        $("#form2").validate({
            rules: {
                zone_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 20
                },
                cities: {
                    required: true,
                    //lettersonly: false,
                    //maxlength: 20
                },
                zone_manager: {
                    required: true
                },
                area: {
                    required: true
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    var $zone_name = $(".zone-name");
    function getZones(){

        $.ajax({
            url: "{{ url('zone/get/stored/zones') }}",
            data: {
                'id': {{$zone_price->zone_id}},
                'zone_type' : {{$zone_price->main_zone['zone_type']}},
                '_token': '{{ csrf_token() }}'
            },
            success: function(data) {
                var $html = '';
                for (var i = 0; i < data.length; i++) {
                    $html += '<label class="col-sm-3 control-label">' + data[i].name + '</label>' +
                        '<div class="form-group">' +
                        '<label class="col-sm-1 control-label"><span class="required">*</span></label>' +
                        '<div class="col-sm-2"><input type="number" min="0" name="price[' + i + ']" value="' + data[i].price + '" class="form-control textnosize prices required"></div>' +
                        '<div class="col-sm-2"><input type="number" min="0" name="per_kilo_price[' + i + ']" value="' + data[i].per_kilo_price + '" class="form-control textnosize prices required"></div>' +
                        '<div class="col-sm-2"><input type="number" min="0" name="starting_slab_price[' + i + ']" value="' + data[i].starting_slab_price + '" class="form-control textnosize prices required"></div>' +
                        ' </div> ' +
                        '<div class="hr-line-dashed"></div>' +
                        '<input type="hidden" name="sub_zone[' + i + ']" value="' + data[i].id + '">';

                    $('input[name="price[' + i + ']"]').rules({
                        require : true,
                        number : true,
                        min : 0,
                    });
                }
                // $("#zones").html('').fadeOut('slow');
                // $(".loader").css('display', 'block');
                setTimeout(function() {
                    $("#zones").append($html).fadeIn('slow');
                    $(".loader").css('display', 'none');
                }, 1000);

            }
        });
    }


</script>
@stop