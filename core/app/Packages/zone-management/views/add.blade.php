@extends('layouts.back.master') @section('current_title','NEW ZONE')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Zone Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Zone </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row  m-b-xl">
        <div class="col-lg-12">
            <br>

            <form method="post" class="form-horizontal" id="form" action="{{ url('zone/store') }}">
                {!!Form::token()!!}


                <div class="form-group">

                    <label class="col-sm-2 control-label">Zone Code</label>
                    <div class="col-sm-3"><input type="text" name="zone_code" readonly class="form-control"
                                                 value="{{ '00' . $zones }}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Zone Name <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" name="zone_name" class="form-control" value="{{old('zone_name')}}">
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><input type="text" name="description" class="form-control"
                                                 value="{{old('description')}}"></div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Zone Manager <span
                                class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="zone_manager">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->first_name . ' ' . $user->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Cities <span
                                class="required">*</span></label>
                    <div class="col-sm-3">

                        <select class="js-source-states" id="citySelect" style="width: 100%" name="cities[]" multiple="multiple"
                                data-placeholder="Choose">
                            <option value="" disabled>Choose</option>
{{--                            @foreach($cities as $city)--}}
{{--                                <option value="{{ $city->id }}">{{ $city->name }}</option>--}}
{{--                            @endforeach--}}
                            @foreach($districts as $district)
                                @if(isset($district->cities) && sizeof($district->cities) > 0)
                                    <optgroup label="{{$district->name}}">
                                        @foreach($district->cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Area <span
                                class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="area">
                            <option value=""></option>
                            @foreach($areas as $area)
                                <option value="{{ $area->id }}">{{ $area->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

{{--                <div class="form-group"><label class="col-sm-2 control-label">Price <span--}}
{{--                                class="required">*</span></label>--}}
{{--                    <div class="col-sm-3"><input type="text" name="price" class="form-control"--}}
{{--                                                 value="{{old('price')}}"></div>--}}
{{--                </div>--}}

{{--                <div class="form-group row">--}}
{{--                    <label class="col-sm-2 control-label">Central Warehouse</label>--}}
{{--                    <div class="col-sm-3">--}}
{{--                        <div class="form-check">--}}
{{--                            <input type="checkbox" id="centralCheck" name="central_warehouse" onclick="myFunction()" class="form-check-input" value="1">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="form-group">
                    <div class="form-check">
                        <label class="col-sm-2 form-check-label" style="text-align: right">Central Warehouse</label>
{{--                                <input type="checkbox" id="centralCheck" name="central_warehouse" onclick="myFunction()" class="form-check-input" value="1">--}}
                        <div class="col-sm-3">
                            <input type="checkbox" id="centralCheck" name="central_warehouse" onclick="myFunction()" class="form-check-input" value="1">
                        </div>
{{--                        <label class="form-check-label" for="gridCheck">--}}
{{--                            Check me out--}}
{{--                        </label>--}}
                    </div>
                </div>

                <div id="zoneTypeDiv">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Zone Type <span class="required">*</span></label>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="form-check">
                            <input type="radio" name="zone_type" class="form-check-input" id="zoneToZoneRadio" style="margin-left: 15px" value="{{old('1', "1")}}">
                            <label class="form-check-label" for="exampleRadios1" id="zoneToZoneLabel" style="margin-left: 10px">
                                Collection Point
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="form-check">
                            <input type="radio" name="zone_type" class="form-check-input" id="zoneVehicleRadio" style="margin-left: 15px" value="{{old('2', "2")}}">
                            <label class="form-check-label" for="exampleRadios1" id="zoneVehicleLabel" style="margin-left: 10px">
                                Home Delivery
                            </label>
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div id="typeErrordiv"></div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Address<span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" name="address" class="form-control" value="{{old('address')}}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Done</button>
                        <button type="button" class="btn btn-danger" style="float: right;"
                                onclick="location.reload();">Cancel
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");

            jQuery.validator.addMethod("number_2_decimal", function (value, element) {
                // return this.optional(element) || /(?:\d*\.\d{1,2}|\d+)$/.test(value);
                return this.optional(element) || /^[1-9]\d*(\.\d{1,2}|\d+)?$/.test(value);
            }, "Please enter a valid price");


            $("#form").validate({
                rules: {
                    zone_name: {
                        required: true,
                        lettersonly: true,
                        maxlength: 20
                    },
                    cities: {
                        required: true,
                        //lettersonly: false,
                        //maxlength: 20
                    },
                    zone_manager: {
                        required: true
                    },
                    area: {
                        required: true
                    },
                    price: {
                        required: true,
                        number_2_decimal: true
                    },
                    zone_type: {
                        required:  {
                            depends:function(){
                                var checkBox = document.getElementById("centralCheck");

                                if (checkBox.checked == false){
                                    return true;
                                }
                            }
                        },
                    }
                    /* username:{
                         required: true,
                         email: true
                     },
                     password:{
                         required: true,
                         minlength: 6
                     },
                     password_confirmation:{
                         required: true,
                         minlength: 6,
                         equalTo: '#password'
                     },
                     "roles[]":{
                         required: true
                     }*/
                },
                errorPlacement : function(error, element) {
                    if(element.attr("name") == "zone_type"){
                        error.appendTo('#typeErrordiv');
                        return;
                    } else {
                        element.after(error);
                    }

                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("#citySelect").rules("add", { 
                required:true,  
            });
        });

        function myFunction() {
            var checkBox = document.getElementById("centralCheck");

            if (checkBox.checked == true){
                $('#zoneTypeDiv').hide();
            } else {
                $('#zoneTypeDiv').show();
            }
        }

    </script>
@stop