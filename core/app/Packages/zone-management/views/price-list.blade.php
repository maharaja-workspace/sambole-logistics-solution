@extends('layouts.back.master') @section('current_title','Zone Price List')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Zone Price Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Zone Price List</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2>
        <small>&nbsp;</small>
    </h2>
    <ol class="breadcrumb text-right">


    </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('zone/price/add')}}';">
    <p class="plus">+</p>
</div>


{!!Form::token()!!}
<div class="row">
    <div class="col-lg-12 margins">


        <div class="ibox-content">
            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th data-search="disable">Id</th>
                        <th data-search="disable">Area Name</th>
                        <th>Main Zone Name</th>
                        <th data-search="disable">Cities</th>
                        <th>Zone Type</th>
                        <th data-search="disable" width="1%" >Edit</th>
{{--                        <th data-search="disable" width="1%" >Status</th>--}}
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@stop
@section('js')

<script>
    $(document).ready(function() {
        $('#example1 thead th').each( function () {
            if($(this).data('search') == "disable"){

            }else{

                var title = $(this).text();
                $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
            }
        } );
        table = $('#example1').DataTable({
            "ajax": '{{url("zone/price/json/list/")}}',
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B>>tp",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            buttons: [{
                    extend: 'copy',
                    className: 'btn-sm'
                },
                {
                    extend: 'csv',
                    title: 'User List',
                    className: 'btn-sm'
                },
                {
                    extend: 'pdf',
                    title: 'User List',
                    className: 'btn-sm'
                },
                {
                    extend: 'print',
                    className: 'btn-sm'
                }
            ],
            "autoWidth": false,
            "order": [
                [0, "asc"]
            ],
            "columnDefs": [
                {"className": "dt-center", "targets": [-1, -2]},
                {
                "targets": [4],
                "orderable": false
            }],
            "columns" : [
                {"width" : "10%"},
                null,
                null,
                {"width" : "20%"},
                {"width" : "20%"},
                null
            ]

        });

        table.columns().every( function () {
            var that = this;

            $('input', this.header()).on('keyup change clear click', function (e) {
                e.stopPropagation();
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

        table.on('draw.dt', function() {
            $('.zone-delete').click(function(e) {
                e.preventDefault();
                id = $(this).data('id');
                confirmAlert(id);

            });
        });

        function confirmAlert(id) {
            if (confirm("Are you sure ?")) {
                $.ajax({
                        method: "POST",
                        url: '{{url("zone/price/delete")}}',
                        data: {
                            'id': id,
                            '_token': '{{ csrf_token() }}'
                        }
                    })
                    .done(function(msg) {
                        table.fnReloadAjax();
                    });
            }
        }

        table.on('draw.dt', function() {
            $('.zone-activate').click(function(e) {
                e.preventDefault();
                id = $(this).data('id');
                $.ajax({
                        method: "POST",
                        url: '{{url("zone/price/activate")}}',
                        data: {
                            'id': id,
                            '_token': '{{ csrf_token() }}'
                        }
                    })
                    .done(function(msg) {
                        table.fnReloadAjax();
                        toastr.success('Successfully Activated.');
                    });
            });
            
            $('.zone-deactivate').click(function(e) {
                e.preventDefault();
                id = $(this).data('id');
                $.ajax({
                        method: "POST",
                        url: '{{url("zone/price/deactivate")}}',
                        data: {
                            'id': id,
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function(data){
                        }
                    })
                    .done(function(msg) {
                        table.fnReloadAjax();
                        toastr.success('Successfully Deactivated.');
                    });
            });
        });
    });
</script>
@stop