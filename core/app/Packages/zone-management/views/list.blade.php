@extends('layouts.back.master') @section('current_title','All Zone')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
              width: 45px;
              padding-left:0px;
            }*/

        /* tr:hover .fixed_float{
              width: 50px;
              padding:  5px 0 0 0px;
            } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }

        .dataTables_filter, .dataTables_info { display: none; }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Zone Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Zone List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop

@section('content')
    @if($user->hasAnyAccess(['zone.add', 'admin']))
        <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
             onclick="location.href = '{{url('zone/add')}}';">
            <p class="plus">+</p>
        </div>
    @endif


    <div class="row">
        <div class="col-lg-12 margins">

            <div class="ibox-content">
                <div class = "table-responsive">
                    <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                        <tr>
                            <th  width="1%">#</th>
                            <th>Zone Code</th>
                            <th>Zone Name</th>
                            <th>Description</th>
                            <th>Zone Manager</th>
                            <th>Cities</th>
                            <th>Address</th>
                            <th>Area</th>
                            <th>Zone Type</th>
                            <th data-search="disable">Is Central Warehouse</th>
                            <th data-search="disable" width="1%" >Edit</th>
                            <th data-search="disable" width="1%" >Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#example1 thead th').each( function () {
                if($(this).data('search') == "disable"){

                }else{

                    var title = $(this).text();
                    $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
                }
            } );
            table = $('#example1').DataTable({
                "processing": true,
                "searching": true,
                "autoWidth": false,
                "order": [[0, "asc"]],
                "serverSide": true,
                "responsive": true,
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "initComplete": function(settings, json) {
                    $('#example1_filter input').unbind();
                    $('#example1_filter input').bind('keyup', function(e) {
                        if(e.keyCode == 13) {
                            table.search( this.value ).draw();
                        }
                    });
                },
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "buttons": [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Zone List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'pdf', title: 'Zone List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'print', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    }
                ],
                "ajax":{
                    "url": "{{ url('zone/list/data') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columnDefs": [
                    {"className": "dt-center", "targets": [-1, -2]},
                    { "searchable": false, "targets": [-1, -2, -6]},
                    { "orderable": false, "targets": [-1, -2, -6] }
                ],
                // "columns": [
                //     { "data": "id", name: "zones.id" },
                //     { "data": "code" },
                //     { "data": "name" },
                //     { "data": "description" },
                //     { "data": "user.first_name" ,defaultContent: "-" },
                //
                //     // { "data": function (data) {
                //     //         return data.user.first_name + (data.user.first_name ? " " + data.user.first_name : "")
                //     //     } , name: 'user.first_name'},
                //     { "data": "cities", name: "cities" },
                //     { "data": "address" },
                //     { "data": "area.name", name: "area.name" },
                //     { "data": "zone_type", name: "zone_type" },
                //     { "data": "is_central_warehouse", name: "is_central_warehouse" },
                //     { "data": "edit" },
                //     { "data": "status" },
                // ]
            });

            table.columns().every( function () {
                var that = this;

                $('input', this.header()).on('keyup change clear click', function (e) {
                    e.stopPropagation();
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });

            $("#example1_filter  input").keyup( function (e) {
                if (e.keyCode == 13) {
                    table.fnFilter( this.value );
                }
            });


            table.on('draw.dt', function() {
                $('.zone-activate').click(function(e) {
                    id = $(this).data('id');
                    swal({
                        title: "Are you sure?",
                        text: "Change the status of the zone?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Change it!"
                    }).then(function (isConfirm) {
                        if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                            $.ajax({
                                method: "POST",
                                url: '{{url("zone/activate")}}',
                                data: {
                                    'id': id,
                                    '_token': '{{ csrf_token() }}'
                                }
                            })
                                .done(function(msg) {
                                    table.ajax.reload();
                                    // toastr.success('Zone Successfully Activated.');
                                    sweetAlert('Success !!!', "Zone Successfully Activated!", 0);
                                });
                        }


                    });
                });
            });

            table.on('draw.dt', function() {
                $('.zone-deactivate').click(function(e) {
                    id = $(this).data('id');
                    swal({
                        title: "Are you sure?",
                        text: "Change the status of the zone?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Change it!"
                    }).then(function (isConfirm) {
                        if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                            $.ajax({
                                method: "POST",
                                url: '{{url("zone/deactivate")}}',
                                data: {
                                    'id': id,
                                    '_token': '{{ csrf_token() }}'
                                },
                                success: function(data){
                                }
                            })
                                .done(function(msg) {
                                    // toastr.success('Zone Successfully Deactivated.');
                                    if (msg == "success") {
                                        table.ajax.reload();
                                        sweetAlert('Success !!!', "Zone Successfully Deactivated!", 0);
                                    } else {
                                        sweetAlert('Failed !!!', msg, 0);
                                    }
                                });
                        }


                    });
                });

            });
        });
    </script>


@stop
