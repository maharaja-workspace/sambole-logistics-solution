<?php

namespace MerchantManage\Http\Controllers;

use Sentinel;
use Response;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use MerchantManage\Models\Merchant;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class MerchantController extends Controller
{

    public function index()
    {
        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;

        return view('MerchantManage::list');
    }

    public function listData(Request $request)
    {

        $permissions = Permission::whereIn('name',['merchant.edit','admin'])->where('status','=',1)->lists('name');

        return Datatables::of(Merchant::query())
            ->addColumn("action", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    $actionData = '<center>';
                    $actionData .= '<a href="#" class="blue" onclick="window.location.href=\''.url('merchants/'.$filteredData->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit merchant"><i class="fa fa-pencil"></i></a>';

                    if($filteredData->status == 1){
                        $actionData .=' &nbsp; <a href="javascript:void(0)" form="noForm" class="blue area-status-toggle merchants-deactivate" data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-times" aria-hidden="true"></i></a></>';
                    }
                    else if($filteredData->status == 0){
                        $actionData .= ' &nbsp; <a href="javascript:void(0)" class="blue area-status-toggle merchants-activate" data-id="'.$filteredData->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-check" aria-hidden="true"></i></a></>';
                    }

                    $actionData .= '</center>';

                    return $actionData;
                }
            })
            ->make(true);

    }

    function add()
    {
        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;
        return view('MerchantManage::add')->with(['merchants' => Merchant::all()->count()]);
    }

    function store(Request $request)
    {
        $merchant = new Merchant();
        $merchant->fill($request->all());
        $merchant->status = 1;

        if($merchant->save()) {
            return redirect('merchants/add')->with([
                'success' => true,
                'success.message' => 'Merchant Created successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('merchants/add')->with([
            'error' => true,
            'error.message' => 'Merchant did not created ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function edit($id)
    {
        $merchant = Merchant::find($id);

        if (!$merchant) {
            return redirect('merchants')->with([
                'error' => true,
                'error.message' => 'Merchant did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;
        return view('MerchantManage::edit')->with(['merchant' => $merchant]);
    }

    public function update(Request $request)
    {
        $merchant = Merchant::find($request->id);

        if (!$merchant) {
            return redirect('merchants')->with([
                'error' => true,
                'error.message' => 'Merchant did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $oldMerchantSecret = $merchant->merchant_secret;
        $merchant->fill($request->all());
        $merchant->merchant_secret = $oldMerchantSecret;

        if($merchant->save()) {
            return redirect('merchants')->with([
                'success' => true,
                'success.message' => 'Merchant updated successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('merchants')->with([
            'error' => true,
            'error.message' => 'Merchant did not updated ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $merchant = Merchant::find($request->id);
            if ($merchant) {
                $merchant->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {
            $merchant = Merchant::find($request->id);
            if ($merchant) {
                $merchant->update([
                    'status' => 1
                ]);
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function deactivate(Request $request)
    {
        if ($request->ajax()) {
            $merchant = Merchant::find($request->id);
            if ($merchant) {
                $merchant->update([
                    'status' => 0
                ]);
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
