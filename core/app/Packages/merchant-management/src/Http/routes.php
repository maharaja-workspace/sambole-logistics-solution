<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'MerchantManage\Http\Controllers', 'prefix' => 'merchants'], function () {
        /**
         * GET Routes
         */
        Route::get('/', [
            'as' => 'merchant.list', 'uses' => 'MerchantController@index'
        ]);

        Route::get('add', [
            'as' => 'merchant.add', 'uses' => 'MerchantController@add'
        ]);

        Route::get('{id}/edit', [
            'as' => 'merchant.edit', 'uses' => 'MerchantController@edit'
        ]);


        /**
         * POST Routes
         */

        Route::post('list/data', [
            'as' => 'merchant.list', 'uses' => 'MerchantController@listData'
        ]);

        Route::post('store', [
            'as' => 'merchant.store', 'uses' => 'MerchantController@store'
        ]);

        Route::post('update', [
            'as' => 'merchant.update', 'uses' => 'MerchantController@update'
        ]);

        Route::post('delete', [
            'as' => 'merchant.delete', 'uses' => 'MerchantController@delete'
        ]);

        Route::post('activate', [
            'as' => 'merchant.update', 'uses' => 'MerchantController@activate'
        ]);
        
        Route::post('deactivate', [
            'as' => 'merchant.update', 'uses' => 'MerchantController@deactivate'
        ]);
    });
});


