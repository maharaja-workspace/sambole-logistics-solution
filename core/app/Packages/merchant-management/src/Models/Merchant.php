<?php


namespace MerchantManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{

    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'merchants';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'telephone_number',
        'merchant_secret',
        'status',
        'address',
        'username',
        'contact_person_name',
        'br_number',
        'nic_number',
        'company_name',
        //'user_id',
        //'first_name',
        //'last_name',
        //'email',
        //'password',
    ];
}