@extends('layouts.back.master') @section('current_title','NEW ROUTE')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <style type="text/css">
        .map-container-6 {
            overflow: hidden;
            padding-bottom: 5%;
            margin-bottom: -10%;
            position: relative;
            height: 0;
        }

        .map-container-6 iframe {
            left: 2%;
            top: 2%;
            height: 75%;
            width: 50%;
            position: absolute;
        }

        #truck_pickup, #location {
            display: none;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 30%;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 40%;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }
    </style>
@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Merchant Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Edit Merchant </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row m-b-xl">
        <div class="col-lg-12" style="padding-top: 20px;">
            {{-- <h2>Route Creation</h2> --}}

            <form method="POST" class="form-horizontal" id="form" action="{{ url('merchants/update') }}">
                {!!Form::token()!!}

                <div class="form-group">
                    <label class="col-sm-3 control-label">Merchant Id 
                        <span class="required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="merchant_id" class="form-control" value="{{old('code', $merchant->id)}}" disabled="true">
                        <input type="hidden" name="id"  value="{{old('code', $merchant->id)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name 
                        <span class="required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control" value="{{old('company_name', $merchant->company_name)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Username
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" value="{{old('username', $merchant->username)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Merchant Secret Code
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="merchant_secret" class="form-control" value="{{old('merchant_secret', $merchant->merchant_secret)}}" disabled="true">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">BR Number
                        <span class="required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="br_number" class="form-control" value="{{old('br_number', $merchant->br_number)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">NIC Number
                        <span class="required"></span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="nic_number" class="form-control" value="{{old('nic_number', $merchant->nic_number)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact Person Name
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-5">
                        <input type="text" name="contact_person_name" class="form-control" value="{{old('contact_person_name', $merchant->contact_person_name)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact Person Number
                        <span class="required">*</span>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="telephone_number" class="form-control" value="{{old('telephone_number', $merchant->telephone_number)}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Address<span class="required"></span></label>
                    <div class="col-sm-9">
                        <textarea type="text" name="address" class="form-control" value="{{old('address', $merchant->address)}}">{{old('address', $merchant->address)}}</textarea>
                   </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-4">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                        </button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')

    <script>
        $("#truck").on('click', function () {
            if (this.checked === true) {
                $("#truck_pickup").fadeIn("slow").css('display', 'block');
                $("#location").fadeIn("slow").css('display', 'block');
            } else {
                $("#truck_pickup").fadeOut("slow").css('display', 'none');
                $("#location").fadeOut("slow").css('display', 'none');
            }
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("validateNIC", function (value, element) {
                return this.optional(element) || /^[0-9]{9}[vV]+$/i.test(value);
            }, "Your entered NIC number is invalid");

            jQuery.validator.addMethod("validatePhone", function (value, element) {
                return this.optional(element) || /^[0-9]{9,10}$/i.test(value);
            }, "Phone number must be 9 or 10 digits");


            $("#form").validate({
                ignore: [],
                rules: {
                    username: {
                        required: true,
                        maxlength: 20,
                    },
                    merchant_secret: {
                        required: true,
                        maxlength: 50,
                    },
                    contact_person_name: {
                        required: true,
                        maxlength: 50,
                    },
                    br_number: {
                        required: false,
                        maxlength: 20,
                        digits: true,
                    },
                    nic_number: {
                        required: false,
                        validateNIC: true,
                        maxlength: 10
                    },
                    telephone_number: {
                        required: true,
                        validatePhone: true,
                    },
                    address: {
                        required: false,
                        maxlength: 100,
                    },
                    company_name: {
                        required: false,
                        maxlength: 200,
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });

    </script>
@stop