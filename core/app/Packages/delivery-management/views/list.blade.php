@extends('layouts.back.master') @section('current_title','All Deliveries')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Delivery Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Delivery List</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2>
        <small>&nbsp;</small>
    </h2>
    <ol class="breadcrumb text-right">


    </ol>
</div>

@stop

@section('content')
{{--    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"--}}
{{--         onclick="location.href = '{{url('delivery/request')}}';">--}}
{{--        <p class="plus">+</p>--}}
{{--    </div>--}}

<div class="row">
    <div class="col-lg-12 margins">

        <input type="hidden" value="{{Sentinel::getUser()->inRole('admin') ? true : false}}" id="isAdmin">

        <div class="ibox-content">
            <h4>Pending Deliveries</h4>
            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Logistic Order ID</th>
                        <th>Merchant Order ID </th>
                        <th>Customer Name</th>
                        <th>Customer Mobile</th>
                        <th>Customer Address</th>
                        <th>Pickup City</th>
                        <th>Destination City</th>
                        <th>Vehicle Number</th>
                        @if(!Sentinel::getUser()->inRole('admin'))
                        <th style="width: 13%">Action</th>
                        @endif
                        @if(Sentinel::getUser()->inRole('admin'))
                            <th style="width: 13%">Zone</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <h4>Scheduled Deliveries</h4>
            <table id="example2" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Logistic Order ID </th>
                        <th>Merchant Order ID</th>
                        <th>Customer Name</th>
                        <th>Customer Mobile</th>
                        <th>Customer Address</th>
                        <th>Pickup City</th>
                        <th>Destination City</th>
                        <th>Vehicle Number</th>
                        <th>Assigned Rider</th>
                        <th>Assigned Vehicle</th>
                        @if(Sentinel::getUser()->inRole('admin'))
                        <th>Zone</th>
                        @endif
                        @if(!Sentinel::getUser()->inRole('admin'))
                        <th style="width: 13%;">Action</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" id="form" class="form-horizontal" action="{{ url('pickup/assign/request') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Rider</h4>
                </div>
                <div class="modal-body">

                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">Rider </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="rider">
                                <option value="">Please select a rider</option>
                                @foreach($riders as $rider)
                                <option value="{{ $rider->id }}">{{ $rider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="req_id" id="req_id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" id="form2" class="form-horizontal" action="{{ url('pickup/assign/request') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Rider</h4>
                </div>
                <div class="modal-body">

                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">Rider </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="rider">
                                <option value="">Please select a rider</option>
                                @foreach($riders as $rider)
                                <option value="{{ $rider->id }}">{{ $rider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="req_id" id="req_id2">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

@stop
@section('js')



<script>
    $(document).ready(function () {
            table = $('#example1').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{url('delivery/json/pending/list')}}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Pending Delivery', className: 'btn-sm', exportOptions: {
                            columns: exportCols()
                        }
                    },
                    {extend: 'pdf', title: 'Pending Delivery', className: 'btn-sm', exportOptions: {
                            columns: exportCols()
                        }
                    },
                    {extend: 'print', className: 'btn-sm'}
                ],
                searching: true,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"targets": sortDisable2(), "orderable": false}
                ]

            });

            table2 = $('#example2').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "{{url('delivery/json/scheduled/list')}}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Scheduled Delivery', className: 'btn-sm', exportOptions: {
                            columns: exportCols2()
                        }
                    },
                    {extend: 'pdf', title: 'Scheduled Delivery', className: 'btn-sm', exportOptions: {
                            columns: exportCols2()
                        }
                    },
                    {extend: 'print', className: 'btn-sm'}
                ],
                searching: true,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"targets": sortDisable(), "orderable": false}
                ]

            });

        function sortDisable()
        {
            if ($('#isAdmin').val()) {
                return [4];
            } else {
                return [4, 10];
            }
        }

        function sortDisable2()
        {
            if ($('#isAdmin').val()) {
                return [4];
            } else {
                return [4, 8];
            }
        }

        function exportCols() {
            if ($('#isAdmin').val()) {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8];
            } else {
                return [0, 1, 2, 3, 4, 5, 6, 7];
            }
        }

        function exportCols2() {
            if ($('#isAdmin').val()) {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9 , 10];
            } else {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
            }
        }

            table.on('draw.dt', function () {
                $(".assign-btn").click(function () {
                    $("#req_id").val($(this).attr('data-id'));
                });
            });
            table2.on('draw.dt', function () {
                $(".change-btn").click(function () {
                    $("#req_id2").val($(this).attr('data-id'));
                });
            });

            $("#form").submit(function (e) {
                // var $valid =
                e.preventDefault();

                if ($("#form").valid()) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('delivery/assign/request')}}',
                        data: $(this).serialize(),
                        success: function (data) {
                           if(data.hasOwnProperty('status')){
                               if(data.status === "success"){
                                   toastr.success('Successfully Assigned');
                               }else if(data.status === "error" && data.hasOwnProperty('message')){

                                   toastr.error(data.message);

                               }
                           }else{
                               toastr.error("Oops !!!. Something went wrong please try again.");
                           }
                            $("#myModal").modal('toggle');
                            table.ajax.url("{{ url('delivery/json/pending/list') }}").load();
                            table2.ajax.url("{{ url('delivery/json/scheduled/list') }}").load();

                        }
                        ,
                        error: function(err){
                            toastr.error("Oops !!!. Something went wrong please try again.");
                        }
                    })
                }
            });

            $("#form2").submit(function (e) {
                // var $valid =
                e.preventDefault();

                if ($("#form2").valid()) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('delivery/change/rider')}}',
                        data: $(this).serialize(),
                        success: function (data) {
                            // console.log(data);
                            if (data == "success") {
                                $("#myModal2").modal('toggle');
                                toastr.success('Successfully Changed');
                                table2.ajax.url("{{ url('delivery/json/scheduled/list') }}").load();
                            } else {
                                toastr.error(data);
                            }
                        }
                    })
                }
            });

            $("#form").validate({
                rules: {
                    rider: {
                        required: true
                    }
                }
            });

            $("#form2").validate({
                rules: {
                    rider: {
                        required: true
                    }
                }
            });

            $(document).on('click', '#example2 tbody tr td button.change-btn', function() {
                //var driverName = $(this).parents('tr').find("td:eq(6)").text();
                var driverId = $(this).data("driver-id");
                $("#form2 select").val(driverId);
            });
            
        });
</script>


@stop