<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Delivery Request Form | Sambole.lk web portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">

    <link href="{{asset('assets/back/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{asset('assets/back/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <link href="{{ asset('assets/back/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

    <link href="{{asset('assets/back/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/css/style.css')}}" rel="stylesheet">
    {{--    <link rel="stylesheet" href="{{ asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css') }}"/>--}}

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

        /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        #Wheader {
            margin-left: 10px;
        }

        .align-middle {
            margin-top: 10px;
        }

        .h4 {
            font-size: 14px;
        }

        .balance {
            margin-top: 2%;
            width: 50%;
        }


        .clickable {
            cursor: pointer;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 10px;
            bottom: -4px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: -2px;
            background-color: #0286f9;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #52abf9;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(18px);
            -ms-transform: translateX(18px);
            transform: translateX(18px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
            bottom: -3px;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 40%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
</head>

<body class="gray-bg">
    <div class="container">
        <h2 class="text-center"><strong>Sambole Logsitics</strong></h2>
        <div class="row" style="margin: 30px auto;">
            <div class="col-lg-12 well">
                <h3 class="text-center">Delivery Request Form</h3><br>
                <form method="POST" class="form-horizontal" id="form" action="{{ url('delivery/request/store') }}">
                    {!!Form::token()!!}


                    <div class="col-sm-5" align="right">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Request Type <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <select name="request_type" class="form-control">
                                    <option value="delivery">Delivery</option>
                                    <option value="return">Return</option>
                                </select>
                            </div>
                        </div>
                        <br>


                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Requester's Name <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_name">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Requester's NIC <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_nic">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Requester's Email <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_email">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Requester's Mobile <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_mobile">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Requester's Address <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <textarea class="form-control" rows="3" name="customer_address"></textarea>
                                <!-- <input type="text" class="form-control" name="packType"> -->
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup City <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <select data-placeholder="Choose" class="js-source-states form-control"
                                    style="width: 100%" name="customer_city" tabindex="-1" title="">
                                    <option value="">Choose</option>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <hr>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Name <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_name">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer NIC <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_nic">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Mobile <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_mobile">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Delivery Address <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <textarea class="form-control" rows="3" name="buyer_address"></textarea>
                            </div>
                        </div>
                        <br>

                        <!-- <div class="row">
                      <div class="col-xs-6">
                        <label>&nbsp</label>
                      </div>
                      <div class="col-xs-6">
                        <input type="text" class="form-control" name="packType">
                      </div>
                    </div><br> -->

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Delivery City <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <select data-placeholder="Choose" class="js-source-states form-control"
                                    style="width: 100%" name="buyer_city" tabindex="-1" title="">
                                    <option value="">Choose</option>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br><br>

                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-4" align="right">
                                <label>Cash on Delivery <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-2" align="center">
                                <label class="switch">
                                    <input type="checkbox" name="payment_type" checked value="1">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="col-xs-4" align="left">
                                <label>Paid</label>
                            </div>
                        </div>
                        <br><br>

                        <div class="row">
                            <div class="col-xs-6" align="right">
                                <input type="radio" id="delivery_type_1" checked name="delivery_type" value="0">
                            </div>
                            <div class="col-xs-6" align="left">
                                <label for="delivery_type_1">Pickup from Store</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6" align="right">
                                <input type="radio" name="delivery_type" id="delivery_type_2" value="1">
                            </div>
                            <div class="col-xs-6" align="left">
                                <label for="delivery_type_2">Pickup from Mobile Store</label>
                                <div class="row" id="vehicle_number" style="display: none">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" name="vehicle_number"
                                            placeholder="Vehicle Number">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6" align="right">
                                <input type="radio" name="delivery_type" id="delivery_type_3" value="2">
                            </div>
                            <div class="col-xs-6" align="left">
                                <label for="delivery_type_3">Door Step Delivery</label>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-5" align="right">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Package Type <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <select data-placeholder="Choose" class="js-source-states form-control"
                                    style="width: 100%" name="package_type" tabindex="-1" title="">
                                    <option value="">Choose</option>
                                    @foreach($package_types as $package_type)
                                    <option value="{{ $package_type->id }}">{{ $package_type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Package Size <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <select data-placeholder="Choose" class="js-source-states form-control"
                                    style="width: 100%" name="package_size" tabindex="-1" title="">
                                    <option value="">Choose</option>
                                    @foreach($package_sizes as $package_size)
                                    <option value="{{ $package_size->id }}">{{ $package_size->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Package Weight <span class="required">*</span></label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <select data-placeholder="Choose" class="js-source-states form-control"
                                    style="width: 100%" name="package_weight_price" tabindex="-1" title="">
                                    <option value="">Choose</option>
                                    @foreach($package_weights as $package_weight)
                                    <option value="{{ $package_weight->id }}">{{ $package_weight->min_weight }}
                                        to {{ $package_weight->max_weight }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Remarks</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <textarea name="remarks" id="remarks" class="form-control"
                                    placeholder="Remarks"></textarea>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-2"></div>

                    <div class="row">
                        <div class="col-sm-8" align="right">
                            <button class="btn btn-info">DELIVER</button>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-danger" type="button"
                                onclick="window.location.reload()">RESET</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/back/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/back/js/plugins/toastr/toastr.min.js')}}"></script>
    {{--<script src="{{ asset('assets/back/js/plugins/select2/select2.full.min.js') }}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/back/select2-3.5.2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/back/select2-3.5.2/select2.min.js') }}"></script>

    <script>
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
        // $(".js-source-states").select2();

        jQuery.validator.addMethod("lettersonly", function (value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The area name can only consist of alphabetical & underscore");
        
        jQuery.validator.addMethod("nic", function (value, element) {
            return this.optional(element) || /^([0-9]{9}[x|X|v|V]|[0-9]{12})$/i.test(value);
        }, "Invalid NIC Number");

        $(function () {
            $("#vehicle1").click(function () {
                if ($(this).is(":checked")) {
                    $("#addLocation").show();
                    //$("#noLocation").hide();
                } else {
                    $("#addLocation").hide();
                    //$("#noLocation").show();
                }
            });
        });

        $("#form").validate({
            ignore: [],
            rules: {
                request_type: {
                    required: true,
                },
                buyer_name: {
                    required: true,
                    lettersonly: true,
                    // maxlength: 20
                },
                buyer_mobile: {
                    required: true,
                    number: true,
                    maxlength: 11
                },
                customer_mobile: {
                    required: true,
                    number: true,
                    maxlength: 11
                },
                customer_address: {
                    required: true,
                },
                pickup_address: {
                    required: true,
                },
                customer_city: {
                    required: true
                },
                customer_name: {
                    required: true
                },
                customer_email: {
                    required: true
                },
                buyer_address: {
                    required: true
                },
                buyer_city: {
                    required: true,
                },
                customer_nic: {
                    required: true,
                    nic :true,
                    maxlength: 12
                },
                buyer_nic: {
                    required: true,
                    nic :true,
                    maxlength: 12
                },
                package_type: {
                    required: true,
                },
                package_size: {
                    required: true,
                },
                package_weight_price: {
                    required: true,
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#delivery_type_2").click(function(){
            if ($(this).is(":checked")) {
            $("#vehicle_number").show();
            //$("#noLocation").hide();
            } else {
            $("#vehicle_number").hide();
            //$("#noLocation").show();
            }
        });
    });

    </script>
</body>

</html>