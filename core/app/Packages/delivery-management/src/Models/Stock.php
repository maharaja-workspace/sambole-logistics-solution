<?php
namespace DeliveryManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use StoreManage\Models\Store;

class Stock extends Model{
    use SoftDeletes;

    protected $fillable = ['request_id', 'store_id', 'bin_id', 'availability'];

    function store()
    {
        return $this->belongsTo(Store::class, "store_id");
    }
}