<?php
namespace DeliveryManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model{
    use SoftDeletes;

    protected $fillable = ['user_id', 'first_name', 'last_name', 'telephone_number', 'merchant_secret', 'username', 'password', 'email', 'status', 'address'];
}