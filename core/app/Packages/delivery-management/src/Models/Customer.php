<?php
namespace DeliveryManage\Models;

use CityManage\Models\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model{
    use SoftDeletes;

    protected $fillable = ['user_id', 'first_name', 'last_name', 'mobile', 'address_line1', 'address_line2', 'city_id', 'email', 'status', 'merchant_id'];

    protected $appends = ['full_name', 'full_address'];



    function getFullAddressAttribute(){
        $address = $this->address_line1;

        return $address;
    }

    function getFullNameAttribute(){
        return $this->first_name." ".$this->last_name;
    }

    function city(){
        return $this->belongsTo(City::class, 'city_id');
    }
}