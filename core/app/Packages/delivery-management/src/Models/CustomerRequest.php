<?php

namespace DeliveryManage\Models;

use CityManage\Models\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ZoneManage\Models\ZoneCity;

class CustomerRequest extends Model
{
    use SoftDeletes;

    protected $table = 'requests';

    protected $fillable = ['request_type', 'name', 'order_id', 'pickup_address', 'pickup_city', 'buyer_name', 'buyer_mobile', 'buyer_address', 'buyer_city', 'payment_type', 'delivery_type', 'package_type', 'package_size', 'package_type_size_price', 'package_weight', 'total_amount', 'tax_amount', 'package_weight_price', 'remark', 'status', 'created_by', 'customer_id', 'customer_nic', 'buyer_nic', ''];

    const orderIdPrefix = "SMBL-";

    public function getZoneCity()
    {
        return $this->hasOne('ZoneManage\Models\ZoneCity', 'city_id', 'buyer_city');
    }

    public function customer()
    {
        return $this->hasOne('DeliveryManage\Models\Customer', 'id', 'customer_id');
    }

    // public function rider_assign()
    // {
    //     return $this->belong('DailyPlanManage\Models\RiderAssign', 'id', 'request_id');
    // }

    public function rider_assign()
    {
        return $this->hasOne('DailyPlanManage\Models\RiderAssign', 'request_id', 'id');
    }

    function pickupCity(){
        return $this->belongsTo(City::class, 'pickup_city');
    }
    function buyerCity(){
        return $this->belongsTo(City::class, 'buyer_city');
    }

    function zoneCity(){
        return $this->belongsTo(ZoneCity::class, 'buyer_city', 'city_id');
    }

}
