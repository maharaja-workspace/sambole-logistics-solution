<?php

Route::group(['namespace' => 'DeliveryManage\Http\Controllers', 'prefix' => 'delivery'], function () {
    Route::get('request', [
        'as' => 'delivery.request', 'uses' => 'DeliveryController@request'
    ]);
});


Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'DeliveryManage\Http\Controllers', 'prefix' => 'delivery'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'delivery.list', 'uses' => 'DeliveryController@index'
        ]);

        Route::post('json/pending/list', [
            'as' => 'delivery.list', 'uses' => 'DeliveryController@jsonPendingList'
        ]);

        Route::post('json/scheduled/list', [
            'as' => 'delivery.list', 'uses' => 'DeliveryController@jsonScheduledList'
        ]);

        Route::get('{id}/edit', [
            'as' => 'delivery.edit', 'uses' => 'DeliveryController@edit'
        ]);


        /**
         * POST Routes
         */
        Route::post('request/store', [
            'as' => 'delivery.request.store', 'uses' => 'DeliveryController@requestStore'
        ]);

        Route::post('assign/request', [
            'as' => 'delivery.store', 'uses' => 'DeliveryController@assignRequest'
        ]);

        Route::post('change/rider', [
            'as' => 'delivery.store', 'uses' => 'DeliveryController@changeRider'
        ]);

        Route::post('update', [
            'as' => 'delivery.update', 'uses' => 'DeliveryController@update'
        ]);

        Route::post('delete', [
            'as' => 'delivery.delete', 'uses' => 'DeliveryController@delete'
        ]);
    });
});
