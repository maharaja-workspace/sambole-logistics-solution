<?php

namespace DeliveryManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use CityManage\Models\City;
use DailyPlanManage\Models\RiderAssign;
use DailyPlanManage\Models\RiderVehicle;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use Illuminate\Http\Request;
use PackageSizeManage\Models\PackageSize;
use PackageTypeManage\Models\PackageType;
use PackageTypeSizeManage\Models\PackageTypeSize;
use PackageWeightManage\Models\PackageWeight;
use PickUpManage\Http\Controllers\PickUpController;
use Response;
use RiderManage\Models\Rider;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\Stock;
use StoreManage\Models\Store;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use ZoneManage\Models\Zone;
use ZoneManage\Models\ZoneCity;

class DeliveryController extends Controller
{
    public $PickUpController;

    public function __construct()
    {
        $this->PickUpController = new PickUpController();
        return $this;
    }

    public function index()
    {
        $logged_in_user = Sentinel::getUser();
//        $rider_ids = RiderVehicle::lists('rider_id')->toArray();
//        $riders = Rider::whereIn('id', $rider_ids);
//        if (!$logged_in_user->inRole('admin')) {
//            $riders = $riders->where('zone', $logged_in_user->zone['id']);
//        }
//        $riders = $riders->get();

        $riders = Rider::where(function ($q){
            $q->whereHas('rider_assign', function ($q){
                    $q->where('active', 1);
                });
            $q->whereHas('rider_vehicle', function ($q){
                    $q->where('status', 1);
                });
        });
        if (!$logged_in_user->inRole('admin')) {
            $riders = $riders->where('zone', $logged_in_user->zone['id']);
        }
        $riders = $riders->get();

        return view('DeliveryManage::list')->with(['riders' => $riders]);
    }

    public function request()
    {
        $logged_in_user = Sentinel::getUser();

        $package_types = PackageType::where('status', 1)->get();
        $package_sizes = PackageSize::where('status', 1)->get();
        $package_weights = PackageWeight::where('status', 1)->get();

        return view('DeliveryManage::request')->with(['package_types' => $package_types, 'package_sizes' => $package_sizes, 'package_weights' => $package_weights, 'logged_in_user' => $logged_in_user, 'cities' => City::where('status', 1)->get()]);
    }

    public function requestStore(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $user = null;

        if ($logged_in_user) {
            $user = $logged_in_user->id;
        }

        if ($request->has('buyer_name')) {
            $customer = Customer::create([
                'first_name' => $request->customer_name,
                'email' => $request->customer_email,
                'mobile' => $request->customer_mobile,
                'address_line1' => $request->customer_address,
                'city_id' => $request->customer_city,
                'merchant _id' => 1,
            ]);
            $vehicle_number = null;
            if ($request->has('vehicle_number')) {
                $vehicle_number = $request->vehicle_number;
            }
            $orderIdPrefix = CustomerRequest::orderIdPrefix;
            $customer_request = CustomerRequest::create([
                'request_type' => $request->request_type,
                'order_id' => $orderIdPrefix . str_pad((CustomerRequest::all()->count() + 1), 8, '0', STR_PAD_LEFT),
                'status' => 0,
                'created_by' => $user,
                'customer_id' => $customer->id,
                'customer_nic' => $request->customer_nic,
                'pickup_address' => $request->customer_address,
                'pickup_city' => $request->customer_city,
                'buyer_name' => $request->buyer_name,
                'buyer_nic' => $request->buyer_nic,
                'buyer_mobile' => $request->buyer_mobile,
                'buyer_address' => $request->buyer_address,
                'buyer_city' => $request->buyer_city,
                'payment_type' => $request->payment_type or 0,
                'delivery_type' => $request->delivery_type,
                'vehicle_number' => $vehicle_number,
                'package_type' => $request->package_type,
                'package_size' => $request->package_size,
                'package_type_size_price' => $package_type_size_price = PackageTypeSize::where('package_type_id', $request->package_type)->where('package_size_id', $request->package_size)->first()['price'],
                'package_weight' => $request->package_weight_price,
                'package_weight_price' => PackageWeight::find($request->package_weight_price)->price,
                'tax_amount' => 0,
                'total_amount' => $package_type_size_price + PackageWeight::find($request->package_weight_price)->price,
                'remark' => $request->remarks,
            ]);
        }

        if ($customer_request) {
            return redirect('delivery/request')->with([
                'success' => true,
                'success.message' => 'Delivery Request Created successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('delivery/request')->with([
            'error' => true,
            'error.message' => 'Delivery Request did not created ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function jsonPendingList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();

        if ($logged_in_user->inRole('admin')) {
            $routes = Routing::with('cities')->get();
        } else {
            $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        }

        $cities = [];
        foreach ($routes as $route) {
            $cities[] = $route->cities->toArray();
        }

        $city_ids = $this->PickUpController->array_column_recursive($cities, 'city_id');

//        $customer_requests = CustomerRequest::where('status', 3)->orderBy('created_at', 'desc');
//        if ($isCentralWarehouse) {
//            $customer_requests = $customer_requests->where('pickup_type', 1)->where('delivery_type', 2);
//        } else {
//            $customer_requests = $customer_requests->whereIn('pickup_city', array_unique($city_ids))
//                ->where(function ($q){
//                    $q->where(function ($q){
//                        $q->where('pickup_type', 1)->where('delivery_type', 1);
//                    })->orWhere(function ($q){
//                        $q->where('pickup_type', 2)->where('delivery_type', 1);
//                    })->orWhere(function ($q){
//                        $q->where('pickup_type', 2)->where('delivery_type', 2);
//                    });
//                });
//        }
//        $customer_requests = $customer_requests->get();

//        $totalData = $customer_requests->count();
//        $totalFiltered = $customer_requests->count();

        $columns = array(
            0 => 'order_id',
            1 => 'customers.first_name',
            2 => 'pickup_address',
            3 => 'buyer_mobile',
            4 => 'action',
            5 => 'customers.city.name',
            6 => 'cities.name',
            7 => 'vehicle_number',
            8 => 'zone',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $customer_requests = CustomerRequest::where('status', 3)->orderBy('created_at', 'desc');
        if ($isCentralWarehouse) {
            $customer_requests = $customer_requests->where('pickup_type', 1)->where('delivery_type', 2);
        } else {
            $customer_requests = $customer_requests->whereIn('buyer_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });
        }
        $totalData = $customer_requests->count();
        if (empty($request->input('search.value'))) {
            if ($orderNo != 2 && $orderNo != 5 && $orderNo != 6 && $orderNo != 8) {
                $customer_requests = $customer_requests->orderBy($order, $dir);
            }

            if ($limit != -1) {
                $customer_requests = $customer_requests->offset($start)
                    ->limit($limit)->get();
            } else {
                $customer_requests = $customer_requests->get();
            }

        } else {
            $search = $request->input('search.value');
            $filteredDatasQuery = $customer_requests->where(function ($or) use ($search, $logged_in_user) {
                $or->where(function ($query1) use ($search) {
                    $query1->whereHas('customer', function ($query1) use ($search) {
                        $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                            ->orWhere('customers.last_name', 'LIKE', "%{$search}%");
                    });
                })
                    ->orWhere('order_id', 'LIKE', "%{$search}%")
                    ->orWhere('merchant_order_id', $search)
                    ->orWhere('vehicle_number', 'LIKE', "%{$search}%")
                    ->orWhere('buyer_name', 'LIKE', "%{$search}%");
                if ($logged_in_user->inRole('admin')){
                    $or->orWhereHas('zoneCity.zone', function ($query) use ($search){
                        $query->where('name', 'LIKE', "%{$search}%");
                    });
                }
            });

            $totalData = $customer_requests->count();

            if ($limit != -1) {
                $customer_requests = $filteredDatasQuery->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $customer_requests = $filteredDatasQuery->get();
            }
        }


        $totalFiltered = $totalData;

        if ($request->input('order.0.column') == 2) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 5) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 6) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 8) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if (!$isCentralWarehouse) {
            if ($customer_requests->count()) {
                $userZoneIds = Zone::where('manager_id', $logged_in_user->id)->get()->pluck('id')->toArray();
                if (value($userZoneIds)) {
                    $zoneCityIds = ZoneCity::whereIn('zone_id', $userZoneIds)->get()->pluck('city_id')->toArray();
                    if (value($zoneCityIds)) {
                        $customer_requests = $customer_requests->reject(function ($customer_request) use ($zoneCityIds) {
                            return (!in_array($customer_request->buyer_city, $zoneCityIds));
                        })->values()->all();
                        $customer_requests = collect($customer_requests);
                    }
                }
            }
        }

        foreach ($customer_requests as $customer_request) {
            $dd = array();
            array_push($dd, $customer_request->order_id);
            array_push($dd, $customer_request->merchant_order_id);
            array_push($dd, $customer_request->buyer_name);
            array_push($dd, $customer_request->buyer_mobile);
            array_push($dd, $customer_request->buyer_address);
            array_push($dd, (isset($customer_request->pickupCity)) ? $customer_request->pickupCity->name : "-");
            array_push($dd, (isset($customer_request->buyerCity)) ? $customer_request->buyerCity->name : "-");
            array_push($dd, $customer_request->vehicle_number);
            if (!Sentinel::getUser()->inRole('admin')) {
                $action = '<center><button class="btn btn-sm btn-info assign-btn" data-id="' . $customer_request->id . '" data-toggle="modal" data-target="#myModal">Assign</button></center>';
                array_push($dd, $action);
            }
            if (Sentinel::getUser()->inRole('admin')) {
                array_push($dd, (isset($customer_request->zoneCity)) ? $customer_request->zoneCity->zone->name : "-");
            }
            array_push($jsonList, $dd);
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $jsonList,
        );

        return Response::json($json_data);
    }

    public function jsonScheduledList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();

        if ($logged_in_user->inRole('admin')) {
            $routes = Routing::with('cities')->get();
        } else {
            $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        }

        $cities = [];
        foreach ($routes as $route) {
            $cities[] = $route->cities->toArray();
        }

        $city_ids = $this->PickUpController->array_column_recursive($cities, 'city_id');

        $customer_requests = CustomerRequest::join('customers', 'requests.customer_id', '=', 'customers.id')
            ->leftJoin('rider_assigns', 'rider_assigns.request_id', '=', 'requests.id')
            ->select('requests.order_id', 'requests.pickup_address', 'customers.first_name', 'requests.buyer_mobile', 'riders.name')
            ->leftJoin('riders', 'riders.id', '=', 'rider_assigns.rider_id')
            ->where('requests.status', 4);
        if ($isCentralWarehouse) {
            $customer_requests = $customer_requests->where('pickup_type', 1)->where('delivery_type', 2);
        } else {
            $customer_requests = $customer_requests->whereIn('buyer_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });
        }
        $customer_requests = $customer_requests->get();

        $totalData = $customer_requests->count();
        $totalFiltered = $customer_requests->count();

        $columns = array(
            0 => 'requests.order_id',
            1 => 'requests.merchant_order_id',
            4 => 'requests.pickup_address',
            2 => 'customers.first_name',
            3 => 'requests.buyer_mobile',
            5 => 'customers.city.name',
            6 => 'cities.name',
            7 => 'requests.vehicle_number',
            8 => 'rider',
            9 => 'zone',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $customer_requests = CustomerRequest::
            // ->leftJoin('rider_assigns', 'rider_assigns.request_id', '=', 'requests.id')
            //->select('requests.order_id', 'requests.pickup_address', 'customers.first_name', 'requests.buyer_mobile', 'riders.name')
            // ->leftJoin('riders', 'riders.id', '=', 'rider_assigns.rider_id')
            with('rider_assign.rider')->has('rider_assign')
            ->with('rider_assign.rider')->has('rider_assign')
            ->with(['rider_assign' => function ($q){
                $q->with('rider.user');
                $q->with(['store' => function ($q){
                    $q->with('vehicle');
                }]);
            }])
            ->where('requests.status', 4);
//        $customer_requests = CustomerRequest::join('customers', 'requests.customer_id', '=', 'customers.id')
//            ->leftJoin('rider_assigns', 'rider_assigns.request_id', '=', 'requests.id')
//            ->select('requests.order_id', 'requests.pickup_address', 'customers.first_name', 'requests.buyer_mobile', 'riders.name')
//            ->leftJoin('riders', 'riders.id', '=', 'rider_assigns.rider_id')
//            ->where('requests.status', 4);
        if ($isCentralWarehouse) {
            $customer_requests = $customer_requests->where('pickup_type', 1)->where('delivery_type', 2);
        } else {
            $customer_requests = $customer_requests->whereIn('buyer_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 1)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });
        }


        $totalFiltered = $customer_requests->count();
        if (empty($request->input('search.value'))) {
            $totalData = sizeof($customer_requests->get());
            if ($limit != -1) {
                $customer_requests = $customer_requests->offset($start)->limit($limit);
            }

            if ($orderNo != 2 && $orderNo != 5 && $orderNo != 6 && $orderNo != 8 && $orderNo != 9) {
                $customer_requests = $customer_requests->orderBy($order, $dir);
            }
            $customer_requests = $customer_requests->get();
        } else {
            $search = $request->input('search.value');
            $filteredDatasQuery = $customer_requests->where(function ($or) use ($search, $logged_in_user) {
                $or->where(function ($query1) use ($search) {
                    $query1->whereHas('customer', function ($query1) use ($search) {
                        $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                            ->orWhere('customers.last_name', 'LIKE', "%{$search}%");
                    });
                })
                    ->orWhere('order_id', 'LIKE', "%{$search}%")
                    ->orWhere('merchant_order_id', $search)
                    ->orWhere('vehicle_number', 'LIKE', "%{$search}%")
                    ->orWhere('buyer_name', 'LIKE', "%{$search}%")
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('rider_assign.getRider.user', function ($q) use ($search) {
                            $q->where('users.first_name', 'LIKE', "%{$search}%");
                            $q->orWhere('users.last_name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('rider_assign.store.vehicle', function ($q) use ($search) {
                            $q->where('vehicles.name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('buyerCity', function ($q) use ($search) {
                            $q->where('name', 'LIKE', "%{$search}%");
                        });
                    });
                if ($logged_in_user->inRole('admin')){
                    $or->orWhereHas('zoneCity.zone', function ($query) use ($search){
                        $query->where('name', 'LIKE', "%{$search}%");
                    });
                }
            });

            $totalData = $customer_requests->count();

            if ($limit != -1) {
                $customer_requests = $filteredDatasQuery->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $customer_requests = $filteredDatasQuery->get();
            }
        }

        if ($request->input('order.0.column') == 2) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 5) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 6) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 8) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->rider_assign->getRider->user->first_name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->rider_assign->getRider->user->first_name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 9) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->rider_assign->store->vehicle->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->rider_assign->store->vehicle->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($request->input('order.0.column') == 10) {
            if ($request->input('order.0.dir') == "asc") {
                $customer_requests = $customer_requests->sortBy(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            } else {
                $customer_requests = $customer_requests->sortByDesc(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            }

            $customer_requests = collect($customer_requests);
        }

        if ($customer_requests->count()) {
            foreach ($customer_requests as $customer_request) {
                $dd = array();

                $riderName = '-';
                if (isset($customer_request->rider_assign)) {
                    $riderName = $customer_request->rider_assign->getRider->user->first_name . " " . $customer_request->rider_assign->getRider->user->last_name;
                }

                if (isset($customer_request->rider_assign->rider)) {
                    $driverId = $customer_request->rider_assign->rider->id;
                } else {
                    $driverId = null;
                }

                array_push($dd, $customer_request->order_id);
                array_push($dd, $customer_request->merchant_order_id);
                array_push($dd, $customer_request->buyer_name);
                array_push($dd, $customer_request->buyer_mobile);
                array_push($dd, $customer_request->buyer_address);
                array_push($dd, (isset($customer_request->pickupCity)) ? $customer_request->pickupCity->name : "-");
                array_push($dd, (isset($customer_request->buyerCity)) ? $customer_request->buyerCity->name : "-");
                array_push($dd, $customer_request->vehicle_number);
                array_push($dd, $riderName);
                array_push($dd, $customer_request->rider_assign->store->vehicle->name);
                if (Sentinel::getUser()->inRole('admin')) {
                    array_push($dd, (isset($customer_request->zoneCity)) ? $customer_request->zoneCity->zone->name : "-");
                }

                if (!Sentinel::getUser()->inRole('admin')) {
                    $action = '<center><button class="btn btn-sm btn-info change-btn" data-driver-id="' . $driverId . '" data-id="' . $customer_request->id . '" data-toggle="modal" data-target="#myModal2">Change</button></center>';
                    array_push($dd, $action);
                }
                array_push($jsonList, $dd);
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $jsonList,
        );

        return Response::json($json_data);
    }

    public function edit($id)
    {
        $logged_user = Sentinel::getUser();

        $vehicle = Vehicle::find($id);

        if (!$vehicle) {
            return redirect('vehicle/list')->with([
                'error' => true,
                'error.message' => 'Vehicle did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        $zones = Zone::select('id')->where('manager_id', $logged_user->id)->lists('id')->toArray();

        if ($logged_user->inRole('zone-manager')) {
            if (!in_array($vehicle['zone_id'], $zones)) {
                return redirect('vehicle/list')->with([
                    'error' => true,
                    'error.message' => 'You do not have access to edit this! ',
                    'error.title' => 'Ooops !',
                ]);
            }
        }

        if ($logged_user->inRole('zone-manager')) {
            $vehicle_types = VehicleType::where('zone_access', 0)->get();
        } else {
            $vehicle_types = VehicleType::all();
        }
        return view('VehicleManage::edit')->with(['vehicle' => $vehicle, 'vehicle_types' => $vehicle_types, 'zones' => Zone::all(), 'logged_in_user' => $logged_user]);
    }

    public function update(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $vehicle = Vehicle::find($request->id);
        if (!$vehicle) {
            return redirect('vehicle/list')->with([
                'error' => true,
                'error.message' => 'Vehicle did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        $vehicle->number = $request->number;
        $vehicle->name = $request->name;
        $vehicle->zone_id = $request->zone or $logged_in_user->zone['id'];
        $vehicle->vehicle_type_id = $request->type;
        $vehicle->customer_pickup = $request->customer_pickup or 0;
        $vehicle->color = $request->color;
        $vehicle->vehicle_model = $request->model;
        $vehicle->make = $request->make;

        if ($vehicle->save()) {

            if (VehicleType::where('id', $request->type)->first()['is_bin']) {

                $stores = Store::all();
                $vehicles = Vehicle::all();

                foreach ($stores as $store) {
                    foreach ($vehicles as $vehicle) {
                        $check_bin = Bin::where('store_id', $store->id)->where('related_id', $vehicle->id)->where('bin_type', 'vehicle')->first();
                        if ($check_bin == null) {
                            Bin::create([
                                'store_id' => $store->id,
                                'related_id' => $vehicle->id,
                                'bin_type' => 'vehicle',
                            ]);
                        }
                    }
                }
            }

            return redirect('vehicle/list')->with([
                'success' => true,
                'success.message' => 'Vehicle updated successfully!',
                'success.title' => 'Well Done!',
            ]);
        }

        return redirect('vehicle/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Vehicle did not updated ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function assignRequest(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        if ($request->has('req_id') && $request->has('rider')) {
            $stock = Stock::where('request_id', $request->req_id)->first();

            if ($stock) {
                Stock::find($stock['id'])->update(['availability', 'out']);
                $rider_assign = RiderAssign::where('rider_id', $request->rider)->where('active', 1)->orderBy('created_at', 'desc')->first();

                $store_id = null;
                $rider_vehicle = RiderVehicle::where('rider_id', $request->rider)->orderBy('created_at', 'desc')->first();
                if ($rider_vehicle) {
                    $store = Store::where('related_id', $rider_vehicle->vehicle_id)
                        ->where('store_type', 'vehicle')
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if ($store) {
                        $store_id = $store->id;
                    }

                }

//                RiderAssign::create([
//                    'rider_id' => $request->rider,
//                    'store_id' => (is_null($store_id)) ? $rider_assign['store_id'] : $store_id,
//                    'route_id' => $rider_assign['route_id'],
//                    'request_id' => $request->req_id,
//                    'assigned_by' => $logged_in_user->id,
//                ]);

                $riderAssign = new RiderAssign();
                $riderAssign->rider_id = $request->rider;
                $riderAssign->store_id = (is_null($store_id)) ? $rider_assign['store_id'] : $store_id;
                $riderAssign->route_id = $rider_assign['route_id'];
                $riderAssign->request_id = $request->req_id;
                $riderAssign->assigned_by = $logged_in_user->id;
                $riderAssign->save();

                CustomerRequest::find($request->req_id)->update(['status' => 4]);
                return response([
                    "status" => "success",
                ]);
            } else {
                return response([
                    "status" => "error",
                    "message" => "Stock not available for the delivery",
                ]);
            }
        }

        return response('Invalid Request', 400);
    }

    public function changeRider(Request $request)
    {
        $rider_assign = RiderAssign::where('request_id', $request->req_id)->orderBy('created_at', 'desc')->first();

        if ($rider_assign) {

            $store = Store::where('id', $rider_assign->store_id)->first();
            if ($store) {
                $stock = Stock::where('store_id', $store->id)->where('request_id', $request->req_id)->where('availability', 'in')->first();
                if ($stock) {
                    return response('cannot change rider because current request is already in stock');
                }
            }

            $newRidersLatestAssign = RiderAssign::where('rider_id', $request->rider)->where('active', 1)->orderBy('created_at', 'desc')->first();
            if (!$newRidersLatestAssign) {
                return response('there is no active rider assign record for new rider');
            }

            $rider_assign2 = RiderAssign::find($rider_assign['id']);
            $rider_assign2->rider_id = $request->rider;
            $rider_assign2->store_id = $newRidersLatestAssign->store_id;
            $rider_assign2->route_id = $newRidersLatestAssign->route_id;
            $rider_assign2->save();
            return response('success');
        } else {
            return redirect('pickup/list')->with([
                'error' => true,
                'error.message' => 'Request did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $vehicle = Vehicle::find($request->id);
            if ($vehicle) {
                $vehicle->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
