@extends('layouts.back.masterWithoutFtr') @section('current_title','All User')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Bin to Bin Transfer</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New Transfer</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2>
        <small>&nbsp;</small>
    </h2>
    <ol class="breadcrumb text-right">


    </ol>
</div>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <!-- h2>Bin 2 Bin Transfer</h2> -->
        <div class="ibox-content">
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label">Source Bin</label>
                    <div class="col-sm-4">
                        <select class="form-control source_bin" name="parent_city">
                            <option value="null" selected disabled>Choose</option>
                            <optgroup label="ZONE | ROUTE BIN NAME">
                                @foreach($route_bins as $route_bin)
                                    <option value="{{ $route_bin['id'] }}">{{ $route_bin['zone'] }} |
                                        {{ $route_bin['name'] }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="ZONE | BIN NAME">
                                @foreach($source_bins as $source_bin)
                                <option value="{{ $source_bin['id'] }}">{{ $source_bin['zone'] }} |
                                    {{ $source_bin['name'] }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="ZONE | EXTRA BIN NAME">
                                @foreach($f_exdesti_bins as $f_exdesti_bin)
                                <option value="{{ $f_exdesti_bin['id'] }}">{{ $f_exdesti_bin['zone'] }}|
                                    {{ $f_exdesti_bin['bin_name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Destination Bin</label>
                    <div class="col-sm-4">
                        <select class="form-control destination_bin" name="area">
                            <option value="" selected disabled>Choose</option>
                            <optgroup label="ZONE | ROUTE BIN NAME">
                                @foreach($route_bins as $route_bin)
                                    <option value="{{ $route_bin['id'] }}">{{ $route_bin['zone'] }} |
                                        {{ $route_bin['name'] }}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="ZONE | BIN NAME">
                                @foreach($source_bins as $source_bin)
                                <option value="{{ $source_bin['id'] }}">{{ $source_bin['zone'] }} |
                                    {{ $source_bin['name'] }}</option>
                                @endforeach
                                {{-- @foreach($desti_bins as $desti_bin)
                                <option value="{{ $desti_bin['id'] }}">{{ $desti_bin['zone'] }} |
                                {{ $desti_bin['bin_name'] }}</option>
                                @endforeach --}}
                            </optgroup>
                            <optgroup label="ZONE | EXTRA BIN NAME">
                                @foreach($f_exdesti_bins as $f_exdesti_bin)
                                <option value="{{ $f_exdesti_bin['id'] }}">{{ $f_exdesti_bin['zone'] }}|
                                    {{ $f_exdesti_bin['bin_name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>

            </form>
        </div>

        <form method="POST" class="form-horizontal" id="form">
            {!!Form::token()!!}
            <div class="row">
                <div class="col-lg-12 margins">


                    <div class="ibox-content">
                        <div class="col-sm-2 col-sm-offset-10" align="right">
                            <button class="btn btn-info" id="transfer_all" type="button">Transfer All</button>
                            <!-- <button type="button" class="btn btn-danger" onclick="location.reload();">Cancel</button>-->
                        </div>
                        <br><br><br>
                        <div class="row">
                            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th><input id="checkall" type="checkbox"></th>
                                        <th>Reference Number</th>
                                        <th>Merchant Order Id</th>
                                        <th>Merchant Name</th>
                                        <th>Customer Name</th>
                                        <th>Address</th>
                                        <th>Merchant Mobile Number</th>
                                        <th>Pickup City</th>
                                        <th>Destination</th>
                                        <th>Transfer</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
@stop
@section('js')
<script>
    $(document).ready(function () {

        $('.source_bin').select2();
        $('.destination_bin').select2();

            table = $('#example1').DataTable({
                "ajax": '{{url('bin/json/transfer/list?id=null')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Bin Transfer List', className: 'btn-sm', exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'pdf', title: 'Bin Transfer List', className: 'btn-sm', exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'print', className: 'btn-sm', exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    }
                ],
                "autoWidth": false,
                "order": [[1, "asc"]],
                "columnDefs": [
                    {"targets": [0], "orderable": false}
                ]
            });

            $(".source_bin").on("change", function () {
                var id = $(this).val();
                // $('.destination_bin option[value="'+value+'"]').attr("disabled", true);
                $('.destination_bin option').each(function() {
                    if ( $(this).val() == id ) {
                        $(this).attr('disabled', true);
                    } else {
                        $(this).attr('disabled', false);
                    }
                });
                table.ajax.url("{{ url('bin/json/transfer/list?id=') }}" + id).load();
            });

            // table.on('draw.dt', function () {
                $("#checkall").click(function () {
                    $('#example1 tbody .check').not(this).prop('checked', this.checked);
                });

                $("#example1 tbody").on("click", ".transfer", function (e) {
                    var $destination_bin = $(".destination_bin").find(':selected').val();
                    var $source_bin = $(".source_bin").find(':selected').val();
                    $(this).prop('disabled', true);
                    if ($destination_bin === null || $destination_bin === '') {
                        toastr.error("Please Select a Destination Bin");
                        $(this).prop('disabled', false);
                    } else {
                        if ($source_bin != $destination_bin) {
                            $.ajax({
                                method: "POST",
                                url: '{{url('bin/transfer/request')}}',
                                data: {
                                    'stock_id': $(this).attr('data-id'),
                                    'destination_bin': $destination_bin,
                                    'source_bin': $source_bin,
                                    '_token': '{{ csrf_token() }}'
                                },
                                success: function (data) {
                                    if (data == "Success") {
                                        toastr.success('Successfully Transferred');
                                        $(this).prop('disabled', false);
                                        {{--table.ajax.url("{{ url('bin/json/transfer/list?id=') }}" + $(".source_bin").val()).load();--}}
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        toastr.error(data);
                                    }
                                }
                            });
                        } else {
                            $(this).prop('disabled', false);
                            toastr.error("Destination bin cant be same as source bin");
                        }
                    }
                });
            // });


            // table.on('draw.dt', function () {
                var stock_ids = new Array();
                $("#transfer_all").click(function (e) {
                    $("#transfer_all").prop('disabled', true);
                    $('#example1 tbody .check').each(function (i) {
                        if ($(this).prop('checked') === true) {
                            stock_ids[i] = $(this).val();
                        }
                    });
                    var $destination_bin = $(".destination_bin").find(':selected').val();
                    var $source_bin = $(".source_bin").find(':selected').val();
                    console.log($destination_bin);
                    if ($destination_bin === null || $destination_bin === '') {
                        toastr.error("Please Select a Destination Bin");
                        $("#transfer_all").prop('disabled', false);
                    } else {
                        if ($source_bin != $destination_bin) {
                            e.preventDefault();
                            if (stock_ids.length === 0) {
                                toastr.error("Please Select at least one Request");
                                $("#transfer_all").prop('disabled', false);
                            } else {
                                $.ajax({
                                    method: "POST",
                                    url: '{{url('bin/transfer/requests')}}',
                                    data: {
                                        'stock_ids': stock_ids,
                                        'destination_bin': $destination_bin,
                                        'source_bin': $source_bin,
                                        '_token': '{{ csrf_token() }}'
                                    },
                                    success: function (data) {
                                        if (data == "Success") {
                                            toastr.success('Successfully Transferred');
                                            $("#transfer_all").prop('disabled', false);
                                            {{--table.ajax.url("{{ url('bin/json/transfer/list?id=') }}" + $(".source_bin").val()).load();--}}
                                            setTimeout(function () {
                                                location.reload();
                                            }, 3000);
                                        } else {
                                            toastr.error(data);
                                        }
                                    }
                                });
                            }
                        } else {
                            $("#transfer_all").prop('disabled', false);
                            toastr.error("Destination bin cant be same as source bin");
                        }
                    }
                });
            // });
            
        });
</script>
@stop