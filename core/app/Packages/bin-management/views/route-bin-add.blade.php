@extends('layouts.back.master') @section('current_title','NEW BIN')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Bin Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Route Bin </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h2>Bin Creation</h2>

            <form method="post" class="form-horizontal" id="form" action="{{ url('bin/store/route-bin') }}">
                {!!Form::token()!!}

                <div class="form-group"><label class="col-sm-2 control-label">Store <span
                                class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="store">
                            <option value=""></option>

                            @foreach($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->id }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Route <span
                                class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="routing">
                            <option value=""></option>
                            @foreach($routings as $routing)
                                <option value="{{ $routing->id }}">{{ $routing->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Done</button>
                        <button type="button" class="btn btn-danger" style="float: right;"
                                onclick="location.reload();">Cancel
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");


            $("#form").validate({
                rules: {
                    store: {
                        required: true,
                    },
                    routing: {
                        required: true,
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop