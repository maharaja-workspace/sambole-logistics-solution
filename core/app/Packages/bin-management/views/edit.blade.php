@extends('layouts.back.master') @section('current_title','EDIT BIN')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Bin Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Edit Bin </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h2>Bin Update</h2>
            <form method="POST" class="form-horizontal" id="form" action="{{url("areas/".$area->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Area Name <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="area_name" class="form-control"
                                                 value="{{$area->name}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Area Code
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled name="area_code" class="form-control"
                                                 value="{{$area->id}}"></div>
                </div>
                <!-- <div class="form-group"><label class="col-sm-2 control-label">CITY SELECTION<span class="required">*</span></label>
                    <div class="col-sm-10">
                         <select class="form-control" name="city_selection">
                         <option value="">Choose</option>
                         <option value="colombo7">Colombo - 7</option>
                         <option value="Galle">Galle</option>
                         <option value="kaluthara">Kaluthara<option>
                         </select>
                    </div>
                </div> -->
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><input type="text" name="description" class="form-control"
                                                 value="{{$area->description}}"></div>
                </div>
                <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button class="btn btn-danger" type="button" style="float: right;" onclick="location.reload();">
                            Cancel
                        </button>
                    </div>
                </div>


            </form>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");


            $("#form").validate({
                rules: {
                    area_name: {
                        required: true,
                        lettersonly: true,
                        maxlength: 20
                    },
                    /* area_code:{
                         required: true,
                         lettersonly: false,
                         maxlength: 20
                     },
                     city_selection:{
                         required: true
                     },*/
                    /* username:{
                         required: true,
                         email: true
                     },
                     password:{
                         required: true,
                         minlength: 6
                     },
                     password_confirmation:{
                         required: true,
                         minlength: 6,
                         equalTo: '#password'
                     },
                     "roles[]":{
                         required: true
                     }*/
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop