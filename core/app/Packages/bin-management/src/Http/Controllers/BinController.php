<?php

namespace BinManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use BinManage\Models\ExtraBin;
use Carbon\Carbon;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use DeliveryManage\Models\Stock;
use Illuminate\Http\Request;
use Response;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\StockTransaction;
use StoreManage\Models\Store;
use VehicleManage\Models\Vehicle;
use ZoneManage\Models\Zone;

class BinController extends Controller
{

    function index()
    {
        $logged_in_user = Sentinel::getUser();

        $zone = $logged_in_user->zone;

        $stores = Store::where('store_type', 'zone');
        if (!$logged_in_user->inRole('admin')){
            $stores =  $stores->where('related_id', $zone['id']);
        }
        $stores = $stores->where('status', 1)->get();

        foreach ($stores as $key => $store) {
            if ($store->store_type == 'zone') {
                $zone = Zone::where('id', $store->related_id)->where('status', 1)->first()['name'];
                if ($zone) {
                    $stores[$key]['name'] = $zone;
                } else {
                    unset($stores[$key]);
                }
            }
            if ($store->store_type == 'vehicle') {
                $stores[$key]['name'] = Vehicle::where('id', $store->related_id)->first()['name'];
            }
        }

        return view('BinManage::list')->with(['stores' => $stores, 'zone' => $zone]);
    }

    function indexRouteBinList()
    {
        $logged_in_user = Sentinel::getUser();

        $zone = $logged_in_user->zone;

        $stores = Store::where('related_id', $zone['id'])->where('store_type', 'zone')->get();

        return view('BinManage::route-bin-list')->with(['stores' => $stores]);
    }

    public function jsonList(Request $request)
    {
        $jsonList = array();
        $i = 1;
        $user = Sentinel::getUser();

        $columns = array(
            0 => '',
            1 => 'postal_code',
            4 => 'name'
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $store = null;
        if (!$user->hasAccess('admin') && $request->id == '') {
            return Response::json(array('data' => $jsonList));
        } else {

            $bins = Bin::where('bin_type', 'extra_bin');
            if(($user->hasAccess('admin') && $request->id != '') || !$user->hasAccess('admin')){
                $bins = $bins->where('store_id', $request->id);
            }
            if (!$user->inRole('admin')) {
                $store = Store::where('store_type', 'zone')->where('related_id', $user->zone['id'])->first();
            }

            $totalData = $bins->count();
            $totalFilteredData = $totalData;

            if (empty($request->input('search.value'))) {
                if ($limit != -1) {
                    $bins = $bins->offset($start)
                        ->limit($limit);
                }
            } else {
                $search = $request->input('search.value');
                $bins = $bins->where(function ($q) use ($search) {
                    $q->whereHas('store.zone', function ($q) use ($search) {
                        $q->where('name', 'LIKE', "%{$search}%");
                    })
                    ->orWhereHas('extraBin', function ($q) use ($search) {
                        $q->where('name', 'LIKE', "%{$search}%");
                    });
                });

                if ($limit != -1) {
                    $bins = $bins->offset($start)
                        ->limit($limit);
                }
            }

            $bins = $bins->get();

            foreach ($bins as $bin) {

                $dd = array();
                array_push($dd, $i);
                $extra_bin = ExtraBin::where('id', $bin->related_id)->first();
//                if ($user->hasAccess('admin')){
                    array_push($dd, $bin->store && $bin->store->zone ? $bin->store->zone->name : "-");
                    array_push($dd, $extra_bin ? $extra_bin['name'] : "-");
                    array_push($dd, Carbon::parse($bin->created_at)->toDateString());
//                    array_push($dd, $extra_bin && $extra_bin['status'] == 1 ? "Active" : "Inactive");
//                    array_push($dd, $bin->getProducts() ? $bin->getProducts()->count() : 0);
                 $storeId = $store ? $store->id : $bin->store_id;
                    array_push($dd, $this->getProductsCount($bin->id, $storeId));
                if ($bin->status == 1) {
                    array_push($dd, '<center><a href="#!" class="blue bin-deactivate action-items" data-id="' . $bin['id'] . '" title="Deactivate Bin"><i class="fa fa-toggle-on"></i></a></center>');
                } else {
                    array_push($dd, '<center><a href="#!" class="blue bin-activate action-items" data-id="' . $bin['id'] . '" title="Activate Bin"><i class="fa fa-toggle-off"></i></a></center>');
                }
//                }else{
//
//                    array_push($dd, $extra_bin['name']);
//                    if ($extra_bin['status'] == 1) {
//                        array_push($dd, '<center><a href="#!" class="blue bin-deactivate action-items" data-id="' . $extra_bin['id'] . '" title="Deactivate Bin"><i class="fa fa-toggle-on"></i></a></center>');
//                    } else {
//                        array_push($dd, '<center><a href="#!" class="blue bin-activate action-items" data-id="' . $extra_bin['id'] . '" title="Activate Bin"><i class="fa fa-toggle-off"></i></a></center>');
//                    }
//                }


                array_push($jsonList, $dd);
                $i++;
            }

            $json_data = array(
                "draw" => intval($request->input('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFilteredData),
                "data" => $jsonList,
            );

            echo json_encode($json_data);
        }
    }

    public function jsonRouteBinList(Request $request)
    {
        $jsonList = array();
        $i = 1;

        if ($request->id == 'null') {
            return Response::json(array('data' => $jsonList));
        } else {
            $bins = Bin::where('store_id', $request->id)->where('bin_type', 'route')->get();

            foreach ($bins as $bin) {

                $dd = array();
                $routing = null;
                $routing = Routing::where('id', $bin->related_id)->first();

                array_push($dd, $i);
                array_push($dd, $routing['name']);

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        }
    }

    function add()
    {
        $logged_in_user = Sentinel::getUser();
        if ($logged_in_user->hasAccess('admin')){
            return redirect('bin/list')->with([
                'error' => true,
                'error.message' => "Sorry, you don't have permission",
                'error.title' => 'Ooops !'
            ]);
        }else{
            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

            return view('BinManage::add')->with(['extra_bins' => ExtraBin::where('status', 1)->get(), 'store' => $store, 'zone' => $logged_in_user->zone['name']]);

        }

    }

    function addRouteBin()
    {
        $logged_in_user = Sentinel::getUser();

        $stores = $logged_in_user->zone->stores;
        $routings = $logged_in_user->zone->routings;

        return view('BinManage::route-bin-add')->with(['stores' => $stores, 'routings' => $routings]);
    }

    function store(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        if ($logged_in_user->hasAccess('admin')){
            return redirect('bin/list')->with([
                'error' => true,
                'error.message' => 'Sorry, you dont have permission',
                'error.title' => 'Ooops !'
            ]);
        }else {
            $bin = Bin::create([
                'store_id' => $request->store,
                'related_id' => $request->extra_bin,
                'bin_type' => 'extra_bin',
                'status' => 1
            ]);

            if ($bin) {
                return redirect('bin/add')->with([
                    'success' => true,
                    'success.message' => 'Bin Created successfully!',
                    'success.title' => 'Well Done!'
                ]);
            }

            return redirect('bin/add')->with([
                'error' => true,
                'error.message' => 'Bin did not created ',
                'error.title' => 'Ooops !'
            ]);
        }
    }

    function storeRouteBin(Request $request)
    {
        //        dd($request->all());
        $bin = Bin::create([
            'store_id' => $request->store,
            'related_id' => $request->routing,
            'bin_type' => 'route',
            'status' => 1
        ]);

        if ($bin) {
            return redirect('bin/add/route-bin')->with([
                'success' => true,
                'success.message' => 'Route Bin Created successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('bin/add/route-bin')->with([
            'error' => true,
            'error.message' => 'Route Bin did not created ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function transferList()
    {
        $logged_in_user = Sentinel::getUser();

        $zone = $logged_in_user->zone;

        if ($logged_in_user->zone) {
            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
            $stores = Store::where('related_id', '!=', $logged_in_user->zone['id'])->where('store_type', 'zone')->get()->lists('id');
            $source_bins = Bin::where('store_id', $store['id'])->where('bin_type', 'zone')->where('status', 1)->get();
            $desti_bins = Bin::distinct('store_id')->whereIn('store_id', $stores)->where('related_id', '!=', $store['related_id'])->where('bin_type', 'zone')
                            ->where('status', 1)->get();
            $extra_desti_bins = Bin::where('store_id', $store['id'])->where('bin_type', 'extra_bin')->where('status', 1)->get();
            $route_bins = Bin::where('store_id', $store['id'])->where('bin_type', 'route')->where('status', 1)->get();
            // dd($desti_bins);

            foreach ($route_bins as $key => $route_bin) {
                $store = Store::find($route_bin['store_id']);
                $f_route_bins[$key]['id'] = $route_bin['id'];
                $f_route_bins[$key]['name'] = Routing::where('id', $route_bin['related_id'])->first()['name'];
                $f_route_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
            }

            foreach ($source_bins as $key => $source_bin) {
                if (Zone::where('id', $source_bin['related_id'])->first()['name'] !== null) {
                    $store = Store::find($source_bin['store_id']);
                    $f_source_bins[$key]['id'] = $source_bin['id'];
                    $f_source_bins[$key]['name'] = Zone::where('id', $source_bin['related_id'])->first()['name'];
                    $f_source_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                }
            }

            foreach ($desti_bins as $key => $desti_bin) {
                if (Zone::where('id', $desti_bin['related_id'])->first()['name'] !== null) {
                    $store = Store::find($desti_bin['store_id']);
                    $f_desti_bins[$key]['id'] = $desti_bin['id'];
                    $f_desti_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                    $f_desti_bins[$key]['bin_name'] = Zone::where('id', $desti_bin['related_id'])->first()['name'];
                }
            }

            foreach ($extra_desti_bins as $key => $extra_desti_bin) {
                $f_exdesti_bins[$key]['id'] = $extra_desti_bin['id'];
                $store = Store::find($extra_desti_bin['store_id']);
                $f_exdesti_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                $f_exdesti_bins[$key]['bin_name'] = ExtraBin::where('id', $extra_desti_bin['related_id'])->first()['name'];
            }

            // dd($extra_desti_bins, $f_exdesti_bins);

            return view('BinManage::transfer')->with([
                'source_bins' => $f_source_bins,
                'desti_bins' => isset($f_desti_bins) ? $f_desti_bins : [],
                'f_exdesti_bins' => isset($f_exdesti_bins) ? $f_exdesti_bins : [],
                'route_bins' => isset($f_route_bins) ? $f_route_bins : []
            ]);
        }

        if ($logged_in_user->inRole('admin')) {
            $store = Store::where('store_type', 'zone')->get()->lists('id');
            $stores = Store::where('store_type', 'zone')->get()->lists('id');
            $source_bins = Bin::whereIn('store_id', $store)->where('bin_type', 'zone')->where('status', 1)->get();
            $desti_bins = Bin::distinct('store_id')->whereIn('store_id', $stores)->where('bin_type', 'zone')->where('status', 1)->get();
            $extra_desti_bins = Bin::where('bin_type', 'extra_bin')->where('status', 1)->get();
            $route_bins = Bin::where('bin_type', 'route')->where('status', 1)->get();
            // dd($desti_bins);

            foreach ($route_bins as $key => $route_bin) {
                $store = Store::find($route_bin['store_id']);
                $f_route_bins[$key]['id'] = $route_bin['id'];
                $f_route_bins[$key]['name'] = Routing::where('id', $route_bin['related_id'])->first()['name'];
                $f_route_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
            }

            foreach ($source_bins as $key => $source_bin) {
                if (Zone::where('id', $source_bin['related_id'])->first()['name'] !== null) {
                    $store = Store::find($source_bin['store_id']);
                    $f_source_bins[$key]['id'] = $source_bin['id'];
                    $f_source_bins[$key]['name'] = Zone::where('id', $source_bin['related_id'])->first()['name'];
                    $f_source_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                }
            }

            foreach ($desti_bins as $key => $desti_bin) {
                if (Zone::where('id', $desti_bin['related_id'])->first()['name'] !== null) {
                    $store = Store::find($desti_bin['store_id']);
                    $f_desti_bins[$key]['id'] = $desti_bin['id'];
                    $f_desti_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                    $f_desti_bins[$key]['bin_name'] = Zone::where('id', $desti_bin['related_id'])->first()['name'];
                }
            }

            foreach ($extra_desti_bins as $key => $extra_desti_bin) {
                $f_exdesti_bins[$key]['id'] = $extra_desti_bin['id'];
                $store = Store::find($extra_desti_bin['store_id']);
                $f_exdesti_bins[$key]['zone'] = Zone::where('id', $store['related_id'])->first()['name'];
                $f_exdesti_bins[$key]['bin_name'] = ExtraBin::where('id', $extra_desti_bin['related_id'])->first()['name'];
            }

            // dd($extra_desti_bins, $f_exdesti_bins);

            return view('BinManage::transfer')->with([
                'source_bins' => $f_source_bins,
                'desti_bins' => isset($f_desti_bins) ? $f_desti_bins : [],
                'f_exdesti_bins' => isset($f_exdesti_bins) ? $f_exdesti_bins : [],
                'route_bins' => isset($f_route_bins) ? $f_route_bins : []
            ]);
        }

        return redirect('/')->with([
            'warning' => true,
            'warning.message' => 'Zone manager must have a valid zone to access this page!',
            'warning.title' => 'Invalid!'
        ]);
    }

    public function jsonTransferList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }
        $jsonList = array();
        $i = 1;

        if ($request->id == 'null') {
            return Response::json(array('data' => $jsonList));
        } else {
            $bin = Bin::find($request->id);

            if ($bin) {
                $stocks = Stock::where('store_id', $bin->store_id)->where('bin_id', $bin->id)->get()->pluck('request_id')->toArray();
                // dd($stocks);
//                foreach ($stocks as $stock) {

                    $c_requests = CustomerRequest::whereIn('id', $stocks)->where('status', 3);
                    if ($isCentralWarehouse) {
                        $c_requests = $c_requests->where(function ($q){
                            $q->where(function ($q){
                                $q->where('pickup_type', 1)->where('delivery_type', 2);
                            })->orWhere(function ($q){
                                $q->where('pickup_type', 1)->where('delivery_type', 1);
                            });
                        });
                    } else {
                        $c_requests = $c_requests->where(function ($q){
                            $q->where(function ($q){
                                $q->where('pickup_type', 2)->where('delivery_type', 1);
                            })->orWhere(function ($q){
                                $q->where('pickup_type', 2)->where('delivery_type', 2);
                            })->orWhere(function ($q){
                                $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                            });
                        });
                    }
                if (!empty($request->input('search.value'))) {
                    $search = $request->input('search.value');
                    $c_requests = $c_requests->where(function ($or) use ($search) {
                        $or->where(function ($query1) use ($search) {
                            $query1->whereHas('customer', function ($query1) use ($search) {
                                $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                                    ->orWhere('customers.last_name', 'LIKE', "%{$search}%");
                            });
                        })
                            ->orWhere('order_id', $search)
                            ->orWhere('merchant_order_id', $search)
                            ->orWhere('buyer_name', 'LIKE', "%{$search}%")
                            ->orWhere(function ($q) use ($search) {
                                $q->whereHas('buyerCity', function ($q) use ($search) {
                                    $q->where('name', 'LIKE', "%{$search}%");
                                });
                            })
                            ->orWhere(function ($q) use ($search) {
                                $q->whereHas('pickupCity', function ($q) use ($search) {
                                    $q->where('name', 'LIKE', "%{$search}%");
                                });
                            });
                    });
                }
                    $c_requests = $c_requests->get();

                foreach ($c_requests as $c_request) {
                    if ($c_request) {
                        $dd = array();

                        $stock = Stock::where('request_id', $c_request->id)->first();

                        $Customer = Customer::where('id', $c_request->customer_id)->first();

                        array_push($dd, '<td><input class="check" type="checkbox" value="' . $stock->id . '"></td>');
                        array_push($dd, $c_request['order_id']);
                        array_push($dd, $c_request['merchant_order_id']);
                        array_push($dd, $Customer['first_name']);
                        array_push($dd, $c_request['buyer_name']);
                        array_push($dd, $c_request['pickup_address']);
                        array_push($dd, $Customer['mobile']);
                        array_push($dd, $c_request->pickupCity->name);
                        array_push($dd, $c_request->buyerCity->name);
                        $action = '<button type="button" class="btn btn-info transfer" data-id="' . $stock->id . '" title="Transfer">Transfer</button>';
//                    if ($logged_in_user->inRole('admin')) {
//                        $action = '-';
//                    }
                        array_push($dd, $action);

                        array_push($jsonList, $dd);
                        $i++;
                    }
                }
//                }
            }
            return Response::json(array('data' => $jsonList));
        }
    }

    public function TransferRequest(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

//        Stock::find($request->stock_id)->update([
//            'bin_id' => $request->destination_bin
//        ]);

        try {

            \DB::beginTransaction();

            $stock = Stock::find($request->stock_id);
            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

            $stock->store_id = $store->id;
            $stock->bin_id = $request->destination_bin;
            $stock->availability = "in";
            $stock->save();

            StockTransaction::create([
                'request_id' => Stock::find($request->stock_id)->request_id,
                'store_id' => $store->id,
                'bin_id' => $request->source_bin,
                'user_id' => $logged_in_user->id,
                'transaction_type' => "out",
                'type' => 'bin',
            ]);

            StockTransaction::create([
                'request_id' => Stock::find($request->stock_id)->request_id,
                'store_id' => $store->id,
                'bin_id' => $request->destination_bin,
                'user_id' => $logged_in_user->id,
                'transaction_type' => "in",
                'type' => 'bin',
            ]);

            \DB::commit();

            return response('Success');

        } catch (\PDOException $exception) {
            \Log::error("BIN TRANSFER ERROR = " . $exception->getMessage());
            \DB::rollBack();
            return response('Something went wrong');
        }
    }

    public function TransferRequests(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        foreach ($request->stock_ids as $stock_id) {
//            Stock::find($stock_id)->update([
//                'bin_id' => $request->destination_bin
//            ]);
//
//            StockTransaction::create([
//                'request_id' => Stock::find($stock_id)->request_id,
//                'store_id' => Stock::find($stock_id)->store_id,
//                'bin_id' => Stock::find($stock_id)->bin_id,
//                'user_id' => $logged_in_user->id,
//                'transaction_type' => Stock::find($stock_id)->availability,
//                'type' => 'bin',
//            ]);

            try {

                \DB::beginTransaction();

                $stock = Stock::find($stock_id);
                $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

                $stock->store_id = $store->id;
                $stock->bin_id = $request->destination_bin;
                $stock->availability = "in";
                $stock->save();

                StockTransaction::create([
                    'request_id' => Stock::find($stock_id)->request_id,
                    'store_id' => $store->id,
                    'bin_id' => $request->source_bin,
                    'user_id' => $logged_in_user->id,
                    'transaction_type' => "out",
                    'type' => 'bin',
                ]);

                StockTransaction::create([
                    'request_id' => Stock::find($stock_id)->request_id,
                    'store_id' => $store->id,
                    'bin_id' => $request->destination_bin,
                    'user_id' => $logged_in_user->id,
                    'transaction_type' => "in",
                    'type' => 'bin',
                ]);

                \DB::commit();

            } catch (\PDOException $exception) {
                \Log::error("BIN TRANSFER ERROR = " . $exception->getMessage());
                \DB::rollBack();
                return response('Something went wrong');
            }

        }

        return response('Success');
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {

            $logged_in_user = Sentinel::getUser();

            $Bin = Bin::find($request->id);
            if ($Bin) {
                try{

                    \DB::beginTransaction();

                    if ($logged_in_user->inRole('zone-manager')) {
                        $store = Store::where('store_type', 'zone')->where('related_id', $logged_in_user->zone['id'])->first();
                    } else {
                        $store = Store::where('store_type', 'zone')->where('related_id', $request->store_id)->first();
                    }

                    // Vehicle Bin Activation
                    Bin::where('bin_type', 'extra_bin')->where('store_id', $store->id)
                        ->where('related_id', $Bin->extraBin->id)->update([
                            'status' => 1
                        ]);

                    $Bin->update([
                        'status' => 1
                    ]);

                    \DB::commit();

                } catch (\PDOException $exception) {
                    \Log::error("BIN ACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response('Something went wrong');
                }
            }

//            $ExtraBin = ExtraBin::find($request->id);
//            if ($ExtraBin) {
//                $ExtraBin->update([
//                    'status' => 1
//                ]);
//                return response()->json('success');
//            }
//            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function deactivate(Request $request)
    {

        $logged_in_user = Sentinel::getUser();

        if ($request->ajax()) {

            $Bin = Bin::find($request->id);
            if ($Bin) {
                try{

                    \DB::beginTransaction();

                    if ($logged_in_user->inRole('zone-manager')) {
                        $store = Store::where('store_type', 'zone')->where('related_id', $logged_in_user->zone['id'])->first();
                    } else {
                        $store = Store::where('store_type', 'zone')->where('related_id', $request->store_id)->first();
                    }
//                    $bins = Bin::where('store_id', $store->id)->where('bin_type', 'extra_bin')->get()->pluck('id')->toArray();
                    $stock = Stock::where('bin_id', $request->id)->where('availability', 'in')->count();
                    if ($stock > 0) {
                        return response()->json('Please clear the extra bins to deactivate the bin');
                    }

                    // Vehicle Bin Deactivation
                    Bin::where('bin_type', 'extra_bin')->where('store_id', $store->id)
                        ->where('related_id', $Bin->extraBin->id)->update([
                            'status' => 0
                        ]);

                    $Bin->update([
                        'status' => 0
                    ]);

                    \DB::commit();

                    return response('success');

                } catch (\PDOException $exception) {
                    \Log::error("BIN DEACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response('Something went wrong');
                }
            }

//            $ExtraBin = ExtraBin::find($request->id);
//            $Bin = Bin::where("related_id", $request->id)->where("bin_type", "extra_bin")->where("store_id", $request->store_id)->first();
//            if($Bin !== null){
//                $Stock = Stock::where('bin_id', $Bin['id'])->first();
//            }
//
//            if ($Stock == null) {
//                $ExtraBin->update([
//                    'status' => 0
//                ]);
//                return response('success');
//            }
//            else{
//                return response('warning');
//            }
            return response('error');
        }
        return response()->json('invalid');
    }

    function checkAvailability(Request $request){
        try {
            return response()->json(0 == (
                Bin::whereStoreId($request->store)
                    ->whereRelatedId($request->extra_bin)
                    ->whereBinType('extra_bin')
                    ->count()
                ));
        } catch (\Exception $exception) {
            $exceptionId = rand(0, 99999999);
            Log::error("Ex " . $exceptionId . " | Error in " . __CLASS__ . " ->" . __FUNCTION__ . " " . $exception->getMessage(), $request->all());
            return response()->json(false);
        }
    }



    private function getProductsCount($binId, $storeId)
    {
        return \StockManage\Models\Stock::where('bin_id', $binId)->where('store_id', $storeId)->count();
    }
}
