<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'BinManage\Http\Controllers', 'prefix' => 'bin'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'bin.list', 'uses' => 'BinController@index'
        ]);

        Route::get('list/route-bin', [
            'as' => 'route-bin.list', 'uses' => 'BinController@indexRouteBinList'
        ]);

        Route::get('json/list', [
            'as' => 'bin.list', 'uses' => 'BinController@jsonList'
        ]);

        Route::get('json/list/route-bin', [
            'as' => 'route-bin.list', 'uses' => 'BinController@jsonRouteBinList'
        ]);

        Route::get('json/transfer/list', [
            'as' => 'bin-transfer.list', 'uses' => 'BinController@jsonTransferList'
        ]);

        Route::get('transfer/list', [
            'as' => 'bin-transfer.list', 'uses' => 'BinController@TransferList'
        ]);

        Route::get('add', [
            'as' => 'bin.add', 'uses' => 'BinController@add'
        ]);

        Route::get('add/route-bin', [
            'as' => 'route-bin.add', 'uses' => 'BinController@addRouteBin'
        ]);

        Route::get('{id}/edit', [
            'as' => 'bin.edit', 'uses' => 'BinController@edit'
        ]);


        /**
         * POST Routes
         */
        Route::post('store', [
            'as' => 'bin.store', 'uses' => 'BinController@store'
        ]);

        Route::post('store/route-bin', [
            'as' => 'route-bin.store', 'uses' => 'BinController@storeRouteBin'
        ]);

        Route::post('transfer/request', [
            'as' => 'bin-transfer.transfer', 'uses' => 'BinController@TransferRequest'
        ]);

        Route::post('transfer/requests', [
            'as' => 'bin-transfer.transfer', 'uses' => 'BinController@TransferRequests'
        ]);

        Route::post('update', [
            'as' => 'bin.update', 'uses' => 'BinController@update'
        ]);
        
        Route::post('activate', [
            'as' => 'bin.update', 'uses' => 'BinController@activate'
        ]);
        
        Route::post('deactivate', [
            'as' => 'bin.update', 'uses' => 'BinController@deactivate'
        ]);

        Route::post('delete', [
            'as' => 'bin.delete', 'uses' => 'BinController@delete'
        ]);
        Route::post('check/availability', [
            'as' => 'bin.check.availability', 'uses' => 'BinController@checkAvailability'
        ]);
    });
});


