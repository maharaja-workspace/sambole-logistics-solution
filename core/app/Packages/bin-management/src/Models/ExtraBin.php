<?php


namespace BinManage\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraBin extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'extra_bins';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['name', 'description', 'status'];
}