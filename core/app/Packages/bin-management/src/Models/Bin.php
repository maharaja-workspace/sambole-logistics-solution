<?php


namespace BinManage\Models;

use Illuminate\Database\Eloquent\Model;
use StockManage\Models\Stock;
use StoreManage\Models\Store;

class Bin extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bins';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['store_id', 'related_id', 'bin_type', 'status'];

    public $timestamps = false;

    function store(){
        return $this->belongsTo(Store::class, 'store_id');
    }

    function getProducts(){
        Stock::whereBinId($this->id)->whereStoreId($this->store_id);
    }

    function extraBin()
    {
        return $this->belongsTo(ExtraBin::class, "related_id");
    }
}