<?php

namespace RouteManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use CityManage\Models\City;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Response;
use RouteManage\Models\Routing;
use RoutingManage\Models\RoutingCity;
use Sentinel;
use DB;
use StockManage\Models\Stock;
use StoreManage\Models\Store;
use Yajra\Datatables\Datatables;
use function foo\func;


class RouteController extends Controller
{

    public function index()
    {
        $logged_in_user = Sentinel::getUser();

        $zone = $logged_in_user->zone;

        $stores = Store::where('related_id', $zone['id'])->where('store_type', 'zone')->get();

        return view('RouteManage::list')->with(['stores' => $stores]);
    }

    public function listData(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'code',
            3 => 'cities',
            4 => 'pickup_truck_city.name',
            5 => 'no_of_products',
            6 => 'zone.name',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        $queryBuild = Routing::query();
        foreach ($request->get('columns') as $column){
            if ($column['name'] == "cities" && $column['search']['value'] != ""){
                $queryBuild = Routing::whereHas('cities', function ($q) use ($column){
                    $q->where("name", "LIKE", '%'.$column['search']['value'].'%');
                });
                break;
            }
            if ($column['name'] == "zone" && $column['search']['value'] != ""){
                $queryBuild = Routing::whereHas('zone', function ($q) use ($column){
                    $q->where("name", "LIKE", '%'.$column['search']['value'].'%');
                });
                break;
            }
            if ($column['name'] == "route_id" && $column['search']['value'] != ""){
                $queryBuild = Routing::where(function ($q) use ($column){
                    $q->where("id", "LIKE", '%'.$column['search']['value'].'%');
                });
                break;
            }
            if ($column['name'] == "route_name" && $column['search']['value'] != ""){
                $queryBuild = Routing::where(function ($q) use ($column){
                    $q->where("name", "LIKE", '%'.$column['search']['value'].'%');
                });
                break;
            }
            if ($column['name'] == "pickup_location" && $column['search']['value'] != ""){
                $queryBuild = Routing::whereHas('pickupTruckCity', function ($q) use ($column){
                    $q->where("name", "LIKE", '%'.$column['search']['value'].'%');
                });
                break;
            }
        }
        $queryBuild = $queryBuild->with('cities', 'pickupTruckCity');
        $logged_in_user = Sentinel::getUser();

        if($logged_in_user->inRole('zone-manager')){
            $queryBuild = $queryBuild->where('zone_id', $logged_in_user->zone['id']);
        }

        $totalData = $queryBuild->count();

        if ($limit != -1) {
            $filteredDatas = $queryBuild->offset($start)
                ->limit($limit);
        }
        if ($orderNo != 2 && $orderNo != 4) {
            $filteredDatas = $filteredDatas->orderBy($order, $dir);
        }

        $filteredDatas = $filteredDatas->get();

        if($request->input('order.0.column') == 2){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->zone['name'];
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->zone['name'];
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 4){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return isset($filteredData->pickupTruckCity) ? $filteredData->pickupTruckCity->name : null;
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return isset($filteredData->pickupTruckCity) ? $filteredData->pickupTruckCity->name : null;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        $totalFilteredData = $totalData;

        $permissions = Permission::whereIn('name', ['route.edit', 'admin', 'zone_manager'])->where('status', '=', 1)->lists('name');

        $data = array();

        foreach ($filteredDatas as $filteredData) {

            $dd = array();
            $cities = "";
            foreach ($filteredData->cities as $city) {
                if ($cities == "") {
                    $cities = $city->name;
                } else {
                    $cities .= ", " . $city->name;
                }
            }
            array_push($dd, $filteredData->id);
            array_push($dd, $filteredData->name);
            if($logged_in_user->inRole('admin')){
                array_push($dd, $filteredData->zone->name);
            }
            array_push($dd, $cities);
            array_push($dd, isset($filteredData->pickupTruckCity) ? $filteredData->pickupTruckCity->name : "-");
            array_push($dd, $filteredData->no_of_products);
            if($logged_in_user->inRole('admin')){
                if($filteredData->status == 1) {
                    $status = "Active";
                } else {
                    $status = "Inactive";
                }
                array_push($dd, $status);
            }
            if($logged_in_user->inRole('zone-manager')) {
                $edit = "-";
                if (Sentinel::hasAnyAccess($permissions)) {
                    $edit = '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('route/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Route"><i class="fa fa-pencil"></i></a></center>';
                }
                array_push($dd, $edit);
                $activities = "-";
                if (Sentinel::hasAnyAccess($permissions)) {
                    if ($filteredData->status == 1) {
                        $activities = '<center><a href="#" class="blue route-deactivate action-items" data-id="' . $filteredData->id . '" data-toggle="tooltip" data-placement="top" title="Deactivate Route"><i class="fa fa-toggle-on"></i></a></center>';
                    } else if ($filteredData->status == 0) {
                        $activities = '<center><a href="#" class="blue route-activate action-items" data-id="' . $filteredData->id . '" data-toggle="tooltip" data-placement="top" title="Activate Route"><i class="fa fa-toggle-off"></i></a></center>';
                    }
                }
                array_push($dd, $activities);
            }

            array_push($data, $dd);

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFilteredData),
            "data" => $data,
        );

        echo json_encode($json_data);

//        return Datatables::of($queryBuild->get())
//            ->editColumn('cities', function ($filteredData){
//                $cities = "";
//                foreach ($filteredData->cities as $city) {
//                    if ($cities == "") {
//                        $cities = $city->name;
//                    } else {
//                        $cities .= ", " . $city->name;
//                    }
//                }
//
//                return $cities;
//            })
//            ->addColumn("edit", function ($filteredData) use($permissions){
//                if(Sentinel::hasAnyAccess($permissions)){
//                    return '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('route/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Route"><i class="fa fa-pencil"></i></a></center>';
//                }
//            })
//            ->addColumn("activities", function ($filteredData) use($permissions){
//                if(Sentinel::hasAnyAccess($permissions)){
//                    if($filteredData->status == 1){
//                        return '<center><a href="#" class="blue route-deactivate action-items" data-id="' . $filteredData->id . '" data-toggle="tooltip" data-placement="top" title="Deactivate Route"><i class="fa fa-toggle-on"></i></a></center>';
//                    }
//                    else if($filteredData->status == 0){
//                        return  '<center><a href="#" class="blue route-activate action-items" data-id="' . $filteredData->id . '" data-toggle="tooltip" data-placement="top" title="Activate Route"><i class="fa fa-toggle-off"></i></a></center>';
//                    }
//                }
//            })
//            ->make(true);

    }


    function add()
    {
        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;
        $cities = City::whereHas('zones', function ($q) use ($zone){
            $q->where('id', $zone->id);
        })->get();

        return view('RouteManage::add')->with(['zone' => $zone, 'cities' => $cities, 'routes' => Routing::all()->count()]);
    }

    function store(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

        if($store){
            $routing = Routing::create([
                'zone_id' => $request->zone,
                'pickup_truck_city_id' => $request->has('truck_location') ? $request->truck_location : null,
                'name' => $request->name,
                'code' => $request->code,
                'latitude' => $request->lat,
                'longitude' => $request->long,
            ]);

            $routing->cities()->sync($request->cities);

            // $store = Store::create([
            //     'related_id' => $routing->zone_id,
            //     'store_type' => 'route',
            //     'status' => 1
            // ]);
            
            Bin::create([
                'store_id' => $store->id,
                'related_id' => $routing->id,
                'bin_type' => 'route',
                'status' => 1
            ]);

            if ($routing) {
                return redirect('route/add')->with([
                    'success' => true,
                    'success.message' => 'Route Created successfully!',
                    'success.title' => 'Well Done!'
                ]);
            }

            return redirect('route/add')->with([
                'error' => true,
                'error.message' => 'Route did not created ',
                'error.title' => 'Ooops !'
            ]);
        }

        return redirect('route/add')->with([
            'error' => true,
            'error.message' => 'Store data not found. Route did not created.',
            'error.title' => 'Ooops !'
        ]);
    }

    public function edit($id)
    {

        $routing = Routing::find($id);

        if (!$routing) {
            return redirect('route/list')->with([
                'error' => true,
                'error.message' => 'Route did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;
        $cities = City::whereHas('zones', function ($q) use ($zone){
            $q->where('id', $zone->id);
        })->get();
        $routeCities = \DB::table('route_city')->where('route_id', $routing->id)->lists('city_id');

        return view('RouteManage::edit')->with(['zone' => $zone, 'cities' => $cities, 'routes' => Routing::all()->count(),
                                                     'routing' => $routing, 'route_cities' => $routeCities]);
    }

    public function update(Request $request)
    {
        $routing = Routing::find($request->id);

        if (!$routing) {
            return redirect('route/list')->with([
                'error' => true,
                'error.message' => 'Route did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $routing->name = $request->name;
        $routing->code = $request->code;
        $routing->zone_id = $request->zone;
        if ($request->has('truck') && $request->has('truck_location')) {
            $routing->pickup_truck_city_id = $request->truck_location;
        }else{
            $routing->pickup_truck_city_id = null;
        }
        $routing->latitude = $request->lat;
        $routing->longitude = $request->long;

        $routing->cities()->sync($request->cities);

        if ($routing->save()) {
            return redirect('route/list')->with([
                'success' => true,
                'success.message' => 'Route updated successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('route/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Route did not updated ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $routing = Routing::find($request->id);
            if ($routing) {
                $routing->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    public function activate(Request $request)
    {
        if ($request->ajax()) {
            $route = Routing::find($request->id);
            if ($route) {
                try {

                    \DB::beginTransaction();


                    Bin::where('bin_type', 'route')->where('related_id', $request->id)->update([
                        'status' => 1
                    ]);

                    $route->update([
                        'status' => 1
                    ]);


                    \DB::commit();

                    return response()->json('success');

                } catch (\PDOException $exception) {
                    \Log::error("ROUTE ACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response()->json('Something went wrong');
                }
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }


    public function deactivate(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        if ($request->ajax()) {

            $route = Routing::find($request->id);
            if ($route) {

                try {

                    \DB::beginTransaction();

                    $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
                    if ($store){
                        $bins = Bin::where('store_id', $store->id)->where('bin_type', 'route')->get()->pluck('id')->toArray();
                        $stock = Stock::whereIn('bin_id', $bins)->where('availability', 'in')->count();
                        if ($stock > 0) {
                            return response()->json('Please clear the route bins to deactivate the route');
                        }

                        Bin::where('bin_type', 'route')->where('related_id', $request->id)->update([
                            'status' => 0
                        ]);
                    }

                    $route->update([
                        'status' => 0
                    ]);


                    \DB::commit();

                    return response()->json('success');

                } catch (\PDOException $exception) {
                    \Log::error("ROUTE DEACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response()->json('Something went wrong');
                }
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
