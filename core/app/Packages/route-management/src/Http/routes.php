<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'RouteManage\Http\Controllers', 'prefix' => 'route'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'route.list', 'uses' => 'RouteController@index'
        ]);


        Route::get('add', [
            'as' => 'route.add', 'uses' => 'RouteController@add'
        ]);

        Route::get('{id}/edit', [
            'as' => 'route.edit', 'uses' => 'RouteController@edit'
        ]);


        /**
         * POST Routes
         */

        Route::post('list/data', [
            'as' => 'route.list', 'uses' => 'RouteController@listData'
        ]);

        Route::post('store', [
            'as' => 'route.store', 'uses' => 'RouteController@store'
        ]);

        Route::post('update', [
            'as' => 'route.update', 'uses' => 'RouteController@update'
        ]);

        Route::post('delete', [
            'as' => 'route.delete', 'uses' => 'RouteController@delete'
        ]);

        Route::post('activate', [
            'as' => 'route.update', 'uses' => 'RouteController@activate'
        ]);
        
        Route::post('deactivate', [
            'as' => 'route.update', 'uses' => 'RouteController@deactivate'
        ]);
    });
});


