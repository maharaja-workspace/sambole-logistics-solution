<?php
namespace RoutingManage\Models;

use CityManage\Models\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoutingCity extends Model{
    use SoftDeletes;

    public $incrementing = false;

    protected $table = 'route_city';
    protected $fillable = ['route_id', 'city_id'];

    function city(){
        return $this->belongsTo(City::class, 'city_id');
    }
}