<?php


namespace RouteManage\Models;

use BinManage\Models\Bin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use StockManage\Models\Stock;
use ZoneManage\Models\Zone;

class Routing extends Model
{

    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'routes';

    protected $appends = ['no_of_products'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['zone_id', 'pickup_truck_city_id', 'name', 'code', 'latitude', 'longitude', 'status'];

    public function cities()
    {
        return $this->belongsToMany( 'CityManage\Models\City', 'route_city', 'route_id', 'city_id' )->withTimestamps();
    }

    function pickupTruckCity(){
        return $this->belongsTo( 'CityManage\Models\City', 'pickup_truck_city_id' );

    }

    function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id');
    }

    function bin()
    {
        return $this->hasOne(Bin::class, 'related_id', 'id')->where('bin_type', 'route')->first();
    }

    function stockCount()
    {
        $bin = $this->bin();
        if ($bin) {
            return Stock::where('bin_id', $this->bin()->id)->count();
        }
        return 0;
    }

    public  function getNoOfProductsAttribute()
    {
        return $this->stockCount();
    }

}