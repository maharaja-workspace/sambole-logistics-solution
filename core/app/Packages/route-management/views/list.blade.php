@extends('layouts.back.master') @section('current_title','All Route')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
              width: 45px;
              padding-left:0px;
            }*/

        /* tr:hover .fixed_float{
              width: 50px;
              padding:  5px 0 0 0px;
            } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }

        .dataTables_filter, .dataTables_info { display: none; }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Route Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Route List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop

@section('content')
    @if($user->hasAnyAccess(['route.add', 'admin']))
        <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
             onclick="location.href = '{{route('route.add')}}';">
            <p class="plus">+</p>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12 margins">

            <input type="hidden" value="{{Sentinel::getUser()->inRole('admin') ? 'admin' : 'manager'}}" id="loggedUser">

            <div>
                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>Route ID</th>
                        <th>Route Name</th>
                        @if(Sentinel::getUser()->inRole('admin'))
                            <th>Zone Name</th>
                        @endif
                        <th data-search="disable">Cities</th>
                        <th>Pick-up Location</th>
                        <th data-search="disable">No of Products</th>
                        @if(Sentinel::getUser()->inRole('admin'))
                            <th data-search="disable">Status</th>
                        @endif
                        @if(Sentinel::getUser()->inRole('zone-manager'))
                            <th data-search="disable" width="1%">Edit</th>
                            <th data-search="disable" width="1%">Activities</th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#example1 thead th').each( function () {
                if($(this).data('search') == "disable"){

                }else{

                    var title = $(this).text();
                    $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
                }
            } );

            $.fn.dataTable.ext.errMode = 'none';



            table = $('#example1').DataTable({
                "searching": true,
                "autoWidth": false,
                // "order": [[1, "asc"]],
                "processing": true,
                "serverSide": true,
                "responsive": true,
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "buttons": [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Route List', className: 'btn-sm', exportOptions: {
                            columns: exportCols()
                        }
                    },
                    {extend: 'pdf', title: 'Route List', className: 'btn-sm', exportOptions: {
                            columns: exportCols()
                        }
                    },
                    {extend: 'print', className: 'btn-sm', exportOptions: {
                            columns: exportCols()
                        }
                    }
                ],

                "ajax":{
                    "url": "{{ url('route/list/data') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columnDefs": [
                    {"className": "dt-center", "targets": [-1, -2]},
                    { "searchable": false, "targets": [-1, -2, -4] },
                    { "orderable": false, "targets": [-1, -2, -4] }
                ],
                "columns" : cols()
            });

            function cols() {
                let logged = $('#loggedUser').val();
                if (logged == "admin") {
                    return [
                        {"width" : "8%", "name" : "route_id"},
                        {"name" : "route_name"},
                        {"name" : "zone"},
                        null,
                        {"width" : "10%", "name" : "pickup_location"},
                        {"width" : "8%"},
                        {"width" : "10%"}
                    ];
                } else {
                    return [
                        {"width" : "8%", "name" : "route_id"},
                        {"name" : "route_name"},
                        null,
                        {"width" : "10%", "name" : "pickup_location"},
                        {"width" : "8%"},
                        null,
                        null
                    ];
                }

            }

            function exportCols()
            {
                let logged = $('#loggedUser').val();
                if (logged == "admin") {
                    return [
                        0,1,2,3,4,5,6
                    ];
                } else {
                    return [
                        0,1,2,3,4
                    ];
                }
            }

            table.on( 'error.dt', function ( e, settings, techNote, message ) {
                console.log( 'An error has been reported by DataTables: ', message );
            } );

            table.columns().every( function () {
                var that = this;

                $('input', this.header()).on('keyup change clear click', function (e) {
                    e.stopPropagation();
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });

            table.on('draw.dt', function () {
                $('.route-delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAlert(id);
                });
            });

            function confirmAlert(id) {
                if (confirm("Are you sure ?")) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('route/delete')}}',
                        data: {'id': id, '_token': '{{ csrf_token() }}'}
                    })
                        .done(function (msg) {
                            table.ajax.reload();
                        });
                }
            }

            table.on('draw.dt', function() {
                $('.route-activate').click(function(e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    swal({
                        title: "Are you sure?",
                        text: "Change the status of the route?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Change it!"
                    }).then(function (isConfirm) {
                        if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                            $.ajax({
                                method: "POST",
                                url: '{{url("route/activate")}}',
                                data: {
                                    'id': id,
                                    '_token': '{{ csrf_token() }}'
                                }
                            })
                                .done(function(msg) {
                                    if (msg == "success") {
                                        table.ajax.reload();
                                        sweetAlert('Failed !!!', 'Successfully Activated.', 0);
                                    } else {
                                        sweetAlert('Failed !!!', msg, 0);
                                    }
                                });
                        }


                    });
                });

                $('.route-deactivate').click(function(e) {
                    e.preventDefault();
                    id = $(this).data('id');

                    swal({
                        title: "Are you sure?",
                        text: "Change the status of the route?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Change it!"
                    }).then(function (isConfirm) {
                        if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                            $.ajax({
                                method: "POST",
                                url: '{{url("route/deactivate")}}',
                                data: {
                                    'id': id,
                                    '_token': '{{ csrf_token() }}'
                                },
                                success: function(data){
                                }
                            })
                                .done(function(msg) {
                                    if (msg == "success") {
                                        table.ajax.reload();
                                        sweetAlert('Success !!!', 'Successfully Deactivated.', 0);
                                    } else {
                                        sweetAlert('Failed !!!', msg, 0);
                                    }
                                });
                        }


                    });

                });
            });
        });

    </script>


@stop