@extends('layouts.back.master') @section('current_title','NEW ROUTE')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <style type="text/css">
        .map-container-6 {
            overflow: hidden;
            padding-bottom: 5%;
            margin-bottom: -10%;
            position: relative;
            height: 0;
        }

        .map-container-6 iframe {
            left: 2%;
            top: 2%;
            height: 75%;
            width: 50%;
            position: absolute;
        }

        #truck_pickup, #location {
            display: none;
        }
        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 30%;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 40%;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }
    </style>
@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Route Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Route </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row m-b-xl">
        <div class="col-lg-12" style="padding-top: 20px;">
            {{-- <h2>Route Creation</h2> --}}

            <form method="POST" class="form-horizontal" id="form" action="{{ url('route/store') }}">
                {!!Form::token()!!}


                <div class="form-group"><label class="col-sm-2 control-label">Route Name <span
                                class="required">*</span></label>
                    <div class="col-sm-10"><input type="text" name="name" class="form-control"
                                                  value="{{old('name')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Route Code <span
                                class="required">*</span></label>
                    <div class="col-sm-10"><input readonly type="text" name="code" class="form-control"
                                                  value="{{old('code', '0000' . $routes + 1)}}"></div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Pick up Truck Available</label>
                    <div class="col-sm-10 text-left"><input type="checkbox" value="1" name="truck" id="truck"></div>
                </div>

                {!! Form::hidden('zone', $zone['id']) !!}

                <div class="form-group" id="truck_pickup"><label class="col-sm-2 control-label">Pick up Truck Location<span
                                class="required">*</span></label>
                    <div class="col-sm-10">
                        <select data-placeholder="Choose" id="pickup_location" class="js-source-states" style="width: 100%"
                                name="truck_location">
                            <option value="">Choose</option>
                            @foreach($cities as $city)
                                <option data-long="" value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">City Selection<span
                                class="required">*</span></label>
                    <div class="col-sm-10">
                        <select data-placeholder="Choose" class="js-source-states required" style="width: 100%"
                                name="cities[]" multiple="multiple" id="cities">
                            <option value="" disabled>Choose</option>
                            @if($zone)
                                @foreach($zone->cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group" id="location">
                    <label class="col-sm-2 control-label">Route Pick up Location<span
                                class="required">*</span></label>
                    <div class="col-sm-10">
                        <div class="row">
                            <div class="col-md-4"><input type="text" class="form-control" placeholder="Latitude"
                                                         readonly name="lat" id="lat_val"></div>
                            <div class="col-md-4"><input type="text" class="form-control" placeholder="Longitude"
                                                         readonly name="long" id="long_val"></div>
                        </div>
                        <br>
                        <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                        <div id="map" style="height: 400px;">

                        </div>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel
                        </button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')

    <script>
        $("#truck").on('click', function () {
            if (this.checked === true) {
                $("#truck_pickup").fadeIn("slow").css('display', 'block');
                $("#location").fadeIn("slow").css('display', 'block');
            } else {
                $("#truck_pickup").fadeOut("slow").css('display', 'none');
                $("#location").fadeOut("slow").css('display', 'none');
            }
        });
    </script>

    <script type="text/javascript">
        var map;



        function initMap() {
            var latitude = 6.926148085050818; // YOUR LATITUDE VALUE
            var longitude = 79.86179759261086; // YOUR LONGITUDE VALUE

            $('#lat_val').val(latitude);
            $('#long_val').val(longitude);

            var myLatLng = {lat: latitude, lng: longitude};

            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 14,
                disableDoubleClickZoom: true, // disable the default map zoom on double click
            });

            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });

                map.fitBounds(bounds);
                map.setZoom(14);
            });


            // Get current location
            infoWindow = new google.maps.InfoWindow;

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    // infoWindow.setPosition(pos);
                    // infoWindow.setContent('Location found.');
                    // infoWindow.open(map);
                    // map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: latitude + ', ' + longitude
            });

            map.addListener('click', function (event) {
                $('#lat_val').val(event.latLng.lat());
                $('#long_val').val(event.latLng.lng());
                marker.setPosition(event.latLng);
            });
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFszbpxFSqcixbAKDqlPvN_3iqerih00g&callback=initMap&libraries=places"
            type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");


            $("#form").validate({
                ignore: [],
                rules: {
                    name: {
                        required: true,
                        maxlength: 20
                    },
                    code: {
                        required: true,
                        lettersonly: false,
                        maxlength: 20
                    },
                    cities: {
                        required: true
                    },
                    truck_location: {
                        required: '#truck:checked'
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });

    </script>
@stop