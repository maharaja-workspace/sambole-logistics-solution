@extends('layouts.back.master') @section('current_title','All Vehicle Type')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

        /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Vehicle Type Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Vehicle Type List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop

@section('content')
    @if($user->hasAccess(['vehicle.type.add']))
        <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
             onclick="location.href = '{{url('vehicle-type/add')}}';">
            <p class="plus">+</p>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12" style="padding-top: 20px;">

            <div class="table-responsive">
                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th style="max-width: 40px;">#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Zone to Zone delivery?</th>
                        <th>Available for Zone?</th>
                        <th data-search="disable" style="width: 2%;">Edit</th>
                        <th data-search="disable" style="width: 2%;">Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#example1 thead th').each( function () {
                if($(this).data('search') == "disable"){

                }else{

                    var title = $(this).text();
                    $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
                }
            } );
            table = $('#example1').DataTable({
                "ajax": '{{url('vehicle-type/json/list/')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Vehicle Type List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        }
                    },
                    {extend: 'pdf', title: 'Vehicle Type List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        }
                    },
                    {extend: 'print', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        }
                    }
                ],
                "autoWidth": false,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"className": "dt-center", "targets": [-1, -2]},
                    { "orderable": false, "targets": [-1, -2] }
                ]

            });

            table.columns().every( function () {
                var that = this;

                $('input', this.header()).on('keyup change clear click', function (e) {
                    e.stopPropagation();
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });

            table.on('draw.dt', function () {
                $('.zone-delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAlert(id);

                });
                $('.vehicle-type-status').click(function (e) {
                    e.preventDefault();
                    let id = $(this).data('id');
                    let status = $(this).data('status');
                    statusAlert(id, status);

                });
            });

            function confirmAlert(id) {
                if (confirm("Are you sure ?")) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('vehicle-type/delete')}}',
                        data: {'id': id, '_token': '{{ csrf_token() }}'}
                    })
                        .done(function (msg) {
                            table.fnReloadAjax();
                        });
                }
            }

            function statusAlert(id, status) {

                swal({
                    title: "Are you sure?",
                    text: "Change the status of the vehicle type ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Change it!"
                }).then(function (isConfirm) {
                    if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                        $.ajax({
                            method: "POST",
                            url: '{{url('vehicle-type/status')}}',
                            data: {
                                'id': id,
                                'status':status,
                                '_token': '{{ csrf_token() }}'
                            }
                        })
                            .done(function (msg) {
                                toastr.success(msg);
                                table.ajax.reload();
                            });
                    }


                });

            }
        });
    </script>
@stop