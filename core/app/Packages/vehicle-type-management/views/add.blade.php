@extends('layouts.back.master') @section('current_title','NEW VEHICLE TYPE')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <style>
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            margin-top: 5%;
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a green background */
        .container input:checked ~ .checkmark {
            background-color: #18a689;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 7px;
            width: 7px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Vehicle Type Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Vehicle Type </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12" style="padding-top: 20px;">
            {{-- <h2>Vehicle Type Creation</h2> --}}

            <form method="post" class="form-horizontal" id="form" action="{{ url('vehicle-type/store') }}">
                {!!Form::token()!!}


                <div class="form-group">
                    <label class="col-sm-2 control-label">Name <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <input type="text" name="name" class="form-control" value="{{old('name')}}">
                        <div id="nameErrordiv"></div>
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><input type="text" name="description" class="form-control"
                                                 value="{{old('description')}}"></div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Vehicle Type <span class="required">*</span></label>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="form-check">
                        <input type="radio" name="type" class="form-check-input" id="zoneToZoneRadio" style="margin-left: 15px" value="{{old('is_bin', "is_bin")}}" required>
                        <label class="form-check-label" for="exampleRadios1" id="zoneToZoneLabel" style="margin-left: 10px">
                            Zone to Zone delivery
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="form-check">
                        <input type="radio" name="type" class="form-check-input" id="zoneVehicleRadio" style="margin-left: 15px" value="{{old('zone_access', "zone_access")}}" required>
                        <label class="form-check-label" for="exampleRadios1" id="zoneVehicleLabel" style="margin-left: 10px">
                            Zone Vehicle
                        </label>
                    </div>
                    <label class="col-sm-2 control-label"></label>
                    <div id="typeErrordiv"></div>
                </div>

{{--                <div class="form-group"><label class="col-sm-2 control-label">Zone to Zone delivery</label>--}}
{{--                        <div class="col-sm-3"><input type="radio" name="type" class="form-control checkmark"--}}
{{--                                                     value="{{old('is_bin', "is_bin")}}"></div>--}}
{{--                </div>--}}

{{--                <div class="form-group"><label class="col-sm-2 control-label">Zone Vehicle</label>--}}
{{--                        <div class="col-sm-3"><input type="radio" name="type" class="form-control checkmark"--}}
{{--                                                     value="{{old('zone_access', "zone_access")}}"></div>--}}
{{--                </div>--}}

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Done</button>
                        <button type="button" class="btn btn-danger" style="float: right;"
                                onclick="location.reload();">Cancel
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersNumbersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z0-9 _]*$/i.test(value);
            }, "The vehicle type name can only consist of letters, Numbers and underscore.");

            $('#zoneVehicleLabel').on('click', function () {
                $("#zoneToZoneRadio").prop("checked", false);
                $("#zoneVehicleRadio").prop("checked", true);
            });

            $('#zoneToZoneLabel').on('click', function () {
                $("#zoneVehicleRadio").prop("checked", false);
                $("#zoneToZoneRadio").prop("checked", true);
            });

            $("#form").validate({
                rules: {
                    name: {
                        required: true,
                        lettersNumbersonly: true
                    },
                    type: {
                        required: true
                    }
                },
                messages:{
                    name: {
                        required:"The vehicle type name is required"
                    },
                    type: {
                        required:"The vehicle type is required"
                    }
                },
                errorPlacement : function(error, element) {
                    if(element.attr("name") == "type"){
                        error.appendTo('#typeErrordiv');
                        return;
                    }

                    if(element.attr("name") == "name"){
                        error.appendTo('#nameErrordiv');
                        return;
                    }

                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop