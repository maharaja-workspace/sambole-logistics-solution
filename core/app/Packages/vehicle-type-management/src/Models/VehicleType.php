<?php
namespace VehicleTypeManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends Model{
    use SoftDeletes;

    protected $table = 'vehicle_types';

    protected $fillable = ['name', 'description', 'is_bin', 'zone_access'];
}