<?php

namespace VehicleTypeManage\Http\Controllers;

use App\Http\Controllers\Controller;
use VehicleTypeManage\Models\VehicleType;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;


class VehicleTypeController extends Controller
{

    function index()
    {
        return view('VehicleTypeManage::list');
    }

    function add()
    {
        return view('VehicleTypeManage::add');
    }

    function store(Request $request)
    {
        //        dd($request->all());

        $is_bin = 0;
        $zone_access = 0;
        $type = $request->get('type');
        if ($type == "is_bin") {
            $is_bin = 1;
        } else if ($type == "zone_access") {
            $zone_access = 1;
        }

        if ($request->has('name')) {
            $vehicle_type = VehicleType::create([
                'name' => $request->name,
                'description' => $request->description,
                'is_bin' => $is_bin,
                'zone_access' => $zone_access,
            ]);
        }

        if ($vehicle_type) {
            return redirect('vehicle-type/add')->with([
                'success' => true,
                'success.message' => 'Vehicle Type Created successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('vehicle-type/add')->with([
            'error' => true,
            'error.message' => 'Vehicle Type did not created ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function jsonList(Request $request)
    {
        $jsonList = array();
        $i = 1;
        foreach (VehicleType::all() as $vehicle_type) {

            $dd = array();

            array_push($dd, $i);

            array_push($dd, $vehicle_type->name);

            array_push($dd, $vehicle_type->description);

            if ($vehicle_type->is_bin == 1) {
                array_push($dd, 'Yes');
            } else {
                array_push($dd, 'No');
            }

            if ($vehicle_type->zone_access == 1) {
                array_push($dd, 'Yes');
            } else {
                array_push($dd, 'No');
            }

            $permissions = Permission::whereIn('name', ['vehicle.type.edit', 'admin'])->where('status', '=', 1)->lists('name');
            // if (Sentinel::hasAnyAccess($permissions)) {
            array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('vehicle-type/' . $vehicle_type->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Vehicle Type"><i class="fa fa-pencil"></i></a></center>');
            // }

            /*$permissions = Permission::whereIn('name', ['vehicle.type.delete', 'admin'])->where('status', '=', 1)->lists('name');
            if (Sentinel::hasAnyAccess($permissions)) {
                array_push($dd, '<a href="#" class="zone-delete" data-id="' . $vehicle_type->id . '" data-toggle="tooltip" data-placement="top" title="Delete Vehicle Type"><i class="fa fa-trash-o"></i></a>');
            }*/
            $vehicleStatus = $vehicle_type->status;
            $statusIcon = $vehicleStatus == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>';
            $statusTitle = $vehicleStatus == 1 ? 'Inactive' : 'Active';
            $status = 0;
            if ($vehicle_type->status == 0) {
                $status = 1;
            }
            $permissions = Permission::whereIn('name', ['vehicle.type.status', 'admin'])->where('status', '=', 1)->lists('name');
            // if (Sentinel::hasAnyAccess($permissions)) {
            array_push($dd, '<center><a href="#" class="vehicle-type-status" data-status="' . $status . '" data-id="' . $vehicle_type->id . '" data-toggle="tooltip" data-placement="top" title=' . $statusTitle . '>' . $statusIcon . '</a></center>');
            // }

            array_push($jsonList, $dd);
            $i++;
        }
        return Response::json(array('data' => $jsonList));
    }

    function edit($id)
    {
        $vehicle_type = VehicleType::find($id);

        if (!$vehicle_type) {
            return redirect('vehicle-type/list')->with([
                'error' => true,
                'error.message' => 'Vehicle Type did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        return view('VehicleTypeManage::edit')->with(['vehicle_type' => $vehicle_type]);
    }

    function update(Request $request)
    {
        $vehicle_type = VehicleType::find($request->id);
        if (!$vehicle_type) {
            return redirect('vehicle-type/list')->with([
                'error' => true,
                'error.message' => 'Vehicle Type did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $is_bin = 0;
        $zone_access = 0;
        $type = $request->get('type');
        if ($type == "is_bin") {
            $is_bin = 1;
        } else if ($type == "zone_access") {
            $zone_access = 1;
        }

        $vehicle_type->name = $request->name;
        $vehicle_type->description = $request->description;
        $vehicle_type->is_bin = $is_bin;
        $vehicle_type->zone_access = $zone_access;

        if ($vehicle_type->save()) {
            return redirect('vehicle-type/list')->with([
                'success' => true,
                'success.message' => 'Vehicle Type updated successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('vehicle-type/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Vehicle Type did not updated ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $vehicle_type = VehicleType::findOrFail($request->id);
            if ($vehicle_type) {
                $vehicle_type->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {

        if ($request->ajax()) {
            $vehicle = VehicleType::findOrFail($request->id);
            if ($vehicle) {
                $vehicle->status = $request->status;
                $vehicle->save();
                $msg = $request->status == 1 ? 'You have Activated Successfully' : 'You have Deactivated Successfully';

                return response()->json($msg);
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
