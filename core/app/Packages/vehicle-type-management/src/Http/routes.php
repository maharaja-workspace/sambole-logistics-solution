<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'VehicleTypeManage\Http\Controllers', 'prefix' => 'vehicle-type'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'vehicle.type.list', 'uses' => 'VehicleTypeController@index'
        ]);

        Route::get('json/list', [
            'as' => 'vehicle.type.list', 'uses' => 'VehicleTypeController@jsonList'
        ]);

        Route::get('add', [
            'as' => 'vehicle.type.add', 'uses' => 'VehicleTypeController@add'
        ]);

        Route::get('{id}/edit', [
            'as' => 'vehicle.type.edit', 'uses' => 'VehicleTypeController@edit'
        ]);


        /**
         * POST Routes
         */
        Route::post('store', [
            'as' => 'vehicle.type.store', 'uses' => 'VehicleTypeController@store'
        ]);

        Route::post('update', [
            'as' => 'vehicle.type.update', 'uses' => 'VehicleTypeController@update'
        ]);

        Route::post('delete', [
            'as' => 'vehicle.type.delete', 'uses' => 'VehicleTypeController@delete'
        ]);
        Route::post('status', [
            'as' => 'vehicle.type.status', 'uses' => 'VehicleTypeController@status'
        ]);

    });
});


