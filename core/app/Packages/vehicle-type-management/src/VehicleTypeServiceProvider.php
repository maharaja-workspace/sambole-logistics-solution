<?php

namespace VehicleTypeManage;

use Illuminate\Support\ServiceProvider;

class VehicleTypeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'VehicleTypeManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('VehicleTypeManage', function(){
            return new VehicleTypeManage;
        });
    }
}
