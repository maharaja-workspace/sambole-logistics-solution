<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'AreaManage\Http\Controllers'], function(){
        Route::post('areas/list/data', 'AreaController@listData')->name("areas.index");
        Route::post('areas/verify/{fieldName}', 'AreaController@verify')->name("areas.verify");
        Route::post('areas/status-toggle', 'AreaController@toggleStatus')->name("areas.update");
        Route::post('areas/verify-update/{fieldName}', 'AreaController@verifyUpdate')->name("areas.verify");
        Route::resource('areas', 'AreaController');
    });

});


