<?php
namespace AreaManage\Http\Controllers;

use App\Http\Controllers\Controller;
use AreaManage\Models\Area;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use Datatables;

class
AreaController extends Controller {

    function index(){
        return view( 'AreaManage::list');
    }

    public function listData(Request $request)
    {

        $permissions = Permission::whereIn('name',['areas.edit','admin'])->where('status','=',1)->lists('name');

        return Datatables::of(Area::query())
            ->addColumn("edit", function ($filtereddArea) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<center><a href="#" class="blue" onclick="window.location.href=\''.url('areas/'.$filtereddArea->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit package size"><i class="fa fa-pencil"></i></a></center>';
                }
            })
            ->addColumn("status", function ($filtereddArea) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    if($filtereddArea->status == 1){
                        return '<center><a href="javascript:void(0)" form="noForm" class="blue area-status-toggle" data-id="'.$filtereddArea->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></></center>';
                    }
                    else if($filtereddArea->status == 0){
                        return  '<center><a href="javascript:void(0)" class="blue area-status-toggle" data-id="'.$filtereddArea->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></></center>';
                    }
                }
            })
            ->make(true);

    }

    function create(){
        return view('AreaManage::add');
    }

    function store(Request $request){
        $area =  Area::create([
            'name' => $request->area_name,
            'description' => $request->description
        ]);

        if ($area){
            return redirect('areas/create')->with([ 'success' => true,
                'success.message'=> 'Area Created successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('areas/create')->with([ 'error' => true,
            'error.message'=> 'Area did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $area = Area::find($id);
        return view('AreaManage::edit')->with('area', $area);
    }

    function update(Request $request, $id){
        $area = Area::find($id);
        if (!$area){
            return redirect('areas/')->with([ 'error' => true,
                'error.message'=> 'Area did not found ',
                'error.title' => 'Ooops !']);
        }

        $area->name = $request->area_name;
        $area->description = $request->description;

        if ($area->save()){
            return redirect('areas')->with([ 'success' => true,
                'success.message'=> 'Area updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('areas/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Area did not updated ',
            'error.title' => 'Ooops !']);
    }

    function verify($verify, Request $request){
        if($verify == "name"){
            if (Area::where('name', $request->area_name)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function verifyUpdate($verify, Request $request){
        if($verify == "name"){
            if (Area::where('name', $request->area_name)->where('id', "!=", $request->id)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function  toggleStatus(Request $request){
        $area = Area::find($request->id);
        $msg = "";
        if ($area){
            if ($area->status == 1){
                $area->status = 0;
                $msg = "You have Deactivated Successfully";
            }else if ($area->status == 0){
                $area->status = 1;
                $msg = "You have Activated Successfully";
            }
            $area->save();
            return $msg;
        }
    }
}