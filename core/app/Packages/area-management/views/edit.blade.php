@extends('layouts.back.master') @section('current_title','Edit Area')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Area Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Area </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form" action="{{url("areas/".$area->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Area Code <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled name="area_code" class="form-control" value="{{$area->id}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Area Name <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="area_name" class="form-control" value="{{$area->name}}"></div>
                </div>
               <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                   <div class="col-sm-3">
                       <textarea name="description" class="form-control" >{{$area->description}}</textarea>
                   </div>
                </div>
               <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <a href="{{url('areas')}}" class="btn btn-danger" type="button" style="float: right;" >Cancel</a>
                    </div>
                </div>

                

            </form>

        </div>
    </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();


         $("#form").validate({
             rules: {
                 area_name: {
                     required: true,
                     maxlength: 20,
                     remote:function(){
                         var checkit={
                             type: "POST",
                             url:  "{{url('areas/verify-update/name')}}",
                             dataType: "json",
                             data: {
                                 "_token" : "{{csrf_token()}}",
                                 "id" : "{{$area->id}}"
                             }
                         };
                         return checkit;
                     }
                 },
             },
             messages:
                 {
                     area_name:
                         {
                             remote: jQuery.validator.format("{0} is already taken.")
                         }
                 },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop