<?php

namespace DeliveryApprovalManage;

use Illuminate\Support\ServiceProvider;

class DeliveryApprovalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'DeliveryApprovalManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DeliveryApprovalManage', function(){
            return new DeliveryApprovalManage();
        });
    }
}
