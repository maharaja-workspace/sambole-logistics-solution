<?php

namespace DeliveryApprovalManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use Carbon\Carbon;
use CityManage\Models\City;
use DailyPlanManage\Models\RiderAssign;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use Illuminate\Http\Request;
use PackageSizeManage\Models\PackageSize;
use PackageTypeManage\Models\PackageType;
use PackageWeightManage\Models\PackageWeight;
use PickUpManage\Http\Controllers\PickUpController;
use Response;
use RiderManage\Models\Rider;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\Stock;
use StoreManage\Models\Store;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use ZoneManage\Models\Zone;

class DeliveryApprovalController extends Controller
{
    public $PickUpController;

    public function __construct()
    {
        $this->PickUpController = new PickUpController();
        return $this;
    }

    function index()
    {
        return view('DeliveryApprovalManage::list');
    }

    public function jsonPendingList(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();

        $routes = Routing::with('cities')->get();
        if ($logged_in_user->inRole('zone-manager')) {
            $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        }

        $cities = [];
        foreach ($routes as $route) {
            $cities[] = $route->cities->toArray();
        }

        $city_ids = $this->PickUpController->array_column_recursive($cities, 'city_id');

        $customer_requests = CustomerRequest::orderBy('created_at', 'desc');
        if ($isCentralWarehouse) {
            $customer_requests = $customer_requests->where(function ($q){
                $q->where(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 2);
                })->orWhere(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 1);
                });
            });
        } else {
            $customer_requests = $customer_requests->whereIn('pickup_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });
        }

        foreach ($request->get('columns') as $column){
            if ($column['name'] == "reference_no" && $column['search']['value'] != ""){
                $customer_requests = $customer_requests->where("order_id", "LIKE", '%'.$column['search']['value'].'%');
                break;
            }
            if ($column['name'] == "merchant_order_id" && $column['search']['value'] != ""){
                $customer_requests = $customer_requests->where("merchant_order_id", "LIKE", '%'.$column['search']['value'].'%');
                break;
            }
            if ($column['name'] == "store" && $column['search']['value'] != ""){
//                $queryBuild = Routing::where(function ($q) use ($column){
//                    $q->where("id", "LIKE", '%'.$column['search']['value'].'%');
//                });
//                break;
                $ids = array();
                $result = Zone::where("name", "LIKE", '%'.$column['search']['value'].'%')->get();
                foreach ($result as $r) {
                    foreach ($r->stores as $store) {
                        foreach ($store->stock as $st) {
                            array_push($ids, $st->request_id);
                        }
                    }
                }
                $result2 = Vehicle::where("number", "LIKE", '%'.$column['search']['value'].'%')->get();
                foreach ($result2 as $r) {
                    foreach ($r->store->stock as $st) {
                        array_push($ids, $st->request_id);
                    }
                }

                $customer_requests = $customer_requests->whereIn('id', $ids);
                break;
            }
            if ($column['name'] == "rider" && $column['search']['value'] != ""){
                $customer_requests = $customer_requests->whereHas('rider_assign.rider', function ($q) use ($column){
//                    $q->where("rider.name", "LIKE", '%'.$column['search']['value'].'%');
                    $q->whereRaw('REPLACE (name," ","") LIKE "%'.str_replace(' ','%',$column['search']['value']).'%"');
                });
                break;
            }
            if ($column['name'] == "buyer_name" && $column['search']['value'] != ""){
                \Log::error("BUYER SEARCH = " . $column['search']['value'] );
                $value = str_replace(' ', '', $column['search']['value']);
//                $customer_requests = $customer_requests->where("buyer_name", "LIKE", '%'.$column['search']['value'].'%');
                $customer_requests = $customer_requests->whereRaw('REPLACE (buyer_name," ","") LIKE "%'.str_replace(' ','%',$column['search']['value']).'%"');
                break;
            }
            if ($column['name'] == "address" && $column['search']['value'] != ""){
//                $customer_requests = $customer_requests->where("pickup_address", "LIKE", '%'.$column['search']['value'].'%');
                $customer_requests = $customer_requests->whereRaw('REPLACE (pickup_address," ","") LIKE "%'.str_replace(' ','%',$column['search']['value']).'%"');
                break;
            }
            if ($column['name'] == "mobile_no" && $column['search']['value'] != ""){
//                $customer_requests = $customer_requests->where("buyer_mobile", "LIKE", '%'.$column['search']['value'].'%');
                $customer_requests = $customer_requests->whereRaw('REPLACE (buyer_mobile," ","") LIKE "%'.str_replace(' ','%',$column['search']['value']).'%"');
                break;
            }
            if ($column['name'] == "package_amount" && $column['search']['value'] != ""){
                $customer_requests = $customer_requests->where("total_amount", $column['search']['value']);
                break;
            }
            if ($column['name'] == "payment_status" && $column['search']['value'] != ""){

                $value = strtolower($column['search']['value']);
                if ($value == "paid") {
                    $value = 1;
                } else if ($value == "pending") {
                    $value = 2;
                }

                if ($value == 1) {
                    $customer_requests = $customer_requests->where("paid", 1);
                } else if ($value == 2) {
                    $customer_requests = $customer_requests->where("paid", 0);
                }
                break;
            }
            if ($column['name'] == "status" && $column['search']['value'] != ""){

                $status = $column['search']['value'];
                if ($status == "Pending for pickup") {
                    $status = 0;
                } else if ($status == "Assigned for driver") {
                    $status = 1;
                } else if ($status == "Picked") {
                    $status = 2;
                } else if ($status == "Zone in") {
                    $status = 3;
                } else if ($status == "Zone out") {
                    $status = 4;
                } else if ($status == "Delivered to the customer") {
                    $status = 5;
                } else {
                    $status = null;
                }


                if ($status) {
                    $customer_requests = $customer_requests->where("status", $status);
                }
                break;
            }
        }

        $totalData = $customer_requests->count();

        if ($limit != -1) {
            $customer_requests = $customer_requests->offset($start)
                ->limit($limit);
        }

        $customer_requests = $customer_requests->get();
        //        dd($customer_requests);
        foreach ($customer_requests as $customer_request) {

            //            if (Stock::where('request_id', $customer_request->id)->first()) {
            $dd = array();

            //            array_push($dd, '<td><input class="check" type="checkbox" value="' . $customer_request->id . '"></td>');
            //            array_push($dd, Zone::with('cities')->where('city_id'));

            $Stock = Stock::where('request_id', $customer_request->id)->first();
            $Store = Store::find($Stock['store_id']);
            $StoreData = "<i>NA</i>";
            if($Store){
                if ($Store->store_type == 'zone') {
                    $StoreData = Zone::find($Store->related_id)->name;
                } else {
                    $StoreData = Vehicle::find($Store->related_id)->number;
                }
            }

            $status = "-";
            if ($customer_request->status == 0) {
                $status = "Pending for pickup";
            } else if ($customer_request->status == 1) {
                $status = "Assigned for driver";
            } else if ($customer_request->status == 2) {
                $status = "Picked";
            } else if ($customer_request->status == 3) {
                $status = "Zone in";
            } else if ($customer_request->status == 4) {
                $status = "Zone out";
            } else if ($customer_request->status == 5) {
                $status = "Delivered to the customer";
            }


            array_push($dd, $customer_request->order_id);
            array_push($dd, $customer_request->merchant_order_id);
            array_push($dd, $StoreData);
            $rider_assign = RiderAssign::with('getRider')->where('request_id', $customer_request->id)->first();
//            // dd($rider_assign['getRider']['name']);
            array_push($dd, ($rider_assign && $rider_assign->getRider ) ? $rider_assign['getRider']['name'] : '<i>NA</i>');
            array_push($dd, $customer_request->buyer_name);
            array_push($dd, $customer_request->pickup_address);
            array_push($dd, $customer_request->buyer_mobile);
            array_push($dd, $customer_request->total_amount);
            if ($customer_request->paid == 0) {
                array_push($dd, 'Pending');
            } else {
                array_push($dd, 'Paid');
            }
            array_push($dd, $status);
            array_push($dd, '<button href="#" class="btn btn-info" onclick="window.location.href=\'' . url('delivery-approval/request/' . $customer_request->id . '/view') . '\'" data-toggle="tooltip" data-placement="top" title="View More"><i class="fa fa-eye"></i></button>');
            //                array_push($dd, '<a class="btn  btn-info" type="button" onclick="location.href = \'/sambole-logistics-solution/deliveryApproval/more\';">
            //                    <i class="fa fa-eye"></i></a>');
            array_push($jsonList, $dd);
            //            }
        }
        return Response::json(array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $jsonList,
        ));
    }

    function viewRequest($id)
    {
        $logged_user = Sentinel::getUser();

        $customer_request = CustomerRequest::find($id);
        $customer = Customer::find($customer_request->customer_id);
        $package_type = PackageType::where('id', $customer_request->package_type)->first();
        $package_size = PackageSize::where('id', $customer_request->package_size)->first();
        $package_weight = PackageWeight::where('id', $customer_request->package_weight)->first();
        $rider_assign_id = RiderAssign::where('request_id', $customer_request->id)->first()['rider_id'];
        $rider = Rider::where('id', $rider_assign_id)->first();
        $Stock = Stock::where('request_id', $customer_request->id)->first();
        $Store = Store::find($Stock['store_id']);
        $StoreData = "<i>NA</i>";
        if($Store){
            if ($Store->store_type == 'zone') {
                $StoreData = Zone::find($Store->related_id)->name;
            } else {
                $StoreData = Vehicle::find($Store->related_id)->number;
            }
        }

        if (!$customer_request) {
            return redirect('delivery-approval/list')->with([
                'error' => true,
                'error.message' => 'Request did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        return view('DeliveryApprovalManage::view')->with(['c_request' => $customer_request, 'logged_user' => $logged_user, 'rider' => $rider, 'package_type' => $package_type, 'package_size' => $package_size, 'package_weight' => $package_weight, 'customer' => $customer, 'cities' => City::where('status', 1)->get(), 'store_name' => $StoreData]);
    }

    function update(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $vehicle = Vehicle::find($request->id);
        if (!$vehicle) {
            return redirect('vehicle/list')->with([
                'error' => true,
                'error.message' => 'Vehicle did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $vehicle->number = $request->number;
        $vehicle->name = $request->name;
        $vehicle->zone_id = $request->zone or $logged_in_user->zone['id'];
        $vehicle->vehicle_type_id = $request->type;
        $vehicle->customer_pickup = $request->customer_pickup or 0;
        $vehicle->color = $request->color;
        $vehicle->vehicle_model = $request->model;
        $vehicle->make = $request->make;

        if ($vehicle->save()) {

            if (VehicleType::where('id', $request->type)->first()['is_bin']) {

                $stores = Store::all();
                $vehicles = Vehicle::all();

                foreach ($stores as $store) {
                    foreach ($vehicles as $vehicle) {
                        $check_bin = Bin::where('store_id', $store->id)->where('related_id', $vehicle->id)->where('bin_type', 'vehicle')->first();
                        if ($check_bin == null) {
                            Bin::create([
                                'store_id' => $store->id,
                                'related_id' => $vehicle->id,
                                'bin_type' => 'vehicle'
                            ]);
                        }
                    }
                }
            }

            return redirect('vehicle/list')->with([
                'success' => true,
                'success.message' => 'Vehicle updated successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('vehicle/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Vehicle did not updated ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function assignRequest(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        if ($request->req_id) {
            $c_request = CustomerRequest::find($request->req_id)->update(['status' => 4]);
            $stock = Stock::where('request_id', $request->req_id)->first();
            Stock::find($stock['id'])->update(['availability', 'out']);
            $rider_assign = RiderAssign::where('rider_id', $request->rider)->where('completed', 0)->orderBy('created_at', 'desc')->first();
            RiderAssign::create([
                'rider_id' => $request->rider,
                'store_id' => $rider_assign['store_id'],
                'route_id' => $rider_assign['route_id'],
                'request_id' => $request->req_id,
                'assigned_by' => $logged_in_user->id
            ]);

            return response('success');
        }

        return response('Invalid Request');
    }

    public function changeRider(Request $request)
    {
        $rider_assign = RiderAssign::where('request_id', $request->req_id)->where('created_at', '>=', Carbon::today())->orderBy('created_at', 'desc')->first();
        $rider_assign2 = RiderAssign::find($rider_assign['id']);
        $rider_assign2->rider_id = $request->rider;
        $rider_assign2->save();

        return response('success');
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $vehicle = Vehicle::find($request->id);
            if ($vehicle) {
                $vehicle->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
