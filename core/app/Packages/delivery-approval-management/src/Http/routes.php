<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'DeliveryApprovalManage\Http\Controllers', 'prefix' => 'delivery-approval'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'delivery.approve.list', 'uses' => 'DeliveryApprovalController@index'
        ]);

        Route::get('json/pending/list', [
            'as' => 'delivery.approve.list', 'uses' => 'DeliveryApprovalController@jsonPendingList'
        ]);

        Route::get('json/scheduled/list', [
            'as' => 'delivery.approve.list', 'uses' => 'DeliveryApprovalController@jsonScheduledList'
        ]);

        Route::get('request/{id}/view', [
            'as' => 'delivery.approve.view', 'uses' => 'DeliveryApprovalController@viewRequest'
        ]);


        /**
         * POST Routes
         */
        Route::post('request/store', [
            'as' => 'delivery.approve.store', 'uses' => 'DeliveryApprovalController@requestStore'
        ]);

        Route::post('assign/request', [
            'as' => 'delivery.approve.store', 'uses' => 'DeliveryApprovalController@assignRequest'
        ]);

        Route::post('change/rider', [
            'as' => 'delivery.approve.store', 'uses' => 'DeliveryApprovalController@changeRider'
        ]);

        Route::post('update', [
            'as' => 'delivery.approve.update', 'uses' => 'DeliveryApprovalController@update'
        ]);

        Route::post('delete', [
            'as' => 'delivery.approve.delete', 'uses' => 'DeliveryApprovalController@delete'
        ]);

    });
});


