<?php
namespace DeliveryManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model{
    use SoftDeletes;

    protected $fillable = ['user_id', 'first_name', 'last_name', 'mobile', 'address_line1', 'address_line2', 'city_id', 'email', 'status', 'merchant_id'];
}