<?php
namespace DeliveryManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRequest extends Model{
    use SoftDeletes;

    protected $table = 'requests';

    protected $fillable = ['request_type', 'name', 'order_id', 'pickup_address', 'pickup_city', 'buyer_name', 'buyer_mobile', 'buyer_address', 'buyer_city', 'payment_type', 'delivery_type', 'package_type', 'package_size', 'package_type_size_price', 'package_weight', 'total_amount', 'tax_amount', 'package_weight_price', 'remark', 'status', 'created_by', 'customer_id', 'customer_nic', 'buyer_nic', ''];
}