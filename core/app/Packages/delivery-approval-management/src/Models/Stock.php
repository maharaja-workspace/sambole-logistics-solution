<?php
namespace DeliveryManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model{
    use SoftDeletes;

    protected $fillable = ['request_id', 'store_id', 'bin_id', 'availability'];
}