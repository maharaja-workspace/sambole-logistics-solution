@extends('layouts.back.master') @section('current_title','All Deliveries')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }

    .dataTables_filter, .dataTables_info { display: none; }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Shipment Details</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Shipment Details</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2>
        <small>&nbsp;</small>
    </h2>
    <ol class="breadcrumb text-right">


    </ol>
</div>

@stop

@section('content')
{{--    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"--}}
{{--         onclick="location.href = '{{url('delivery/request')}}';">--}}
{{--        <p class="plus">+</p>--}}
{{--    </div>--}}

<div class="row">
    <div class="col-lg-12 margins">

        <div class="ibox-content">
            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Reference Number</th>
                        <th>Merchant Order Id</th>
                        <th>Store</th>
                        <th>Rider</th>
                        <th>Buyer name</th>
                        <th>Address</th>
                        <th>Mobile Number</th>
                        <th>Package Amount</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        {{--                        <th>Description</th>--}}
                        {{--                        <th style="width: 7%;">Bin</th>--}}
                        {{--                        <th style="width: 9%;">Sub Bin</th>--}}
                        <th data-search="disable" style="width: 13%">More Details</th>
                    </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
</div>

@stop
@section('js')



<script>
    $('#example1 thead th').each( function () {
        if($(this).data('search') == "disable"){

        }else{

            var title = $(this).text();
            $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
        }
    } );
    var table;
    $(document).ready(function () {


            table = $('#example1').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{url('delivery-approval/json/pending/list')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                "initComplete": function(settings, json) {
                    $('#example1_filter input').unbind();
                    $('#example1_filter input').bind('keyup', function(e) {
                        if(e.keyCode == 13) {
                            table.search( this.value ).draw();
                        }
                    });
                },
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Shipment Details', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'pdf', title: 'Shipment Details', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                        }
                    },
                    {extend: 'print', className: 'btn-sm'}
                ],
                orderCellsTop: true,
                fixedHeader: true,
                "searching": true,
                // "autoWidth": false,
                "order": [[0, "asc"]],
                "columnDefs": [
                    {"className": "dt-center", "targets": [-1]},
                    ],
                "columns" : [
                    {"name" : "reference_no"},
                    {"name" : "merchant_order_id"},
                    {"name" : "store"},
                    {"name" : "rider"},
                    {"name" : "buyer_name"},
                    {"name" : "address"},
                    {"name" : "mobile_no"},
                    {"name" : "package_amount"},
                    {"name" : "payment_status"},
                    {"name" : "status"},
                    null
                ]
            });

        table.columns().every( function () {
            var that = this;

            $('input', this.header()).on('keyup change clear click', function (e) {
                e.stopPropagation();
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });

        $("#example1_filter  input").keyup( function (e) {
            if (e.keyCode == 13) {
                table.fnFilter( this.value );
            }
        });

        });
</script>


@stop