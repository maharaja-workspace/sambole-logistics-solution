@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Delivery Request Details</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Delivery View</strong>
        </li>
    </ol>
</div>


@stop
@section('content')
<div class="row" style="margin-bottom: 100px;">

    <div class="col-lg-12">
        <br><br><br>
        <form method="POST" class="form-horizontal" id="form">
            {!!Form::token()!!}

            <div class="row">
                <div class="col-sm-5" align="right">
                    <h2 class="text-center" style="font-weight: bold">Merchant's Details</h2><br>
                    <div class="row">
                        <div class="col-xs-6">
                            <label>Merchant Name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="customer_name"
                                   value="{{ $customer['first_name'] }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Merchant NIC</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="customer_nic"
                                   value="{{ $c_request->customer_nic }}" readonly>
                        </div>
                    </div>
                    <br>

                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-xs-6">--}}
                    {{--                            <label>Pickup Requester's Email</label>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-xs-6">--}}
                    {{--                            <input type="text" class="form-control" name="customer_email"--}}
                    {{--                                value="{{ $customer['email'] }}" readonly>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    <br>--}}

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Contact Number</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="customer_mobile"
                                   value="{{ $customer['mobile'] }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Merchant Address</label>
                        </div>
                        <div class="col-xs-6">
                            <textarea class="form-control" rows="3" name="customer_address"
                                      readonly>{{ $customer['address_line1'] }}</textarea>
                            <!-- <input type="text" class="form-control" name="packType"> -->
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Pickup City</label>
                        </div>
                        <div class="col-xs-6">
                            <!-- <input type="text" class="form-control" name="packType"> -->
                            {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="customer_city" tabindex="-1" title=""> --}}
                            {{-- <option value="">Choose</option> --}}
{{--                            @foreach($cities as $city)--}}
{{--                                @if($customer['city_id'] == $city->id)--}}
{{--                                    <input type="text" class="form-control" name="customer_mobile" value="{{ $city->name }}"--}}
{{--                                           readonly>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
                            <input type="text" class="form-control" name="customer_mobile" value="{{ $c_request->pickupCity->name }}"
                                   readonly>
                            {{-- </select> --}}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Pickup Zone</label>
                        </div>
                        <div class="col-xs-6">
                            <!-- <input type="text" class="form-control" name="packType"> -->
                            {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="customer_city" tabindex="-1" title=""> --}}
                            {{-- <option value="">Choose</option> --}}
                            @foreach($cities as $city)
                                @if($customer['city_id'] == $city->id)
                                    @if(isset($c_request->pickupCity->zones[0]))
                                    <input type="text" class="form-control" name="customer_mobile" value="{{ isset($c_request->pickupCity->zones[0]->name) ? $c_request->pickupCity->zones[0]->name : '' }}"
                                           readonly>
                                    @else
                                        <input type="text" class="form-control" name="customer_mobile" value=""
                                               readonly>
                                    @endif
                                @endif
                            @endforeach
                            {{-- </select> --}}
                        </div>
                    </div>
                </div>

                <div class="col-sm-5" align="right">
                    <h2 class="text-center" style="font-weight: bold">Customer Details</h2><br>
                    <div class="row">
                        <div class="col-xs-6">
                            <label>Customer Name</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="buyer_name"
                                   value="{{ $c_request->buyer_name }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Customer NIC </label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="buyer_nic" value="{{ $c_request->buyer_nic }}"
                                   readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Customer Mobile</label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="buyer_mobile"
                                   value="{{ $c_request->buyer_mobile }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Customer Delivery Address</label>
                        </div>
                        <div class="col-xs-6">
                            <!-- <input type="text" class="form-control" name="packType"> -->
                            <textarea class="form-control" rows="3" name="buyer_address"
                                      readonly>{{ $c_request->buyer_address }}</textarea>
                        </div>
                    </div>
                    <br>

                    <!-- <div class="row">
                      <div class="col-xs-6">
                        <label>&nbsp</label>
                      </div>
                      <div class="col-xs-6">
                        <input type="text" class="form-control" name="packType">
                      </div>
                    </div><br> -->

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Delivery City</label>
                        </div>
                        <div class="col-xs-6">
                            <!-- <input type="text" class="form-control" name="packType"> -->
                            {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                            {{-- name="buyer_city" tabindex="-1" title=""> --}}
                            {{-- <option value="">Choose</option> --}}
                            @foreach($cities as $city)
                                @if($c_request->buyer_city == $city->id)
                                    <input type="text" class="form-control" name="buyer_mobile" value="{{ $city->name }}"
                                           readonly>
                                @endif
                            @endforeach
                            {{-- </select> --}}
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Delivery Zone</label>
                        </div>
                        <div class="col-xs-6">
                            <!-- <input type="text" class="form-control" name="packType"> -->
                            {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                            {{-- name="buyer_city" tabindex="-1" title=""> --}}
                            {{-- <option value="">Choose</option> --}}
                            @foreach($cities as $city)
                                @if($c_request->buyer_city == $city->id)
                                    @if(isset($c_request->buyerCity->zones[0]))
                                    <input type="text" class="form-control" name="buyer_mobile" value="{{ isset($c_request->buyerCity->zones[0]->name) ? $c_request->buyerCity->zones[0]->name : '' }}"
                                           readonly>
                                    @else
                                        <input type="text" class="form-control" name="buyer_mobile" value=""
                                               readonly>
                                    @endif
                                @endif
                            @endforeach
                            {{-- </select> --}}
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-5" align="right">
                    <h2 class="text-center" style="font-weight: bold">Package Details</h2><br>
                    <div class="row">
                        <div class="col-xs-6">
                            <label>Package Type </label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="payment_type"
                                   value="{{ $package_type['name'] }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Package Size </label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="payment_type"
                                   value="{{ $package_size['name'] }}" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Package Weight </label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="payment_type"
                                   value="{{ $package_weight['min_weight'] }} to {{ $package_weight['max_weight'] }}"
                                   readonly>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5" align="right">
                    <h2 class="text-center" style="font-weight: bold">Shipment Details</h2><br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Payment Type </label>
                        </div>
                        <div class="col-xs-6">
                            @if($c_request->payment_type == 1)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Online Payment" readonly>
                            @elseif($c_request->payment_type == 2)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Cash On Delivery" readonly>
                            @else
                                <input type="text" class="form-control" name="payment_type"
                                       value="" readonly>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Delivery Type </label>
                        </div>
                        <div class="col-xs-6">
                            @if($c_request->delivery_type == 1)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Collection Point" readonly>
                            @elseif($c_request->delivery_type == 2)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Home Delivery" readonly>
                            @else
                                <input type="text" class="form-control" name="payment_type"
                                       value="" readonly>
                            @endif
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Pickup Type </label>
                        </div>
                        <div class="col-xs-6">
                            @if($c_request->pickup_type == 1)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Harrisons Pickup" readonly>
                            @elseif($c_request->pickup_type == 2)
                                <input type="text" class="form-control" name="payment_type"
                                       value="Merchant's Location" readonly>
                            @else
                                <input type="text" class="form-control" name="payment_type"
                                       value="" readonly>
                            @endif
                        </div>
                    </div>
                    <br>

                    @if($c_request->payment_type == 2)
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Payment Amount </label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="payment_type"
                                       value="{{ $c_request->total_amount }}" readonly>
                            </div>
                        </div>
                        <br>
                    @endif

                    <div class="row">
                        <div class="col-xs-6">
                            <label>Order Code </label>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="payment_type"
                                   value="{{ $c_request->merchant_order_code }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            
            <br><br><br>
            {{-- <div class="hr-line-dashed"></div> --}}
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <a href="{{ url('delivery-approval/list') }}" class="btn btn-sm btn-info">CLOSE</a>
                </div>
            </div>

        </form>
    </div>
</div>
@stop