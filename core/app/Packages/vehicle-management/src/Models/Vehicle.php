<?php
namespace VehicleManage\Models;

use DailyPlanManage\Models\RiderVehicle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model{
    use SoftDeletes;

    protected $fillable = ['name', 'number', 'zone_id', 'vehicle_type_id', 'customer_pickup', 'color', 'vehicle_model', 'make', 'slots'];

    public function type()
    {
        return $this->belongsTo('VehicleTypeManage\Models\VehicleType', 'vehicle_type_id');
    }

    public function zone()
    {
        return $this->belongsTo('ZoneManage\Models\Zone','zone_id');
    }
    public function store()
    {
        return $this->hasOne('StoreManage\Models\Store','related_id','id')->where('store_type','vehicle');
    }

    public function rider_vehicle()
    {
        return $this->hasMany(RiderVehicle::class, 'rider_id', 'id');
    }

}