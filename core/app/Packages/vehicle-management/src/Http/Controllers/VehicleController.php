<?php

namespace VehicleManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use DailyPlanManage\Models\RiderAssign;
use DailyPlanManage\Models\RiderVehicle;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use StockManage\Models\Stock;
use StoreManage\Models\Store;
use Validator;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use Yajra\Datatables\Datatables;
use ZoneManage\Models\Zone;

class VehicleController extends Controller
{

    function index()
    {
        return view('VehicleManage::list');
    }

    function add()
    {
        $logged_in_user = Sentinel::getUser();

        if ($logged_in_user->inRole('zone-manager')) {
            $vehicle_types = VehicleType::where('zone_access', 1)->where('status', 1)->get();
        } else {
            $vehicle_types = VehicleType::where('status', 1)->get();
        }

        $activeZones = Zone::where('status', 1)->get();

        //
        return view('VehicleManage::add')->with(['vehicle_types' => $vehicle_types, 'zones' => $activeZones, 'logged_in_user' => $logged_in_user]);
    }

    function store(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:vehicles',
        ]);

        if ($validator->fails()) {
            return redirect('vehicle/add')
                ->with([
                    'error' => true,
                    'error.message' => 'Vehicle name should not be duplcated!',
                    'error.title' => 'Oops!'
                ]);
        }

        if ($request->has('name')) {
            $zone = 0;
            if ($logged_in_user->inRole('zone-manager')) {
                $zone = $logged_in_user->zone['id'];
            }
            if ($request->zone) {
                $zone = $request->zone;
            }

            $slots = 0;
            if ($request->slots) {
                $slots = $request->slots;
            }

            $vehicle = Vehicle::create([
                'zone_id' => $zone,
                'vehicle_type_id' => $request->type,
                'name' => $request->name,
                'number' => $request->number,
                'customer_pickup' => $request->customer_pickup or 0,
                'color' => $request->color,
                'vehicle_model' => $request->model,
                'make' => $request->make,
                'slots' => $slots
            ]);

            $store = Store::create([
                'related_id' => $vehicle->id,
                'store_type' => 'vehicle',
                'status' => 1
            ]);

            // if ($request->has('slots')) {
            //     $i = 1;
            //     while ($i <= (int)$request->slots) {
            //         Slot::create([
            //             'store_id' => $store->id,
            //             'slot_numbers' => $i
            //         ]);
            //         $i++;
            //     }
            // }

            if (VehicleType::where('id', $request->type)->first()['is_bin']) {

                $zones = Zone::all();
                //$vehicles = Vehicle::all();

                foreach ($zones as $zone) {
                    //foreach ($vehicles as $vehicle) {
                    //$check_bin = Bin::where('store_id', $store->id)->where('related_id', $vehicle->id)->where('bin_type', 'vehicle')->first();
                    $check_bin = Bin::where('store_id', $store->id)->where('related_id', $zone->id)->where('bin_type', 'vehicle')->first();
                    if ($check_bin == null) {
                        Bin::create([
                            'store_id' => $store->id,
                            'related_id' => $zone->id,
                            'bin_type' => 'vehicle',
                            'status' => $zone->status
                        ]);
                    }
                    //}
                }
            }
        }

        if ($vehicle) {
            return redirect('vehicle/add')->with([
                'success' => true,
                'success.message' => 'Vehicle Created successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('vehicle/add')->with([
            'error' => true,
            'error.message' => 'Vehicle did not created ',
            'error.title' => 'Ooops !'
        ]);
    }
    public function listData(Request $request)
    {

        $logged_user = Sentinel::getUser();
        $queryBuild = Vehicle::with('type', 'zone');

//        if($logged_user->inRole('zone-manager')){
//            $zones = Zone::select('id')->where('manager_id', $logged_user->id)->lists('id')->toArray();
//            $queryBuild = $queryBuild->whereIn('zone_id', $zones);
//        }
//
//        $permissions = Permission::whereIn('name', ['vehicle.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
//
//        return Datatables::of($queryBuild)
//            ->editColumn('customer_pick_up', function ($filteredData){
//                return ($filteredData->customer_pickup == 1) ? 'Enable' : 'Disable';
//            })
//            ->addColumn("edit", function ($filteredData) use($permissions){
//                if(Sentinel::hasAnyAccess($permissions)){
//                    return '<a href="#!" class="blue" onclick="window.location.href=\'' . url('vehicle/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Zone"><i class="fa fa-pencil"></i></a> ';
//                }
//            })
//            ->addColumn("status", function ($filteredData) use($permissions){
//                if(Sentinel::hasAnyAccess($permissions)){
//                    if($filteredData->status == 1){
//                        return '<a href="#!" class="blue vehicle-status " data-id="' . $filteredData->id . '"  data-status="' . $filteredData->status . '" title="Deactivate Zone"><i class="fa fa-toggle-on"></i></a>';
//                    }
//                    else if($filteredData->status == 0){
//                        return  '<a href="#!" class="blue vehicle-status " data-id="' . $filteredData->id . '"  data-status="' . $filteredData->status . '" title="Activate Zone"><i class="fa fa-toggle-off"></i></a>';
//                    }
//                }
//            })
//            ->make(true);

        $columns = array( 
            0 => 'id', 
            1 => 'zone',
            2 => 'name', 
            3 => 'number',
            4 => 'type',
            5 => 'customer_pickup',
            6 => 'color',
            7 => 'vehicle_model',
            8 => 'make',
            9 => 'slots',
        );
        
        $logged_user = Sentinel::getUser();
        $dataQuery = Vehicle::with(['type' => function($query){ $query->select('id','name');}, 
                                    'zone' => function($query){ $query->select('id','name');},
                                    ]);

        if($logged_user->inRole('zone-manager')){
            $zones = Zone::select('id')->where('manager_id', $logged_user->id)->lists('id')->toArray();
            $dataQuery = $dataQuery->whereIn('zone_id', $zones);
        }
        else $dataQuery = $dataQuery;
           
        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
//        if(empty($request->input('search.value'))){
//            $filteredDatas = $dataQuery->offset($start)
//                                            ->limit($limit)
//                                            ->orderBy($order,$dir)
//                                            ->get();
//        }
//        else{
//            $search = $request->input('search.value');
//            $filteredDatasQuery = $dataQuery->where(function($or) use($search){
//                                                $or->orWhere(function($query1) use($search){
//                                                    $query1->whereHas('type', function ($query1) use($search){
//                                                        $query1->where('vehicle_types.name','LIKE',"%{$search}%");
//                                                    });
//                                                })
//                                                ->orWhere(function($query2) use($search){
//                                                    $query2->whereHas('zone', function ($query2) use($search){
//                                                        $query2->where('zones.name','LIKE',"%{$search}%");
//                                                    });
//                                                })
//                                                ->orWhere('id', $search)
//                                                ->orWhere('name', 'LIKE',"%{$search}%")
//                                                ->orWhere('number', 'LIKE',"%{$search}%")
//                                                ->orWhere('color', $search)
//                                                ->orWhere('make', $search)
//                                                ->orWhere('vehicle_model', $search);
//                                            });
            $searchExists = false;
            if ($request->input('columns.0.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('id', $request->input('columns.0.search.value'));
            }
            if ($request->input('columns.1.search.value')) {
                $searchExists = true;
                $search = $request->input('columns.1.search.value');
                $filteredDatasQuery = $dataQuery->where(function($query2) use($search){
                                                    $query2->whereHas('zone', function ($query2) use($search){
                                                        $query2->where('zones.name','LIKE',"%{$search}%");
                                                    });
                                                });
            }
            if ($request->input('columns.2.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('name', 'LIKE',"%{$request->input('columns.2.search.value')}%");
            }
            if ($request->input('columns.3.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('number', 'LIKE',"%{$request->input('columns.3.search.value')}%");
            }
            if ($request->input('columns.4.search.value')) {
                $searchExists = true;
                $search = $request->input('columns.4.search.value');
                $filteredDatasQuery = $dataQuery->where(function($query1) use($search){
                                                    $query1->whereHas('type', function ($query1) use($search){
                                                        $query1->where('vehicle_types.name','LIKE',"%{$search}%");
                                                    });
                                                });
            }
            if ($request->input('columns.6.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('color', 'LIKE',"%{$request->input('columns.6.search.value')}%");
            }
            if ($request->input('columns.7.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('vehicle_model', 'LIKE',"%{$request->input('columns.7.search.value')}%");
            }
            if ($request->input('columns.8.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('make', 'LIKE',"%{$request->input('columns.8.search.value')}%");
            }
            if ($request->input('columns.9.search.value')) {
                $searchExists = true;
                $filteredDatasQuery = $dataQuery->where('slots', $request->input('columns.9.search.value'));
            }

            if (!$searchExists) {
                $filteredDatasQuery = $dataQuery;
            }


        if ($limit != -1) {
            $filteredDatas = $filteredDatasQuery->offset($start)
                ->limit($limit);
            if ($order != "zone" && $order != "type"){
                $filteredDatas = $filteredDatas->orderBy($order,$dir);
            }
        }

        $filteredDatas = $filteredDatasQuery->get();

            $totalFilteredData = $filteredDatasQuery->count();
//        }

        if($request->input('order.0.column') == 1){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->zone['name'];
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->zone['name'];
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 4){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->type['name'];
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->type['name'];
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }
        
        $data = array();
        if(!empty($filteredDatas)){
            foreach ($filteredDatas as $key => $filteredData){
                $nestedData['id']              = $filteredData->id;
                $nestedData['zone']            = (isset($filteredData->zone)) ? $filteredData->zone->name : null;
                $nestedData['name']            = $filteredData->name;
                $nestedData['number']          = $filteredData->number;
                $nestedData['type']            = (isset($filteredData->type)) ? $filteredData->type->name : null;
                $nestedData['customer_pickup'] = ($filteredData->customer_pickup == 1) ? 'Yes' : 'No';
                $nestedData['color']           = $filteredData->color;
                $nestedData['vehicle_model']   = $filteredData->vehicle_model;
                $nestedData['make']            = $filteredData->make;
                $nestedData['slots']           = $filteredData->slots;

                $vehicleStatus = $filteredData->status;
                $statusIcon = $vehicleStatus == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>';
                $statusTitle = $vehicleStatus == 1 ? 'Active' : 'Inactive';

                $permissions = Permission::whereIn('name', ['vehicle.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $nestedData['edit'] = '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('vehicle/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Vehicle"><i class="fa fa-pencil"></i></a></center>';
                }
                else $nestedData['edit'] = '';

                $permissions = Permission::whereIn('name', ['vehicle.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                   $nestedData['status'] = '<center><a href="#" class="vehicle-status action-items"  data-status="' . $filteredData->status . '" data-id="' . $filteredData->id . '" data-toggle="tooltip" data-placement="top" title=' . $statusTitle . '>' . $statusIcon . '</i></a></center>';
                } 
                else $nestedData['status'] = '';

                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFilteredData), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }

    function edit($id)
    {
        $logged_user = Sentinel::getUser();

        $vehicle = Vehicle::find($id);

        if (!$vehicle) {
            return redirect('vehicle/list')->with([
                'error' => true,
                'error.message' => 'Vehicle did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $zones = Zone::select('id')->where('manager_id', $logged_user->id)->lists('id')->toArray();

        if ($logged_user->inRole('zone-manager')) {
            if (!in_array($vehicle['zone_id'], $zones)) {
                return redirect('vehicle/list')->with([
                    'error' => true,
                    'error.message' => 'You do not have access to edit this! ',
                    'error.title' => 'Ooops !'
                ]);
            }
        }

        $activeZones = Zone::where('status', 1)->get();

        if ($logged_user->inRole('zone-manager')) {
            $vehicle_types = VehicleType::where('zone_access', 1)->where('status', 1)->get();
        } else {
            $vehicle_types = VehicleType::where('status', 1)->get();
        }
        return view('VehicleManage::edit')->with(['vehicle' => $vehicle, 'vehicle_types' => $vehicle_types, 'zones' => $activeZones, 'logged_in_user' => $logged_user]);
    }

    function update(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $vehicle = Vehicle::find($request->id);
        if (!$vehicle) {
            return redirect('vehicle/list')->with([
                'error' => true,
                'error.message' => 'Vehicle did not found ',
                'error.title' => 'Ooops !'
            ]);
        }

        $zone = 0;
        if ($logged_in_user->inRole('zone-manager')) {
            $zone = $logged_in_user->zone['id'];
        }
        if ($request->zone) {
            $zone = $request->zone;
        }

        $vehicle->number = $request->number;
        $vehicle->name = $request->name;
        $vehicle->zone_id = $zone;
//        $vehicle->vehicle_type_id = $request->type;
        $vehicle->customer_pickup = $request->customer_pickup or 0;
        $vehicle->color = $request->color;
        $vehicle->vehicle_model = $request->model;
        $vehicle->make = $request->make;
        $vehicle->slots = $request->slots;

        if ($vehicle->save()) {

            if (VehicleType::where('id', $request->type)->first()['is_bin']) {

                $stores = Store::all();
                $vehicles = Vehicle::all();

                foreach ($stores as $store) {
                    foreach ($vehicles as $vehicle) {
                        $check_bin = Bin::where('store_id', $store->id)->where('related_id', $vehicle->id)->where('bin_type', 'vehicle')->first();
                        if ($check_bin == null) {
                            Bin::create([
                                'store_id' => $store->id,
                                'related_id' => $vehicle->id,
                                'bin_type' => 'vehicle',
                                'status' => $store->status
                            ]);
                        }
                    }
                }
            }

            return redirect('vehicle/list')->with([
                'success' => true,
                'success.message' => 'Vehicle updated successfully!',
                'success.title' => 'Well Done!'
            ]);
        }

        return redirect('vehicle/list' . $request->id . 'edit')->with([
            'error' => true,
            'error.message' => 'Vehicle did not updated ',
            'error.title' => 'Ooops !'
        ]);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $vehicle = Vehicle::find($request->id);
            if ($vehicle) {
                $vehicle->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status(Request $request)
    {
        if ($request->ajax()) {
            $vehicle = Vehicle::findOrFail($request->id);
            $status = $request->status == 1 ? 0 : 1;
            if ($vehicle) {

                try {

                    \DB::beginTransaction();

                    if($status == 0){
                        $storeIds = Store::where('related_id', $request->id)->where('store_type', 'vehicle')->get()->pluck('id')->toArray();
                        $requests = RiderAssign::whereIn('store_id', $storeIds)->where('active', 1)->count();
                        //$stocksIds = Stock::whereIn('store_id', $storeIds)->get()->pluck('request_id')->toArray();
                        //$requests = CustomerRequest::whereIn('id', $stocksIds)->count();
                        if($requests){
                            return response()->json('You are unable to deactivate this vehicle due to it is assigned for a request', 404);
                        }

                        if ($vehicle->type->is_bin == 1 && $vehicle->type->zone_access == 0) {
                            $bins = Bin::whereIn('store_id', $storeIds)->get()->pluck('id')->toArray();
                            $stock = Stock::whereIn('bin_id', $bins)->count();
                            if ($stock > 0) {
                                return response()->json('Please clear the related bins to deactivate the vehicle', 404);
                            }

                            Bin::where('bin_type', 'vehicle')->whereIn('store_id', $storeIds)->update([
                                'status' => 0
                            ]);

                            Store::whereIn('id', $storeIds)->update(['status' => 0]);

                        } else if ($vehicle->type->is_bin == 0 && $vehicle->type->zone_access == 1) {

                            $storeIds = Store::where('related_id', $request->id)->where('store_type', 'vehicle')->get()->pluck('id')->toArray();
                            $riderVehicle = RiderVehicle::where('vehicle_id', $request->id)->where('status', 1)->count();
                            if($riderVehicle > 0){
                                return response()->json('You are unable to deactivate this vehicle due to it is assigned for a request', 404);
                            }
                            $requests = RiderAssign::whereIn('store_id', $storeIds)->where('active', 1)->count();
                            if($requests > 0){
                                return response()->json('You are unable to deactivate this vehicle due to it is assigned for a request', 404);
                            }
                            $bins = Bin::whereIn('store_id', $storeIds)->get()->pluck('id')->toArray();
                            $stock = Stock::whereIn('bin_id', $bins)->count();
                            if ($stock > 0) {
                                return response()->json('Please clear the related bins to deactivate the vehicle', 404);
                            }

                            Store::whereIn('id', $storeIds)->update(['status' => 0]);

                        }

                    } else {
                        if ($vehicle->type->is_bin == 1 && $vehicle->type->zone_access == 0) {
                            $storeIds = Store::where('related_id', $request->id)->where('store_type', 'vehicle')->get()->pluck('id')->toArray();

                            Bin::where('bin_type', 'vehicle')->whereIn('store_id', $storeIds)->update([
                                'status' => 1
                            ]);

                            Store::whereIn('id', $storeIds)->update(['status' => 1]);
                        } else if ($vehicle->type->is_bin == 0 && $vehicle->type->zone_access == 1) {

                            $storeIds = Store::where('related_id', $request->id)->where('store_type', 'vehicle')->get()->pluck('id')->toArray();

                            Store::whereIn('id', $storeIds)->update(['status' => 1]);

                        }
                    }

                    $vehicle->status = $status;
                    $vehicle->save();
                    $msg = $request->status == 1 ? 'You have Deactivated Successfully' : 'You have Activated Successfully';

                    \DB::commit();

                    return $msg;

                } catch (\PDOException $exception) {
                    \Log::error("VEHICLE ACTIVATE/DEACTIVATE ERROR = " . $exception->getMessage());
                    \DB::rollBack();
                    return response()->json('Something went wrong');
                }

            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}
