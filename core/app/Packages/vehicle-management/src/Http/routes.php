<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'VehicleManage\Http\Controllers', 'prefix' => 'vehicle'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'vehicle.list', 'uses' => 'VehicleController@index'
        ]);

        Route::get('json/list', [
            'as' => 'vehicle.list', 'uses' => 'VehicleController@jsonList'
        ]);

        Route::get('add', [
            'as' => 'vehicle.add', 'uses' => 'VehicleController@add'
        ]);

        Route::get('{id}/edit', [
            'as' => 'vehicle.edit', 'uses' => 'VehicleController@edit'
        ]);


        /**
         * POST Routes
         */
        Route::post('list/data', [
            'as' => 'vehicle.list', 'uses' => 'VehicleController@listData'
        ]);
        
        Route::post('store', [
            'as' => 'vehicle.add', 'uses' => 'VehicleController@store'
        ]);

        Route::post('update', [
            'as' => 'vehicle.edit', 'uses' => 'VehicleController@update'
        ]);

        Route::post('delete', [
            'as' => 'vehicle.delete', 'uses' => 'VehicleController@delete'
        ]);

        Route::post('status', [
            'as' => 'vehicle.status', 'uses' => 'VehicleController@status'
        ]);

    });
});


