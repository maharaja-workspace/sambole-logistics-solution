@extends('layouts.back.master') @section('current_title','EDIT VEHICLE')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
    <style>
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            margin-top: 5%;
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input~.checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a green background */
        .container input:checked~.checkmark {
            background-color: #18a689;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked~.checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 7px;
            width: 7px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Vehicle Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Edit Vehicle </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row" style="margin-bottom: 100px;">
        <div class="col-lg-12">
            <br>
            <form method="POST" class="form-horizontal" id="form" action="{{ url('vehicle/update') }}">
                {!!Form::token()!!}
                @if($vehicle->vehicle_type_id == 1)
                <div class="form-group"><label class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-3">
{{--                        <select class="form-control" id="v_type" name="type">--}}
{{--                            <option value="">Select One</option>--}}
{{--                            @foreach($vehicle_types as $vehicle_type)--}}

{{--                                @if($vehicle->vehicle_type_id == $vehicle_type->id)--}}
{{--                                    <option selected value="{{ $vehicle_type->id }}">{{ $vehicle_type->name }}</option>--}}
{{--                                @else--}}
{{--                                    <option value="{{ $vehicle_type->id }}" data-value="{{ $vehicle_type->zone_access }}">--}}
{{--                                        {{ $vehicle_type->name }}</option>--}}
{{--                                @endif--}}

{{--                            @endforeach--}}
{{--                        </select>--}}
                        <label class="control-label">{{$vehicle->type->name}}</label>
                    </div>
                </div>
                @endif

                @if(Sentinel::getUser()->inRole('admin') && $vehicle->vehicle_type_id == 1)
                    <div class="form-group"><label class="col-sm-2 control-label">Zone</label>
                        <div class="col-sm-3">
                            <label class="control-label">{{isset($vehicle->zone->name) ? $vehicle->zone->name : "-"}}</label>
                        </div>
                    </div>
                @endif

                @if($vehicle->vehicle_type_id == 2)
                <div class="form-group" id="zones"><label class="col-sm-2 control-label">Zone Name <span
                                class="required">*</span></label>
                    <div class="col-sm-3">

                        @if($logged_in_user->inRole('zone-manager'))
                            @foreach($zones as $zone)
                                @if($zone->id == $logged_in_user->zone['id'])
                                    <input class="form-control" readonly value="{{ $zone->name }}">
                                @endif
                            @endforeach
                        @else
                            <select class="form-control" name="zone">
                                <option value="">Select a zone</option>
                                @foreach($zones as $zone)
                                    @if($vehicle->zone_id == $zone->id)
                                        <option selected value="{{ $zone->id }}">{{ $zone->name }}</option>
                                    @else
                                        <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        @endif

                    </div>
                </div>
                @endif

                <div class="form-group"><label class="col-sm-2 control-label">Vehicle Name <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="name" class="form-control"
                                                 value="{{old('name', $vehicle->name)}}"></div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Vehicle Number <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="number" class="form-control"
                                                 value="{{old('number', $vehicle->number)}}"></div>
                </div>

                @if($vehicle->vehicle_type_id == 2)
                     <div class="form-group"><label class="col-sm-2 control-label">Customer Pick up Truck
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-3"><label class="container">
                            @if($vehicle->customer_pickup == 1)
                                <input type="checkbox" checked id="vehicle1" name="customer_pickup" value="1"><span
                                    class="checkmark"></span></label>
                            @else
                                <input type="checkbox" id="vehicle1" name="customer_pickup" value="1"><span
                                    class="checkmark"></span></label>
                            @endif
                        </div>
                    </div>
                    <br>
                @endif

                <div class="form-group"><label class="col-sm-2 control-label">Color <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="color" class="form-control"
                                                 value="{{old('color', $vehicle->color)}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Model <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="model" class="form-control"
                                                 value="{{old('model', $vehicle->vehicle_model)}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Make <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="make" class="form-control"
                                                 value="{{old('make', $vehicle->make)}}"></div>
                </div>

                <input type="hidden" value="{{$vehicle->customer_pickup}}" id="isCustomerPickupTruck">

                @if($vehicle->customer_pickup == 1)
                    <div id="addLocation" class="form-group"><label class="col-sm-2 control-label">Slots</label>
                        <div class="col-sm-3"><input type="text" id="slots" name="slots"
                                                     value="{{old('slots', $vehicle->slots)}}" class="form-control">
                        </div>
                    </div>
                @else
                    <div id="addLocation" class="form-group" style="display: none"><label class="col-sm-2 control-label">Slots</label>
                        <div class="col-sm-3"><input type="text" id="slots" name="slots" class="form-control">
                        </div>
                    </div>
                @endif
                {!! Form::hidden('id', $vehicle->id) !!}

                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button type="button" class="btn btn-danger" style="float: right;"
                                onclick="location.reload();">Cancel
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')
    {{-- <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");

            $(function () {
                $("#vehicle1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#addLocation").show();
                        //$("#noLocation").hide();
                    } else {
                        $("#addLocation").hide();
                        //$("#noLocation").show();
                    }
                });
            });

            $("#form").validate({
                rules: {
                    zone: {
                        required: true,
                        // lettersonly: true,
                        // maxlength: 20
                    },
                    number: {
                        required: true,
                        // lettersonly: true,
                        // maxlength: 20
                    },
                    name: {
                        required: true,
                        // maxlength: 20
                    },
                    type: {
                        required: true,
                        //lettersonly: false,
                        //maxlength: 20
                    },
                    color: {
                        required: true
                    },
                    model: {
                        required: true
                    },
                    make: {
                        required: true
                    },
                    slots: {
                        integer: true
                    }
                    /* username:{
                         required: true,
                         email: true
                     },
                     password:{
                         required: true,
                         minlength: 6
                     },
                     password_confirmation:{
                         required: true,
                         minlength: 6,
                         equalTo: '#password'
                     },
                     "roles[]":{
                         required: true
                     }*/
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("#v_type").change(function(){
                if($(this).find(':selected').data('value') === 0){
                    $("#zones").fadeOut().css('display', 'none');
                }
                else{
                    $("#zones").fadeIn().css('display', 'block');
                }
            });
        });


    </script>
@stop