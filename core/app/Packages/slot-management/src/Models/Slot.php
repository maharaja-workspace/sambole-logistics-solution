<?php
namespace SlotManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slot extends Model{
    use SoftDeletes;

    protected $table = 'slots';

    protected $fillable = ['store_id', 'slot_numbers', 'status'];
}