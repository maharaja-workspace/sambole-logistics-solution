<?php

namespace SlotManage\Http\Controllers;

use App\Http\Controllers\Controller;
use VehicleTypeManage\Models\VehicleType;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Sentinel;
use Response;


class SlotController extends Controller
{

    function index()
    {
        return view('VehicleTypeManage::list');
    }

    function add()
    {
        return view('VehicleTypeManage::add');
    }

    function store(Request $request)
    {
//        dd($request->all());
        if ($request->has('name')) {
            $vehicle_type = VehicleType::create([
                'name' => $request->name,
                'description' => $request->description,
                'is_bin' => $request->is_bin or 0,
                'zone_access' => $request->zone_access or 0,
            ]);
        }

        if ($vehicle_type) {
            return redirect('vehicle-type/add')->with(['success' => true,
                'success.message' => 'Vehicle Type Created successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('vehicle-type/add')->with(['error' => true,
            'error.message' => 'Vehicle Type did not created ',
            'error.title' => 'Ooops !']);
    }

    public function jsonList(Request $request)
    {
        $jsonList = array();
        $i = 1;
        foreach (VehicleType::all() as $vehicle_type) {

            $dd = array();

            array_push($dd, $i);

            array_push($dd, $vehicle_type->name);

            array_push($dd, $vehicle_type->description);

            if ($vehicle_type->is_bin == 1) {
                array_push($dd, 'Yes');
            } else {
                array_push($dd, 'No');
            }

            if ($vehicle_type->zone_access == 1) {
                array_push($dd, 'Yes');
            } else {
                array_push($dd, 'No');
            }

            $permissions = Permission::whereIn('name', ['vehicle.type.edit', 'admin'])->where('status', '=', 1)->lists('name');
            if (Sentinel::hasAnyAccess($permissions)) {
                array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\'' . url('vehicle-type/' . $vehicle_type->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Vehicle Type"><i class="fa fa-pencil"></i></a>');
            }

            $permissions = Permission::whereIn('name', ['vehicle.type.delete', 'admin'])->where('status', '=', 1)->lists('name');
            if (Sentinel::hasAnyAccess($permissions)) {
                array_push($dd, '<a href="#" class="zone-delete" data-id="' . $vehicle_type->id . '" data-toggle="tooltip" data-placement="top" title="Delete Vehicle Type"><i class="fa fa-trash-o"></i></a>');
            }

            array_push($jsonList, $dd);
            $i++;
        }
        return Response::json(array('data' => $jsonList));

    }

    function edit($id)
    {
        $vehicle_type = VehicleType::find($id);

        if (!$vehicle_type){
            return redirect('vehicle-type/')->with([ 'error' => true,
                'error.message'=> 'Vehicle Type did not found ',
                'error.title' => 'Ooops !']);
        }

        return view('VehicleTypeManage::edit')->with(['vehicle_type' => $vehicle_type]);
    }

    function update(Request $request)
    {
        $vehicle_type = VehicleType::findOrFail($request->id);
        if (!$vehicle_type) {
            return redirect('vehicle-type/list')->with(['error' => true,
                'error.message' => 'Vehicle Type did not found ',
                'error.title' => 'Ooops !']);
        }

        $vehicle_type->name = $request->name;
        $vehicle_type->description = $request->description;
        $vehicle_type->is_bin = $request->is_bin or 0;
        $vehicle_type->zone_access = $request->zone_access or 0;

        if ($vehicle_type->save()) {
            return redirect('vehicle-type/list')->with(['success' => true,
                'success.message' => 'Vehicle Type updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('vehicle-type/' . $request->id . 'edit')->with(['error' => true,
            'error.message' => 'Vehicle Type did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        VehicleType::findOrFail($request->id)->delete();

        return response()->json('success');
    }
}