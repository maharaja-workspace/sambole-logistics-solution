@extends('layouts.back.master') @section('current_title','All User')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

        /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Zone</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Vehicle Type List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop

@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
         onclick="location.href = '{{url('vehicle-type/add')}}';">
        <p class="plus">+</p>
    </div>

    <div class="row">
        <div class="col-lg-12 margins">
            <h2>Vehicle Type Management</h2>
            <div class="ibox-content">

{{--                <div class="row">--}}

{{--                    <div class="col-sm-1 align-middle">--}}
{{--                        <h3><b>Filter: </b></h3>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <select class="form-control" id="status">--}}
{{--                            <option value="all">All</option>--}}
{{--                            <option value="active">Active</option>--}}
{{--                            <option value="pending">Pending</option>--}}
{{--                            <option value="deleted">Deleted</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 margins">

            <div class="ibox-content">
                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Bin</th>
                        <th>Zone Access</th>
                        <th style="width: 2%;">Edit</th>
                        <th style="width: 2%;">Delete</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@stop
@section('js')

    <script>
        $(document).ready(function () {
            table = $('#example1').dataTable({
                "ajax": '{{url('vehicle-type/json/list/')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'User List', className: 'btn-sm'},
                    {extend: 'pdf', title: 'User List', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "autoWidth": false,
                "order": [[1, "asc"]],
                "columnDefs": [
                    {"targets": [0], "orderable": false}],

            });

            table.on('draw.dt', function () {
                $('.zone-delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAlert(id);

                });
            });

            function confirmAlert(id) {
                if (confirm("Are you sure ?")) {
                    $.ajax({
                        method: "POST",
                        url: '{{url('vehicle-type/delete')}}',
                        data: {'id': id, '_token': '{{ csrf_token() }}'}
                    })
                        .done(function (msg) {
                            table.fnReloadAjax();
                        });
                }
            }
        });
    </script>
@stop