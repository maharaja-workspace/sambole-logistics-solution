@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
    <style>
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            margin-top: 5%;
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a green background */
        .container input:checked ~ .checkmark {
            background-color: #18a689;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 7px;
            width: 7px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>User</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Vehicle Type </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 margins">
            <div class="ibox-content">
                <h2>Vehicle Type Creation</h2>

                <form method="post" class="form-horizontal" id="form" action="{{ url('vehicle-type/store') }}">
                    {!!Form::token()!!}


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name <span class="required">*</span></label>
                        <div class="col-sm-3">
                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-3"><input type="text" name="description" class="form-control"
                                                     value="{{old('description')}}"></div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">Is Bin?</label>
                        <div class="col-sm-3"><input type="checkbox" name="is_bin" class="form-control checkmark"
                                                     value="{{old('is_bin', 1)}}"></div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">Zone Access?</label>
                        <div class="col-sm-3"><input type="checkbox" name="zone_access" class="form-control checkmark"
                                                     value="{{old('zone_access', 1)}}"></div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-3">
                            <button class="btn btn-info" type="submit">Done</button>
                            <button type="button" class="btn btn-danger" style="float: right;"
                                    onclick="location.reload();">Cancel
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");


            $("#form").validate({
                rules: {
                    name: {
                        required: true,
                        lettersonly: true
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop