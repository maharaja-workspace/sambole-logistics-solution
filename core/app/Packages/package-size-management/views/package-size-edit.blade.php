@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Package Sizes</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Package Size </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form" action="{{url("package-sizes/".$packageSize->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Package Size Code <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled name="package_size__code" class="form-control" value="{{$packageSize->id}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Package Size Name <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="package_size_name" class="form-control" value="{{$packageSize->name}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><textarea type="text" name="description" class="form-control" >{{$packageSize->description}}</textarea></div>
                </div>
               <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <a class="btn btn-danger" type="button" style="float: right;" href="{{url('package-sizes')}}">Cancel</a>
                    </div>
                </div>

                

            </form>

        </div>
    </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

           $("#form").validate({
               rules: {
                   package_size_name: {
                       required: true,
                       maxlength: 20,
                       remote:function(){
                           var checkit={
                               type: "POST",
                               url:  "{{url('package-sizes/verify-update/name')}}",
                               dataType: "json",
                               data: {
                                   "_token" : "{{csrf_token()}}",
                                   "id" : {{$packageSize->id}}
                               }
                           };
                           return checkit;
                       }
                   }
               },
               messages:
                   {
                       package_size_name:
                           {
                               remote: jQuery.validator.format("{0} is already taken.")
                           }
                   },
               submitHandler: function(form) {
                   form.submit();
               }
           });
     });


 </script>
 @stop