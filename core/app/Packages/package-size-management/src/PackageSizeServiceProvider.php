<?php

namespace PackageSizeManage;

use Illuminate\Support\ServiceProvider;

class PackageSizeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'PackageSizeManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackageSizeManage', function(){
            return new PackageSizeManage;
        });
    }
}
