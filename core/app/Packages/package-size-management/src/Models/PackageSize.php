<?php
namespace PackageSizeManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageSize extends Model{
    use SoftDeletes;
    protected $fillable = ['name', 'description'];
}