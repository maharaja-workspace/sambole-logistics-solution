<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'PackageSizeManage\Http\Controllers'], function(){
        Route::post('package-sizes/list/data', 'PackageSizeController@listData')->name('package-sizes.index');
        Route::post('package-sizes/verify/{fieldName}', 'PackageSizeController@verify')->name('package-sizes.verify');
        Route::post('package-sizes/status-toggle', 'PackageSizeController@toggleStatus')->name('package-sizes.update');
        Route::post('package-sizes/verify-update/{fieldName}', 'PackageSizeController@verifyUpdate')->name('package-sizes.update');
        Route::resource('package-sizes', 'PackageSizeController');
    });

});


