<?php
namespace PackageSizeManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageSizeManage\Models\PackageSize;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use Datatables;

class PackageSizeController extends Controller {

    function index(){
        return view( 'PackageSizeManage::package-sizes');
    }


    public function listData(Request $request)
    {
        $permissions = Permission::whereIn('name',['package-sizes.edit','admin'])->where('status','=',1)->lists('name');

        return Datatables::of(PackageSize::query())
            ->addColumn("edit", function ($filteredItem) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<center><a href="#" class="blue" onclick="window.location.href=\''.url('package-sizes/'.$filteredItem->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit package size"><i class="fa fa-pencil"></i></a></center>';
                }
            })
            ->addColumn("status", function ($filteredItem) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    if($filteredItem->status == 1){
                        return '<center><a href="javascript:void(0)" form="noForm" class="blue package-size-status-toggle" data-id="'.$filteredItem->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></></center>';
                    }
                    else if($filteredItem->status == 0){
                        return '<center><a href="javascript:void(0)" class="blue package-size-status-toggle " data-id="'.$filteredItem->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></></center>';
                    }
                }
            })
            ->make(true);
    }

    function create(){
        return view('PackageSizeManage::package-size-add');
    }

    function store(Request $request){
       $packageSize =  PackageSize::create([
           'name' => $request->package_size_name,
           'description' => $request->description
        ]);

       if ($packageSize){
           return redirect('package-sizes/create')->with([ 'success' => true,
               'success.message'=> 'Package Size Created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('package-sizes/create')->with([ 'error' => true,
            'error.message'=> 'Package Size did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $packageSize = PackageSize::find($id);
        return view('PackageSizeManage::package-size-edit')->with('packageSize', $packageSize);
    }

    function update(Request $request, $id){
        $packageSize = PackageSize::find($id);
        if (!$packageSize){
            return redirect('package-sizes/')->with([ 'error' => true,
                'error.message'=> 'Package size did not found ',
                'error.title' => 'Ooops !']);
        }

        $packageSize->name = $request->package_size_name;
        $packageSize->description = $request->description;

        if ($packageSize->save()){
            return redirect('package-sizes')->with([ 'success' => true,
                'success.message'=> 'Pacage size updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('package-sizes/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Pacage size  did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $packageSize = PackageSize::find($id);
            if($packageSize){
                $packageSize->delete();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    function verify($verify, Request $request){
        if($verify == "name"){
            if (PackageSize::where('name', $request->package_size_name)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function verifyUpdate($verify, Request $request){
        if($verify == "name"){
            if (PackageSize::where('name', $request->package_size_name)->where('id', "!=", $request->id)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function  toggleStatus(Request $request){
        $size = PackageSize::find($request->id);
        $msg = "";
        if ($size){
            if ($size->status == 1){
                $size->status = 0;
                $msg = "You have Deactivated Successfully";
            }else if ($size->status == 0){
                $size->status = 1;
                $msg = "You have Activated Successfully";
            }
            $size->save();
            return $msg;
        }
    }
}