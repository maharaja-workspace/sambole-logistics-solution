<?php
namespace RiderManage\Models;

use DailyPlanManage\Models\RiderAssign;
use DailyPlanManage\Models\RiderVehicle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rider extends Model{
    use SoftDeletes;

    protected $fillable = ['user_id', 'name', 'nic', 'license_number', 'mobile', 'zone', 'username', 'password'];

    public function zone()
    {
        return $this->belongsTo('ZoneManage\Models\Zone','zone');
    }

    public function zone_data()
    {
        return $this->belongsTo('ZoneManage\Models\Zone','zone', 'id');
    }

    public function user()
    {
        return $this->hasOne('UserManage\Models\User', 'id', 'user_id');
    }

    public function rider_vehicle()
    {
        return $this->hasOne(RiderVehicle::class)->latest();
    }

    public function rider_assign()
    {
        return $this->hasMany(RiderAssign::class, 'rider_id', 'id');
    }

}