<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'RiderManage\Http\Controllers', 'prefix' => 'rider'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'rider.list', 'uses' => 'RiderController@index'
        ]);

        Route::get('json/list', [
            'as' => 'rider.list', 'uses' => 'RiderController@jsonList'
        ]);

        Route::get('add', [
            'as' => 'rider.add', 'uses' => 'RiderController@add'
        ]);

        Route::get('{id}/edit', [
            'as' => 'rider.edit', 'uses' => 'RiderController@edit'
        ]);


        /**
         * POST Routes
         */
        Route::post('list/data', [
            'as' => 'rider.list', 'uses' => 'RiderController@listData'
        ]);
        
        Route::post('store', [
            'as' => 'rider.store', 'uses' => 'RiderController@store'
        ]);

        Route::post('update', [
            'as' => 'rider.update', 'uses' => 'RiderController@update'
        ]);

        Route::post('delete', [
            'as' => 'rider.delete', 'uses' => 'RiderController@delete'
        ]);
        Route::post('status', [
            'as' => 'rider.status', 'uses' => 'RiderController@status'
        ]);

    });
});


