<?php

namespace RiderManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use Response;
use RiderManage\Models\Rider;
use Sentinel;
use UserManage\Models\User;
use Yajra\Datatables\Datatables;
use ZoneManage\Models\Zone;

class RiderController extends Controller
{

    function index()
    {
        return view('RiderManage::list');
    }

    function add()
    {
        $logged_in_user = Sentinel::getUser();

        $activeZones = Zone::where('status', 1)->get();

        return view('RiderManage::add')->with(['zones' => $activeZones, 'logged_in_user' => $logged_in_user]);
    }

    function store(Request $request)
    {        
        $email_submitted = User::where('email', $request->get('email'))->first();
        if ($email_submitted) {
            return redirect('rider/add')->with(['error' => true,
                'error.message' => 'Email Already Exist!',
                'error.title' => 'Try Again!']);
        }

        /*
         * username == email so no need to check this
         */
        /*$username_submitted = User::where('username', $request->get('username'))->first();
        if ($username_submitted) {
            return redirect('rider/add')->with(['error' => true,
                'error.message' => 'Username Already Exist!',
                'error.title' => 'Try Again!']);
        }*/

        $supervisor = User::find(1);
        $credentials = [
            'first_name' => $request->name,
            'email' => $request->email,
            'confirmed' => 1,
           // 'username' => $request->username,
            'username' => $request->email,
            'password' => $request->password
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $user->app_password = md5($request->password);
        $user->save();

        $user->makeChildOf($supervisor);
        $role = Sentinel::findRoleById(5);
        $role->users()->attach($user);

        $user->makeChildOf($supervisor);
        if ($request->has('name')) {
            $rider = Rider::create([
                'user_id' => $user->id,
                'name' => $request->name,
                'nic' => $request->nic,
                'license_number' => $request->license_number,
                'mobile' => $request->mobile,
                'zone' => $request->zone
            ]);
        }

        if ($rider) {
            return redirect('rider/add')->with(['success' => true,
                'success.message' => 'Rider Created successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('rider/add')->with(['error' => true,
            'error.message' => 'Rider did not created ',
            'error.title' => 'Ooops !']);
    }
    public function listData(Request $request)
    {
        $logged_user = Sentinel::getUser();

        $queryBuild = Rider::with('user', 'zone');

        if($logged_user->inRole('zone-manager')){
            $queryBuild = $queryBuild->where('zone', $logged_user->zone['id']);
        }

        $permissions = Permission::whereIn('name', ['rider.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');

        return Datatables::of($queryBuild)
            ->addColumn("edit", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<a href="#!" class="blue" onclick="window.location.href=\'' . url('rider/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Zone"><i class="fa fa-pencil"></i></a> ';
                }
            })
            ->addColumn("status", function ($filteredData) use($permissions){
                $userStatus = $filteredData->user()->first()['status'];
                $userId = $filteredData->user()->first()['id'];
                if(Sentinel::hasAnyAccess($permissions)){
                    if($userStatus == 1){
                        return '<a href="#!" class="blue rider-active " data-id="' . $userId . '"  data-status="'.$userStatus.'" title="Deactivate Zone"><i class="fa fa-toggle-on"></i></a>';
                    }
                    else if($userStatus == 0){
                        return  '<a href="#!" class="blue rider-active " data-id="' . $userId . '"  data-status="'.$userStatus.'" title="Activate Zone"><i class="fa fa-toggle-off"></i></a>';
                    }
                }
            })
            ->make(true);

        $columns = array( 
            0 => 'id', 
            1 => 'name', 
            2 => 'nic', 
            3 => 'license_number',
            4 => 'mobile',
            5 => 'name',
            6 => 'name',
        );
        
        $logged_user = Sentinel::getUser();
    
        $dataQuery = Rider::with(['user' => function($query){ $query->select('id','username');}, 
                                  'zone' => function($query){ $query->select('id','name');},
                                ]);

        if($logged_user->inRole('zone-manager')){
            $dataQuery = $dataQuery->where('zone', $logged_user->zone['id']);
        }
        else $dataQuery = $dataQuery;
           
        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value'))){   
            $filteredDatas = $dataQuery->offset($start)
                                            ->limit($limit)
                                            ->orderBy($order,$dir)
                                            ->get();
        }
        else{
            $search = $request->input('search.value'); 
            $filteredDatasQuery = $dataQuery->where(function($or) use($search){
                                                $or->orWhere(function($query1) use($search){
                                                    $query1->whereHas('user', function ($query1) use($search){
                                                        $query1->where('users.username','LIKE',"%{$search}%");
                                                    });
                                                })
                                                ->orWhere(function($query2) use($search){
                                                    $query2->whereHas('zone', function ($query2) use($search){
                                                        $query2->where('zones.name','LIKE',"%{$search}%");
                                                    });
                                                })
                                                ->orWhere('id', $search)
                                                ->orWhere('name', 'LIKE',"%{$search}%")
                                                ->orWhere('nic', $search)
                                                ->orWhere('license_number', $search)
                                                ->orWhere('mobile', $search);                        
                                            });

            $filteredDatas = $filteredDatasQuery->offset($start)
                                                ->limit($limit)
                                                ->orderBy($order,$dir)
                                                ->get();

            $totalFilteredData = $filteredDatasQuery->count();
        }

        if($request->input('order.0.column') == 5){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->zone()->first()['name'];
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->zone()->first()['name'];
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 6){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->user['username'];
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->user['username'];
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }
    
        $data = array();
        if(!empty($filteredDatas)){
            foreach ($filteredDatas as $key => $filteredData){
                $nestedData['id']             = $filteredData->id;
                $nestedData['name']           = $filteredData->name;
                $nestedData['nic']            = $filteredData->nic;
                $nestedData['license_number'] = $filteredData->license_number;
                $nestedData['mobile']         = $filteredData->mobile;
                $nestedData['zone']           = $filteredData->zone()->first()['name'];
                $nestedData['user']           = (isset($filteredData->user)) ? $filteredData->user->username : null;
               
                $userStatus = $filteredData->user()->first()['status'];
                $statusIcon = $userStatus == 1 ? '<i class="fa fa-toggle-on"></i>' : '<i class="fa fa-toggle-off"></i>';
                $statusTitle = $userStatus == 1 ? 'Active' : 'Inactive';

                $permissions = Permission::whereIn('name', ['rider.edit', 'admin', 'zone-manager'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $nestedData['edit'] = '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('rider/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Rider"><i class="fa fa-pencil"></i></a></center>';
                }
                else $nestedData['edit'] = '';

                $permissions = Permission::whereIn('name', ['rider.status', ])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $nestedData['status'] = '<center><a href="#" class="rider-active" data-status="'.$userStatus.'" data-id="' . $filteredData->user_id . '" data-toggle="tooltip" data-placement="top" title='.$statusTitle.'>'.$statusIcon.' </a></center>';
                }
                else $nestedData['status'] = '';
                
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFilteredData), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }


    function edit($id)
    {
        $logged_in_user = Sentinel::getUser();

        $rider = Rider::find($id);

        if (!$rider) {
            return redirect('rider/list')->with(['error' => true,
                'error.message' => 'Rider did not found ',
                'error.title' => 'Ooops !']);
        }

        $activeZones = Zone::where('status', 1)->get();

        return view('RiderManage::edit')->with(['rider' => $rider, 'zones' => $activeZones, 'logged_in_user' => $logged_in_user]);
    }

    function update(Request $request)
    {
        $rider = Rider::find($request->id);
        $email_submitted = User::where('id', '!=', $rider->user_id)->where('email', $request->get('email'))->first();
        if ($email_submitted) {
            return redirect('rider/' . $request->id . '/edit')->with(['error' => true,
                'error.message' => 'Email Already Exist!',
                'error.title' => 'Try Again!']);
        }
        /*
         * username == email so no need to check this
         */
        /*$username_submitted = User::where('id', '!=', $rider->user_id)->where('username', $request->get('username'))->first();
        if ($username_submitted) {
            return redirect('rider/' . $request->id . '/edit')->with(['error' => true,
                'error.message' => 'Username Already Exist!',
                'error.title' => 'Try Again!']);
        }*/

        $user = User::where('id', $rider->user_id)->first();
        $credentials = [
            'first_name' => $request->name,
            'email' => $request->email,
            'username' => $request->email,
            //'password' => $request->password
        ];
        if (isset($request->new_password))
            $credentials = ['password' => $request->password];

        Sentinel::update($user, $credentials);

        if (isset($request->new_password)){
            $user->app_password = md5($request->password);
            $user->save();
        }

        if (!$rider) {
            return redirect('rider/list')->with(['error' => true,
                'error.message' => 'Rider did not found ',
                'error.title' => 'Ooops !']);
        }

        $rider->name = $request->name;
        $rider->nic = $request->nic;
        $rider->license_number = $request->license_number;
        $rider->mobile = $request->mobile;
        $rider->zone = $request->zone;

        if ($rider->save()) {
            return redirect('rider/list')->with(['success' => true,
                'success.message' => 'Rider updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('rider/list' . $request->id . 'edit')->with(['error' => true,
            'error.message' => 'Rider did not updated ',
            'error.title' => 'Ooops !']);
    }

    public
    function delete(Request $request)
    {
        if ($request->ajax()) {
            $rider = Rider::find($request->id);
            if ($rider) {
                $rider->delete();
                return response()->json('success');
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }


    public function status(Request $request){

        if ($request->ajax()) {
            $user = User::findOrFail($request->id);
            if ($user) {
                $user->status = $request->status == 1 ? 0 : 1;
                $user->save();
                $msg = $request->status == 1 ? 'You have Deactivated Successfully' : 'You have Activated Successfully';
                return response()->json($msg);
            }
            return response()->json('not found');
        }
        return response()->json('invalid');
    }
}