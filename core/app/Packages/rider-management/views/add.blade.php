@extends('layouts.back.master') @section('current_title','NEW RIDER')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Rider Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New Rider </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row" style="margin-bottom: 100px;">
        <div class="col-lg-12">
            <br>
            <form method="POST" class="form-horizontal" id="form" action="{{ url('rider/store') }}">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">Rider Name <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="name" class="form-control"
                                                 value="{{old('name')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">NIC <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="nic" class="form-control" value="{{old('nic')}}">
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">License number <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="license_number" class="form-control"
                                                 value="{{old('license_number')}}">
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Mobile number <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="mobile" class="form-control"
                                                 value="{{old('mobile')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Zone <span
                                class="required">*</span></label>
                    <div class="col-sm-3">
                        @if($logged_in_user->inRole('zone-manager'))
                            @foreach($zones as $zone)
                                @if($zone->id == $logged_in_user->zone['id'])
                                    <input class="form-control" type="text" readonly value="{{ $zone->name }}">
                                    <input class="form-control" name="zone" type="hidden" value="{{ $zone->id }}">
                                @endif
                            @endforeach
                        @else
                            <select class="form-control" name="zone">
                                <option value="">Choose</option>
                                @foreach($zones as $zone)
                                    <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                @endforeach
                            </select>
                        @endif

                    </div>
                </div>

                <div class="form-group"><label class="col-sm-2 control-label">Email <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="email" name="email" class="form-control"
                                                 value="{{old('email')}}"></div>
                </div>
                {{--<div class="form-group"><label class="col-sm-2 control-label">Username <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="username" class="form-control"
                                                 value="{{old('username')}}"></div>
                </div>--}}
                <div class="form-group"><label class="col-sm-2 control-label">Password <span
                                class="required">*</span></label>
                    <div class="col-sm-3"><input type="password" name="password" class="form-control"
                                                 value="{{old('password')}}"></div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Done</button>
                        <button class="btn btn-danger" type="button" onclick="location.reload();">Cancel</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The first name can only consist of alphabetical & underscore");

            /**
             * Todo: should move this to a common js.
             * */

            jQuery.validator.addMethod("nic_sl", function (value, element) {
                if(value.length === 10){
                    return this.optional(element) || /([1-9 ])([0-9]{8})([V])+$/i.test(value);

                }else if(value.length === 12){
                    return this.optional(element) || /([1-9 ])([0-9 ]{11})+$/i.test(value);
                }else{
                    return false;
                }

            }, "Please enter a valid NIC number.");

            jQuery.validator.addMethod("license_sl", function (value, element) {
                if(value.length === 8){
                    return this.optional(element) || /([B])([0-9]{7})+$/i.test(value);
                }else{
                    return false;
                }
            }, "Please enter a valid license number.");

            jQuery.validator.addMethod("mobile_sl", function (value, element) {
                if(value.length === 11){
                    return this.optional(element) || /([9])([4])([1-9])([0-9]{8})+$/.test(value);
                }else{
                    return false;
                }
            }, "Please enter a valid SL mobile number. Ex 94XXXXXXXXX");
            jQuery.validator.addMethod("email_r", function (value, element) {
                return this.optional(element) || /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            }, "Please enter a valid email address.");



            $("#form").validate({
                rules: {
                    rider_name: {
                        required: true,
                        lettersonly: true,
                        // maxlength: 10
                    },
                    nic: {
                        required: true,

                        nic_sl:true,
                        //maxlength: 10
                    },
                    license_number: {
                        required: true,
                        license_sl:true

                    },
                    mobile: {
                        required: true,
                        //maxlength: 10,
                        integer: true,
                        mobile_sl:true,

                    },
                    zone: {
                        required: true,
                    },
                    email: {
                         required: true,
                        email_r:true,
                    },
                    /*username: {
                        required: true,
                        // maxlength: 20,
                    },*/
                    password: {
                        required: true
                    },
                    /*username:{
                        required: true,
                        email: true
                    },
                    password:{
                        required: true,
                        minlength: 6
                    },
                    password_confirmation:{
                        required: true,
                        minlength: 6,
                        equalTo: '#password'
                    },
                    "roles[]":{
                        required: true
                    }*/
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop