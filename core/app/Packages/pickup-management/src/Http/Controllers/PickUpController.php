<?php

namespace PickUpManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use CityManage\Models\City;
use DailyPlanManage\Models\RiderAssign;
use DailyPlanManage\Models\RiderVehicle;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use Illuminate\Http\Request;
use Log;
use PackageSizeManage\Models\PackageSize;
use PackageTypeManage\Models\PackageType;
use PackageTypeSizeManage\Models\PackageTypeSize;
use PackageWeightManage\Models\PackageWeight;
use Response;
use RiderManage\Models\Rider;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\Stock;
use StockManage\Models\StockTransaction;
use StoreManage\Models\Store;
use ZoneManage\Models\ZoneCity;

class PickUpController extends Controller
{

    public function index()
    {
        $logged_in_user = Sentinel::getUser();
        $rider_ids = RiderVehicle::lists('rider_id')->toArray();
//        $riders = Rider::whereIn('id', $rider_ids);
//        if (!$logged_in_user->inRole('admin')) {
//            $riders = $riders->where('zone', $logged_in_user->zone['id']);
//        }
//        $riders = $riders->get();

        $riders = Rider::where(function ($q){
            $q->whereHas('rider_assign', function ($q){
                $q->where('active', 1);
            });
            $q->whereHas('rider_vehicle', function ($q){
                $q->where('status', 1);
            });
        });
        if (!$logged_in_user->inRole('admin')) {
            $riders = $riders->where('zone', $logged_in_user->zone['id']);
        }
        $riders = $riders->get();

        return view('PickUpManage::list')->with(['riders' => $riders]);
    }

    public function array_column_recursive(array $haystack, $needle)
    {
        $found = [];
        array_walk_recursive($haystack, function ($value, $key) use (&$found, $needle) {
            if ($key == $needle) {
                $found[] = $value;
            }

        });
        return $found;
    }

    public function jsonPendingList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $jsonList = array();

        $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        $cities = [];
        foreach ($routes as $route) {
            $cities[] = $route->cities->toArray();
        }

        $city_ids = $this->array_column_recursive($cities, 'city_id');

        $customer_requests = CustomerRequest::whereIn('pickup_city', array_unique($city_ids))->where('status', 0)->orderBy('created_at', 'desc')->get();

        $totalData = $customer_requests->count();
        $totalFiltered = $customer_requests->count();

        $columns = array(
            0 => 'id',
            1 => 'order_id',
            2 => 'first_name',
            3 => 'buyer_mobile',
            4 => 'pickup_address',
            5 => 'buyer_name',
            6 => 'edit',
            7 => 'assign',
            8 => 'pickup',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $customer_requests = CustomerRequest::join('customers', 'requests.customer_id', '=', 'customers.id')
            ->whereIn('pickup_city', array_unique($city_ids))
            ->where('requests.status', 0)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();

        foreach ($customer_requests as $customer_request) {
            $dd = array();

            array_push($dd, $customer_request->id);
            array_push($dd, $customer_request->order_id);
            array_push($dd, $customer_request->first_name);
            array_push($dd, $customer_request->buyer_mobile);
            array_push($dd, $customer_request->pickup_address);
            array_push($dd, $customer_request->buyer_address);
            array_push($dd, $customer_request->buyer_name);
            array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('pickup/request/' . $customer_request->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Pickup"><i class="fa fa-pencil"></i></a></center>');

            array_push($dd, '<center><td><button class="btn btn-sm btn-info assign-btn" data-id="' . $customer_request->id . '" data-toggle="modal" data-target="#myModal">Assign</button></td></center>');

            array_push($dd, '<center><a href="#" class="btn btn-sm btn-success" onclick="window.location.href=\'' . url('pickup/request/' . $customer_request->id . '/confirm') . '\'" data-toggle="tooltip" data-placement="top" title="Pickup">Pick Up</a></center>');
            array_push($jsonList, $dd);
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $jsonList,
        );

        return Response::json($json_data);
    }

    public function pendingListData(Request $request)
    {
        $columns = array(
            0 => 'order_id',
            1 => 'merchant_order_id',
            2 => 'full_name',
            3 => 'mobile',
            4 => 'full_address',
            5 => 'full_address',
            6 => 'pickup_city',
            7 => 'destination_city',
            8 => 'vehicle_number',
            9 => 'edit',
            10 => 'assign',
            11 => 'pickup',
        );

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }
        $jsonList = array();

        if ($logged_in_user->inRole('admin')) {
            $routes = Routing::with('cities')->get();
        } else {
            $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        }

        $cities = [];
        if (value($routes)) {
            foreach ($routes as $route) {
                $cities[] = $route->cities->toArray();
            }

        }

        $city_ids = $this->array_column_recursive($cities, 'city_id');

        $dataQuery = CustomerRequest::with('customer')->where('status', 0);
        if ($isCentralWarehouse) {
            $dataQuery = $dataQuery->where(function ($q){
                $q->where(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 2);
                })->orWhere(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 1);
                });
            });
        } else {
            $dataQuery = $dataQuery->whereIn('pickup_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });
        }

        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit != -1) {
                $filteredDatas = $dataQuery->offset($start)
                    ->limit($limit);
                if ($orderNo == 0 || $orderNo == 1) {
                    $filteredDatas = $filteredDatas->orderBy($order, $dir);
                }
                $filteredDatas = $filteredDatas->get();
            } else {
                $filteredDatas = $dataQuery->get();
            }
        } else {
            $search = $request->input('search.value');
            \Log::error("PENDING SEARCH = " . $search);
            $filteredDatasQuery = $dataQuery->where(function ($or) use ($search, $logged_in_user) {
                $or->where(function ($query1) use ($search) {
                    $query1->whereHas('customer', function ($query1) use ($search) {
                        $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                        ->orWhere('customers.last_name', 'LIKE', "%{$search}%");
                    });
                })
                    ->orWhere('order_id', 'LIKE', "%{$search}%")
                    ->orWhere('merchant_order_id', $search)
                    ->orWhere('vehicle_number', 'LIKE', "%{$search}%");
                if ($logged_in_user->inRole('admin')){
                    $or->orWhereHas('zoneCity.zone', function ($query) use ($search){
                        $query->where('name', 'LIKE', "%{$search}%");
                    });
                }
            });

            if ($limit != -1) {
                $filteredDatas = $filteredDatasQuery->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $filteredDatas = $filteredDatasQuery->get();
            }



            $totalFilteredData = $filteredDatasQuery->count();
        }

        if ($request->input('order.0.column') == 2) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 3) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->mobile;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->mobile;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 4) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_address;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_address;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 5) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 6) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->buyerCity->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 7) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->vehicle_number;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->vehicle_number;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 8) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        $data = array();
        if (!empty($filteredDatas)) {
            foreach ($filteredDatas as $key => $filteredData) {
//                $nestedData['order_id']       = $filteredData->order_id;
                //                $nestedData['merchant_order_code']       = $filteredData->merchant_order_code;
                //                $nestedData['full_name']     = (isset($filteredData->customer)) ? $filteredData->customer->full_name : "-";
                //                $nestedData['mobile']   = (isset($filteredData->customer)) ? $filteredData->customer->mobile : "-";
                //                $nestedData['full_address'] = (isset($filteredData->customer)) ? $filteredData->customer->full_address : "-";
                //                $nestedData['pickup_city']  = (isset($filteredData->customer) && $filteredData->customer->city) ? $filteredData->customer->city->name : "-";
                //                $nestedData['destination_city']  = (isset($filteredData->buyerCity)) ? $filteredData->buyerCity->name : "-";
                //                $nestedData['vehicle_number']    = $filteredData->vehicle_number;
                //
                //                $edit = '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('pickup/request/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Pickup"><i class="fa fa-pencil"></i></a></center>';
                //                if (!$logged_in_user->inRole('admin')) {
                //                    $nestedData['edit'] = $edit;
                //                }
                //
                //
                //                $assign = '<center><td><button class="btn btn-sm btn-info assign-btn" data-id="' . $filteredData->id . '" data-toggle="modal" data-target="#myModal">Assign</button></td></center>';
                //                if ($logged_in_user->inRole('admin')) {
                //                    $assign = '-';
                //                }
                //                $nestedData['assign'] = $assign;
                //
                //                $pickup = '<center><a href="#" class="btn btn-sm btn-success" onclick="window.location.href=\'' . url('pickup/request/' . $filteredData->id . '/confirm') . '\'" data-toggle="tooltip" data-placement="top" title="Pickup">Pick Up</a></center>';
                //                if ($logged_in_user->inRole('admin')) {
                //                    $pickup = '-';
                //                }
                //
                //                $nestedData['pickup'] = $pickup;
                //
                //                $data[] = $nestedData;

                $dd = array();
                array_push($dd, $filteredData->order_id);
                array_push($dd, $filteredData->merchant_order_id);
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->full_name : "-");
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->mobile : "-");
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->full_address : "-");
                array_push($dd, (isset($filteredData->pickupCity)) ? $filteredData->pickupCity->name : "-");
                array_push($dd, (isset($filteredData->buyerCity)) ? $filteredData->buyerCity->name : "-");
                array_push($dd, $filteredData->vehicle_number);
//                $edit = '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('pickup/request/' . $filteredData->id . '/edit') . '\'" data-toggle="tooltip" data-placement="top" title="Edit Pickup"><i class="fa fa-pencil"></i></a></center>';
                //                if (!$logged_in_user->inRole('admin')) {
                //                    array_push($dd, $edit);
                //                }

                $assign = '<center><td><button class="btn btn-sm btn-info assign-btn" data-id="' . $filteredData->id . '" data-toggle="modal" data-target="#myModal">Assign</button></td></center>';
                if (!$logged_in_user->inRole('admin')) {
                    array_push($dd, $assign);
                }

                $pickup = '<center><a href="#" class="btn btn-sm btn-success" onclick="window.location.href=\'' . url('pickup/request/' . $filteredData->id . '/confirm') . '\'" data-toggle="tooltip" data-placement="top" title="Pickup">Pick Up</a></center>';
                if (!$logged_in_user->inRole('admin')) {
                    array_push($dd, $pickup);
                }

                if ($logged_in_user->inRole('admin')) {
                    array_push($dd, (isset($filteredData->zoneCity)) ? $filteredData->zoneCity->zone->name : "-");
                }

                array_push($data, $dd);
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFilteredData),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function jsonScheduledList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $jsonList = array();
        $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        $cities = [];
        foreach ($routes as $route) {
            $cities[] = $route->cities->toArray();
        }

        $city_ids = $this->array_column_recursive($cities, 'city_id');

        $rider_assigns = RiderAssign::lists('request_id')->toArray();

        $customer_requests = CustomerRequest::whereIn('pickup_city', array_unique($city_ids))->whereIn('id', $rider_assigns)->where('status', 1)->orderBy('created_at', 'desc')->get();

        $totalData = $customer_requests->count();
        $totalFiltered = $customer_requests->count();

        $columns = array(
            0 => 'order_id',
            1 => 'customers.first_name',
            2 => 'pickup_address',
            3 => 'pickup_address',
            4 => 'buyer_mobile',
            5 => 'buyer_name',
            6 => 'rider',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $customer_requests = CustomerRequest::join('customers', 'requests.customer_id', '=', 'customers.id')
            ->with('rider_assign.rider')->has('rider_assign')
            ->whereIn('pickup_city', array_unique($city_ids))
        // ->whereIn('rider_id', $rider_assigns)
            ->where('requests.status', 1)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();

        foreach ($customer_requests as $customer_request) {
            $dd = array();

            if (is_null($customer_request->rider_assign->rider)) {
                $driverId = null;
            } else {
                $driverId = $customer_request->rider_assign->rider->id;
            }

            if (is_null($customer_request->rider_assign->rider)) {
                $driverName = null;
            } else {
                $driverName = $customer_request->rider_assign->rider->name;
            }

            array_push($dd, $customer_request->order_id);
            array_push($dd, $customer_request->first_name);
            array_push($dd, $customer_request->pickup_address);
            array_push($dd, $customer_request->buyer_mobile);
            array_push($dd, $customer_request->buyer_address);
            array_push($dd, $customer_request->buyer_name);
            array_push($dd, $driverName);

            array_push($dd, '<td><button class="btn btn-sm btn-info change-btn" data-driver-id="' . $driverId . '" data-id="' . $customer_request->id . '" data-toggle="modal" data-target="#myModal2">Change</button></td>');
            array_push($jsonList, $dd);
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $jsonList,
        );

        return Response::json($json_data);
    }

    public function scheduledListData(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'order_id',
            2 => 'full_name',
            3 => 'mobile',
            4 => 'full_address',
            5 => 'city',
            6 => 'pickup_city',
            7 => 'get_zone_city',
            8 => 'vehicle_number',
            9 => 'zone',
        );

        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }
        $jsonList = array();

        if ($logged_in_user->inRole('admin')) {
            $routes = Routing::with('cities')->get();
        } else {
            $routes = Routing::with('cities')->where('zone_id', $logged_in_user->zone['id'])->get();
        }

        $cities = [];
        if (value($routes)) {
            foreach ($routes as $route) {
                $cities[] = $route->cities->toArray();
            }

        }

        $city_ids = $this->array_column_recursive($cities, 'city_id');

        $dataQuery = CustomerRequest::with('customer')
            ->with('zoneCity.zone')
            ->with(['rider_assign' => function ($q){
                $q->with('rider');
                $q->with(['store' => function ($q){
                    $q->with('vehicle');
                }]);
            }])->has('rider_assign')
            ->where('status', 1);
        if ($isCentralWarehouse) {
            $dataQuery = $dataQuery->where(function ($q){
                $q->where(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 2);
                })->orWhere(function ($q){
                    $q->where('pickup_type', 1)->where('delivery_type', 1);
                });
            });
        } else {
            $dataQuery = $dataQuery->whereIn('pickup_city', array_unique($city_ids))
                ->where(function ($q){
                    $q->where(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 1);
                    })->orWhere(function ($q){
                        $q->where('pickup_type', 2)->where('delivery_type', 2);
                    })->orWhere(function ($q){
                        $q->whereNull('pickup_type')->orWhereNull('delivery_type');
                    });
                });;
        }

        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $orderNo = $request->input('order.0.column');
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit != -1) {
                $filteredDatas = $dataQuery->offset($start)
                    ->limit($limit);
                if ($orderNo == 0 || $orderNo == 1) {
                    $filteredDatas = $filteredDatas->orderBy($order, $dir);
                }
                $filteredDatas = $filteredDatas->get();
            } else {
                $filteredDatas = $dataQuery->get();
            }
        } else {
            $search = $request->input('search.value');
            $filteredDatasQuery = $dataQuery->where(function ($or) use ($search, $logged_in_user) {
                $or->where(function ($query1) use ($search) {
                    $query1->whereHas('customer', function ($query1) use ($search) {
                        $query1->where('customers.first_name', 'LIKE', "%{$search}%");
                        $query1->orWhere('customers.last_name', 'LIKE', "%{$search}%");
                    });
                })
                    ->orWhere('order_id', 'LIKE', "%{$search}%")
                    ->orWhere('merchant_order_id', $search)
                    ->orWhere('vehicle_number', 'LIKE', "%{$search}%")
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('rider_assign.getRider.user', function ($q) use ($search) {
                            $q->where('users.first_name', 'LIKE', "%{$search}%");
                            $q->orWhere('users.last_name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('rider_assign.store.vehicle', function ($q) use ($search) {
                            $q->where('vehicles.name', 'LIKE', "%{$search}%");
                        });
                    })
                    ->orWhere(function ($q) use ($search) {
                        $q->whereHas('buyerCity', function ($q) use ($search) {
                            $q->where('name', 'LIKE', "%{$search}%");
                        });
                    });
                    if ($logged_in_user->inRole('admin')){
                        $or->orWhereHas('zoneCity.zone', function ($query) use ($search){
                            $query->where('name', 'LIKE', "%{$search}%");
                        });
                    }
            });

            if ($limit != -1) {
                $filteredDatas = $filteredDatasQuery->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $filteredDatas = $filteredDatasQuery->get();
            }


            $totalFilteredData = $filteredDatasQuery->count();
        }

//        if($request->input('order.0.column') == 1){
        //            if($request->input('order.0.dir') == "asc"){
        //                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
        //                    return $filteredData->customer['first_name'];
        //                })->values()->all();
        //            }
        //            else{
        //                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
        //                    return $filteredData->customer['first_name'];
        //                })->values()->all();
        //            }
        //
        //            $filteredDatas = collect($filteredDatas);
        //        }

        if ($request->input('order.0.column') == 2) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 3) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->mobile;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->mobile;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 4) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->customer->full_address;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->customer->full_address;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 5) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->pickupCity->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 7) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->rider_assign->getRider->user->first_name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->rider_assign->getRider->user->first_name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if ($request->input('order.0.column') == 8) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->rider_assign->store->vehicle->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->rider_assign->store->vehicle->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }


        if ($request->input('order.0.column') == 10) {
            if ($request->input('order.0.dir') == "asc") {
                $filteredDatas = $filteredDatas->sortBy(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            } else {
                $filteredDatas = $filteredDatas->sortByDesc(function ($filteredData) {
                    return $filteredData->zoneCity->zone->name;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        $data = array();
        if (!empty($filteredDatas)) {
            foreach ($filteredDatas as $key => $filteredData) {
                if (is_null($filteredData->rider_assign->rider)) {
                    $driverId = null;
                } else {
                    $driverId = $filteredData->rider_assign->rider->id;
                }

//                $nestedData['id']       = $filteredData->id;
                //                $nestedData['order_id']       = $filteredData->order_id;
                //                $nestedData['full_name']     = (isset($filteredData->customer)) ? $filteredData->customer->full_name : "-";
                //                $nestedData['mobile']   = (isset($filteredData->customer)) ? $filteredData->customer->mobile : "-";
                //                $nestedData['full_address'] = (isset($filteredData->customer)) ? $filteredData->customer->full_address : "-";
                //                $nestedData['pickup_city']  = (isset($filteredData->customer) && $filteredData->customer->city) ? $filteredData->customer->city->name : "-";
                //                $nestedData['destination_city']  = (isset($filteredData->buyerCity)) ? $filteredData->buyerCity->name : "-";
                //                $nestedData['vehicle_number']    = $filteredData->vehicle_number;
                //                $nestedData['zone']    = (isset($filteredData->zoneCity)) ? $filteredData->zoneCity->zone->name : "-";
                //
                //                $action = '<center><td><button class="btn btn-sm btn-info change-btn" data-driver-id="'.$driverId.'" data-id="' . $filteredData->id . '" data-toggle="modal" data-target="#myModal2">Change</button></td></center>';
                //                if ($logged_in_user->inRole('admin')) {
                //                    $action = '-';
                //                }
                //                $nestedData['action'] = $action;
                //
                //                $data[] = $nestedData;

                $riderName = '-';
                if (isset($filteredData->rider_assign)) {
                    $riderName = $filteredData->rider_assign->getRider->user->first_name . " " . $filteredData->rider_assign->getRider->user->last_name;
                }

                $dd = array();
                array_push($dd, $filteredData->order_id);
                array_push($dd, $filteredData->merchant_order_id);
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->full_name : "-");
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->mobile : "-");
                array_push($dd, (isset($filteredData->customer)) ? $filteredData->customer->full_address : "-");
                array_push($dd, (isset($filteredData->pickupCity)) ? $filteredData->pickupCity->name : "-");
                array_push($dd, (isset($filteredData->buyerCity)) ? $filteredData->buyerCity->name : "-");
                array_push($dd, $riderName);
                array_push($dd, $filteredData->rider_assign->store->vehicle->name);
                if ($logged_in_user->inRole('admin')) {
                    array_push($dd, $filteredData->vehicle_number);
                    array_push($dd, (isset($filteredData->zoneCity)) ? $filteredData->zoneCity->zone->name : "-");
                }

                $action = '<center><td><button class="btn btn-sm btn-info change-btn" data-driver-id="' . $driverId . '" data-id="' . $filteredData->id . '" data-toggle="modal" data-target="#myModal2">Change</button></td></center>';
                if (!$logged_in_user->inRole('admin')) {
                    array_push($dd, $action);
                }

                array_push($data, $dd);

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFilteredData),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function assignRequest(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        if ($request->req_id) {
            $c_request = CustomerRequest::find($request->req_id)->update(['status' => 1]);
            $rider_assign = RiderAssign::where('rider_id', $request->rider)->where('active', 1)->orderBy('created_at', 'desc')->first();
//            RiderAssign::create([
//                'rider_id' => $request->rider,
//                'store_id' => $rider_assign['store_id'],
//                'route_id' => $rider_assign['route_id'],
//                'request_id' => $request->req_id,
//                'assigned_by' => $logged_in_user->id,
//            ]);

            $riderAssign = new RiderAssign();
            $riderAssign->rider_id = $request->rider;
            $riderAssign->store_id = $rider_assign['store_id'];
            $riderAssign->route_id = $rider_assign['route_id'];
            $riderAssign->request_id = $request->req_id;
            $riderAssign->assigned_by = $logged_in_user->id;
            $riderAssign->save();


            return response('success');
        }

        return response('Invalid Request');
    }

    public function changeRider(Request $request)
    {
        $rider_assign = RiderAssign::where('request_id', $request->req_id)->orderBy('created_at', 'desc')->first();

        //$rider_assign = RiderAssign::where('request_id', $request->req_id)->where('created_at', '<=', Carbon::today())->orderBy('created_at', 'desc')->first();
        if ($rider_assign) {

            $store = Store::where('id', $rider_assign->store_id)->first();
            if ($store) {
                $stock = Stock::where('store_id', $store->id)->where('request_id', $request->req_id)->where('availability', 'in')->first();
                if ($stock) {
                    return response('cannot change rider because current request is already in stock');
                }
            }

            $newRidersLatestAssign = RiderAssign::where('rider_id', $request->rider)->where('active', 1)->orderBy('created_at', 'desc')->first();
            if (!$newRidersLatestAssign) {
                return response('there is no active rider assign record for new rider');
            }

            $rider_assign2 = RiderAssign::find($rider_assign['id']);
            $rider_assign2->rider_id = $request->rider;
            $rider_assign2->store_id = $newRidersLatestAssign->store_id;
            $rider_assign2->route_id = $newRidersLatestAssign->route_id;
            $rider_assign2->save();
            return response('success');
        } else {
            return redirect('pickup/list')->with([
                'error' => true,
                'error.message' => 'Request did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

    }

    public function editRequest($id)
    {
        $logged_in_user = Sentinel::getUser();

        $package_types = PackageType::where('status', 1)->get();
        $package_sizes = PackageSize::where('status', 1)->get();
        $package_weights = PackageWeight::where('status', 1)->get();

        $c_request = CustomerRequest::find($id);
        $customer = Customer::where('id', $c_request->customer_id)->first();
        //dd($customer, $c_request->customer_id, $c_request);
        if (!$c_request) {
            return redirect('pickup/list')->with([
                'error' => true,
                'error.message' => 'Request did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        return view('PickUpManage::edit-request')->with(['c_request' => $c_request, 'customer' => $customer, 'package_types' => $package_types, 'package_sizes' => $package_sizes, 'package_weights' => $package_weights, 'logged_in_user' => $logged_in_user, 'cities' => City::all()]);
    }

    public function qrCodeHash()
    {
        return strtoupper(str_random(8));
    }

    public function pickupRequest($id)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $package_types = PackageType::where('status', 1)->get();
        $package_sizes = PackageSize::where('status', 1)->get();
        $package_weights = PackageWeight::where('status', 1)->get();

        $c_request = CustomerRequest::find($id);
        $customer = Customer::where('id', $c_request->customer_id)->first();

//        $zroutes = City::find($c_request->buyer_city)->routes;
        $cityId = $c_request->buyer_city;
        $zroutes = Routing::where('status', 1)->whereHas('cities', function ($q) use ($cityId){
            $q->where('id', $cityId);
        })->get();

        $status = false;
        $zone = null;
        if (in_array($c_request->buyer_city, $logged_in_user->zone->cities()->lists('id')->toArray())) {
            $status = true;
        } else {
            $zone = City::find($c_request->buyer_city)->zones()->first();
        }

        if ($isCentralWarehouse && !($c_request->pickup_type == 1 && $c_request->delivery_type == 1)) {
            $status = true;
            $zroutes = Routing::where('zone_id', $logged_in_user->zone['id'])->where('status', 1)->get();
        }

        if (!$c_request) {
            return redirect('pickup/list')->with([
                'error' => true,
                'error.message' => 'Request did not found ',
                'error.title' => 'Ooops !',
            ]);
        }

        return view('PickUpManage::pickup-request')->with([
            'c_request' => CustomerRequest::find($id),
            'customer' => $customer,
            'package_types' => $package_types, 'package_sizes' => $package_sizes, 'package_weights' => $package_weights, 'logged_in_user' => $logged_in_user, 'cities' => City::all(), 'zroutes' => $zroutes, 'status' => $status, 'zone' => $zone]);
    }

    public function pickupRequestConfirm(Request $request)
    {
        try {
            $logged_in_user = Sentinel::getUser();

            $check = CustomerRequest::where('id', "!=", $request->id)
                ->whereQrCode($request->qr_code)
                ->count();
            if ($check > 0) {
                return response('QR/Barcode already exists in the Database');
            }

            $CustomerRequest = CustomerRequest::find($request->id);
            $CustomerRequest->qr_code = $request->qr_code;

            $ZoneCity = ZoneCity::where('city_id', $CustomerRequest->buyer_city)->first();

            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
            $bin = Bin::where('store_id', $store['id'])->where('related_id', $ZoneCity['zone_id'])->where('bin_type', 'zone')->first();
            if ($request->route_id) {
                $bin = Bin::where('store_id', $store['id'])->where('related_id', $request->route_id)->where('bin_type', 'route')->first();
            }

            $Stockcheck = Stock::where('request_id', $CustomerRequest->id)
                ->where('store_id', $store['id'])
                ->where('availability', 'in')
                ->first();

            if (!$Stockcheck) {
                stock::create([
                    'request_id' => $CustomerRequest->id,
                    'store_id' => $store['id'],
                    'bin_id' => $bin['id'],
                    'availability' => 'in',
                ]);
                StockTransaction::create([
                    'user_id' => $logged_in_user->id,
                    'request_id' => $CustomerRequest['id'],
                    'store_id' => $store['id'],
                    'bin_id' => $bin['id'],
                    'transaction_type' => 'in',
                    'type' => 'bin',
                ]);

                $CustomerRequest->status = 3;
                $CustomerRequest->save();

                return response('success');
            } else {
                return response('Stock not available');
            }

        } catch (\Exception $exception) {
            $exceptionId = rand(0, 99999999);
            Log::error("Ex " . $exceptionId . " | Error in " . __CLASS__ . " ->" . __FUNCTION__ . " " . $exception->getMessage(), $request->all());
            return response()->json([
                "error" => true,
                "exceptionId" => $exceptionId,
            ], 500);
        }
    }

    public function requestUpdate(Request $request)
    {
        //        dd($request->all());
        $logged_in_user = Sentinel::getUser();

        if ($request->has('buyer_name')) {
            //            dd($request->customer_id);
            $customer = Customer::find($request->customer_id)->update([
                'first_name' => $request->customer_name,
                'email' => $request->customer_email,
                'mobile' => $request->customer_mobile,
                'address_line1' => $request->customer_address,
                'city_id' => $request->customer_city,
                'merchant_id' => 1,
            ]);
            $vehicle_number = null;
            if ($request->has('vehicle_number')) {
                $vehicle_number = $request->vehicle_number;
            }
            $customer_request = CustomerRequest::find($request->req_id)->update([
                'request_type' => $request->request_type,
                //                'order_id' => '0000' . (CustomerRequest::all()->count() + 1),
                'status' => 0,
                'created_by' => $logged_in_user->id,
                //                'customer_id' => $customer['id'],
                'customer_nic' => $request->customer_nic,
                'pickup_address' => $request->buyer_address,
                'pickup_city' => $request->buyer_city,
                'buyer_name' => $request->buyer_name,
                'buyer_nic' => $request->buyer_nic,
                'buyer_mobile' => $request->buyer_mobile,
                'buyer_address' => $request->buyer_address,
                'buyer_city' => $request->buyer_city,
                'payment_type' => $request->payment_type,
                'delivery_type' => $request->delivery_type,
                'vehicle_number' => $vehicle_number,
                'package_type' => $request->package_type,
                'package_size' => $request->package_size,
                'package_type_size_price' => $package_type_size_price = PackageTypeSize::where('package_type_id', $request->package_type)->where('package_size_id', $request->package_size)->first()['price'],
                'package_weight' => $request->package_weight_price,
                'package_weight_price' => PackageWeight::find($request->package_weight_price)->price,
                'tax_amount' => 0,
                'total_amount' => $package_type_size_price + PackageWeight::find($request->package_weight_price)->price,
                'remark' => $request->remarks,
            ]);

            if ($customer_request) {
                return redirect('pickup/list')->with([
                    'success' => true,
                    'success.message' => 'Delivery Request Updated successfully!',
                    'success.title' => 'Well Done!',
                ]);
            }
        }

        return redirect('pickup/list')->with([
            'error' => true,
            'error.message' => 'Delivery Request did not updated ',
            'error.title' => 'Ooops !',
        ]);
    }

    public function checkUniqueQr(Request $request)
    {
        try {
            $check = (0 == CustomerRequest::where('id', "!=", $request->id)
                    ->whereQrCode($request->qr_code)
                    ->count());

            return response()->json($check);
        } catch (\Exception $exception) {
            $exceptionId = rand(0, 99999999);
            Log::error("Ex " . $exceptionId . " | Error in " . __CLASS__ . " ->" . __FUNCTION__ . " " . $exception->getMessage(), $request->all());
            return response()->json(false);
        }
    }

}
