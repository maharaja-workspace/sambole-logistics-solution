<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'PickUpManage\Http\Controllers', 'prefix' => 'pickup'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'pickup.list', 'uses' => 'PickUpController@index'
        ]);

        Route::get('request/{id}/edit', [
            'as' => 'pickup.add', 'uses' => 'PickUpController@editRequest'
        ]);
        
        Route::get('request/{id}/confirm', [
            'as' => 'pickup.add', 'uses' => 'PickUpController@pickupRequest'
        ]);


        /**
         * POST Routes
         */
        Route::post('pending_list/data', [
            'as' => 'pickup.list.pending', 'uses' => 'PickUpController@pendingListData'
        ]);

        Route::post('scheduled_list/data', [
            'as' => 'pickup.list.scheduled', 'uses' => 'PickUpController@scheduledListData'
        ]);

        Route::post('request/update', [
            'as' => 'pickup.add', 'uses' => 'PickUpController@requestUpdate'
        ]);

        Route::post('assign/request', [
            'as' => 'pickup.add', 'uses' => 'PickUpController@assignRequest'
        ]);

        Route::post('change/rider', [
            'as' => 'pickup.add', 'uses' => 'PickUpController@changeRider'
        ]);

        Route::post('request/confirm', [
            'as' => 'pickup.store', 'uses' => 'PickUpController@pickupRequestConfirm'
        ]);
        Route::post('request/check/qr', [
            'as' => 'pickup.store', 'uses' => 'PickUpController@checkUniqueQr'
        ]);
    });
});


