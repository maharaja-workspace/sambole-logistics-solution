<?php
namespace DailyPlanManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RiderVehicle extends Model{
    use SoftDeletes;

    protected $fillable = ['rider_id', 'vehicle_id', 'assigned_date', 'assigned_by', 'status'];
}