<?php

namespace PickUpManage;

use Illuminate\Support\ServiceProvider;

class PickUpServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'PickUpManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PickUpManage', function(){
            return new PickUpManage();
        });
    }
}
