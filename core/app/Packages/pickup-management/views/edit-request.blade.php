@extends('layouts.back.master') @section('current_title','EDIT VEHICLE')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<style>
    .container {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        margin-top: 5%;
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input~.checkmark {
        background-color: #ccc;
    }

    /* When the checkbox is checked, add a green background */
    .container input:checked~.checkmark {
        background-color: #18a689;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked~.checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
        left: 9px;
        top: 7px;
        width: 7px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 2px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    #Wheader {
        margin-left: 10px;
    }

    .align-middle {
        margin-top: 10px;
    }

    .h4 {
        font-size: 14px;
    }

    .balance {
        margin-top: 2%;
        width: 50%;
    }


    .clickable {
        cursor: pointer;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 10px;
        bottom: -4px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 16px;
        left: 4px;
        bottom: -2px;
        background-color: #0286f9;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #52abf9;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(18px);
        -ms-transform: translateX(18px);
        transform: translateX(18px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
        bottom: -3px;
    }

    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 100px;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 40%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>


@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Pickup Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Request </strong>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row" style="margin-bottom: 70px;">
    <div class="col-lg-12">
        <h2>Request Update</h2>
        <form method="POST" class="form-horizontal" id="form" action="{{ url('pickup/request/update') }}">
            {!!Form::token()!!}

            {!! Form::hidden('req_id', $c_request->id) !!}
            {!! Form::hidden('customer_id', $customer['id']) !!}


            <div class="col-sm-5" align="right">
                <div class="row">
                    <div class="col-xs-6">
                        <label>Request Type <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <select name="request_type" class="form-control">
                            @if($c_request->request_type == 'delivery')
                            <option value="delivery">Delivery</option>
                            @endif
                            @if($c_request == 'return')
                            <option value="return">Return</option>
                            @endif
                        </select>
                    </div>
                </div>
                <br>


                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup Requester's Name <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="customer_name"
                            value="{{ $customer['first_name'] }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup Requester's NIC <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="customer_nic"
                            value="{{ $c_request->customer_nic }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup Requester's Email <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="customer_email" value="{{ $customer['email'] }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup Requester's Mobile <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="customer_mobile"
                            value="{{ $customer['mobile'] }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup Requester's Address <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <textarea class="form-control" rows="3"
                            name="customer_address">{{ $customer['address_line1'] }}</textarea>
                        <!-- <input type="text" class="form-control" name="packType"> -->
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Pickup City <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="customer_city" tabindex="-1" title="">
                            <option value="">Choose</option>
                            @foreach($cities as $city)
                            @if($customer['city_id'] == $city->id)
                            <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                            @else
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <hr>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Customer Name <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="buyer_name" value="{{ $c_request->buyer_name }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Customer NIC <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="buyer_nic" value="{{ $c_request->buyer_nic }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Customer Mobile <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" name="buyer_mobile"
                            value="{{ $c_request->buyer_mobile }}">
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Customer Delivery Address <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <textarea class="form-control" rows="3"
                            name="buyer_address">{{ $c_request->buyer_address }}</textarea>
                    </div>
                </div>
                <br>

                <!-- <div class="row">
                      <div class="col-xs-6">
                        <label>&nbsp</label>
                      </div>
                      <div class="col-xs-6">
                        <input type="text" class="form-control" name="packType">
                      </div>
                    </div><br> -->

                <div class="row">
                    <div class="col-xs-6">
                        <label>Customer Delivery City <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="buyer_city" tabindex="-1" title="">
                            <option value="">Choose</option>
                            @foreach($cities as $city)
                            @if($c_request->pickup_city == $city->id)
                            <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                            @else
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br><br>

                <div class="row">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-4" align="right">
                        <label>Cash on Delivery <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-2" align="center">
                        <label class="switch">
                            @if($c_request->payment_type == '1')
                            <input type="checkbox" name="payment_type" checked value="1">
                            @else
                            <input type="checkbox" name="payment_type" value="0">
                            @endif
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-xs-4" align="left">
                        <label>Paid</label>
                    </div>
                </div>
                <br><br>

                <div class="row">
                    <div class="col-xs-6" align="right">
                        @if($c_request->delivery_type == 0)
                        <input type="radio" id="delivery_type_1" checked name="delivery_type" value="0">
                        @else
                        <input type="radio" id="delivery_type_1" name="delivery_type" value="0">
                        @endif
                    </div>
                    <div class="col-xs-6" align="left">
                        <label for="delivery_type_1">Pickup from Store</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6" align="right">
                        @if($c_request->delivery_type == 1)
                        <input type="radio" name="delivery_type" checked id="delivery_type_2" value="1">
                        @else
                        <input type="radio" name="delivery_type" id="delivery_type_2" value="1">
                        @endif
                    </div>
                    <div class="col-xs-6" align="left">
                        <label for="delivery_type_2">Pickup from Mobile Store</label>
                        <div class="row" id="vehicle_number" style="display: none">
                            <div class="col-xs-12">
                                <input type="text" class="form-control" value="{{ $c_request->vehicle_number }}"
                                    name="vehicle_number" placeholder="Vehicle Number">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6" align="right">
                        @if($c_request->delivery_type == 2)
                        <input type="radio" name="delivery_type" checked id="delivery_type_3" value="2">
                        @else
                        <input type="radio" name="delivery_type" id="delivery_type_3" value="2">
                        @endif
                    </div>
                    <div class="col-xs-6" align="left">
                        <label for="delivery_type_3">Door Step Delivery</label>
                    </div>
                </div>

            </div>

            <div class="col-sm-5" align="right">
                <div class="row">
                    <div class="col-xs-6">
                        <label>Package Type <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="package_type" tabindex="-1" title="">
                            <option value="">Choose</option>
                            @foreach($package_types as $package_type)
                            @if($c_request->package_type == $package_type->id)
                            <option value="{{ $package_type->id }}" selected>{{ $package_type->name }}</option>
                            @else
                            <option value="{{ $package_type->id }}">{{ $package_type->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Package Size <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="package_size" tabindex="-1" title="">
                            <option value="">Choose</option>
                            @foreach($package_sizes as $package_size)
                            @if($c_request->package_size == $package_size->id)
                            <option value="{{ $package_size->id }}" selected>{{ $package_size->name }}</option>
                            @else
                            <option value="{{ $package_size->id }}">{{ $package_size->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Package Weight <span class="required">*</span></label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                            name="package_weight_price" tabindex="-1" title="">
                            <option value="">Choose</option>
                            @foreach($package_weights as $package_weight)
                            @if($c_request->package_weight == $package_weight->id)
                            <option value="{{ $package_weight->id }}" selected>{{ $package_weight->min_weight }}
                                to {{ $package_weight->max_weight }}</option>
                            @else
                            <option value="{{ $package_weight->id }}">{{ $package_weight->min_weight }}
                                to {{ $package_weight->max_weight }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <label>Remarks</label>
                    </div>
                    <div class="col-xs-6">
                        <!-- <input type="text" class="form-control" name="packType"> -->
                        <textarea name="remarks" id="remarks" class="form-control"
                            placeholder="Remarks">{{ $c_request->remark }}</textarea>
                    </div>
                </div>
                <br>
            </div>
            <div class="col-sm-2"></div>

            <div class="row">
                <div class="col-sm-8" align="right">
                    <button class="btn btn-info">DELIVER</button>
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-danger" type="button" onclick="window.location.reload()">RESET</button>
                </div>
            </div>

        </form>

    </div>
</div>

@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
{{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

<script type="text/javascript">
    $(document).ready(function () {
            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");

            $(function () {
                $("#vehicle1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#addLocation").show();
                        //$("#noLocation").hide();
                    } else {
                        $("#addLocation").hide();
                        //$("#noLocation").show();
                    }
                });
            });

            $("#form").validate({
                ignore: [],
                rules: {
                    request_type: {
                        required: true,
                    },
                    buyer_name: {
                        required: true,
                        lettersonly: true,
                        // maxlength: 20
                    },
                    buyer_mobile: {
                        required: true,
                    },
                    customer_mobile: {
                        required: true,
                    },
                    customer_address: {
                        required: true,
                    },
                    pickup_address: {
                        required: true,
                    },
                    customer_city: {
                        required: true
                    },
                    customer_name: {
                        required: true
                    },
                    customer_email: {
                        required: true
                    },
                    buyer_address: {
                        required: true
                    },
                    buyer_city: {
                        required: true,
                    },
                    customer_nic: {
                        required: true,
                    },
                    buyer_nic: {
                        required: true,
                    },
                    package_type: {
                        required: true,
                    },
                    package_size: {
                        required: true,
                    },
                    package_weight_price: {
                        required: true,
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });

            $("#delivery_type_2").click(function(){
            if ($(this).is(":checked")) {
            $("#vehicle_number").show();
            //$("#noLocation").hide();
            } else {
            $("#vehicle_number").hide();
            //$("#noLocation").show();
            }
            });
        });


</script>
@stop