@extends('layouts.back.master') @section('current_title','PICKUP REQUEST CONFIRM')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
    <style>
        .container {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            margin-top: 5%;
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container:hover input~.checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a green background */
        .container input:checked~.checkmark {
            background-color: #18a689;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container input:checked~.checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container .checkmark:after {
            left: 9px;
            top: 7px;
            width: 7px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
    </style>
    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
              width: 45px;
              padding-left:0px;
            }*/

        /* tr:hover .fixed_float{
              width: 50px;
              padding:  5px 0 0 0px;
            } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        #Wheader {
            margin-left: 10px;
        }

        .align-middle {
            margin-top: 10px;
        }

        .h4 {
            font-size: 14px;
        }

        .balance {
            margin-top: 2%;
            width: 50%;
        }


        .clickable {
            cursor: pointer;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 10px;
            bottom: -4px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 16px;
            left: 4px;
            bottom: -2px;
            background-color: #0286f9;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #52abf9;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(18px);
            -ms-transform: translateX(18px);
            transform: translateX(18px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
            bottom: -3px;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 40%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }
    </style>


@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>Pickup Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Pickup Confirm </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row" style="margin-bottom: 70px;">
        <div class="col-lg-12">
            <h2>Pickup Confirm</h2>
            <form method="POST" class="form-horizontal" id="form" action="{{ url('pickup/request/update') }}">
                {!!Form::token()!!}

                {!! Form::hidden('req_id', $c_request->id) !!}
                {!! Form::hidden('customer_id', $customer['id']) !!}

                <div class="row">
                    <div class="col-sm-5" align="right">
                        <h2 class="text-center" style="font-weight: bold">Merchants's Details</h2><br>
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Merchant Name</label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_name"
                                       value="{{ $customer['first_name'] }}" readonly>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Merchant NIC</label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_nic"
                                       value="{{ $c_request->customer_nic }}" readonly>
                            </div>
                        </div>
                        <br>

                        {{--                    <div class="row">--}}
                        {{--                        <div class="col-xs-6">--}}
                        {{--                            <label>Pickup Requester's Email</label>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-xs-6">--}}
                        {{--                            <input type="text" class="form-control" name="customer_email"--}}
                        {{--                                value="{{ $customer['email'] }}" readonly>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}
                        {{--                    <br>--}}

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Contact Number</label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="customer_mobile"
                                       value="{{ $customer['mobile'] }}" readonly>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Merchant Address</label>
                            </div>
                            <div class="col-xs-6">
                            <textarea class="form-control" rows="3" name="customer_address"
                                      readonly>{{ $customer['address_line1'] }}</textarea>
                                <!-- <input type="text" class="form-control" name="packType"> -->
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup City</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                                name="customer_city" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
{{--                                @foreach($cities as $city)--}}
{{--                                    @if($customer['city_id'] == $city->id)--}}
{{--                                        <input type="text" class="form-control" name="customer_mobile" value="{{ $city->name }}"--}}
{{--                                               readonly>--}}
{{--                                    @endif--}}
{{--                                @endforeach--}}
                                <input type="text" class="form-control" name="customer_mobile" value="{{ $c_request->pickupCity->name }}"
                                       readonly>
                                {{-- </select> --}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Zone</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%"
                                name="customer_city" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
                                @foreach($cities as $city)
                                    @if($customer['city_id'] == $city->id)
                                        @if(isset($c_request->pickupCity->zones[0]))
                                        <input type="text" class="form-control" name="customer_mobile" value="{{ isset($c_request->pickupCity->zones[0]->name) ? $c_request->pickupCity->zones[0]->name : '' }}"
                                               readonly>
                                        @else
                                            <input type="text" class="form-control" name="customer_mobile" value=""
                                                   readonly>
                                        @endif
                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5" align="right">
                        <h2 class="text-center" style="font-weight: bold">Customer Details</h2><br>
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Name</label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_name"
                                       value="{{ $c_request->buyer_name }}" readonly>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer NIC </label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_nic" value="{{ $c_request->buyer_nic }}"
                                       readonly>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Mobile</label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="buyer_mobile"
                                       value="{{ $c_request->buyer_mobile }}" readonly>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Customer Delivery Address</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                <textarea class="form-control" rows="3" name="buyer_address"
                                          readonly>{{ $c_request->buyer_address }}</textarea>
                            </div>
                        </div>
                        <br>

                        <!-- <div class="row">
                          <div class="col-xs-6">
                            <label>&nbsp</label>
                          </div>
                          <div class="col-xs-6">
                            <input type="text" class="form-control" name="packType">
                          </div>
                        </div><br> -->

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Delivery City</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                                {{-- name="buyer_city" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
                                @foreach($cities as $city)
                                    @if($c_request->buyer_city == $city->id)
                                        <input type="text" class="form-control" name="buyer_mobile" value="{{ $city->name }}"
                                               readonly>
                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Delivery Zone</label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                                {{-- name="buyer_city" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
                                @foreach($cities as $city)
                                    @if($c_request->buyer_city == $city->id)
                                        @if(isset($c_request->buyerCity->zones[0]))
                                        <input type="text" class="form-control" name="buyer_mobile" value="{{ isset($c_request->buyerCity->zones[0]->name) ? $c_request->buyerCity->zones[0]->name : '' }}"
                                               readonly>
                                        @else
                                            <input type="text" class="form-control" name="buyer_mobile" value=""
                                                   readonly>
                                        @endif
                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-sm-5" align="right">
                        <h2 class="text-center" style="font-weight: bold">Package Details</h2><br>
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Type </label>
                            </div>
                            <div class="col-xs-6">

                                @foreach($package_types as $package_type)
                                    @if($c_request->package_type == $package_type->id)
                                        <input type="text" class="form-control" name="payment_type" value="{{ $package_type->name }}"
                                               readonly>

                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Size </label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                                {{-- name="package_size" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
                                @foreach($package_sizes as $package_size)
                                    @if($c_request->package_size == $package_size->id)
                                        <input type="text" class="form-control" name="payment_type" value="{{ $package_size->name }}"
                                               readonly>
                                        {{-- <option value="{{ $package_size->id }}" selected>{{ $package_size->name }}</option> --}}
                                        {{-- @else --}}
                                        {{-- <option value="{{ $package_size->id }}">{{ $package_size->name }}</option> --}}
                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Weight </label>
                            </div>
                            <div class="col-xs-6">
                                <!-- <input type="text" class="form-control" name="packType"> -->
                                {{-- <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" --}}
                                {{-- name="package_weight_price" tabindex="-1" title=""> --}}
                                {{-- <option value="">Choose</option> --}}
                                @foreach($package_weights as $package_weight)
                                    @if($c_request->package_weight == $package_weight->id)
                                        <input type="text" class="form-control" name="payment_type"
                                               value="{{ $package_weight->min_weight }} to {{ $package_weight->max_weight }}" readonly>
                                        {{-- <option value="{{ $package_weight->id }}" selected>{{ $package_weight->min_weight }} --}}
                                        {{-- to {{ $package_weight->max_weight }}</option> --}}
                                        {{-- @else --}}
                                        {{-- <option value="{{ $package_weight->id }}">{{ $package_weight->min_weight }} --}}
                                        {{-- to {{ $package_weight->max_weight }}</option> --}}
                                    @endif
                                @endforeach
                                {{-- </select> --}}
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5" align="right">
                        <h2 class="text-center" style="font-weight: bold">Shipment Details</h2><br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Payment Type </label>
                            </div>
                            <div class="col-xs-6">
                                @if($c_request->payment_type == 1)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Online Payment" readonly>
                                @elseif($c_request->payment_type == 2)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Cash On Delivery" readonly>
                                @else
                                    <input type="text" class="form-control" name="payment_type"
                                           value="" readonly>
                                @endif
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Delivery Type </label>
                            </div>
                            <div class="col-xs-6">
                                @if($c_request->delivery_type == 1)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Collection Point" readonly>
                                @elseif($c_request->delivery_type == 2)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Home Delivery" readonly>
                                @else
                                    <input type="text" class="form-control" name="payment_type"
                                           value="" readonly>
                                @endif
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Pickup Type </label>
                            </div>
                            <div class="col-xs-6">
                                @if($c_request->pickup_type == 1)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Harrisons Pickup" readonly>
                                @elseif($c_request->pickup_type == 2)
                                    <input type="text" class="form-control" name="payment_type"
                                           value="Merchant's Location" readonly>
                                @else
                                    <input type="text" class="form-control" name="payment_type"
                                           value="" readonly>
                                @endif
                            </div>
                        </div>
                        <br>

                        @if($c_request->payment_type == 2)
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Payment Amount </label>
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="payment_type"
                                           value="{{ $c_request->total_amount }}" readonly>
                                </div>
                            </div>
                            <br>
                        @endif

                        <div class="row">
                            <div class="col-xs-6">
                                <label>Order Code </label>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" name="payment_type"
                                       value="{{ $c_request->merchant_order_code }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                <br/>

                <div class="col-sm-2"></div>

                <div class="row">
                    <div class="col-sm-8" align="right">
                        <button type="button" class="btn btn-info pick" data-toggle="modal"
                                data-target="#myModal">PICKUP</button>
                    </div>
                    {{-- <div class="col-sm-4">
                        <button class="btn btn-danger" type="reset">CANCEL</button>
                    </div> --}}
                </div>

            </form>

        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="width: 600px">
                <form method="POST" id="form2" class="form-horizontal" action="#">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Pickup Confirm Box</h4>
                    </div>
                    <div class="modal-body">
                        <div id="qr_block" style="text-align: center;">
                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">QR Code</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" value="{{ $c_request->qr_code }}" id="qr_code" class="form-control" name="qr_code">
                                </div>
                            </div>
                            <br>
                            <input type="hidden" value="{{ $c_request->buyer_city }}" name="buyer_city">
                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Route</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @if($status == true)

                                        <select name="route_id" id="route_id" class="form-control" required>
                                            <option value="">Select Route</option>
                                            @if(sizeof($zroutes) == 1)
                                                <option value="{{ $zroutes[0]->id }}" selected="selected">{{ $zroutes[0]->name }}</option>
                                            @else
                                                @foreach ($zroutes as $zroute)
                                                    <option value="{{ $zroute->id }}">{{ $zroute->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @else
                                        <input type="text" class="form-control text-center" value="{{ $zone['name'] }} Zone"
                                               readonly style="width: 60%; margin: 0 auto;">
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div id="invoice_block" style="display: none;">
                        <div id="printable2">
                            <div style="text-align: center;">
                                <h3 style="font-weight: 600;">Pick up Invoice</h3>
                                <hr>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Reference Number</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control order_id" name="order_id"
                                           value="{{ $c_request['order_id'] }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Requester's Name</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control buyer_name" name="buyer_name"
                                           value="{{ $c_request->customer->first_name . " " . $c_request->customer->last_name }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Requester's Number</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control buyer_mobile" name="buyer_mobile"
                                           value="{{ $c_request->customer->mobile }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Pick up city</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control package_type" name="package_type"
                                           value="{{ $c_request->pickupCity->name  }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Customer Name</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control package_type" name="package_type"
                                           value="{{ $c_request->buyer_name }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Customer Number</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control package_type" name="package_type"
                                           value="{{ $c_request->buyer_mobile }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Delivery city</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    <input type="text" class="form-control package_type" name="package_type"
                                           value="{{ $c_request->buyerCity->name  }}" readonly>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Package Type</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @foreach($package_types as $package_type)
                                        @if($c_request->package_type == $package_type->id)
                                            <input type="text" class="form-control" name="payment_type"
                                                   value="{{ $package_type->name }}" readonly>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Package Size</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @foreach($package_sizes as $package_size)
                                        @if($c_request->package_size == $package_size->id)
                                            <input type="text" class="form-control" name="payment_type"
                                                   value="{{ $package_size->name }}" readonly>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label class="align-middle">Package Weight</label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @foreach($package_weights as $package_weight)
                                        @if($c_request->package_weight == $package_weight->id)
                                            <input type="text" class="form-control" name="payment_type"
                                                   value="{{ $package_weight->min_weight }} to {{ $package_weight->max_weight }}"
                                                   readonly>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label>Delivery Type </label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @if($c_request->delivery_type == 1)
                                        <input type="text" class="form-control" name="payment_type"
                                               value="Collection Point" readonly>
                                    @elseif($c_request->delivery_type == 2)
                                        <input type="text" class="form-control" name="payment_type"
                                               value="Home Delivery" readonly>
                                    @else
                                        <input type="text" class="form-control" name="payment_type"
                                               value="" readonly>
                                    @endif
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 3px;">
                                <div class="col-xs-5 col-sm-offset-1 Fill">
                                    <label>Payment Type </label>
                                </div>
                                <div class="col-xs-5 Fill">
                                    @if($c_request->payment_type == 1)
                                        <input type="text" class="form-control" name="payment_type"
                                               value="Online Payment" readonly>
                                    @elseif($c_request->payment_type == 2)
                                        <input type="text" class="form-control" name="payment_type"
                                               value="Cash On Delivery" readonly>
                                    @else
                                        <input type="text" class="form-control" name="payment_type"
                                               value="" readonly>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <br>
                        <hr>
                        <div class="text-center">
                            <button type="button" class="btn btn-info print-btn2">PRINT <i
                                        class="fa fa-print"></i></button>
                        </div>

                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submit" type="submit">Confirm</button>
                <a href="{{ url('pickup/list') }}" style="display: none;" class="btn btn-primary" id="Finish"
                   type="submit">Finish</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>

    </div>
    </div>

@stop
@section('js')
    {{-- <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}
    <script type='text/javascript' src="{{asset('assets/back/js/plugins/JqueryPrint/jQuery.print.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#form2").validate({
                rules: {
                    route_id: {
                        required: true
                    },
                    qr_code: {
                        required: true,
                        remote: {
                            url: "{{url('pickup/request/check/qr')}}",
                            type: "post",
                            data: {
                                id: "{{$c_request->id}}",
                                '_token': '{{ csrf_token() }}',
                                qr_code: function () {
                                    return $("input[name='qr_code']").val()
                                }
                            },
                            complete : function (data) {
                                let resp = data.responseText;
                                if (resp == "true") {
                                    $('#submit').prop('disabled', false);
                                } else {
                                    $('#submit').prop('disabled', true);
                                    return false;
                                }
                            }
                        }
                    }
                },
                messages :{
                    qr_code: {
                        remote : "QR code is already used"
                    }
                }
            });

            $("#submit").on("click", function(e){
                if($("#form2").valid()){
                    $("#submit").prop('disabled', true);
                    var route_id = null;
                    @if($status == true)
                        route_id = $("#route_id").val();
                    @endif
                    e.preventDefault();
                    $.ajax({
                        method: "POST",
                        url: '{{url('pickup/request/confirm')}}',
                        data: {
                            'qr_code': $("input[name='qr_code']").val(),
                            'id': $("input[name='req_id']").val(),
                            'route_id': route_id,
                            '_token': '{{ csrf_token() }}'
                        },
                        success: function (data) {
                            if(data == 'success'){
                                toastr.success('Pickup transferred to the stock successfully');
                                $(".model-header").fadeOut();
                                $(".model-footer").fadeOut();
                                $("#qr_block").fadeOut();
                                $("#invoice_block").fadeIn();
                                $("#submit").prop('disabled', true).fadeOut();
                                $("#Finish").fadeIn();
                            }
                            else{
                                $("#submit").prop('disabled', false);
                                if (data == 'Stock not available') {
                                    toastr.warning('This pickup request already transferred to the stock!');
                                } else {
                                    toastr.warning(data);
                                }
                            }
                        },
                        error: function () {
                            toastr.error("Something went wrong. Please try again");
                        }
                    });
                }

            });
            $(".print-btn2").click(function(){
                $.print("#printable2");
            });
        });

    </script>
@stop