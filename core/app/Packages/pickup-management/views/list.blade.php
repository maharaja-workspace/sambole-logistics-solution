@extends('layouts.back.master') @section('current_title','Pickup')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

    /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Pickup Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        {{--            <li class="active">--}}
        {{--                <strong>Delivery List</strong>--}}
        {{--            </li>--}}
    </ol>
</div>
<div class="col-lg-7">
    <h2>
        <small>&nbsp;</small>
    </h2>
    <ol class="breadcrumb text-right">


    </ol>
</div>

@stop

@section('content')
{{-- <div class="row">
        <div class="col-lg-12 margins">
            <h2>Pickup</h2>
        </div>
    </div> --}}

<!--<form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}-->
<div class="row">
    <div class="col-lg-12 margins" style="padding-top: 20px;">

        <input type="hidden" value="{{Sentinel::getUser()->inRole('admin') ? true : false}}" id="isAdmin">

        <div>
            <div class="row">
                <div class="col-sm-4">
                    <h4>Pending Pick Ups</h4>
                </div>
            </div>
            <br>

            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        {{--                        <th><input id="checkall" type="checkbox"></th>--}}
                        <th>Logistic order ID</th>
                        <th>Merchant Order ID</th>
                        <th>Merchant Name</th>
                        <th>Merchant Mobile</th>
                        <th>Merchant Address</th>
                        <th>Pickup City</th>
                        <th>Destination City</th>
                        <th>Vehicle Number</th>
                        @if(Sentinel::getUser()->inRole('admin'))
                            <th>Zone</th>
                        @endif
                        @if(!Sentinel::getUser()->inRole('admin'))
                            <th>Assign</th>
                            <th>Pickup</th>
                        @endif

                    </tr>
                </thead>
            </table>
        </div>


        <div class="ibox-content">
            <div class="row">
                <div class="col-sm-4">
                    <h4>Scheduled Pick Ups</h4>
                </div>
            </div>
            <br>

            <table id="example2" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Logistic order ID</th>
                        <th>Merchant Order ID</th>
                        <th>Merchant Name</th>
                        <th>Merchant Mobile</th>
                        <th>Merchant Address</th>
                        <th>Pickup City</th>
                        <th>Destination City</th>
                        <th>Assigned Rider</th>
                        <th>Assigned Vehicle</th>
                        @if(Sentinel::getUser()->inRole('admin'))
                        <th>Vehicle Number</th>
                        <th>Zone</th>
                        @endif
                        @if(!Sentinel::getUser()->inRole('admin'))
                        <th>Change Rider</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>


    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" id="form" class="form-horizontal" action="{{ url('pickup/assign/request') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Rider</h4>
                </div>
                <div class="modal-body">

                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">Rider </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="rider">
                                <option value="">Please select a rider</option>
                                @foreach($riders as $rider)
                                <option value="{{ $rider->id }}">{{ $rider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="req_id" id="req_id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" id="form2" class="form-horizontal" action="{{ url('pickup/assign/request') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Rider</h4>
                </div>
                <div class="modal-body">

                    {!!Form::token()!!}
                    <div class="form-group"><label class="col-sm-2 control-label">Rider </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="rider" required="">
                                <option value="">Please select a rider</option>
                                @foreach($riders as $rider)
                                <option value="{{ $rider->id }}">{{ $rider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="req_id" id="req_id2">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

@stop
@section('js')
<script>
    $(document).ready(function () {
        table = $('#example1').DataTable({
            "processing": true,
            "searching": true,
            "autoWidth": false,
            "order": [[0, "asc"]],
            "serverSide": true,
            "responsive": true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            // "initComplete": function(settings, json) {
            //     $('#example1_filter input').unbind();
            //     $('#example1_filter input').bind('keyup', function(e) {
            //         if(e.keyCode == 13) {
            //             table.search( this.value ).draw();
            //         }
            //     });
            // },
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Pending Pickups', className: 'btn-sm', exportOptions: {
                        columns: exportCols()
                    }
                },
                {extend: 'pdf', title: 'Pending Pickups', className: 'btn-sm', exportOptions: {
                        columns: exportCols()
                    }
                },
                {extend: 'print', className: 'btn-sm'}
            ],
            "ajax":{
                "url": "{{ url('pickup/pending_list/data') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
            "columnDefs": [
                // { "searchable": false, "targets": [4,5,7,9] },
                { "orderable": false, "targets": sortDisable() }
            ],
            // "columns": [
            //     { "data": "order_id" },
            //     { "data": "merchant_order_code" },
            //     { "data": "full_name" },
            //     { "data": "mobile" },
            //     { "data": "full_address" },
            //     { "data": "pickup_city" },
            //     { "data": "destination_city" },
            //     { "data": "vehicle_number" },
            //     { "data": "edit" },
            //     { "data": "assign" },
            //     { "data": "pickup" },
            // ]
        });

        function sortDisable()
        {
            if ($('#isAdmin').val()) {
                // return [8, 9];
                return [];
            } else {
                return [7, 8];
            }
        }

        function exportCols() {
            if ($('#isAdmin').val()) {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8];
            } else {
                return [0, 1, 2, 3, 4, 5, 6, 7];
            }
        }

        // $("#example1_filter  input").keyup( function (e) {
        //     if (e.keyCode == 13) {
        //         table.fnFilter( this.value );
        //     }
        // });

        table2 = $('#example2').DataTable({
            "processing": true,
            "searching": true,
            "autoWidth": false,
            "order": [[0, "asc"]],
            "serverSide": true,
            "responsive": true,
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            // "initComplete": function(settings, json) {
            //     $('#example1_filter input').unbind();
            //     $('#example1_filter input').bind('keyup', function(e) {
            //         if(e.keyCode == 13) {
            //             table2.search( this.value ).draw();
            //         }
            //     });
            // },
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Scheduled Pickups', className: 'btn-sm', exportOptions: {
                        columns: exportCols2()
                    }
                },
                {extend: 'pdf', title: 'Scheduled Pickups', className: 'btn-sm', exportOptions: {
                        columns: exportCols2()
                    }
                },
                {extend: 'print', className: 'btn-sm'}
            ],
            "ajax":{
                "url": "{{ url('pickup/scheduled_list/data') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
            "columnDefs": [
                // { "searchable": false, "targets": [4,5,7,9] },
                { "orderable": false, "targets": sortDisable2() }
            ],
            // "columns": [
            //     { "data": "id" },
            //     { "data": "order_id" },
            //     { "data": "full_name" },
            //     { "data": "mobile" },
            //     { "data": "full_address" },
            //     { "data": "pickup_city" },
            //     { "data": "destination_city" },
            //     { "data": "vehicle_number" },
            //     { "data": "zone" },
            //     { "data": "action" },
            // ]
        });

        function sortDisable2()
        {
            if ($('#isAdmin').val()) {
                // return [8, 9];
                return [6];
            } else {
                return [6, 9];
            }
        }

        function exportCols2() {
            if ($('#isAdmin').val()) {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            } else {
                return [0, 1, 2, 3, 4, 5, 6, 7, 8];
            }
        }

        // $("#example1_filter  input").keyup( function (e) {
        //     if (e.keyCode == 13) {
        //         table2.fnFilter( this.value );
        //     }
        // });

        table.on('draw.dt', function () {
            $(".assign-btn").click(function () {
                $("#req_id").val($(this).attr('data-id'));
            });
        });
        table2.on('draw.dt', function () {
            $(".change-btn").click(function () {
                $("#req_id2").val($(this).attr('data-id'));
            });
        });

        $("#form").submit(function (e) {
            // var $valid =
            e.preventDefault();

            if ($("#form").valid()) {
                $.ajax({
                    method: "POST",
                    url: '{{url('pickup/assign/request')}}',
                    data: $(this).serialize(),
                    success: function (data) {
                        // console.log(data);
                        $("#myModal").modal('toggle');
                        toastr.success('Successfully Assigned');
                        table.ajax.url("{{ url('pickup/pending_list/data') }}").load();
                        table2.ajax.url("{{ url('pickup/scheduled_list/data') }}").load();
                    }
                })
            }
        });

        $("#form2").submit(function (e) {
            // var $valid =
            e.preventDefault();

            if ($("#form2").valid()) {
                $.ajax({
                    method: "POST",
                    url: '{{url('pickup/change/rider')}}',
                    data: $(this).serialize(),
                    success: function (data) {
                        // console.log(data);
                        if (data == "success") {
                            $("#myModal2").modal('toggle');
                            toastr.success('Successfully Changed');
                            // table.ajax.url("{{ url('pickup/pending_list/data') }}").load();
                            table2.ajax.url("{{ url('pickup/scheduled_list/data') }}").load();
                        } else {
                            toastr.error(data);
                        }

                    }
                })
            }
        });

        $("#form").validate({
            rules: {
                rider: {
                    required: true
                }
            }
        });

        $("#form2").validate({
            rules: {
                rider: {
                    required: true
                }
            }
        });

        $(document).on('click', '#example2 tbody tr td button.change-btn', function() {
            //var driverName = $(this).parents('tr').find("td:eq(6)").text();
            var driverId = $(this).data("driver-id");
            $("#form2 select").val(driverId);
        });

        $("#form2").validate({
            rules: {
                rider: {
                    required: true
                }
            }
        });

        });
</script>


@stop