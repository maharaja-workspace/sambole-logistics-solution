<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'RiderAssignManage\Http\Controllers', 'prefix' => 'rider-assign'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'rider-assign.list', 'uses' => 'RiderAssignController@index'
        ]);


        /**
         * POST Routes
         */

        Route::post('list/data', [
            'as' => 'rider-assign.list', 'uses' => 'RiderAssignController@listData'
        ]);

        Route::post('un-assign', [
            'as' => 'rider-assign.un-assign', 'uses' => 'RiderAssignController@unAssign'
        ]);

    });
});


