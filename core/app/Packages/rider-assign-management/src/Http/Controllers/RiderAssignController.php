<?php

namespace RiderAssignManage\Http\Controllers;

use DailyPlanManage\Models\RiderVehicle;
use DB;
use DeliveryManage\Models\CustomerRequest;
use Sentinel;
use Response;
use Validator;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use MerchantManage\Models\Merchant;
use App\Http\Controllers\Controller;
use DailyPlanManage\Models\RiderAssign;

use StoreManage\Models\Store;

use StockManage\Models\Stock;
use Yajra\Datatables\Datatables;

class RiderAssignController extends Controller
{
    public function index()
    {
        $logged_in_user = Sentinel::getUser();
        $zone = $logged_in_user->zone;

        return view('RiderAssignManage::list');
    }

    public function listData(Request $request)
    {
        $permissions = Permission::whereIn('name', ['rider-assign.un-assign', 'developer', 'admin'])->where('status', '=', 1)->lists('name');
        return $this->listDataNew($request, $permissions);

        $logged_in_user = Sentinel::getUser();
        $queryBuild = RiderAssign::select(['*', 'rider_assigns.created_at as my_assigned_date'])->with('rider')
            ->with('rider.zone_data')
            ->with('route')
            ->with('rider_vehicle.vehicle.type')
            ->groupBy('rider_id')
//            ->where('completed', '!=', 1)
            ->where('active', 1);
        if (!$logged_in_user->inRole('admin')) {
            $queryBuild = $queryBuild->whereHas('rider', function ($q) use ($logged_in_user){
                $q->where('zone', $logged_in_user->zone['id']);
            });
        }

        $permissions = Permission::whereIn('name', ['rider-assign.un-assign', 'developer', 'admin'])->where('status', '=', 1)->lists('name');

        return Datatables::of($queryBuild)
            ->addColumn("assigned_date", function ($filteredData){
                return $filteredData->my_assigned_date;
            })
            ->addColumn("no_products", function ($filteredData){
                return $filteredData->no_products = $this->getProductCount($filteredData->rider_id);
            })
            ->addColumn("package_count", function ($filteredData){
                return $filteredData->package_count = $this->getPackageCount($filteredData->store_id);
            })
            ->addColumn("action", function ($filteredData) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<td><button class="btn btn-sm btn-info un-assign-btn" data-id="'.$filteredData->rider_id.'">Un Assign</button></td>';
                }
            })
            ->make(true);
        $columns = array( 
            0  => 'rider_id', 
            1  => 'rider_id',
            2  => 'rider_id',
            3  => 'rider_id',
            4  => 'route_id',
            5  => 'created_at',
        );
    
        $dataQuery = RiderAssign::with('rider')
                                    ->with('rider.zone_data')
                                    ->with('route')
                                    // ->with('rider_vehicle')
                                    ->with('rider_vehicle.vehicle.type')
                                    ->groupBy('rider_id')
                                    ->where('active', 1)
                                    ->get();

        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $filteredDatas = $dataQuery;

        if($filteredDatas->count()){
            $filteredDatas  = $filteredDatas->map(function($filteredData){
                                $filteredData->no_products = $this->getProductCount($filteredData->rider_id);
                                return $filteredData;
                            });
        }

        $data = array();
        if(!empty($filteredDatas)){
            foreach ($filteredDatas as $key => $filteredData){
                if(isset($filteredData->rider_vehicle->vehicle->type)){
                    $vType = $filteredData->rider_vehicle->vehicle->type->name;
                }
                else $vType = null;

                if(isset($filteredData->rider_vehicle->vehicle)){
                    $vName = $filteredData->rider_vehicle->vehicle->number;
                }
                else $vName = null;

                $nestedData['id']            = $filteredData->id;
                $nestedData['rider_name']    = (isset($filteredData->rider)) ? $filteredData->rider->name : null;
                $nestedData['zone_name']     = (isset($filteredData->rider->zone_data)) ? $filteredData->rider->zone_data->name : null;
                $nestedData['vehicle_type']  = $vType;
                $nestedData['vehicle_name']  = $vName;
                $nestedData['route_name']    = (isset($filteredData->route)) ? $filteredData->route->name : null;
                $nestedData['assigned_date'] = $filteredData->created_at->toDateString();
                $nestedData['no_products']   = $filteredData->no_products;

                $actionData = "";
                $permissions = Permission::whereIn('name', ['developer', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                   $actionData .= '<td><button class="btn btn-sm btn-info un-assign-btn" data-id="'.$filteredData->id.'">Un Assign</button></td>';
                }

                $nestedData['action'] = $actionData;
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFilteredData), 
            "data"            => $data   
        );
            
        echo json_encode($json_data); 
    }

    public function listDataNew($request, $permissions)
    {
        $logged_in_user = Sentinel::getUser();

        $columns = array(
            0  => 'rider_id',
            1  => 'rider_id',
            2  => 'rider_id',
            3  => 'rider_id',
            4  => 'route_id',
            5  => 'created_at',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $dataQuery = RiderAssign::with('rider')
            ->with('rider.zone_data')
            ->with('route')
            // ->with('rider_vehicle')
            ->with('rider_vehicle.vehicle.type')
            ->groupBy('rider_id')
            ->where('active', 1);

        if (!$logged_in_user->inRole('admin')) {
            $dataQuery = $dataQuery->whereHas('rider', function ($q) use ($logged_in_user){
                $q->where('zone', $logged_in_user->zone['id']);
            });
        }

        if ($request->input('columns.0.search.value')) {
            $value = $request->input('columns.0.search.value');
            $dataQuery = $dataQuery->whereHas('rider', function ($q) use ($value){
                $q->where("name", "LIKE", '%'.$value.'%');
            });
        }

        if ($request->input('columns.1.search.value')) {
            $value = $request->input('columns.1.search.value');
            $dataQuery = $dataQuery->whereHas('rider.zone_data', function ($q) use ($value){
                $q->where("name", "LIKE", '%'.$value.'%');
            });
        }

        if ($request->input('columns.2.search.value')) {
            $value = $request->input('columns.2.search.value');
            $dataQuery = $dataQuery->whereHas('rider_vehicle.vehicle.type', function ($q) use ($value){
                $q->where("name", "LIKE", '%'.$value.'%');
            });
        }

        if ($request->input('columns.3.search.value')) {
            $value = $request->input('columns.3.search.value');
            $dataQuery = $dataQuery->whereHas('rider_vehicle.vehicle', function ($q) use ($value){
                $q->where("number", "LIKE", '%'.$value.'%');
            });
        }

        if ($request->input('columns.4.search.value')) {
            $value = $request->input('columns.4.search.value');
            $dataQuery = $dataQuery->whereHas('route', function ($q) use ($value){
                $q->where("name", "LIKE", '%'.$value.'%');
            });
        }

        if ($request->input('columns.5.search.value')) {
            $value = $request->input('columns.5.search.value');
            $dataQuery = $dataQuery->where("created_at", "LIKE", '%'.$value.'%');
        }

        if ($limit != -1) {
            $dataQuery = $dataQuery->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir);
        }

        $dataQuery = $dataQuery->get();

        $totalData = $dataQuery->count();
        $totalFilteredData = $totalData;


        $filteredDatas = $dataQuery;

        if($request->input('order.0.column') == 0){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return strtolower($filteredData->rider->name);
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return strtolower($filteredData->rider->name);
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 1){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return strtolower($filteredData->rider->zone_data->name);
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return strtolower($filteredData->rider->zone_data->name);
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 2){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return strtolower($filteredData->rider_vehicle->vehicle->type->name);
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return strtolower($filteredData->rider_vehicle->vehicle->type->name);
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 3){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return strtolower($filteredData->rider_vehicle->vehicle->number);
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return strtolower($filteredData->rider_vehicle->vehicle->number);
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 4){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return strtolower($filteredData->route->name);
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return strtolower($filteredData->route->name);
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($request->input('order.0.column') == 5){
            if($request->input('order.0.dir') == "asc"){
                $filteredDatas = $filteredDatas->sortBy(function($filteredData){
                    return $filteredData->created_at;
                })->values()->all();
            }
            else{
                $filteredDatas = $filteredDatas->sortByDesc(function($filteredData){
                    return $filteredData->created_at;
                })->values()->all();
            }

            $filteredDatas = collect($filteredDatas);
        }

        if($filteredDatas->count()){
            $filteredDatas  = $filteredDatas->map(function($filteredData){
                $filteredData->no_products = $this->getProductCount($filteredData->rider_id);
                return $filteredData;
            });
        }

        $data = array();
        if(!empty($filteredDatas)){
            foreach ($filteredDatas as $key => $filteredData){
                $dd = array();
                if(isset($filteredData->rider_vehicle->vehicle->type)){
                    $vType = $filteredData->rider_vehicle->vehicle->type->name;
                }
                else $vType = null;

                if(isset($filteredData->rider_vehicle->vehicle)){
                    $vName = $filteredData->rider_vehicle->vehicle->number;
                }
                else $vName = null;

//                array_push($dd, $filteredData->id);
                array_push($dd, (isset($filteredData->rider)) ? $filteredData->rider->name : null);
                array_push($dd,(isset($filteredData->rider->zone_data)) ? $filteredData->rider->zone_data->name : null);
                array_push($dd, $vType);
                array_push($dd, $vName);
                array_push($dd,(isset($filteredData->route)) ? $filteredData->route->name : null);
                array_push($dd, $filteredData->created_at->toDateString());
                array_push($dd, $filteredData->no_products);
                array_push($dd, $this->getPackageCount($filteredData->store_id));

                $actionData = "";
//                $permissions = Permission::whereIn('name', ['developer', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    $actionData .= '<td><button class="btn btn-sm btn-info un-assign-btn" data-id="'.$filteredData->rider_id.'">Un Assign</button></td>';
                }

                array_push($dd, $actionData);
                array_push($data, $dd);
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFilteredData),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function getProductCount($riderId)
    {
        return $reserves = DB::table('rider_assigns')
                                    ->where('rider_id',$riderId)
                                    ->selectRaw('*, count(*)')
                                    ->where('completed', 0)
                                    ->where('active', 1)
                                    ->groupBy('rider_id')
                                    ->count();
    }

    public function getPackageCount($storeId)
    {
        return Stock::where('store_id', $storeId)
                ->where('availability', 'in')
                ->groupBy('store_id')
                ->count();
    }

    public function unAssign(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:rider_assigns,rider_id',
        ]);
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $riderAssigns = RiderAssign::with('rider_vehicle.vehicle')->where('rider_id', $request->id)->get();

        foreach ($riderAssigns as $riderAssign) {
            if (isset($riderAssign->rider_vehicle->vehicle)) {
                $vehicleId = $riderAssign->rider_vehicle->vehicle->id;
                $storeIds = Store::where('related_id', $vehicleId)->where('store_type', 'vehicle')->get()->pluck('id')->toArray();
                $stocksCount = Stock::whereIn('store_id', $storeIds)->where('availability', 'in')->count();
                if ($stocksCount) {
//                    return response()->json('You are unable to deactivate this vehicle due to it is assigned for a request');
                    return response()->json('Please clear the vehicle to unassign the rider');
                }

                RiderVehicle::where('vehicle_id', $vehicleId)->update(['status' => 0]);
            }

            $updateRes = RiderAssign::where('rider_id', $riderAssign->rider_id)->update(['active' => 0]);

            $req = CustomerRequest::where('id', $riderAssign->request_id)->first();
            if ($req) {
                if ($req->status == 1) {
                    $req->status = 0;
                    $req->save();
                } else if ($req->status == 4) {
                    $req->status = 3;
                    $req->save();
                }
            }


        }
        return response()->json('Successfully Unassigned.');
    }
}