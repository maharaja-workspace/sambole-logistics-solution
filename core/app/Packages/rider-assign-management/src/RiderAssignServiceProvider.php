<?php

namespace RiderAssignManage;

use Illuminate\Support\ServiceProvider;

class RiderAssignServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'RiderAssignManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RiderAssignManage', function(){
            return new RiderAssignManage;
        });
    }
}
