@extends('layouts.back.master') @section('current_title','Stock In')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

    /* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Stock In Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Stock In</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2><small>&nbsp;</small></h2>
    <ol class="breadcrumb text-right">



    </ol>
</div>

@stop

@section('content')


<form method="POST" class="form-horizontal" id="form">
    {!!Form::token()!!}

    <div class="row" style="margin-bottom: 100px">

        <div class="col-lg-12" style="padding-top: 20px;">
            {{-- <h2>Stock In Management</h2> --}}

            <div>
                <div class="form-group"><label class="col-sm-2 control-label">Vehicle
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><select data-placeholder="Choose" class="js-source-states vehicle"
                            style="width: 100%" name="vehicle_number">
                            <option value="">Choose</option>
                            @foreach($vehicles as $vehicle)
                                <option value="{{ $vehicle->id }}">{{ $vehicle->number}} - {{$vehicle->zone_id == 0 ? "Zone to Zone" : "Zone vehicle"}}</option>
                            @endforeach
                        </select>
                        <!-- <input type="text" name="search" class="form-control" value="{{old('search')}}"> -->
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Rider Name
                        <!--<span class="required">*</span>--></label>

                    <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control rider_name"
                            readonly></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Color
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_color"
                            readonly></div>

                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Make
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_make"
                            readonly></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Model
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_model"
                            readonly></div>
                </div>
                {{-- <hr>
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <!--  <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>-->
                        <button class="btn btn-primary" type="button" id="transfer">Transfer</button>
                        <!--  <button class="btn btn-primary" type="button" onclick="location.href = '/sambole-logistics-solution/sPickUp/list';" >Reject</button>-->
                    </div>
                </div> --}}
                <div class="hr-line-dashed"></div>

                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox"></th>
                            <th>Reference Number</th>
                            <th>Merchant Order Id</th>
                            <th>Merchant Name</th>
                            <th>Merchant Mobile Number</th>
                            <th>Buyer Name</th>
                            <th>Buyer Mobile Number</th>
                            <th>Root Bin</th>
                            <th>Transfer</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</form>
<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content" style="width:70%; align-content: center;">

            <!-- Modal Header -->
            <div class="modal-header">
                <span class="modal-title">QR Scan Confirm Box</span>
                <button type="button" class="close" id="modalClose" data-dismiss="modal">&times;</button>
            </div>
            <form method="POST" id="form2" class="form-horizontal" action="{{ url('stock/in/confirm/qr') }}">
                <!-- Modal body -->
                <div class="modal-body" align="center">

                    {{ csrf_field() }}
                    <div class="modal-body" style="text-align: center;">
                        <input type="text" class="form-control qr_code" name="qr_code"
                            placeholder="Enter QR code to confirm" required>
                        <h5>Press Enter key to confirm</h5>
                        <input type="hidden" class="request" name="request">
                        <input type="hidden" class="route" name="route">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
{{-- <div id="myModal" class="modal text-center">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" id="form2" class="form-horizontal" action="{{ url('stock/in/confirm/qr') }}">
{{ csrf_field() }}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">QR Scan Confirm Box</h4>
</div>
<div class="modal-body" style="text-align: center;">
    <input type="text" class="form-control qr_code" name="qr_code" placeholder="Enter QR code to confirm">
    <input type="hidden" class="request" name="request">
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</form>
</div>

</div>
</div> --}}

@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".js-source-states").select2();

        table = $('#example1').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Stock-in list', className: 'btn-sm', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                },
                {extend: 'pdf', title: 'Stock-in list', className: 'btn-sm', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                },
                {extend: 'print', className: 'btn-sm'}
            ],
            orderCellsTop: true,
            fixedHeader: true,
            searching: true,
            "bFilter": true,
            // "autoWidth": false,
            "order": [[1, "asc"]],
            "columnDefs": [
                {"targets": [0], "orderable": false}],

        });

        $(".vehicle").change(function(e) {
            // e.preventDefault();
            $.ajax({
                method: "POST",
                url: '{{url("stock/get/vehicle")}}',
                data: {
                    'id': $(this).val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function(data) {
                    if(data === 'null'){
                        toastr.warning('Data Not Available');
                        $('.rider_name').val('');
                        $('.v_color').val('');
                        $('.v_make').val('');
                        $('.v_model').val('');
                    }
                    else{
                        $('.rider_name').val(data.rider_name);
                        $('.v_color').val(data.color);
                        $('.v_make').val(data.make);
                        $('.v_model').val(data.model);
                    }
                }
            });
            table.ajax.url("{{ url('stock/in/json/list?vehicle=') }}" + $(this).val()).load();
        });

        table.on('draw.dt', function () {
            $(".transfer").click(function(){
                $("#myModal").toggle();
                $(".request").val($(this).attr('data-id'));
                $(".route").val($(".route_bin-"+$(this).attr('data-id')).val());
            });
        });

        $(".qr_code").keypress(function(e){
            var key = e.which;
            if(key == 13) {
                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: '{{url("stock/in/check/qr")}}',
                    data: {
                        'id': $(".request").val(),
                        'qr_code': $(".qr_code").val(),
                        'route_id': $(".route").val(),
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function(data) {
                        if(data.status === 'success'){
                            toastr.success('QR Code Confirmed and transferred successfully');
                            $("#myModal").toggle();
                            table.ajax.url("{{ url('stock/in/json/list?vehicle=') }}" + $('.vehicle').val()).load();
                          //  table.ajax.url("{{ url('stock/in/json/list?=vehicle') }}").load();
                            $(".qr_code").val('');
                        }
                        else if(data.status === 'error'){
                            toastr.warning('QR Code not matched with records');
                        }
                        else{
                            toastr.error('Something went wrong.');
                        }
                    }
                })
            }
        });

        $("#form2 button").click(function(){
            $("#myModal").toggle();
        });
        $("#form2 .close").click(function(){
            $("#myModal").toggle();
        });
        $('#modalClose').click(function () {
            $('#myModal').modal('toggle');
        });
    });
</script>
@stop