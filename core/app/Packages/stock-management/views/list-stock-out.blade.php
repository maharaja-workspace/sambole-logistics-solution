@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
    #floating-button {
        width: 55px;
        height: 55px;
        border-radius: 50%;
        background: #db4437;
        position: fixed;
        bottom: 50px;
        right: 30px;
        cursor: pointer;
        box-shadow: 0px 2px 5px #666;
        z-index: 2
    }

    .aligncenter {
        text-align: center;
    }

    .btn.btn-secondary {
        margin: 0 2px 0 2px;
    }

    /*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

    /* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
    .fixed_float {
        left: 85%;
    }

    .plus {
        color: white;
        position: absolute;
        top: 0;
        display: block;
        bottom: 0;
        left: 0;
        right: 0;
        text-align: center;
        padding: 0;
        margin: 0;
        line-height: 55px;
        font-size: 38px;
        font-family: 'Roboto';
        font-weight: 300;
        animation: plus-out 0.3s;
        transition: all 0.3s;
    }

    .btn.btn-primary.btn-sm.ad-view {
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        text-shadow: none;
        font-size: 13px;
    }

    .row-highlight-clr {
        /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
        background-color: rgba(0, 0, 0, 0.5) !important;
        color: #fff !important;
    }

    #checkAll {
        margin-right: 10px;
    }

    .align-middle {
        padding-top: 6px;
        width: 80px;
    }
</style>

@stop
@section('page_header')
<div class="col-lg-5">
    <h2>Stock Out Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Stock Out</strong>
        </li>
    </ol>
</div>
<div class="col-lg-7">
    <h2><small>&nbsp;</small></h2>
    <ol class="breadcrumb text-right">



    </ol>
</div>

@stop

@section('content')

<form method="POST" class="form-horizontal" id="form">
    {!!Form::token()!!}
    <div class="row" style="margin-bottom: 100px;">
        <div class="col-lg-12" style="padding-top: 20px;">

            <div>

                <form method="POST" class="form-horizontal" id="form">
                    {!!Form::token()!!}

                    <div class="form-group"><label class="col-sm-2 control-label">Vehicle Number <span
                                class="required">*</span></label>
                        <div class="col-sm-3">
                            <select class="form-control js-source-states vehicle" style="width: 100%"
                                name="vehicle_number" id="vehicle_number">
                                <option value="">Choose</option>
                                @if(value($vehicles))
                                    @foreach($vehicles as $vehicle)
                                        @if($vehicle->zone_id == 0)
                                            <option value="{{ $vehicle->id }}">{{ $vehicle->number}} - Zone to Zone</option>
                                        @else
                                            <option value="{{ $vehicle->id }}">{{ $vehicle->number}} - Zone vehicle</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            <!-- <input type="text" name="search" class="form-control" value="{{old('search')}}"> -->
                        </div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">Rider Name
                            <!--<span class="required">*</span>--></label>

                        <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control rider_name"
                                readonly></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Color
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_color"
                                readonly></div>

                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Make
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_make"
                                readonly></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Model
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control v_model"
                                readonly></div>
                    </div>
                    {{-- <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-8">
                            <button class="btn btn-danger" style="float: right;" type="button"
                                onclick="location.reload();">CANCEL</button>
                        </div>
                        <div class="col-sm-2">
                            {{--<button class="btn btn-primary" type="submit">Transfer All</button>--}}
                    {{-- <button class="btn btn-info" type="button"
                                onclick="location.href = '/sambole-logistics-solution/sPickUp/list';">SELECT</button>
                        </div>
                    </div> --}}
                    <hr>
                    <div class="form-group"><label class="col-sm-2 control-label">BIN SELECTION <span
                                class="required">*</span></label>
                        <div class="col-sm-3">

                            <select class="form-control" name="bin_selection" id="bin_selection">
                                <option value="">Choose</option>
                                @if(value($bins))
                                    @foreach ($bins as $bin)
                                    <option class="<?php echo $bin['class']; ?>" value="<?php echo $bin['bin_id']; ?>">
                                        {{ $bin['zone'] }} Bin</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-4">
                            <button class="btn btn-info" type="button" id="select">SELECT</button>
                        </div>
                    </div> --}}
                    <hr>

                    {{-- <div class="form-group"><label class="col-sm-2 control-label">SEARCH <span
                                class="required">*</span></label>
                        <div class="col-sm-4"><input type="text" name="search" class="form-control"
                                value="{{old('search')}}">
            </div>

        </div> --}}

        <div class="form-group">
            <div class="col-sm-2 col-sm-offset-10">
                <!--  <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>-->
                <button class="btn btn-primary" type="button" id="transfer">Transfer</button>
                <!--  <button class="btn btn-primary" type="button" onclick="location.href = '/sambole-logistics-solution/sPickUp/list';" >Reject</button>-->
            </div>
        </div>

</form>
<div class="hr-line-dashed"></div>
<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
    <thead>
        <tr>
            <th><input id="checkall" type="checkbox"></th>
            <th>Reference Number</th>
            <th>Merchant Order Id</th>
            <th>Merchant Name</th>
            <th>Merchant Mobile Number</th>
            <th>Buyer Name</th>
            <th>Mobile Number</th>
            <th class="aligncenter">Stock Transfer</th>
        </tr>
    </thead>
</table>
</div>
</div>
</div>


@stop
@section('js')
{{-- <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script> --}}
{{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

<script type="text/javascript">
    $(document).ready(function(){
        // $(".js-source-states").select2({
        //     ajax: {
        //         url: '{{url("stock/get/vehicles")}}',
        //         dataType: 'json',
        //         // data: {
        //         //     'value': $(this).val(),
        //         //     '_token': '{{ csrf_token() }}'
        //         // },
        //         data: function (params) {
        //             var query = {
        //                 value: params.term,
        //                 _token: '{{ csrf_token() }}'
        //             }

        //             // Query parameters will be ?search=[term]&type=public
        //             return query;
        //         },
        //         // success: function(data) {
        //         //     var html = '';
        //         //     for (var index = 0; index < data.length; index++) {
        //         //         // const element = data[index].id;
        //         //         html+= '<option value="'+ data[index].id +'">'+ data[index].value +'</option>'
        //         //     }
        //         //     $("#vehicle_number").append(html);
        //         // }
        //         processResults: function (data) {
        //             // Transforms the top-level key of the response object from 'items' to 'results'
        //             return {
        //                 results: data.items
        //             };
        //         }
        //     }
        // });

        // $(".vehicle").keyup(function(e) {
            
        // });
        
        $(".vehicle").change(function(e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: '{{url("stock/get/vehicle")}}',
                data: {
                    'id': $(this).val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function(data) {
                    if(data === 'null'){
                        toastr.warning('Rider Not Available');
                        $('.rider_name').val('');
                        $('.v_color').val('');
                        $('.v_make').val('');
                        $('.v_model').val('');
                    }
                    else{
                        $('.rider_name').val(data.rider_name);
                        $('.v_color').val(data.color);
                        $('.v_make').val(data.make);
                        $('.v_model').val(data.model);
                        // console.log(data.zone);
                        if(data.zone === 0){
                            // console.log(data.zone);
                            $("#bin_selection .route").each(function(){
                                $(this).fadeOut();
                            })
                            $("#bin_selection .zone").each(function(){
                                $(this).fadeIn();
                            })
                        }
                        else{
                            $("#bin_selection .zone").each(function(){
                                $(this).fadeOut();
                            })
                            $("#bin_selection .route").each(function(){
                                $(this).fadeIn();
                            })
                        }
                    }
                }
            })
        });

        table = $('#example1').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            buttons: [
                {extend: 'copy', className: 'btn-sm'},
                {extend: 'csv', title: 'Stock-out list', className: 'btn-sm', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                },
                {extend: 'pdf', title: 'Stock-out list', className: 'btn-sm', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                },
                {extend: 'print', className: 'btn-sm', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                }
            ],
            orderCellsTop: true,
            fixedHeader: true,
            searching: true,
            "bFilter": true,
            // "autoWidth": false,
            "order": [[1, "asc"]],
            "columnDefs": [
                {"targets": [0], "orderable": false}],

        });
        
        $("#bin_selection").change(function(){
            var bin = $(this).val();
            table.ajax.url("{{ url('stock/out/json/list?bin_id=') }}" + bin).load();
        });
        // table.on('draw.dt', function () {
            $("#checkall").click(function () {
                $('.check').not(this).prop('checked', this.checked);
            });
            var req_ids = new Array();

            $("#transfer").click(function(){
                
            // var c_id = $(this).attr('data-id');
            var vehicle = $("#vehicle_number").val();
            var bin = $("#bin_selection").val();
            // table.on('draw.dt', function () {
                // console.log('da');
                $('#example1 tbody .check').each(function (i) {
                    if ($(this).prop('checked') === true) {
                        req_ids[i] = $(this).val();
                    }
                });
            // });

            if(req_ids.length == 0){
                toastr.warning('Please load data to the table and make sure you have selected at least one item');
            }
            else{

                if (vehicle != '' && vehicle != null) {

                    swal({
                        title: "Are you sure?",
                        text: "Are you sure whether the selected values are correct?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes !"
                    }).then(function (isConfirm) {
                        $.ajax({
                            method: "POST",
                            url: '{{url('stock/out/request')}}',
                            data: {
                                'vehicle': vehicle,
                                'bin': bin,
                                'req_ids': req_ids,
                                '_token': '{{ csrf_token() }}'
                            },
                            success: function (data) {
                                if(data.status === 'success'){
                                    toastr.success('Successfully Transferred');
                                    table.ajax.url("{{ url('stock/out/json/list?bin_id=') }}" + bin).load();
                                } else {
                                    toastr.error('Something went wrong.');
                                }
                                // table.ajax.url("{{ url('stock/out/json/list') }}").load();
                            }
                        });
                    });

                } else {
                    toastr.error('Please select vehicle for transfer this request.');
                }

                // if (confirm('Are you sure whether the selected values are correct?')) {
                //     $.ajax({
                //         method: "POST",
                //         url: '{{url('stock/out/request')}}',
                //         data: {
                //             'vehicle': vehicle,
                //             'bin': bin,
                //             'req_ids': req_ids,
                //             '_token': '{{ csrf_token() }}'
                //         },
                //         success: function (data) {
                //             toastr.success('Successfully Transferred');
                //             table.ajax.url("{{ url('stock/out/json/list?bin_id=') }}" + bin).load();
                //             // table.ajax.url("{{ url('stock/out/json/list') }}").load();
                //         }
                //     })
                // }
            }
                
        });
        
     });

    function transferRequest(id) {

        var req_ids = new Array();
        var vehicle = $("#vehicle_number").val();
        var bin = $("#bin_selection").val();

        req_ids[0] = id;

        if (vehicle != '' && vehicle != null) {

            swal({
                title: "Are you sure?",
                text: "Are you sure whether the selected values are correct?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes !"
            }).then(function (isConfirm) {
                $.ajax({
                    method: "POST",
                    url: '{{url('stock/out/request')}}',
                    data: {
                        'vehicle': vehicle,
                        'bin': bin,
                        'req_ids': req_ids,
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        if(data.status === 'success'){
                            toastr.success('Successfully Transferred');
                            table.ajax.url("{{ url('stock/out/json/list?bin_id=') }}" + bin).load();
                        } else {
                            toastr.error('Something went wrong.');
                        }
                        // table.ajax.url("{{ url('stock/out/json/list') }}").load();
                    }
                });
            });
        } else {
            toastr.error('Please select vehicle for transfer this request.');
        }

    }


</script>
@stop