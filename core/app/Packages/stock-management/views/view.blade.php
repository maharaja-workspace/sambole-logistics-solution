@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Delivery Approval Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Delivery List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop
@section('content')
    <div class="row" style="margin-bottom: 50px;">

        <div class="col-lg-12 margins">
            <div class="ibox-content">
                <h2>Delivery Approval Request</h2>
                <form method="POST" class="form-horizontal" id="form">
                    {!!Form::token()!!}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">REFERENCE NUMBER</label>
                        <div class="col-sm-10">{{ $customer_request->order_id }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">RIDER NAME
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $rider['name'] }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">BUYER NAME
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->buyer_name }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">ADDRESS
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->buyer_address }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">MOBILE NUMBER
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->buyer_mobile }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PACKAGE AMOUNT
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->total_amount }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PACKAGE TYPE
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $package_type['name'] }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PACKAGE SIZE
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $package_size['name'] }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">PACKAGE WEIGHT
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $package_weight['name'] }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">DATE TIME
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->created_at }}</div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">REMARKS
                            <!--<span class="required">*</span>--></label>
                        <div class="col-sm-10">{{ $customer_request->remark }}</div>
                    </div>
{{--                    <div class="form-group"><label class="col-sm-2 control-label">RIDER SELECTION <span--}}
{{--                                    class="required">*</span></label>--}}
{{--                        <div class="col-sm-10">--}}
{{--                            <select class="form-control" name="rider_selection">--}}
{{--                                <option value="">Choose</option>--}}
{{--                                <option value="colombo7">Bike</option>--}}
{{--                                <option value="Galle">Lorry</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                            <button class="btn btn-primary" type="submit">Approve</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@stop
@section('js')

@stop