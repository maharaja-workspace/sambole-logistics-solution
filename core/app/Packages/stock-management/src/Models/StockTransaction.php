<?php
namespace StockManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockTransaction extends Model{
    use SoftDeletes;
    protected $table = 'stock_transaction';
    protected $fillable = ['request_id', 'store_id', 'bin_id', 'user_id', 'transaction_type', 'type','slot_id'];
}