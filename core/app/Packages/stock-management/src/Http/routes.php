<?php

Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'StockManage\Http\Controllers', 'prefix' => 'stock'], function () {
        /**
         * GET Routes
         */
        Route::get('list', [
            'as' => 'stock.in.list', 'uses' => 'StockController@index'
        ]);
        
        Route::get('out/list', [
            'as' => 'stock.out.list', 'uses' => 'StockController@outIndex'
        ]);

        Route::get('json/pending/list', [
            'as' => 'stock.pending.list', 'uses' => 'StockController@jsonPendingList'
        ]);
        
        Route::get('out/json/list', [
            'as' => 'stock.out.list', 'uses' => 'StockController@jsonOutList'
        ]);
        
        Route::get('in/json/list', [
            'as' => 'stock.in.list', 'uses' => 'StockController@jsonInList'
        ]);

        Route::get('request/{id}/view', [
            'as' => 'stock.edit', 'uses' => 'StockController@viewRequest'
        ]);


        /**
         * POST Routes
         */
        Route::post('get/vehicle', [
            'as' => 'stock.vehicle', 'uses' => 'StockController@getVehilceData'
        ]);
        
        Route::get('get/vehicles', [
            'as' => 'stock.vehicle', 'uses' => 'StockController@getVehicles'
        ]);

        Route::post('assign/request', [
            'as' => 'stock.store', 'uses' => 'StockController@assignRequest'
        ]);
        
        Route::post('out/request', [
            'as' => 'stock.store', 'uses' => 'StockController@outRequest'
        ]);
        
        Route::post('in/check/qr', [
            'as' => 'stock.store', 'uses' => 'StockController@checkQRCode'
        ]);

        Route::post('change/rider', [
            'as' => 'stock.store', 'uses' => 'StockController@changeRider'
        ]);

        Route::post('update', [
            'as' => 'stock.update', 'uses' => 'StockController@update'
        ]);

        Route::post('delete', [
            'as' => 'stock.delete', 'uses' => 'StockController@delete'
        ]);

    });
});


