<?php

namespace StockManage\Http\Controllers;

use App\Http\Controllers\Controller;
use BinManage\Models\Bin;
use DailyPlanManage\Models\RiderVehicle;
use DeliveryManage\Models\Customer;
use DeliveryManage\Models\CustomerRequest;
use Illuminate\Http\Request;
use Response;
use RiderManage\Models\Rider;
use RouteManage\Models\Routing;
use Sentinel;
use StockManage\Models\Stock;
use StockManage\Models\StockTransaction;
use StoreManage\Models\Store;
use VehicleManage\Models\Vehicle;
use VehicleTypeManage\Models\VehicleType;
use ZoneManage\Models\Zone;

class StockController extends Controller
{
    public function index()
    {
        $logged_in_user = Sentinel::getUser();

        $VehicleTypes = VehicleType::where('zone_access', 0)->get()->lists('id');

        $vehicles = Vehicle::where(function ($q) use ($logged_in_user, $VehicleTypes) {
            $q->where('zone_id', $logged_in_user->zone['id'])
                ->orWhereIn('vehicle_type_id', $VehicleTypes);
        })
            ->where('status', 1)
            ->get();

        return view('StockManage::list-stock-in')->with(['vehicles' => $vehicles]);
    }

    public function getVehilceData(Request $request)
    {
        $Vehicle = Vehicle::with(['type'])->where('id', $request->id)->first();
        $VehicleType = VehicleType::where('id', $Vehicle['vehicle_type_id'])->first();

        if ($Vehicle) {
            if ($Vehicle->type->zone_access) {
                $RiderVehicle = RiderVehicle::where('vehicle_id', $request->id)->orderBy('created_at', 'desc')->first();
                if ($RiderVehicle) {
                    $Rider = Rider::where('id', $RiderVehicle['rider_id'])->first();

                    $data = array(
                        'rider_name' => $Rider['name'],
                        'color' => $Vehicle['color'],
                        'make' => $Vehicle['make'],
                        'model' => $Vehicle['vehicle_model'],
                        'zone' => $VehicleType['zone_access'],
                    );
                    return response($data);
                } else {
                    return response('null');
                }
            } else {
                $data = array(
                    'rider_name' => '-',
                    'color' => $Vehicle['color'],
                    'make' => $Vehicle['make'],
                    'model' => $Vehicle['vehicle_model'],
                    'zone' => $VehicleType['zone_access'],
                );
                return response($data);
            }
        }
    }

    public function getVehicles(Request $request)
    {

        $VehicleTypes = VehicleType::where('is_bin', 1)->get()->lists('id');

        $vehicles = Vehicle::where('number', 'LIKE', "%{$$request->value}%")->where('zone_id', $logged_in_user->zone['id'])->orWhereIn('vehicle_type_id', $VehicleTypes)->get()->lists('number')->toArray();

        return response($vehicles);
    }

    public function outIndex()
    {
        $logged_in_user = Sentinel::getUser();

        $VehicleTypes = VehicleType::where('is_bin', 1)->get()->lists('id');
        $vehicles = Vehicle::where(function ($q) use ($logged_in_user, $VehicleTypes) {
            $q->where('zone_id', $logged_in_user->zone['id'])
                ->orWhereIn('vehicle_type_id', $VehicleTypes);
        })->whereHas('type', function ($q) {
            $q->where('is_bin', 1);
            $q->where('zone_access', 0);
        })
            ->where('status', 1)
            ->get();

        $Store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
        // dd($Store['id']);
        // $bins = Bin::where('related_id', '!=', $logged_in_user->zone['id'])->where('store_id', $Store['id'])->where('bin_type', 'zone')->get();
        $bins = Bin::where('store_id', $Store['id'])->where(function ($query) {
            $query->where('bin_type', 'zone');
        })->where('status', 1)->get();
        $selected_bins = null;
        foreach ($bins as $key => $bin) {
            if ($bin->bin_type == 'zone') {
                if ($bin->related_id != $logged_in_user->zone['id']) {
                    $zone = Zone::where('id', $bin->related_id)->first();
                    if ($zone) {
                        $selected_bins[$key] = array(
                            'bin_id' => $bin->id,
                            'zone' => $zone['name'],
                            'class' => 'zone',
                        );
                    }
                }
            } elseif ($bin->bin_type == 'route') {
                $route = Routing::where('status', 1)->where('id', $bin->related_id)->first();
                if ($route) {
                    $selected_bins[$key] = array(
                        'bin_id' => $bin->id,
                        'zone' => $route['name'],
                        'class' => 'route',
                    );
                }
            }
        }
        // return $selected_bins;
        // dd($selected_bins);

        return view('StockManage::list-stock-out')->with(['vehicles' => $vehicles, 'bins' => $selected_bins]);
    }

    public function jsonOutList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        $jsonList = array();
        if ($request->bin_id && $request->bin_id !== null) {

            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

            $stocks = Stock::where('store_id', $store['id'])->where('bin_id', $request->bin_id)->where('availability', 'in')->get()->lists('request_id');

            $customer_requests = CustomerRequest::whereIn('id', $stocks)->where('status', 3);
            if (empty($request->input('search.value'))) {
                $customer_requests = $customer_requests->get();
            } else {
                $search = $request->input('search.value');
                $filteredDatasQuery = $customer_requests->where(function ($or) use ($search) {
                    $or->where(function ($query1) use ($search) {
                        $query1->whereHas('customer', function ($query1) use ($search) {
                            $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                                ->orWhere('customers.last_name', 'LIKE', "%{$search}%")
                                ->orWhere('customers.mobile', 'LIKE', "%{$search}%");
                        });
                    })
                        ->orWhere('order_id', $search)
                        ->orWhere('merchant_order_id', $search);
                });

                $customer_requests = $filteredDatasQuery->get();
            }

            foreach ($customer_requests as $customer_request) {

                $customer = Customer::where('id', $customer_request->customer_id)->first();

                $dd = array();

                array_push($dd, '<td><input class="check" type="checkbox" value="' . $customer_request->id . '"></td>');
                array_push($dd, $customer_request->order_id);
                array_push($dd, $customer_request->merchant_order_id);
                array_push($dd, $customer['first_name']);
                array_push($dd, $customer['mobile']);
                array_push($dd, $customer_request->buyer_name);
                array_push($dd, $customer_request->buyer_mobile);
                array_push($dd, '<center><button class="btn btn-info text-center" onclick="transferRequest('.$customer_request->id.')" type="button" data-id="'. $customer_request->id  .'"><i class="fa fa-exchange"></i></button></center>');
                array_push($jsonList, $dd);
            }
            return Response::json(array('data' => $jsonList));
        }

        return Response::json(array('data' => $jsonList));
    }

    public function jsonInList(Request $request)
    {
        $logged_in_user = Sentinel::getUser();
        $isCentralWarehouse = 0;
        if (!$logged_in_user->inRole('admin')) {
            $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
        }

        $jsonList = array();

        if ($request->vehicle && $request->vehicle !== null) {
            $vehicle = Vehicle::with(['store'])->find($request->vehicle);
            $VehicleType = VehicleType::where('id', $vehicle->vehicle_type_id)->first();
            $stocks = array();
            if ($VehicleType->zone_access == 0) {
                // Zone to Zone Vehicles
                $Bin = Bin::where('related_id', $logged_in_user->zone['id'])->where('bin_type', 'vehicle')->where('store_id', $vehicle->store->id)->first();
                //Get Request Id List For related Stock
                $stocks = Stock::where('store_id', $vehicle->store->id)->where('bin_id', $Bin['id'])->where('availability', 'in')->get()->lists('request_id');
            } else {
                //    Local Riders (Bike and etc)
                //    Get Request Id List For related Stock
                $stocks = Stock::where('store_id', $vehicle->store->id)->where('availability', 'out')->get()->lists('request_id');
            }

            $customer_requests = CustomerRequest::whereIn('id', $stocks)->whereIn('status', [4, 6]);

            if (empty($request->input('search.value'))) {
                $customer_requests = $customer_requests->get();
            } else {
                $search = $request->input('search.value');
                $filteredDatasQuery = $customer_requests->where(function ($or) use ($search) {
                    $or->where(function ($query1) use ($search) {
                        $query1->whereHas('customer', function ($query1) use ($search) {
                            $query1->where('customers.first_name', 'LIKE', "%{$search}%")
                                ->orWhere('customers.last_name', 'LIKE', "%{$search}%")
                                ->orWhere('customers.mobile', 'LIKE', "%{$search}%");
                        });
                    })
                        ->orWhere('order_id', $search)
                        ->orWhere('merchant_order_id', $search);
                });

                $customer_requests = $filteredDatasQuery->get();
            }

            foreach ($customer_requests as $customer_request) {

                $cityId = $customer_request->buyer_city;
                $Routings = Routing::where('status', 1)->whereHas('cities', function ($q) use ($cityId){
                    $q->where('id', $cityId);
                })->get();

                if ($isCentralWarehouse && !($customer_request->pickup_type == 1 && $customer_request->delivery_type == 1)) {
                    $Routings = Routing::where('zone_id', $logged_in_user->zone['id'])->where('status', 1)->get();
                }

                $customer = Customer::where('id', $customer_request->customer_id)->first();

                $dd = array();

                array_push($dd, '<td><input class="check" type="checkbox" value="' . $customer_request->id . '"></td>');
                array_push($dd, $customer_request->order_id);
                array_push($dd, $customer_request->merchant_order_id);
                array_push($dd, $customer['first_name']);
                array_push($dd, $customer['mobile']);
                array_push($dd, $customer_request->buyer_name);
                array_push($dd, $customer_request->buyer_mobile);

                $selectS = "<select class='form-control route_bin-{$customer_request->id}'>";
                $options = '';
                $selectE = '</select>';

                foreach ($Routings as $Routing) {
                    $options .= '<option value="' . $Routing['id'] . '">' . $Routing['name'] . '</option>';
                }

                array_push($dd, $selectS . $options . $selectE);
                array_push($dd, '<button class="btn btn-info text-center transfer" type="button" data-id="' . $customer_request->id . '"><i class="fa fa-exchange"></i></button>');
                array_push($jsonList, $dd);
            }
            return Response::json(array('data' => $jsonList));
        }

        return Response::json(array('data' => $jsonList));
    }

    public function outRequest(Request $request)
    {
        $logged_in_user = Sentinel::getUser();

        if ($request->req_ids) {
            $store = Store::where('related_id', $request->vehicle)->where('store_type', 'vehicle')->first();
            $oldstore = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
            $Vehicle = Vehicle::where('id', $request->vehicle)->first();
            try {

                \DB::beginTransaction();

                foreach ($request->req_ids as $req_id) {
                    if ($req_id != '') {

                        $destination_zone = CustomerRequest::with(['getZoneCity'])->find($req_id);
                        $VehicleType = VehicleType::where('id', $Vehicle['vehicle_type_id'])->first();

                        if ($VehicleType['zone_access']) {

                            $stock = Stock::where('request_id', $req_id)->update([
                                'store_id' => $store['id'],
                                'bin_id' => 0,
                                'availability' => 'in',
                            ]);

                            $CustomerRequest = CustomerRequest::find($req_id);
                            $CustomerRequest->status = 4;
                            $CustomerRequest->save();

                            StockTransaction::create([
                                'request_id' => $CustomerRequest->id,
                                'user_id' => $logged_in_user->id,
                                'store_id' => $store['id'],
                                'bin_id' => 0,
                                'transaction_type' => 'in',
                            ]);
                        } else {
                            $oldstore = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
                            $bin = Bin::where('store_id', $store['id'])->where('related_id', $destination_zone->getZoneCity->zone_id)->first();
                            $stock = Stock::where('request_id', $req_id)->update([
                                'store_id' => $store['id'],
                                'bin_id' => $bin->id,

                            ]);

                            $CustomerRequest = CustomerRequest::find($req_id);
                            $CustomerRequest->status = 4;
                            $CustomerRequest->save();

                            StockTransaction::create([
                                'request_id' => $CustomerRequest->id,
                                'user_id' => $logged_in_user->id,
                                'store_id' => $oldstore['id'],
                                'bin_id' => $request->bin,
                                'transaction_type' => 'out',
                                'type' => 'bin',
                            ]);

                            StockTransaction::create([
                                'request_id' => $CustomerRequest->id,
                                'user_id' => $logged_in_user->id,
                                'store_id' => $store['id'],
                                'bin_id' => $bin->id,
                                'transaction_type' => 'in',
                                'type' => 'bin',
                            ]);
                        }

                    }
                }

                \DB::commit();

            } catch (\PDOException $exception) {
                \Log::error("ERROR = " . $exception->getMessage());
                \DB::rollBack();
                return response(['status' => 'error']);
            }
            return response(['status' => 'success']);
        }

        //return response($request->req_ids);
    }

    public function checkQRCode(Request $request)
    {
        if ($request->id) {
            $logged_in_user = Sentinel::getUser();

            $CustomerRequest = CustomerRequest::where('id', $request->id)->where('qr_code', $request->qr_code)->first();
            if (!$CustomerRequest) {
                return response(['status' => 'error']);
            }

            $isCentralWarehouse = 0;
            if (!$logged_in_user->inRole('admin')) {
                $isCentralWarehouse = $logged_in_user->zone['is_central_warehouse'];
            }

            $buyerCityId = $CustomerRequest->buyer_city;

            $isCityBelongsToLoggedUser = Zone::whereHas('cities', function ($q) use ($buyerCityId){
                $q->where('id', $buyerCityId);
            })->where('id', $logged_in_user->zone['id'])->first();


            $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();
            if ($isCentralWarehouse && ($CustomerRequest->pickup_type == 1 && $CustomerRequest->delivery_type == 2)) {
                $bin_data = Bin::where('related_id', $request->route_id)->where('store_id', $store->id)->where('bin_type', 'route')->first();
            } else {
                if ($isCityBelongsToLoggedUser) {
                    $bin_data = Bin::where('related_id', $request->route_id)->where('store_id', $store->id)->where('bin_type', 'route')->first();
                } else {
                    $bin_data = Bin::where('related_id', $request->route_id)->where('store_id', $store->id)->where('bin_type', 'zone')->first();
                }
            }


            if (!$store || !$bin_data) {
                return response(['status' => 'invalid']);
            }

            $Stock = Stock::where('request_id', $request->id)->where('availability', 'in')->first();



            if ($CustomerRequest) {
//                $stock = Stock::where('request_id', $CustomerRequest['id'])->where('availability', 'out')->update([
                //                    'store_id' => $store->id,
                //                    'bin_id' => $Bin['id'],
                //                    'availability' => 'in',
                //                ]);

//                $stock = Stock::where('request_id', $CustomerRequest['id'])->where('availability', 'out')->first();
                //                if ($stock) {
                //                    $binOfCurrentStock = $stock->bin_id;
                //                    $stock->store_id = $store->id;
                //                    $stock->bin_id = $Bin['id'];
                //                    $stock->availability = 'in';
                //                    $stock->save();
                //                }

//                CustomerRequest::find($CustomerRequest->id)->update([
                //                    'status' => 3,
                //                ]);

                $stock = Stock::where('request_id', $CustomerRequest->id)->first();
                if ($stock) {
                    $store = Store::where('id', $stock->store_id)->where('store_type', 'vehicle')->first();
                    $vehicle = Vehicle::where('id', $store->related_id)->whereHas('type', function ($q) {
                        $q->where('is_bin', 1);
                        $q->where('zone_access', 0);
                    })->first();
                    if ($vehicle) {
                        $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

                        try {

                            \DB::beginTransaction();

                            CustomerRequest::find($CustomerRequest->id)->update([
                                'status' => 3,
                            ]);

                            $stockTransactionRecord = StockTransaction::where('request_id', $CustomerRequest->id)->orderBy('id', 'desc')->first();

                            StockTransaction::create([
                                'request_id' => $CustomerRequest->id,
                                'user_id' => $logged_in_user->id,
                                'store_id' => $stockTransactionRecord->store_id,
                                'bin_id' => $stockTransactionRecord->bin_id,
                                'transaction_type' => 'out',
                                'type' => 'bin',
                            ]);
//
                            StockTransaction::create([
                                'request_id' => $CustomerRequest->id,
                                'user_id' => $logged_in_user->id,
                                'store_id' => $store['id'],
                                'bin_id' => $bin_data->id,
                                'transaction_type' => 'in',
                                'type' => 'bin',
                            ]);

                            $stock->store_id = $store['id'];
                            $stock->bin_id = $bin_data->id;
                            $stock->availability = "in";
                            $stock->save();

                            \DB::commit();

                        } catch (\PDOException $exception) {
                            \Log::error("ERR = " . $exception->getMessage());
                            \DB::rollBack();
                            return response(['status' => 'error']);
                        }

                    } else {
                        $vehicle = Vehicle::where('id', $store->related_id)->whereHas('type', function ($q) {
                            $q->where('is_bin', 0);
                            $q->where('zone_access', 1);
                        })->first();
                        if ($vehicle) {

                            try {

                                \DB::beginTransaction();

                                $store = Store::where('related_id', $logged_in_user->zone['id'])->where('store_type', 'zone')->first();

                                CustomerRequest::find($CustomerRequest->id)->update([
                                    'status' => 3,
                                ]);

                                StockTransaction::create([
                                    'request_id' => $CustomerRequest->id,
                                    'user_id' => $logged_in_user->id,
                                    'store_id' => $store['id'],
                                    'bin_id' => $bin_data->id,
                                    'transaction_type' => 'in',
                                    'type' => 'bin',
                                ]);

                                $stock->store_id = $store['id'];
                                $stock->bin_id = $bin_data->id;
                                $stock->availability = "in";
                                $stock->save();

                                \DB::commit();

                            } catch (\PDOException $exception) {
                                \Log::error("ERR = " . $exception->getMessage());
                                \DB::rollBack();
                                return response(['status' => 'error']);
                            }

                        }
                    }
                }

//                $riderAssign = RiderAssign::where('request_id', $CustomerRequest->id)->first();
                //                if ($riderAssign) {
                //                    $riderVehicle = RiderVehicle::where('rider_id', $riderAssign->rider_id)->first();
                //                    if ($riderVehicle) {
                //
                //                    }
                //                }

                return response(['status' => 'success']);
            }

            return response(['status' => 'error']);
        } else {
            return response(['status' => 'error']);
        }
        return response(['status' => 'invalid']);
    }
}
