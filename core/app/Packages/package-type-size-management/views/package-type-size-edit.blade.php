@extends('layouts.back.master') @section('current_title','Package Type & Size Management | Edit Package Type Size')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Package Type & Size Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Package Type Size </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form" action="{{url("package-type-size/".$packageTypeSize->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Package Type <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="type">
                            @foreach($packageTypes as $packageType)
                                <option value="{{$packageType->id}}" {{$packageTypeSize->package_type_id == $packageType->id ? "selected" : ""}}>{{$packageType->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Package Size <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <select class="form-control" name="size">
                            @foreach($packageSizes as $packageSize)
                                <option value="{{$packageSize->id}}" {{$packageTypeSize->package_size_id == $packageSize->id ? "selected" : ""}}>{{$packageSize->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><input type="text" name="description" class="form-control" value="{{$packageTypeSize->description}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Price <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="price" class="form-control" value="{{$packageTypeSize->price}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Color <span class="required">*</span></label>
                    <div class="col-sm-3"><input type="color" name="color" class="form-control" value="{{$packageTypeSize->color}}"></div>
                </div>
               <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button class="btn btn-danger" type="button" style="float: right;" onclick="location.reload();">Cancel</button>
                    </div>
                </div>

                

            </form>

        </div>
    </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The package size name can only consist of alphabetical & underscore");


         $("#form").validate({
            rules: {
                package_type_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 20             
                },
                type: {
                    required: true,
                    //lettersonly: true,
                    //  maxlength: 20
                },
                size:{
                    required: true,
                    //lettersonly: false,
                    //maxlength: 20
                },
                description:{
                    required: true
                },
                color:{
                    required: true
                },
                price:{
                    required: true,
                    currency: ['$', false]
                },
            },
             messages : {
                 price: {
                     currency: "Please enter a valid amount"
                 }
             },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop