<?php

namespace PackageTypeSizeManage;

use Illuminate\Support\ServiceProvider;

class PackageTypeSizeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'PackageTypeSizeManage');
        require __DIR__ . '/Http/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackageTypeSizeManage', function(){
            return new PackageTypeSizeManage;
        });
    }
}
