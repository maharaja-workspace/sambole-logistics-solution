<?php
namespace PackageTypeSizeManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackageSizeManage\Models\PackageSize;
use PackageTypeManage\Models\PackageType;

class PackageTypeSize extends Model{
    use SoftDeletes;
    protected $fillable = ['package_type_id', 'package_size_id', 'price', 'color', 'description', 'status'];

    function size(){
        return $this->belongsTo(PackageSize::class, 'package_size_id');
    }

    function type(){
        return $this->belongsTo(PackageType::class, 'package_type_id');
    }
}