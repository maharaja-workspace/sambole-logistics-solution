<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'PackageTypeSizeManage\Http\Controllers'], function(){
        Route::post('package-type-size/list/data', 'PackageTypeSizeController@listData')->name("package-type-size.index");
        Route::post('package-type-size/status-toggle', 'PackageTypeSizeController@toggleStatus')->name('package-type-size.update');
        Route::resource('package-type-size', 'PackageTypeSizeController');
    });

});


