<?php
namespace PackageTypeSizeManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageSizeManage\Models\PackageSize;
use PackageTypeManage\Models\PackageType;
use PackageTypeSizeManage\Models\PackageTypeSize;
use Permissions\Models\Permission;
use Response;
use Sentinel;
use Yajra\Datatables\Datatables;

class PackageTypeSizeController extends Controller {

    function index(){

        return view( 'PackageTypeSizeManage::package-type-size');
    }


    public function listData(Request $request)
    {
        $permissions = Permission::whereIn('name',['package-type-size.edit','admin'])->where('status','=',1)->lists('name');

        return Datatables::of(PackageTypeSize::with('size', 'type'))
            ->addColumn("edit", function ($filteredItem) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    return '<center><a href="#" class="blue" onclick="window.location.href=\''.url('package-type-size/'.$filteredItem->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit package type size"><i class="fa fa-pencil"></i></a></center>';
                }
            })
            ->addColumn("status", function ($filteredItem) use($permissions){
                if(Sentinel::hasAnyAccess($permissions)){
                    if($filteredItem->status == 1){
                        return '<center><a href="javascript:void(0)" form="noForm" class="blue package-type-size-status-toggle" data-id="'.$filteredItem->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i></a></></center>';
                    }
                    else if($filteredItem->status == 0){
                        return  '<center><a href="javascript:void(0)" class="blue package-type-size-status-toggle" data-id="'.$filteredItem->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i></a></></center>';
                    }
                }
            })
            ->make(true);
    }

    function create(){

        $packageTypes = PackageType::where('status', 1)->get();
        $packageSizes = PackageSize::where('status', 1)->get();
        return view('PackageTypeSizeManage::package-type-size-add',[
            'packageTypes' => $packageTypes,
            "packageSizes" => $packageSizes
        ]);
    }

    function store(Request $request){
        $packageTypeSize =  PackageTypeSize::create([
            'package_type_id' => $request->type,
            'package_size_id' => $request->size,
            'price' => $request->price,
            'color' => $request->color,
            'description' => $request->description
        ]);

       if ($packageTypeSize){
           return redirect('package-type-size/create')->with([ 'success' => true,
               'success.message'=> 'Package Type Size Created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('package-type-size/create')->with([ 'error' => true,
            'error.message'=> 'Package Type Size  did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $packageTypeSize = PackageTypeSize::find($id);
        $packageTypes = PackageType::where('status', 1)->get();
        $packageSizes = PackageSize::where('status', 1)->get();
        return view('PackageTypeSizeManage::package-type-size-edit',[
            'packageTypes' => $packageTypes,
            "packageSizes" => $packageSizes
        ])->with('packageTypeSize', $packageTypeSize);
    }

    function update(Request $request, $id){
        $packageTypeSize = PackageTypeSize::find($id);
        if (!$packageTypeSize){
            return redirect('package-type-size/')->with([ 'error' => true,
                'error.message'=> 'Package type size did not found ',
                'error.title' => 'Ooops !']);
        }

        $packageTypeSize->package_type_id = $request->type;
        $packageTypeSize->package_size_id = $request->size;
        $packageTypeSize->price = $request->price;
        $packageTypeSize->color = $request->color;
        $packageTypeSize->description = $request->description;

        if ($packageTypeSize->save()){
            return redirect('package-type-size')->with([ 'success' => true,
                'success.message'=> 'Pacage type size updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('package-type-size/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Pacage type size  did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $packageTypeSize = PackageTypeSize::find($id);
            if($packageTypeSize){
                $packageTypeSize->delete();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    function  toggleStatus(Request $request){
        $packageTypeSize = PackageTypeSize::find($request->id);
        if ($packageTypeSize){
            if ($packageTypeSize->status == 1){
                $packageTypeSize->status = 0;
            }else if ($packageTypeSize->status == 0){
                $packageTypeSize->status = 1;
            }
            $packageTypeSize->save();
            return "Status updated";
        }
    }
}