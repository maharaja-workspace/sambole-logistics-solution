@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>Package Type Management</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>Edit Package Type </strong>
        </li>
    </ol>
</div>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins" style="padding-top: 20px;">
        <div>

            <form method="POST" class="form-horizontal" id="form" action="{{url("package-types/".$packageType->id)}}">
                {!!Form::token()!!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group"><label class="col-sm-2 control-label">Package Type Code
                        <!--<span class="required">*</span>--></label>
                    <div class="col-sm-3"><input type="text" disabled name="package_type__code" class="form-control"
                            value="{{$packageType->id}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Package Type Name <span
                            class="required">*</span></label>
                    <div class="col-sm-3"><input type="text" name="package_type_name" class="form-control"
                            value="{{$packageType->name}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-3"><textarea type="text" name="description"
                            class="form-control">{{$packageType->description}}</textarea></div>
                </div>
                <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Update</button>
                        <button class="btn btn-danger" type="button" style="float: right;"
                            onclick="location.reload();">Cancel</button>
                    </div>
                </div>



            </form>

        </div>
    </div>
</div>

@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
{{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

<script type="text/javascript">
    $(document).ready(function(){
         $(".js-source-states").select2();


         $("#form").validate({
             rules: {
                 package_type_name: {
                     required: true,
                     maxlength: 20,
                     remote:function(){
                         var checkit={
                             type: "POST",
                             url:  "{{url('package-types/verify-update/name')}}",
                             dataType: "json",
                             data: {
                                 "_token" : "{{csrf_token()}}",
                                 "id" : {{$packageType->id}}
                             }
                         };
                         return checkit;
                     }
                 }
             },
             messages:
                 {
                     package_type_name:
                         {
                             remote: jQuery.validator.format("{0} is already taken.")
                         }
                 },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


</script>
@stop