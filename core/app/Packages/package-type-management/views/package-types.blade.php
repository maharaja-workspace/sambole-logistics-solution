@extends('layouts.back.master') @section('current_title','Package Type Management | Package Type List')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>Package Type Management</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>Package Type List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2><small>&nbsp;</small></h2>
        <ol class="breadcrumb text-right">



        </ol>
    </div>

@stop

@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
         onclick="location.href = '{{url('package-types/create')}}';">
        <p class="plus">+</p>
    </div>

    <div class="row">
        <div class="col-lg-12 margins" style="padding-top: 20px;">


            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="row">
                    <div class="col-lg-12 margins">

                        <div>
                            <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>Package Type Code</th>
                                    <th>Package Type Name</th>
                                    <th>Description</th>
                                    <th data-search="disable" width="1%" >Edit</th>
                                    <th data-search="disable" width="1%" >Status</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
@stop
@section('js')

    <script !src="">
        var table;
        $(document).ready(function(){

            $('#example1 thead th').each( function () {
                if($(this).data('search') == "disable"){

                }else{

                    var title = $(this).text();
                    $(this).html( $(this).html() + '<br><input type="text"  class="" placeholder="Search" />' );
                }
            } );

            table=$('#example1').DataTable( {
                "ajax": '{{url('package-types/json')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B>>tp",
                "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                "columnDefs": [
                    {"className": "dt-center", "targets": [-1, -2]},
                ],
                buttons: [
                    {extend: 'copy',className: 'btn-sm'},
                    {extend: 'csv',title: 'Package Type List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2]
                        }
                    },
                    {extend: 'pdf', title: 'Package Type List', className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2]
                        }
                    },
                    {extend: 'print',className: 'btn-sm', exportOptions: {
                            columns: [0, 1, 2]
                        }
                    }
                ],
                "autoWidth": false
            });

            table.columns().every( function () {
                var that = this;

                $('input', this.header()).on('keyup change clear click', function (e) {
                    e.stopPropagation();
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });

            table.on( 'draw.dt', function () {
                $('.gallery-category-delete').click(function(e){
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAlert(id);

                });

                $('.package-type-status-toggle').click(function(e){
                    e.preventDefault();
                    id = $(this).data('id');

                    swal({
                        title: "Are you sure?",
                        text: "Change the status of the package type ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Change it!"
                    }).then(function (isConfirm) {
                        if (isConfirm.hasOwnProperty('value') && isConfirm.value === true) {
                            $.ajax({
                                method: "POST",
                                url: '{{url('package-types/status-toggle')}}',
                                data: {
                                    'id': id,
                                    '_token': "{{csrf_token()}}"
                                }
                            })
                                .done(function (msg) {
                                    sweetAlert('Success !!!', msg, 0);
                                    table.ajax.reload();
                                });
                        }


                    });

                });

            });



        });
    </script>


@stop