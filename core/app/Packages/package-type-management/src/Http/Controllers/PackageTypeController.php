<?php
namespace PackageTypeManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PackageTypeManage\Models\PackageType;
use Permissions\Models\Permission;
use Response;
use Sentinel;


class PackageTypeController extends Controller {

    function index(){
        return view( 'PackageTypeManage::package-types');
    }

    public function jsonList(Request $request)
    {
            $jsonList = array();
            $i=1;
            foreach (PackageType::get() as $item) {

                $dd = array();
                array_push($dd, $item->id);
                array_push($dd, $item->name);

                array_push($dd, $item->description);

                $permissions = Permission::whereIn('name',['package-types.edit','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){

                    array_push($dd,  '<a href="#" class="blue" onclick="window.location.href=\''.url('package-types/'.$item->id.'/edit').'\'" data-toggle="tooltip" data-placement="top" title="Edit package size"><i class="fa fa-pencil"></i></a>');
                    if ($item->status == 1){
                        array_push($dd, '<a href="javascript:void(0)" form="noForm" class="blue package-type-status-toggle " data-id="'.$item->id.'"  data-toggle="tooltip" data-placement="top" title="Deactivate"><i class="fa fa-toggle-on"></i>
</a></>');
                    }else if ($item->status == 0){
                        array_push($dd, '<a href="javascript:void(0)" class="blue package-type-status-toggle " data-id="'.$item->id.'"  data-toggle="tooltip" data-placement="top" title="Activate"><i class="fa fa-toggle-off"></i>
</a></>');
                    }

                }

                $permissions = Permission::whereIn('name',['package-types.delete','admin'])->where('status','=',1)->lists('name');
                if(Sentinel::hasAnyAccess($permissions)){
                    array_push($dd, '<center><a href="#" class="package-size=delete" data-id="'.$item->id.'" data-toggle="tooltip" data-placement="top" title="Delete package size"><i class="fa fa-trash-o"></i></a></center>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data'=>$jsonList));

    }

    function create(){
        return view('PackageTypeManage::package-type-add');
    }

    function store(Request $request){
       $packageType =  PackageType::create([
           'name' => $request->package_type_name,
           'description' => $request->description
        ]);

       if ($packageType){
           return redirect('package-types/create')->with([ 'success' => true,
               'success.message'=> 'Package Type Created successfully!',
               'success.title' => 'Well Done!']);
       }

        return redirect('package-types/create')->with([ 'error' => true,
            'error.message'=> 'Package Type did not created ',
            'error.title' => 'Ooops !']);
    }

    function edit($id){
        $packageType = PackageType::find($id);
        return view('PackageTypeManage::package-type-edit')->with('packageType', $packageType);
    }

    function update(Request $request, $id){
        $packageType = PackageType::find($id);
        if (!$packageType){
            return redirect('package-types/')->with([ 'error' => true,
                'error.message'=> 'Package type did not found ',
                'error.title' => 'Ooops !']);
        }

        $packageType->name = $request->package_type_name;
        $packageType->description = $request->description;

        if ($packageType->save()){
            return redirect('package-types')->with([ 'success' => true,
                'success.message'=> 'Pacage type updated successfully!',
                'success.title' => 'Well Done!']);
        }

        return redirect('package-types/'.$id.'/create')->with([ 'error' => true,
            'error.message'=> 'Pacage type  did not updated ',
            'error.title' => 'Ooops !']);
    }

    public function delete(Request $request)
    {
        if($request->ajax()){
            $id = $request->input('id');

            $packageType = PackageType::find($id);
            if($packageType){
                $packageType->delete();
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'invalid_id']);
            }
        }else{
            return response()->json(['status' => 'not_ajax']);
        }
    }

    function verify($verify, Request $request){
        if($verify == "name"){
            if (PackageType::where('name', $request->package_type_name)->count() == 0){
              return 'true';
            }
        }

        return 'false';
    }

    function verifyUpdate($verify, Request $request){
        if($verify == "name"){
            if (PackageType::where('name', $request->package_type_name)->where('id', "!=", $request->id)->count() == 0){
                return 'true';
            }
        }

        return 'false';
    }

    function  toggleStatus(Request $request){
        $type = PackageType::find($request->id);
        $msg = "";
        if ($type){
            if ($type->status == 1){
                $type->status = 0;
                $msg = "You have Deactivated Successfully";
            }else if ($type->status == 0){
                $type->status = 1;
                $msg = "You have Activated Successfully";
            }
            $type->save();
            return $msg;
        }
    }
}