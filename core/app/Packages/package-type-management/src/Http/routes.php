<?php
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['namespace' => 'PackageTypeManage\Http\Controllers'], function(){
        Route::get('package-types/json', 'PackageTypeController@jsonList')->name('package-types.index');
        Route::post('package-types/verify/{fieldName}', 'PackageTypeController@verify')->name('package-types.verify');
        Route::post('package-types/verify-update/{fieldName}', 'PackageTypeController@verifyUpdate')->name('package-types.verify');
        Route::post('package-types/status-toggle', 'PackageTypeController@toggleStatus')->name('package-types.update');
        Route::resource('package-types', 'PackageTypeController');
    });

});


