<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessProfile extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sa_business_profile';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['business_profile', 'user_id'];

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function getProduct()
    {
        return $this->hasMany('ProductManage\Models\Product', 'user_id', 'user_id');
    }
    public function getBusinessDetails()
    {
        return $this->hasOne('App\Models\BusinessRegistration', 'user_id', 'user_id');
    }


}