<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ProductManage\Models\Product;
use AdManage\Models\Ad;

class ShortCode extends Model
{
	
 private $codeLength = 4;

 private $blockedCodes = [];

 protected $fillable = ['short_code', 'ad_id', 'product_id'];

 public function newCode(){

    $codeFound = false;
    $code = "";

    while(!$codeFound){
        $code = $this->generateRandomString();
        $codeFound = $this->checkForShortCode($code) && $this->checkBlockCodes($code);
    }

    return $code;
 }

 public function checkForShortCode($code){
    return SELF::where('short_code', $code)->count() == 0;
 }


 public function checkBlockCodes($code){
    return !in_array($code, $this->blockedCodes);
 }

 public function generateRandomString() {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $this->codeLength; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

}