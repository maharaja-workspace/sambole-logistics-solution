<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'locations';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'district_id','wp_id'];
  
}