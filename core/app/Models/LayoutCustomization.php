<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LayoutCustomization extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sa_layout_customization_details';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['logo_img', 'header_style_id','header_color_code','background_color_code','user_id'];

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Eloquent relationship between LayoutCustomisation and SliderImage models
     */
    public function sliderimage()
    {
        return $this->hasMany('App\Models\SliderImage','layout_id','id');
    }
}