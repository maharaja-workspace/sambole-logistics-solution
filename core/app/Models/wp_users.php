<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_users extends Model
{
   protected $table = 'wp_users';

   public function getdetails()
    {
    	return $this->hasMany('App\Models\wp_usermeta', 'user_id','ID');
    }
}
