<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdImage extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ad_images';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['ad_id', 'img_path', 'img_name', 'is_main','is_wp'];


    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Eloquent relationship between AdImage and Ad models
     */
    public function adimage()
    {
    	return $this->belongsTo('AdManage\Models\Ad');
    }
}