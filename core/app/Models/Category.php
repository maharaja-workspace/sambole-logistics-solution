<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'main_category_id','wp_id'];


    /**
     * Eloquent relationship between Category and Ad models
     */
    public function ad()
    {
    	return $this->hasMany('AdManage\Models\Ad');
    }
}