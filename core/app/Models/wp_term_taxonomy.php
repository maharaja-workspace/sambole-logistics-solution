<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_term_taxonomy extends Model
{
    protected $table = 'wp_term_taxonomy';

    protected $fillable = ['term_taxonomy_id', 'term_id','taxonomy','description','parent','count'];

    public function getChild()
    {
    	return $this->hasMany('App\Models\wp_term_taxonomy', 'parent','term_id');
    }
    public function getDetails()
    {
    	return $this->hasOne('App\Models\wp_terms', 'term_id','term_id');
    }
}
