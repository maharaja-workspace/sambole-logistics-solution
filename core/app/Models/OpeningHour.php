<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpeningHour extends Model
{
    public function storeTimes($bisRegId, $request)
    {
        $response = $this->validateInputs($request);
        if($response){
            return $response;
        }
        $opening = SELF::where('business_registration_detail_id', $bisRegId)->first();
        if(!$opening){
             $opening = new SELF;
        }
        $opening->business_registration_detail_id = $bisRegId;
        $opening->format = $request->input('opening-hours-format');

        $opening->monday_start = null;
        $opening->monday_end = null;
        $opening->tuesday_start = null;
        $opening->tuesday_end = null;
        $opening->wednesday_start = null;
        $opening->wednesday_end = null;
        $opening->thursday_start = null;
        $opening->thursday_end = null;
        $opening->friday_start = null;
        $opening->friday_end = null;
        $opening->saturday_start = null;
        $opening->saturday_end = null;
        $opening->sunday_start = null;
        $opening->sunday_end = null;

        if($request->input('opening-hours-format') == 'selected'){
                foreach ($request->input('opening-hours') as $day => $times) {
                    $opening->{$day.'_start'} = $times['start'];
                    $opening->{$day.'_end'} = $times['end'];
                }
        }
        $opening->save();
        return false;
    }

    public function validateInputs($request)
    {
       
        if(!$request->has('opening-hours-format') || !in_array($request->has('opening-hours-format'), ['selected', 'always', 'unavailable', 'p-closed'])){
                return redirect()->back()->with(['error' => true,
                'error.message' => 'Please select an opening hours format',
                'error.title' => 'Try Again!']);
            }elseif($request->input('opening-hours-format') == 'selected' && count($request->input('opening-hours')) == 0){
                return redirect()->back()->with(['error' => true,
                'error.message' => 'Please check, at least one day for given opening hours format',
                'error.title' => 'Try Again!']);
            }elseif($request->input('opening-hours-format') == 'selected'){
                foreach ($request->input('opening-hours') as $day => $times) {
                    if(!in_array($day, ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'])){
                        return redirect()->back()->with(['error' => true,
                        'error.message' => 'Please input a valid day name for opening hours format',
                        'error.title' => 'Try Again!']);
                    }elseif(!isset($times['start']) || !preg_match("/^(?:2[0-3]|[01][0-9]|[00][0-9]):[0-5][0-9]$/", $times['start'])){
                        return redirect()->back()->with(['error' => true,
                        'error.message' => 'Please input a valid start time for '.$day.' in opening hours format' ,
                        'error.title' => 'Try Again!']);
                    }elseif(!isset($times['end']) || !preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $times['end'])){
                        return redirect()->back()->with(['error' => true,
                        'error.message' => 'Please input a valid end time for '.$day.' in opening hours format' ,
                        'error.title' => 'Try Again!']);
                    }
                }
            }

            return false;
             // end validating opening hours
    }
}
