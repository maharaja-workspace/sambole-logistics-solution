<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_terms extends Model
{
   protected $table = 'wp_terms';

   protected $fillable = ['term_id', 'name','slug','term_group'];
}
