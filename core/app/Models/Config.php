<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Font Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class Config extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'config';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['days_count', 'version', 'logo', 'logo_width', 'logo_height', 'contact', 'email', 'facebook', 'watermark', 'wt_width', 'wt_height','instagram'];

	

}
