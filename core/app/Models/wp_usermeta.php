<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_usermeta extends Model
{
   protected $table = 'wp_usermeta';
}
