<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_posts extends Model
{
   protected $table = 'wp_posts';

   protected $fillable = ['ID'];

   public function getChild()
    {
    	return $this->hasMany('App\Models\wp_posts', 'post_parent','ID');
    }
     public function getTermRelationship()
    {
    	return $this->hasMany('App\Models\wp_term_relationship', 'object_id','ID');
    }
    public function getMeta()
    {
      return $this->hasMany('App\Models\wp_postmeta', 'post_id','ID');
    }
}
