<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visits';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['item_id', 'ip_address', 'city', 'user', 'type'];

    public function visitor()
    {
        return $this->hasOne('UserManage\Models\User', 'id', 'user');
    }

    public function product()
    {
        return $this->hasOne('ProductManage\Models\Product', 'id', 'item_id');
    }

    public function ad()
    {
        return $this->hasOne('AdManage\Models\Ad', 'id', 'item_id');
    }

    public function business()
    {
        return $this->hasOne('App\Models\BusinessRegistration', 'id', 'item_id');
    }

}
