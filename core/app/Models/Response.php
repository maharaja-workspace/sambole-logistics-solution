<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'responses';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['item_id', 'item_type', 'user_id', 'type', 'email', 'name', 'reason', 'message', 'reply'];

    public function ad()
    {
        return $this->belongsTo('AdManage\Models\Ad', 'item_id');
    }

    public function product()
    {
        return $this->belongsTo('ProductManage\Models\Product', 'item_id');
    }

    public function user()
    {
        return $this->belongsTo('UserManage\Models\User');
    }
}
