<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessRegistration extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'business_registration_details';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'business_name', 'industry_id', 'company_address', 'contact_number', 'nominal_capital', 'website', 'facebook_url', 'googleplus_url', 'electronic_signature', 'agreement','business_page_name', 
        'about',
        'open',
        'close',
        'instagram_url',
        'twitter_url',
        'linkedin_url'];

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getProduct()
    {
        return $this->hasMany('ProductManage\Models\Product', 'user_id', 'user_id');
    }
    public function getOwner()
    {
        return $this->hasOne('UserManage\Models\User', 'id', 'user_id');
    }
    public function getOpeningTimes()
    {
        return $this->hasOne(OpeningHour::class, 'business_registration_detail_id');
    }


}