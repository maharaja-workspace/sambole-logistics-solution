<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class wp_term_relationship extends Model
{
    protected $table = 'wp_term_relationships';

    protected $fillable = ['object_id', 'term_taxonomy_id','term_order'];

     public function getTaxonomy()
    {
    	return $this->hasMany('App\Models\wp_term_taxonomy', 'term_taxonomy_id','term_taxonomy_id');
    }

}
