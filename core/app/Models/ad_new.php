<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\AdImage_new;
use App\ActiveUser;
use Carbon\Carbon;
use Sentinel;
class ad_new extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = 'ads';

    protected $fillable = ['email', 'ad_title', 'ad_description','ad_code', 'ad_price','call_for_info','ad_negotiable_price','ad_location_id','ad_category_id','status','user_id','contactNos','short_code', 'start_date', 'ad_expire','ad_gmap_longitude','ad_gmap_latitude','ad_visibility','ad_views','ad_display_map','ad_featured','created_at','updated_at','wp_id','address'];



    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        // Ad::updating(function($ad) {
        //     $adUpdatedColumns = $ad->getDirty();

        //   //   $editHistory = AdEditHistory::create([
        //   //     "ad_id" => $ad->id,
        //   //     "user_id" => \Sentinel::getUser()->id,
        //   //     "edited" => json_encode($adUpdatedColumns)
        //   // ]);
        // });
        // // dd($adUpdatedColumns);
        // AdImage::creating(function($img){
        //     $adUpdatedColumns = null;
        //     $imgUpdatedColumns = null;
        //     Ad::updated(function($ad) {
        //         $adUpdatedColumns = $ad->getDirty();
        //     });
        //     $imgUpdatedColumns = $img->getDirty();
        //     dd($adUpdatedColumns, $imgUpdatedColumns);
        // });

    }

    public function getUser()
    {
        return $this->belongsTo('UserManage\Models\User', 'user_id', 'id');
    }
    /**
     * Eloquent relationship between Ad and Location models
     */
    public function location()
    {
    	return $this->belongsTo('App\Models\Location', 'ad_location_id');
    }

    /**
     * Eloquent relationship between Ad and Category models
     */
    public function category()
    {
    	return $this->belongsTo('App\Models\Category', 'ad_category_id');
    }

    /**
     * Eloquent relationship between Ad and AdImage models
     */
    public function adimage()
    {
    	return $this->hasMany('App\Models\AdImage_new', 'ad_id');
    }

    /**
     * Eloquent relationship between Ad and User models
     */
    public function user()
    {
        return $this->belongsTo('UserManage\Models\User');
    }

    public function views()
    {
        return $this->hasMany('App\Models\Visit', 'item_id');
    }

    public function getActiveUser()
    {
      return $this->hasOne(ActiveUser::class, 'model_id')->where('model', 'ad')->first();
    }
    public function setEditing($token)
    {
      $activeUser = $this->getActiveUser();
      if ($activeUser) {
        $user = Sentinel::getUser();

        if ($user->id != $activeUser->user_id && $activeUser->token != $token) { // new session
  				$dif = Carbon::now()->diffInSeconds($activeUser->updated_at);
  				if ($dif > 60) {
  					$activeUser->user_id = request()->user ;
  					$activeUser->token = request()->_token ;
  					$activeUser->save();
  				}else{
  					return ['editing' => 'User '.$user->first_name.' '.$user->last_name.' is editing this advetisement from some other location/ browser tab.'];
  				}
  			}else{ // same session
  				$activeUser->updated_at = Carbon::now()->toDateTimeString() ;
  				$activeUser->save();
  				return ['msg' => 'successfully updated'];
  			}

  		}else{
  			ActiveUser::create([
  				'user_id' => request()->user,
  				'model_id' => request()->id,
  				'model' => 'ad',
  				'url' => request()->url,
  				'token' => request()->_token,
  			]);
  			return ['msg' => 'success'];

  		}
    }
    public function checkAdEditing()
    {
      $activeUser = $this->getActiveUser();

      	if ($activeUser && $activeUser->user_id != Sentinel::getUser()->id) {
      		$dif = Carbon::now()->diffInSeconds($activeUser->updated_at);
      		if ($dif <= 90) {
      			$user = Sentinel::findById($activeUser->user_id);
      			return ['editing' => 'User '.$user->first_name.' '.$user->last_name.' is editing this advetisement from some other location/ browser tab.'];
      		}
      	}

      	return ['msg' => 'inactive'];
    }
}
