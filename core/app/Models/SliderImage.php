<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderImage extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sa_slider_images';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['layout_id', 'img_path', 'img_name'];

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Eloquent relationship between LayoutCustomisation and SliderImage models
     */
    public function layout()
    {
    	return $this->belongsTo('App\Models\LayoutCustomization');
    }
}