<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
/*Migration*/

 

    


/*DONT USE THIS ROUTE FOR OTHER USAGE ----ONLY FOR THIS */
Route::group(['middleware' => ['auth']], function()
{
  Route::get('/', [
      'as' => 'index', 'uses' => 'WelcomeController@index'
    ]);
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);

    Route::get('config', [
      'as' => 'config', 'uses' => 'ConfigController@index'
    ]);

    Route::post('config', [
      'as' => 'config', 'uses' => 'ConfigController@update'
    ]);
     Route::get('city/list', [
      'as' => 'index', 'uses' => 'WireframeController@city_list'
    ]);
    Route::get('packagetype/list', [
      'as' => 'index', 'uses' => 'WireframeController@packagetype_list'
    ]);
    Route::get('delivery_type/list', [
      'as' => 'index', 'uses' => 'WireframeController@delivery_type_list'
    ]);

    Route::get('package_size/list', [
      'as' => 'index', 'uses' => 'WireframeController@package_size_list'
    ]);
    

    Route::get('package_weight/list', [
      'as' => 'index', 'uses' => 'WireframeController@package_weight_list'
    ]);
    
    Route::get('store_management/list', [
      'as' => 'index', 'uses' => 'WireframeController@store_management_list'
    ]);
    Route::get('bin_management/list', [
      'as' => 'index', 'uses' => 'WireframeController@bin_management_list'
    ]);


     Route::get('city/list', [
      'as' => 'index', 'uses' => 'WireframeController@city_list'
    ]);
     Route::get('city/add', [
      'as' => 'index', 'uses' => 'WireframeController@city_add'
    ]);

    Route::get('dailyplan/listadd', [
        'as' => 'index', 'uses' => 'WireframeController@dailyPlan_listAdd'
    ]);


    Route::get('packagetype/list', [
      'as' => 'index', 'uses' => 'WireframeController@packagetype_list'
    ]);
     Route::get('packagetype/add', [
      'as' => 'index', 'uses' => 'WireframeController@packagetype_add'
    ]);

    Route::get('packageTypeSize/list', [
        'as' => 'index', 'uses' => 'WireframeController@packageTypeSize_list'
    ]);
    Route::get('packageTypeSize/add', [
        'as' => 'index', 'uses' => 'WireframeController@packageTypeSize_add'
    ]);
    
     Route::get('delivery_type/list', [
      'as' => 'index', 'uses' => 'WireframeController@delivery_type_list'
    ]);
    
     Route::get('delivery_type/add', [
      'as' => 'index', 'uses' => 'WireframeController@delivery_type_add'
    ]);
    
    Route::get('package_size/list', [
      'as' => 'index', 'uses' => 'WireframeController@package_size_list'
    ]);
    
     Route::get('package_size/add', [
      'as' => 'index', 'uses' => 'WireframeController@package_size_add'
    ]);

    Route::get('area/add', [
      'as' => 'index', 'uses' => 'WireframeController@area_add'
    ]);
    Route::get('area/list', [
      'as' => 'index', 'uses' => 'WireframeController@area_list'
    ]);
//    Route::get('route/add', [
//      'as' => 'index', 'uses' => 'WireframeController@route_add'
//    ]);
//    Route::get('route/list', [
//      'as' => 'index', 'uses' => 'WireframeController@route_list'
//    ]);
//    Route::get('zone/add', [
//      'as' => 'index', 'uses' => 'WireframeController@zone_add'
//    ]);
//    Route::get('zone/list', [
//      'as' => 'index', 'uses' => 'WireframeController@zone_list'
//    ]);
//    Route::get('zonePrice/add', [
//      'as' => 'index', 'uses' => 'WireframeController@zonePrice_add'
//    ]);
//     Route::get('zone/price/list', [
//      'as' => 'index', 'uses' => 'WireframeController@zonePrice_list'
//    ]);
    Route::get('wareHouse/add', [
      'as' => 'index', 'uses' => 'WireframeController@wareHouse_add'
    ]);
    Route::get('wareHouse/list', [
      'as' => 'index', 'uses' => 'WireframeController@wareHouse_list'
    ]);
    // Route::get('merchant/add', [
    //   'as' => 'index', 'uses' => 'WireframeController@merchant_add'
    // ]);
    // Route::get('merchant/list', [
    //   'as' => 'index', 'uses' => 'WireframeController@merchant_list'
    // ]);

    Route::get('package_weight/list', [
      'as' => 'index', 'uses' => 'WireframeController@package_weight_list'
    ]);
    
     Route::get('package_weight/add', [
      'as' => 'index', 'uses' => 'WireframeController@package_weight_add'
    ]);
    
    Route::get('store_management/list', [
      'as' => 'index', 'uses' => 'WireframeController@store_management_list'
    ]);
    
     Route::get('store_management/add', [
      'as' => 'index', 'uses' => 'WireframeController@store_management_add'
    ]);
    
    Route::get('bin_management/list', [
      'as' => 'index', 'uses' => 'WireframeController@bin_management_list'
    ]);
    
     Route::get('bin_management/add', [
      'as' => 'index', 'uses' => 'WireframeController@bin_management_add'
    ]);


    Route::get('bintobin/list', [
        'as' => 'index', 'uses' => 'WireframeController@binToBinTransfer_list'
    ]);
    Route::get('bintobin/add', [
        'as' => 'index', 'uses' => 'WireframeController@binToBinTransfer_add'
    ]);

    Route::get('truck/add', [
      'as' => 'index', 'uses' => 'WireframeController@truck_add'
    ]);
    Route::get('truck/list', [
      'as' => 'index', 'uses' => 'WireframeController@truck_list'
    ]);

//    Route::get('vehicle/add', [
////        'as' => 'index', 'uses' => 'WireframeController@vehicle_add'
////    ]);
////    Route::get('vehicle/list', [
////        'as' => 'index', 'uses' => 'WireframeController@vehicle_list'
////    ]);

//    Route::get('rider/add', [
//      'as' => 'index', 'uses' => 'WireframeController@rider_add'
//    ]);
//    Route::get('rider/list', [
//      'as' => 'index', 'uses' => 'WireframeController@rider_list'
//    ]);

    Route::get('subBin/add', [
      'as' => 'index', 'uses' => 'WireframeController@subBin_add'
    ]);
    Route::get('subBin/list', [
      'as' => 'index', 'uses' => 'WireframeController@subBin_list'
    ]);

    Route::get('pickup/lists', [
        'as' => 'index', 'uses' => 'WireframeController@pickup_list'
    ]);

    Route::get('pendingPickUp/list', [
      'as' => 'index', 'uses' => 'WireframeController@pendingPickUp_list'
    ]);
    Route::get('pendingPickUp/more', [
      'as' => 'index', 'uses' => 'WireframeController@pendingPickUp_more'
    ]);
    Route::get('pendingPickUp/edit', [
      'as' => 'index', 'uses' => 'WireframeController@pendingPickUp_edit'
    ]);
    

    Route::get('sPickUp/list', [
      'as' => 'index', 'uses' => 'WireframeController@sPickUp_list'
    ]);
    Route::get('sPickUp/more', [
      'as' => 'index', 'uses' => 'WireframeController@sPickUp_more'
    ]);

//    Route::get('delivery/list', [
//        'as' => 'index', 'uses' => 'WireframeController@delivery_list'
//    ]);
//    Route::get('delivery/edit', [
//        'as' => 'index', 'uses' => 'WireframeController@delivery_edit'
//    ]);

    Route::get('pendingDelivery/list', [
      'as' => 'index', 'uses' => 'WireframeController@pendingDelivery_list'
    ]);
    Route::get('pendingDelivery/more', [
      'as' => 'index', 'uses' => 'WireframeController@pendingDelivery_more'
    ]);

    Route::get('sDelivery/list', [
      'as' => 'index', 'uses' => 'WireframeController@sDelivery_list'
    ]);
    Route::get('sDelivery/more', [
      'as' => 'index', 'uses' => 'WireframeController@sDelivery_more'
    ]);

    Route::get('deliveryApproval/list', [
      'as' => 'index', 'uses' => 'WireframeController@deliveryApproval_list'
    ]);
    Route::get('deliveryApproval/more', [
      'as' => 'index', 'uses' => 'WireframeController@deliveryApproval_more'
    ]);

    Route::get('stockIn/list', [
      'as' => 'index', 'uses' => 'WireframeController@stockIn_list'
    ]);
    Route::get('stockIn/add', [
      'as' => 'index', 'uses' => 'WireframeController@stockIn_add'
    ]);

    Route::get('stockOut/listSearch', [
      'as' => 'index', 'uses' => 'WireframeController@stockOut_listSearch'
    ]);
});

/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('forget-password', [
    'as' => 'forget.password', 'uses' => 'PasswordController@getForgetPassword'
]);

Route::post('forget-password', [
    'as' => 'forget.password', 'uses' => 'PasswordController@putForgetPassword'
]);

Route::get('password-reset/{token}', [
    'as' => 'password.reset', 'uses' => 'PasswordController@getResetPassword'
]);

Route::post('password-reset/new', [
    'as' => 'password.reset.new', 'uses' => 'PasswordController@passwordReset'
]);










