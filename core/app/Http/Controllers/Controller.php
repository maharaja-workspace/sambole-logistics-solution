<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\Activity;
use Route;
use Session;
use Request;
use Sentinel;


abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	public function activity_create($data)
	{
		$ip=Request::ip();
		$user_id=0;
		if ($user = Sentinel::getUser())
		{
		   $user_id=$user->id;
		}
		

		/*Insert to activity table*/
		$activity = Activity::create([
			'user_id' => $user_id,
			'description' => $data,
			'ip_address' => $ip,
			'type' => 'type',
		]);
	}

}
