<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;

/*USAGE LIBRARY*/
use File;
use Response;
use Input;
use DB;
use Session;
use Mail;
use Sentinel;
use PDF;
use View;
use Hash;
use Carbon\Carbon;
use Permissions\Models\Permission;
use Validator;
use Image;
use Storage;
// use Intervention\Image\ImageManager;



class ImageController extends Controller {

    /*
        |--------------------------------------------------------------------------
        | Image Controller
        |--------------------------------------------------------------------------
        |
        | This controller renders the "marketing page" for the application and
        | is configured to only allow guests. Like most of the other sample
        | controllers, you are free to modify or remove it as you desire.
        |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest');
    }


    /*
    $folder => folder name 
    $file => upload file
    $file_name =>name of save file
    */
    public function main_path()
    {
        // return url('') . '/core/storage/';
        return Config('constants.bucket.url');
      
    }
    public function Upload($folder,$file,$file_name,$id) { 
       /* $path='uploads/images/'.$folder;
        if (!file_exists(storage_path($path))) {
             File::makeDirectory(storage_path($path));
        }

        $file=$file;        
        $destinationPath = storage_path($path);  
        $file->move($destinationPath, $file_name);
        $img = Image::make($destinationPath.'/'.$file_name);
        // $img = Image::canvas(272, 204, '#ff0000');
       // now you are able to resize the instance
        $img->resize(272, 204);

        // and insert a canverse for example
         // $img->canvas(272, 204, '#ccc');;

        // and insert a watermark for example
        // $img->insert('public/watermark.png');

        // finally we save the image as a new file
        $img->save($destinationPath.'/'.$file_name);
        // $img->canvas(272, 204, '#ccc');;
        return $path;*/
/*=======================*/
        // $disk = Storage::disk('gcs');
        // $path='uploads/images/'.$folder.'/'.$id;
        // $homepage = file_get_contents($file);
        // $disk->put($path.'/'.$file_name, $homepage); 
        // return $path;
        /*======================*/

        $disk = Storage::disk('gcs');
        $path='uploads/TEST/images/'.$folder.'/'.$id;
        /*$img = Image::make($file);
        $img->insert(url('') . '/core/storage/uploads/sambole_watermark.png','center', 10, 10);


        // $homepage = file_get_contents($file);
        $disk->put($path.'/'.$file_name, $img->stream()); 
        return $path;*/

        /*start*/
        $watermark =  Image::make(storage_path('uploads/sambole_watermark.png'));
        $img_orginel = Image::make($file);
        $img_orginel = Image::make($file)->exif();
       
        $degree=0;
        if (isset($img_orginel['Orientation'])) {
          $degree=$this->orientation_to_degree($img_orginel['Orientation'] );
       
        }
       
        $img_orginel = Image::make($file)->rotate($degree);      
        $img = Image::make($file)->rotate($degree);
        //#1
        $watermarkSize = $img->width() - 20; //size of the image minus 20 margins
        //#2
        $watermarkSize = $img->width() / 2; //half of the image size
        //#3
        $resizePercentage = 50;//70% less then an actual image (play with this value)
        $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

        // resize watermark width keep height auto
        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        //insert resized watermark to image center aligned
        // $img->insert($watermark, 'center');
        $img->insert($watermark, 'bottom-center');
        // $newimg = (string) Image::make($img->stream())->encode('data-url',75);
        $newimg = (string) Image::make($img->stream())->encode('jpg',20);
        $low_reselution =  Image::make($newimg);
        $low_reselution->pixel('#ff0000', 64, 64);
        //save new image
        //$img->save(storage_path('app/images/watermark-test.jpg'));
         // $disk->put($path.'/'.$file_name, $img->stream()); 
         $disk->put($path.'/'.$file_name, $low_reselution->stream()); 
         $disk->put($path.'/'.'S-'.$file_name, $img_orginel->stream()); 
        return $path;



        /*End*/
        //  $disk = Storage::disk('gcs');
        //  $manager = new ImageManager(array('driver' => 'imagick'));
        // $path='uploads/images/'.$folder.'/'.$id;
        // $img=$manager->make($file);
         // resize image to 200x200 and keep the aspect ratio
        // $img->resize(200, 200, function ($constraint) {
        //     $constraint->aspectRatio();
        // });

        // Fill up the blank spaces with transparent color
        // $img->resizeCanvas(300, 150, 'center', false, array(255, 255, 255, 0));
        // $homepage = file_get_contents($file);
        // $disk->put($path.'/'.$file_name, $img->stream()); 
    
        // return $path;

     
    }
    public function Upload_slider($folder,$file,$file_name) { 
       //  $path='uploads/images/'.$folder;
       //  if (!file_exists(storage_path($path))) {
       //       File::makeDirectory(storage_path($path));
       //  }

       //  $file=$file;        
       //  $destinationPath = storage_path($path);  
       //  $file->move($destinationPath, $file_name);
       //  $img = Image::make($destinationPath.'/'.$file_name);
       // // now you are able to resize the instance
       //  $img->resize(1024, 250);

       //  // and insert a watermark for example
       //  // $img->insert('public/watermark.png');

       //  // finally we save the image as a new file
       //  $img->save($destinationPath.'/'.$file_name);
       //  return $path;
        
        $disk = Storage::disk('gcs');
        $path='uploads/TEST/images/'.$folder.'/';
        $slider_image = Image::make($file);
        $disk->put($path.'/'.$file_name,$slider_image->stream()); 
        return $path;
     
    }

    public function upload_logo($folder,$file,$file_name,$id)
    {
      
        $disk = Storage::disk('gcs');
        $path='uploads/TEST/images/'.$folder.'/'.$id;
        /*start*/
       
        $img_orginel = Image::make($file);
     
         $disk->put($path.'/'.$file_name, $img_orginel->stream()); 
        return $path;

    }
    public function read()
    {
        # code...
    }

    public function upload_google_bucket_form()
    {
        return view('front.test');

      $disk = Storage::disk('gcs');
      $disk->put('$file_name', $file);
      // $exists = $disk->exists('cloudlogin.txt');
      // print_r($exists);

        // create a file
        // return $time = $disk->lastModified('ppl.sql');
    }
    public function upload_google_bucket_post_form(Request $request)
        {
             
         
           $project_files = $request->file('product_image');
            if ($project_files) {
                $i=0;
                foreach ($project_files as $key => $project_file) {
                    if (File::exists($project_file)) {
                        $file = $project_file;

                        $extn =$file->getClientOriginalExtension();                         
                        $project_fileName = 'product-' .date('YmdHis') .'-'.$i. '.' . $extn;
                        /*IMAGE UPLOAD*/
                            /*
                                $path=$image->upload('FOLDERNAME',FILE,FILE NAME);
                            */
                               $this->upload_google_bucket('product',$file,$project_fileName);      
                              
                    }
                }
            }

         
          // $exists = $disk->exists('cloudlogin.txt');
          // print_r($exists);

            // create a file
            // return $time = $disk->lastModified('ppl.sql');
        }
    public function get_image_bucket()
    {
       return $this->get_google_bucket('uploads/images/product','product-20180719120734-0.jpg');

    }

    public function upload_google_bucket($folder,$file,$file_name)
    {
        $disk = Storage::disk('gcs');
        $path='uploads/images/'.$folder;
        $homepage = file_get_contents($file);
        $disk->put($path.'/'.$file_name, $homepage); 

    }
    public function get_google_bucket($folder,$file_name)
    {
        $disk = Storage::disk('gcs');
        $path='uploads/images/'.$folder;
        return $url = $disk->url($path.'/'.$file_name);
    }
  public function orientation_to_degree($oriention_no)
  {
    if ($oriention_no==3) {
      return 180;
    } else if($oriention_no==4) {
      return -180;
    }else if($oriention_no==5){
      return 90;
    }else if($oriention_no==6){
      return -90;
    }else if($oriention_no==7){
      return 270;
    }else if($oriention_no==8){
      return -270;
    }else{
      return 0;
    }
    
  }

   
}
