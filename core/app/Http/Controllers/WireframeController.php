<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WireframeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wireframe.index');
    }

     public function city_list()
    {
        return view('wireframe.city.list');
    }
    public function city_add()
    {
        return view('wireframe.city.add');
    }
	 public function packagetype_list()
    {
        return view('wireframe.packagetype.list');
    }
    public function packagetype_add()
    {
        return view('wireframe.packagetype.add');
    }
    public function delivery_type_list()
    {
        return view('wireframe.delivery_type.list');
    }
    public function delivery_type_add()
    {
        return view('wireframe.delivery_type.add');
    }
    public function package_size_list()
    {
        return view('wireframe.package_size.list');
    }
    public function package_size_add()
    {
        return view('wireframe.package_size.add');
    }
    
    public function package_weight_list()
    {
        return view('wireframe.package_weight.list');
    }
    public function package_weight_add()
    {
        return view('wireframe.package_weight.add');
    }
       public function store_management_list()
    {
        return view('wireframe.store_management.list');
    }
    public function store_management_add()
    {
        return view('wireframe.store_management.add');
    }
    public function bin_management_list()
    { 
    return view('wireframe.bin_management.list');
    }
    public function bin_management_add()
    {
        return view('wireframe.bin_management.add');
    }

    public function area_list()
    {
        return view('wireframe.area.list');
    }
    public function area_add()
    {
        return view('wireframe.area.add');
    }
    

//    public function route_list()
//    {
//        return view('wireframe.route.list');
//    }
//    public function route_add()
//    {
//        return view('wireframe.route.add');
//    }
//
//    public function zone_list()
//    {
//        return view('wireframe.zone.list');
//    }
//    public function zone_add()
//    {
//        return view('wireframe.zone.add');
//    }

    public function wareHouse_list()
    {
        return view('wireframe.wareHouse.list');
    }
    public function wareHouse_add()
    {
        
        return view('wireframe.wareHouse.add');
    }

    public function merchant_list()
    {
        return view('wireframe.merchant.list');
    }
    public function merchant_add()
    {
        return view('wireframe.merchant.add');
    }

    public function truck_list()
    {
        return view('wireframe.truck.list');
    }
    public function truck_add()
    {
        return view('wireframe.truck.add');
    }

    public function rider_list()
    {
        return view('wireframe.rider.list');
    }
    public function rider_add()
    {
        return view('wireframe.rider.add');
    }

    public function subBin_list()
    {
        return view('wireframe.subBin.list');
    }
    public function subBin_add()
    {
        return view('wireframe.subBin.add');
    }

    public function pendingPickUp_list()
    {
        return view('wireframe.pendingPickUp.list');
    }
    public function pendingPickUp_more()
    {
        return view('wireframe.pendingPickUp.more');
    }
    public function pendingPickUp_edit()
    {
        return view('wireframe.pendingPickUp.edit');
    }

    public function sPickUp_list()
    {
        return view('wireframe.sPickUp.list');
    }
    public function sPickUp_more()
    {
        return view('wireframe.sPickUp.more');
    }

    public function pendingDelivery_list()
    {
        return view('wireframe.pendingDelivery.list');
    }
    public function pendingDelivery_more()
    {
        return view('wireframe.pendingDelivery.more');
    }

    public function sDelivery_list()
    {
        return view('wireframe.sDelivery.list');
    }
    public function sDelivery_more()
    {
        return view('wireframe.sDelivery.more');
    }

    public function deliveryApproval_list()
    {
        return view('wireframe.deliveryApproval.list');
    }
    public function deliveryApproval_more()
    {
        return view('wireframe.deliveryApproval.more');
    }

    /*public function stockIn_list()
    {
        return view('wireframe.stockIn.list');
    }*/

    public function stockOut_listSearch()
    {
        return view('wireframe.stockOut.listSearch');
    }

    public function vehicle_list()
    {
        return view('wireframe.vehicle.list');
    }
//    public function vehicle_add()
//    {
//        return view('wireframe.vehicle.add');
//    }
//
   public function zonePrice_list()
   {
       return view('wireframe.zonePrice.list');
   }
    public function zonePrice_add()
    {
        return view('wireframe.zonePrice.add');
    }

    public function packageTypeSize_list()
    {
        return view('wireframe.packageTypeSize.list');
    }
    public function packageTypeSize_add()
    {
        return view('wireframe.packageTypeSize.add');
    }

    public function dailyPlan_listAdd()
    {
        return view('wireframe.dailyPlan.listAdd');
    }

    public function pickup_list()
    {
        return view('wireframe.pickup.list');
    }

    public function delivery_list()
    {
        return view('wireframe.delivery.list');
    }
    public function delivery_edit()
    {
        return view('wireframe.delivery.edit');
    }

    public function stockIn_list()
    {
        return view('wireframe.stockIn.list');
    }
    public function stockIn_add()
    {
        return view('wireframe.stockIn.add');
    }

    public function stockOut_list()
    {
        return view('wireframe.stockOut.list');
    }
    public function stockOut_add()
    {
        return view('wireframe.stockOut.add');
    }

    public function binToBinTransfer_list()
    {
        return view('wireframe.binToBinTransfer.list');
    }
    public function binToBinTransfer_add()
    {
        return view('wireframe.binToBinTransfer.add');
    }
}
