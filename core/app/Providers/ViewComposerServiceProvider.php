<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

/* MODELS */
use App\Models\Location;
use App\Models\Category;
use App\Models\Config;


class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

       
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
