<?php namespace App\Providers;

use Validator;
use Schema;
use Illuminate\Support\ServiceProvider;
use App\Validators\ReCaptcha;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        Validator::extend('recaptcha','App\\Validators\\ReCaptcha@validate');
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
