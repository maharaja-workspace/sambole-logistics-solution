<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransactionTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stock_transaction';

    /**
     * Run the migrations.
     * @table stock_transaction
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('bin_id')->nullable()->comment('only required when the stock is transferred to the zone');
            $table->integer('store_id')->nullable();
            $table->enum('transaction_type', ['in', 'out'])->nullable();
            $table->enum('type', ['bin', 'store'])->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
