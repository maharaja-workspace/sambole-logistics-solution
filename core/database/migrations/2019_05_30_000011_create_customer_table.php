<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customers';

    /**
     * Run the migrations.
     * @table customer
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('user_id', 45)->nullable();
            $table->string('first_name', 45)->nullable();
            $table->string('last_name', 45)->nullable();
            $table->string('mobile', 45)->nullable();
            $table->string('address_line1', 45)->nullable();
            $table->string('address_line2', 45)->nullable();
            $table->integer('city_id')->nullable();
            $table->string('email', 45)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('merchant_id')->nullable();

//            $table->index(["city_id"], 'fk_city_idx');
//
//            $table->index(["merchant _id"], 'fk_merchant_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


//            $table->foreign('merchant _id', 'fk_merchant_idx')
//                ->references('id')->on('merchant')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('city_id', 'fk_city_idx')
//                ->references('id')->on('city')
//                ->onDelete('no action')
//                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
