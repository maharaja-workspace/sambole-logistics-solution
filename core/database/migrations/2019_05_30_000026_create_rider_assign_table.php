<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiderAssignTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'rider_assigns';

    /**
     * Run the migrations.
     * @table rider_assign
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('rider_id');
            $table->integer('store_id');
            $table->integer('route_id');
            $table->integer('request_id');
            $table->tinyInteger('active')->default(1)->comment('0= deactive
1= active');
            $table->integer('assigned_by');
            $table->tinyInteger('completed')->default(0)->comment('default value is 0
when the zone manger assigs a request to a user value = 0
when a rider pickups the package  value = 1 ');
            $table->nullableTimestamps();
            $table->softDeletes();

//            $table->index(["rider_id"], 'fk_rider_idx');
//
//            $table->index(["route_id"], 'fk_route_idx');
//
//            $table->index(["request_id"], 'fk_request_idx');
//
//            $table->index(["assinged _by"], 'fk_user_idx');
//
//            $table->index(["store_id"], 'fk_store_idx');
//
//
//            $table->foreign('request_id', 'fk_request_idx')
//                ->references('id')->on('request')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('rider_id', 'fk_rider_idx')
//                ->references('id')->on('rider')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('route_id', 'fk_route_idx')
//                ->references('id')->on('route')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('store_id', 'fk_store_idx')
//                ->references('id')->on('store')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('assinged _by', 'fk_user_idx')
//                ->references('id')->on('user')
//                ->onDelete('no action')
//                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
