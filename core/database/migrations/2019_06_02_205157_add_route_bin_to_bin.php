<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddRouteBinToBin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE bins CHANGE COLUMN bin_type bin_type ENUM('zone', 'vehicle', 'route', 'extra_bin') NOT NULL DEFAULT 'zone'");
//        Schema::table('bins', function (Blueprint $table) {
//            $table->enum('bin_type', ['zone', 'extra_bin', 'vehicle', 'route'])->change();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('bins', function (Blueprint $table) {
//            $table->enum('bin_type', ['zone', 'extra_bin', 'vehicle', 'route']);
//        });
    }
}
