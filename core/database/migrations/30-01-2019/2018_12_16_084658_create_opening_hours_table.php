<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_registration_detail_id');
            $table->text('format');
            $table->text('monday_start')->nullable();
            $table->text('monday_end')->nullable();
            $table->text('tuesday_start')->nullable();
            $table->text('tuesday_end')->nullable();
            $table->text('wednesday_start')->nullable();
            $table->text('wednesday_end')->nullable();
            $table->text('thursday_start')->nullable();
            $table->text('thursday_end')->nullable();
            $table->text('friday_start')->nullable();
            $table->text('friday_end')->nullable();
            $table->text('saturday_start')->nullable();
            $table->text('saturday_end')->nullable();
            $table->text('sunday_start')->nullable();
            $table->text('sunday_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opening_hours');
    }
}
