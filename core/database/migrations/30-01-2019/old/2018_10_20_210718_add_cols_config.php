<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config', function (Blueprint $table) {
            $table->string('logo_width')->change();
            $table->string('logo_height')->change();
            $table->text('watermark')->nullable()->after('logo_height');
            $table->string('wt_width')->nullable()->after('watermark');
            $table->string('wt_height')->nullable()->after('wt_width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config', function (Blueprint $table) {
            $table->dropColumn(['watermark', 'wt_width', 'wt_height']);
        });
    }
}
