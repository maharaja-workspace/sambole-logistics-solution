<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
         Schema::create('sa_product_image', function (Blueprint $table) {
            $table->increments('id');     
            $table->integer('product_id');                   
            $table->text('path')->nullable();   
            $table->text('filename')->nullable();              
            $table->tinyInteger('status')->default(0); 
                    
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_product_image');
    }
}
