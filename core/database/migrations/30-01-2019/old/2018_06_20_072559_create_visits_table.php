<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('visits', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('item_id');                                   
            $table->text('ip_address')->nullable(); 
            $table->text('city')->nullable(); 
            $table->text('type')->nullable(); 
            $table->integer('user_id'); 
            $table->tinyInteger('status')->default(0);;

            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visits');
    }
}