<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessRegisterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('business_registration_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');             
            $table->text('business_name')->nullable();   
            $table->integer('industry_id');     
            $table->text('company_address')->nullable();              
            $table->text('contact_number')->nullable();              
            $table->text('nominal_capital')->nullable();              
            $table->text('business_page_name')->nullable();              
            $table->text('website')->nullable();              
            $table->text('facebook_url')->nullable();              
            $table->text('googleplus_url')->nullable();              
            $table->text('electronic_signature')->nullable();              
            $table->tinyInteger('agreement');              
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('business_registration_details');
    }
}
