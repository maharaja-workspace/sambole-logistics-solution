<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSortTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
         Schema::create('sa_sort_type', function (Blueprint $table) {
            $table->increments('id');     
            $table->integer('value');                   
            $table->text('name')->nullable(); 
            $table->tinyInteger('status')->default(0); 
                    
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_sort_type');
    }
}
