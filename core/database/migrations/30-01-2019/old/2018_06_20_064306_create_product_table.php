<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       public function up()
    {
         Schema::create('sa_product', function (Blueprint $table) {
            $table->increments('id');                      
            $table->text('name')->nullable();   
            $table->text('description')->nullable();   
            $table->double('price')->default(0);   
            $table->double('current_stock')->default(0);   
            $table->integer('category_id');     
            $table->tinyInteger('price_negotiable');     
            $table->integer('user_id');      
            $table->tinyInteger('status')->default(0);;  
                       
                    
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_product');
    }
}
