<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config', function (Blueprint $table) {
            $table->string('version', 15)->after('days_count')->nullable();
            $table->text('logo')->after('version')->nullable();
            $table->string('contact')->after('logo')->nullable();
            $table->string('email')->after('contact')->nullable();
            $table->string('facebook')->after('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config', function (Blueprint $table) {
            $table->dropColumn(['version', 'logo', 'contact', 'email', 'facebook']);
        });
    }
}