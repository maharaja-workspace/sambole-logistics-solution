<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       public function up()
    {
         Schema::create('sa_slider_images', function (Blueprint $table) {
            $table->increments('id');     
            $table->integer('layout_id');                   
            $table->text('img_path')->nullable();   
            $table->text('img_name')->nullable();              
            $table->tinyInteger('status')->default(0); 
                    
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_slider_images');
    }
}
