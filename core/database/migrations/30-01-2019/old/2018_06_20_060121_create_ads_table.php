<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');          
            $table->text('ad_title');          
            $table->text('ad_description')->nullable();;          
            $table->text('ad_price')->nullable();;          
            $table->tinyInteger('call_for_info');          
            $table->tinyInteger('ad_negotiable_price')->nullable();          
            $table->integer('ad_location_id');          
            $table->integer('ad_category_id');          
            $table->tinyInteger('status')->default(0);          
            $table->integer('user_id');          
            $table->text('contactNos')->nullable();          
            $table->text('reject_reason')->nullable();          
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
