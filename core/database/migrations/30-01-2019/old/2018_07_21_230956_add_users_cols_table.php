<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('city', 32)->after('email')->nullable();
            $table->string('cover_image')->after('mobile')->nullable();
            $table->string('avatar')->after('cover_image')->nullable();
            $table->string('description')->after('avatar')->nullable();
            $table->string('twitter_url')->after('google_plus_url')->nullable();
            $table->string('instagram_url')->after('twitter_url')->nullable();
            $table->string('linkedin_url')->after('instagram_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumns(['city', 'cover_image', 'avatar', 'description', 'twitter_url', 'instagram_url', 'linkedin_url'])
        });
    }
}
