<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdEditHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
       Schema::create('ad_edit_history', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('ad_id'); 
            $table->integer('user_id');                              
            $table->text('edited')->nullable(); 
            $table->tinyInteger('status')->default(0);;

            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_edit_history');
    }
}
