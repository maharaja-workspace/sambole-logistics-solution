<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_registration_details', function (Blueprint $table) {
            $table->text('about')->nullable()->after('website');
            $table->string('open')->nullable()->after('about');
            $table->string('close')->nullable()->after('open');
            $table->text('instagram_url')->nullable()->after('googleplus_url');
            $table->text('twitter_url')->nullable()->after('instagram_url');
            $table->text('linkedin_url')->nullable()->after('twitter_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_registration_details', function (Blueprint $table) {
            $table->dropColumn(['instagram_url', 'twitter_url', 'linkedin_url', 'about', 'open', 'close']);
        });
    }
}
