<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFontsListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       public function up()
    {
       Schema::create('fonts-list', function (Blueprint $table) {
            $table->increments('id');                      
            $table->text('type')->nullable();   
            $table->text('icon')->nullable();   
            $table->text('unicode')->nullable(); 
            $table->tinyInteger('status')->default(0);;

            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fonts-list');
    }
}
