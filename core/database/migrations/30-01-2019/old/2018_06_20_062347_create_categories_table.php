<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
         Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');              
            $table->text('name')->nullable();   
            $table->integer('main_category_id'); 
            $table->tinyInteger('status');   
                      
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
