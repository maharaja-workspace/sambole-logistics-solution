<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutCustomizationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
         Schema::create('sa_layout_customization_details', function (Blueprint $table) {
            $table->increments('id');                      
            $table->text('logo_img')->nullable();   
            $table->integer('header_style_id');     
            $table->string('header_color_code')->nullable();              
            $table->string('background_color_code')->nullable();  
            $table->integer('user_id');             
                    
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable(); ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sa_layout_customization_details');
    }
}
