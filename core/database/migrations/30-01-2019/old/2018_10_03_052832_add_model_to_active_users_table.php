<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModelToActiveUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('active_users', function (Blueprint $table) {
        $table->dropColumn('ad_id');
          $table->integer('model_id');
          $table->text('model');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('active_users', function (Blueprint $table) {
          $table->dropColumn('model_id');
          $table->dropColumn('model');
      });
    }
}
