<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiderVehicleTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'rider_vehicles';

    /**
     * Run the migrations.
     * @table rider_vehicle
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('rider_id');
            $table->integer('vehicle_id');
            $table->timestamp('assigned_date');
            $table->integer('assigned_by');
            $table->tinyInteger('status')->default(0);
            $table->nullableTimestamps();
            $table->softDeletes();

//            $table->index(["rider_id"], 'fk_rider_idx');
//
//            $table->index(["vehicle_id"], 'fk_vehicle_idx');
//
//            $table->index(["assigned_by"], 'fk_user_idx');
//
//
//            $table->foreign('rider_id', 'fk_rider_idx')
//                ->references('id')->on('rider')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('vehicle_id', 'fk_vehicle_idx')
//                ->references('id')->on('vehicle')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('assigned_by', 'fk_user_idx')
//                ->references('id')->on('user')
//                ->onDelete('no action')
//                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
