<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCallForInfoProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('sa_temporary_products', function ($table) {
            $table->integer('call_for_info');
        });
         Schema::table('sa_product', function ($table) {
            $table->integer('call_for_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sa_temporary_products', function ($table) {
            $table->drop('call_for_info');
        });
        Schema::table('sa_product', function ($table) {
            $table->drop('call_for_info');
        });
    }
}
