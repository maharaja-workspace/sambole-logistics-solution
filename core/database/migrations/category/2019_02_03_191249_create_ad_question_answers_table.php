<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_question_id');
            $table->integer('ad_id');
            $table->text('answer')->nullable();
            $table->integer('answer_option_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad_question_answers');
    }
}
