<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToCategoryQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
        Schema::table('category_questions', function ($table) {
            $table->text('hint')->nullable();
            $table->integer('checkbox')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_questions', function ($table) {
            $table->drop('hint');
            $table->drop('checkbox');
        });
    }
}
