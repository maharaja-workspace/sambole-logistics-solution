<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesToCategoryQuestionsTable extends Migration
{
   
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
        Schema::table('category_questions', function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_questions', function ($table) {
            $table->drop('deleted_at');
        });
    }
}
