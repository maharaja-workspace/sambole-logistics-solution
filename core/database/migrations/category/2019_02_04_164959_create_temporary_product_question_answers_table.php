<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryProductQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary_product_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_question_id');
            $table->integer('product_id');
            $table->text('answer');
            $table->integer('answer_option_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temporary_product_question_answers');
    }
}
