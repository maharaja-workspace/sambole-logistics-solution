<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRoute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function ($table) {
            $table->increments('id');
            $table->integer('zone_id');
            $table->integer('pickup_truck_city_id')->nullable();
            $table->string('name');
            $table->string('code');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routes');
    }
}
