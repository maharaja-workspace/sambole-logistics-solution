<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBinType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bins', function (Blueprint $table) {
            $table->dropColumn('bin_type');
        });

        Schema::table('bins', function (Blueprint $table) {
            $table->enum('bin_type', ['zone', 'extra_bin', 'vehicle']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bins', function (Blueprint $table) {
            $table->enum('bin_type', ['zone', 'extra_bin', 'vehicle']);
        });
    }
}
