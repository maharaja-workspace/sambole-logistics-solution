<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToZoneToZonePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zone_to_zone_prices', function (Blueprint $table){

            $table->integer('per_kilo_price')->nullable()->after('price');
            $table->integer('starting_slab_price')->nullable()->after('per_kilo_price');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zone_to_zone_prices', function (Blueprint $table){

            $table->dropColumn('per_kilo_price');
            $table->dropColumn('starting_slab_price');

        });
    }
}
