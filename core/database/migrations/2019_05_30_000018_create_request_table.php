<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'requests';

    /**
     * Run the migrations.
     * @table request
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('request_type', ['delivery', 'return'])->nullable();
            $table->string('order_id', 45)->nullable()->comment('unique prefix - ex: 000001');
            $table->string('pickup_address', 45)->nullable()->comment('NO:');
//            $table->string('pickup_address_line2', 45)->nullable();
            $table->integer('pickup_city')->nullable();
            $table->string('buyer_name', 45)->nullable();
            $table->string('buyer_mobile', 45)->nullable();
            $table->string('buyer_address', 45)->nullable();
//            $table->string('buyer_address_line2', 45)->nullable();
            $table->integer('buyer_city')->nullable();
            $table->integer('payment_type')->nullable();
            $table->integer('delivery_type')->nullable();
            $table->integer('package_type')->nullable();
            $table->integer('package_size')->nullable();
            $table->double('package_type_size_price')->nullable();
            $table->integer('package_weight')->nullable();
            $table->double('total_amount')->nullable();
            $table->double('tax_amount')->nullable();
            $table->double('package_weight_price')->nullable();
            $table->string('remark', 45)->nullable();
            $table->tinyInteger('status')->nullable()->comment('0 = pending 
1 = assigned for a rider
2 =  picked
3 = zone in 
4 = zone out
5 = delivered ');
            $table->integer('created_by')->nullable();
            $table->integer('customer_id')->nullable();
            $table->string('customer_nic', 45)->nullable()->comment('image URL');
            $table->string('buyer_nic', 45)->nullable()->comment('image URL');

//            $table->index(["package_size"], 'fk_package_size_idx');
//
//            $table->index(["payment_type"], 'fk_payment_type_idx');
//
//            $table->index(["pickup_city"], 'fk_city_idx');
//
//            $table->index(["delivery_type"], 'fk_delivery_type_idx');
//
//            $table->index(["customer_id"], 'fk_customer_idx');
//
//            $table->index(["package_type"], 'fk_package_type_idx');
//
//            $table->index(["buyer's_city"], 'fk_city_idx1');
//
//            $table->index(["package_weight"], 'fk_package_weight_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


//            $table->foreign('pickup_city', 'fk_city_idx')
//                ->references('id')->on('city')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('s_city', 'fk_city_idx1')
//                ->references('id')->on('city')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('delivery_type', 'fk_delivery_type_idx')
//                ->references('id')->on('pickup_delivery_type')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('package_type', 'fk_package_type_idx')
//                ->references('id')->on('package_type')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('package_size', 'fk_package_size_idx')
//                ->references('id')->on('package_size')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('package_weight', 'fk_package_weight_idx')
//                ->references('id')->on('package_weight')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('payment_type', 'fk_payment_type_idx')
//                ->references('id')->on('payment_type')
//                ->onDelete('no action')
//                ->onUpdate('no action');
//
//            $table->foreign('customer_id', 'fk_customer_idx')
//                ->references('id')->on('customer')
//                ->onDelete('no action')
//                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
