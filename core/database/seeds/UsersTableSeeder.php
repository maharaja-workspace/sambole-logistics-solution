<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	/*DEVELOPER CREATION*/
        $credentials_developer = [
                    'first_name' => 'Developer',
                    'last_name' => 'Cybertech',
                    'email' => 'developer@cybertech.lk',                    
                    'username' => 'developer@cybertech.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_developer = Sentinel::registerAndActivate($credentials_developer);
         $user_developer->makeRoot();
         $role_developer= Sentinel::findRoleById(1);
         $role_developer->users()->attach($user_developer);

         /*SYSTEM ADMIN CREATION*/
        $credentials_admin = [
                    'first_name' => 'Admin',
                    'last_name' => 'Logistics',
                    'email' => 'admin@logistics.lk',                    
                    'username' => 'admin@logistics.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_admin = Sentinel::registerAndActivate($credentials_admin);
         $user_admin->makeChildOf($user_developer);
         $role_admin = Sentinel::findRoleById(2);
         $role_admin->users()->attach($user_admin);

          /*Zone User CREATION*/
        $credentials_zone = [
                    'first_name' => 'Zone',
                    'last_name' => 'Manager',
                    'email' => 'zone@logistics.lk',                    
                    'username' => 'zone@logistics.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_zone = Sentinel::registerAndActivate($credentials_zone);
         $user_zone->makeChildOf($user_admin);
         $role_zone = Sentinel::findRoleById(3);
         $role_zone->users()->attach($user_zone);

          /*Mobile Driver User CREATION*/
        $credentials_mobile_driver = [
                    'first_name' => 'Mobile',
                    'last_name' => 'Driver',
                    'email' => 'mdriver@logistics.lk',                    
                    'username' => 'mdriver@logistics.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_mobile_driver = Sentinel::registerAndActivate($credentials_mobile_driver);
         $user_mobile_driver->makeChildOf($user_zone);
         $role_mobile_driver = Sentinel::findRoleById(4);
         $role_mobile_driver->users()->attach($user_mobile_driver);

           /*Mobile Driver User CREATION*/
        $credentials_rider = [
                    'first_name' => 'Zone',
                    'last_name' => 'Rider',
                    'email' => 'rider@logistics.lk',                    
                    'username' => 'rider@logistics.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_rider = Sentinel::registerAndActivate($credentials_rider);
         $user_rider->makeChildOf($user_zone);
         $role_rider = Sentinel::findRoleById(5);
         $role_rider->users()->attach($user_rider);

          /*Truck Driver User CREATION*/
        $credentials_truck_driver = [
                    'first_name' => 'Truck',
                    'last_name' => 'Driver',
                    'email' => 'tdriver@logistics.lk',                    
                    'username' => 'tdriver@logistics.lk',
                    'password' =>'123456',
                    'confirmed'=>1
                ];
         $user_truck_driver = Sentinel::registerAndActivate($credentials_truck_driver);
         $user_truck_driver->makeChildOf($user_admin);
         $role_truck_driver = Sentinel::findRoleById(6);
         $role_truck_driver->users()->attach($user_truck_driver);


    }
}
