<?php

use Illuminate\Database\Seeder;

class CategoryQuestionsValidationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_questions_validations')->insert([
            'name' => 'Required',
            'rule_tag' => 'required'
        ]);
        DB::table('category_questions_validations')->insert([
            'name' => 'Maximum Value',
            'rule_tag' => 'max'
        ]);
        DB::table('category_questions_validations')->insert([
            'name' => 'Minimum Value',
            'rule_tag' => 'min'
        ]);
        DB::table('category_questions_validations')->insert([
            'name' => 'Numeric',
            'rule_tag' => 'int'
        ]);
        DB::table('category_questions_validations')->insert([
            'name' => 'Price',
            'rule_tag' => 'price'
        ]);
    }
}
