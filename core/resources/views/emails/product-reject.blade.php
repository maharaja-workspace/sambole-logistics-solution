<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: left;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User,</b></h3>

    <div>

        <h4>
            Your product  is rejected as it has not followed our guidelines or not posted in the correct category. Please use below URL to edit your product or contact Sambole team.
            
        </h4>
        <a href="{{ $url}}" class="btn">My Products</a><br>
     

        <h5>
            Thank You, 
            <br>
            Team Sambole.
        </h5>
    </div>
    <hr>
</body>
</html>