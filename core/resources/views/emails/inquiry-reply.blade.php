<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h3><b>You asked:</b></h3>
    <h4>{!! $msg !!}</h4>

    <hr>

    <h3><b>Reply:</b></h3>
    <h4>{!! $reply !!}</h4>
    
    <div>
    <h5>
        Thank You, 
        <br>
        Team Sambole.
    </h5>
    </div>
    <hr>
</body>
</html>