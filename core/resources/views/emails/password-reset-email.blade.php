<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: left;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User,</b></h3>

    <div>
        <h4>You have requested a password reset for your Sambole account, click below URL to change your password.</h4>
        
        <a href="{{ $url}}" class="btn">Reset Password</a><br>

        <h4>For further information please write to info@sambole.lk or contact us on 0117 696 300 </h4>
        
        <h5>
            Thank You, 
            <br>
            Team Sambole.
        </h5>
    </div>
    <hr>
    {{-- <h6>
        If you did not request a password reset, no further action is required.
        <br>
        Copyright © SAMBOLE.LK 2017 - 2018. All right reserved.
    </h6> --}}
</body>
</html>