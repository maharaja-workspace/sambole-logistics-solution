<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: left;
        margin-top: 0px;
    }
</style>
</head>
<body>
    <h3><b>Dear User,</b></h3>

    <div>
        @if($status == 2)
        <h4>{{ $email_message }}</h4>
        @if($reason !== 'Item sold')
        <a href="{{ $url}}" class="btn">My Ads</a><br>
        @endif

        @elseif($status == 3)
        <h4>Your ad {{ $title }} is unpublished as it has reached it’s validity period. If you wish to repost this ad please click below URL, or log into to your account to repost ads that has reached it’s validity period.
        </h4>
        <a href="{{ $url}}" class="btn">My Ads</a><br>
        @else

        <h4>
            Your ad is now live and the short code is '{{ strtoupper($short_code) }}', to edit the ad please click below URL.
        </h4>
        <a href="{{ $url}}" class="btn">My Ads</a><br>
        @endif

        <h5>
            Thank You, 
            <br>
            Team Sambole.
        </h5>
    </div>
    <hr>
</body>
</html>