<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h3><b>Dear User,</b></h3>

    <div>
        <h4>Thank you for reaching out! Our support team will attend to your inquiry and revert shortly.</h4>

        <h5>
            Thank You, 
            <br>
            Team Sambole.
        </h5>
    </div>
    <hr>
    {{-- <h5>Thank you for using our application!</h5>
    <h6>Copyright © SAMBOLE.LK 2017 - 2018. All right reserved.</h6> --}}
</body>
</html>