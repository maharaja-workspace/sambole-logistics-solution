<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
    a{
        text-decoration: none;
    }
    .btn{
        color: #fff!important;
        background-color: #630902;
        /*border: none;*/
        border-radius: 2px;
        display: inline-block;
        /*height: 24px;*/
        font-size: 14px;
        line-height: 16px;
        padding: 5px 8px;
        text-transform: uppercase;
        vertical-align: middle;
        text-align: center;
        width: auto;
        float: left;
        margin-top: 0px;
    }
    </style>
</head>
<body>
    <h3><b>Dear User,</b></h3>

    <div>
        <h4>We have received a request to authorize this email address for use with Sambole.lk, if you requested this verification please click below URL to confirm that you are authorized to use this email address. </h4>
        
        <a href="{{ $url}}" class="btn">Confirm Account</a><br>

        <h4>Your request will be processed upon confirming via above URL, this link expires 24hours after your original verification request. If you did NOT request to verify this email address, do not click on the link, kindly forward the request to info@sambole.lk</h4>

        <h5>
            Thank You, 
            <br>
            Team Sambole.
        </h5>
    </div>
    <hr>
    {{-- <h5>Thank you for using our application!</h5>
    <h6>Copyright © SAMBOLE.LK 2017 - 2018. All right reserved.</h6> --}}
</body>
</html>