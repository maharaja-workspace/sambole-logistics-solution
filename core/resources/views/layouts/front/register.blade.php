@extends('layouts.front.master')
<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
    .popover{
    width: 300px!important;
    max-width: none!important;
    z-index: 5!important;
    }
    .funkyradio div {
    margin-top:-20px;
    }
    .funkyradio label {
    width: 100%;
    border-radius: 3px;
    border: 1px solid #D1D3D4;
    font-weight: normal;
    }
    .funkyradio input[type="radio"]:empty,
    .funkyradio input[type="checkbox"]:empty {
    display: none;
    }
    .funkyradio input[type="radio"]:empty ~ label,
    .funkyradio input[type="checkbox"]:empty ~ label {
    position: relative;
    line-height: 2.5em;
    text-indent: 3.25em;
    margin-top: 1em;
    margin-bottom: 1em;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }
    .funkyradio input[type="radio"]:empty ~ label:before,
    .funkyradio input[type="checkbox"]:empty ~ label:before {
    position: absolute;
    display: block;
    top: 0;
    bottom: 0;
    left: 0;
    content: '';
    width: 2.5em;
    background: #D1D3D4;
    border-radius: 3px 0 0 3px;
    }
    .funkyradio input[type="radio"]:hover:not(:checked) ~ label,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
    color: #888;
    }
    .funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
    .funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
    font-family:'FontAwesome'!important;
    content: '\f0c0';
    text-indent: .9em;
    color: #C2C2C2;
    }
    .a input[type="radio"]:hover:not(:checked) ~ label:before,
    .a input[type="checkbox"]:hover:not(:checked) ~ label:before {
    content: '\f007'!important;
    }
    .b input[type="radio"]:hover:not(:checked) ~ label:before,
    .b input[type="checkbox"]:hover:not(:checked) ~ label:before {
    content: '\f0c0'!important;
    }
    .funkyradio input[type="radio"]:checked ~ label,
    .funkyradio input[type="checkbox"]:checked ~ label {
    color: #777;
    }
    .funkyradio input[type="radio"]:checked ~ label:before,
    .funkyradio input[type="checkbox"]:checked ~ label:before {
    font-family:'FontAwesome'!important;
    content: '\f0c0';
    text-indent: .9em;
    color: #333;
    background-color: #ccc;
    }
    .a input[type="radio"]:checked ~ label:before,
    .a input[type="checkbox"]:checked ~ label:before {
    content: '\f007'!important;
    }
    .b input[type="radio"]:checked ~ label:before,
    .b input[type="checkbox"]:checked ~ label:before {
    content: '\f0c0'!important;
    }
    .funkyradio input[type="radio"]:focus ~ label:before,
    .funkyradio input[type="checkbox"]:focus ~ label:before {
    box-shadow: 0 0 0 3px #999;
    }
    .funkyradio-default input[type="radio"]:checked ~ label:before,
    .funkyradio-default input[type="checkbox"]:checked ~ label:before {
    color: #333;
    background-color: #ccc;
    }
    .funkyradio-primary input[type="radio"]:checked ~ label:before,
    .funkyradio-primary input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #337ab7;
    }
    .funkyradio-success input[type="radio"]:checked ~ label:before,
    .funkyradio-success input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #59b453;
    }
    .funkyradio-danger input[type="radio"]:checked ~ label:before,
    .funkyradio-danger input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #d9534f;
    }
    .funkyradio-warning input[type="radio"]:checked ~ label:before,
    .funkyradio-warning input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #f0ad4e;
    }
    .funkyradio-info input[type="radio"]:checked ~ label:before,
    .funkyradio-info input[type="checkbox"]:checked ~ label:before {
    color: #fff;
    background-color: #5bc0de;
    }
</style>
<style type="text/css">
    .btn{
    margin: 0 3px 0 0px!important;
    }
    /* Media screens lesser than 480px */
    @media(max-width: 320px) and (max-height: 480px), (max-width: 320px){
    h1.sub{
    font-size: 20px;
    margin-bottom: -10px;
    }
    #heading {
    font-size: 14px;
    text-align: center;
    margin-bottom: -2px;
    }
    .checkbox label::before{
    left: 4px;
    margin-left: -18px;
    }
    .checkbox input[type="checkbox"]{
    margin-left: -10px;
    }
    .checkbox input[type="checkbox"]:checked + label::after{
    left: 11px;
    }
    .checkbox label{
    padding-left: 10px;
    line-height: 21px;
    }
    #btn .submit, button.reset{
    font-size: 12px;
    padding: 10px;
    }
    .btn-right{
    margin-right: 0px!important;
    }
    .btn-right:hover{
    margin-right: 0px!important;
    }
    }
    /* Media screens lesser than 640px and larger than 360px */
    @media(min-width: 360px) and (max-width: 640px){
    .btn-right{
    margin-right: 0px!important;
    }
    .btn-right:hover{
    margin-right: 0px!important;
    }
    }
    /* Media screens lesser than 1024px and larger than 768px */
    @media(min-width: 768px) and (max-width: 1024px){
    .btn-right{
    margin-right: 0px!important;
    }
    .btn-right:hover{
    margin-right: 0px!important;
    }
    }
    /* Media screens lesser than 1280px and larger than 800px - Portaite Version */
    @media(min-width: 800px) and (max-width: 1280px){
    }
    /* Media screens lesser than 1280px and larger than 980px - Portaite Version */
    @media(min-width: 980px) and (max-width: 1280px){
    }
    /* Media screens lesser than 600px and larger than 1280px - Landscape Version */
    @media(min-width: 1280px) and (max-width: 600px){
    }
    /* Media screens lesser than 900px and larger than 1920px - Landscape Version */
    @media(min-width: 1920px) and (max-width: 900px){
    }
    .btn-right{
    float: right!important;
    background: none!important;
    border: none!important;
    margin-top: 5px!important;
    padding-left: 0px;
    font-weight: 600;
    /*color: #bdbdbd;*/
    color: blue;
    text-decoration: underline;
    }
    .btn-right:hover{
    float: right!important;
    background: none!important;
    border: none!important;
    margin-top: 5px!important;
    padding-left: 0px;
    font-weight: 600;
    color: blue;
    text-decoration: none;
    }
    

</style>
<style type="text/css">
    #success_message{ display: none;}
</style>
@stop
<!-- BODY -->
@section('content')
<section class="martop30">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="white-block" id="company-reg">
                    <div class="white-block-content">
                        <div class="page-content clearfix martop30">

                            <form class="well form-horizontal" action="{{URL::to('front/register')}}" method="post" id="contact_form">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <fieldset>
                                    <!-- Form Name -->
                                    <legend>
                                        <center>
                                            <h2>Register</h2>
                                        </center>
                                    </legend>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input  name="fname" placeholder="First Name" class="form-control " type="text">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input name="lname" placeholder="Last Name" class="form-control" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <!--  <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input name="user_name" placeholder="Username" class="form-control" type="text">
                                            </div>
                                        </div>
                                        </div> -->
                                    <!-- Text input-->
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                <input name="password" placeholder="Password" class="form-control" type="password">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                <input name="confirm_password" placeholder="Confirm Password" class="form-control" type="password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                            <h6 class="col-md-12 text-center" style="padding-bottom: 15px; margin-top: 10px;">
                                              <strong>I want to</strong>
                                            </h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="funkyradio">
                                                        <div class="funkyradio-success a">
                                                            <input type="radio" name="user_type" id="radio1" value="ad.user" checked="true" />
                                                            <label value="1"  for="radio1">Post Ads</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="funkyradio">
                                                        <div class="funkyradio-success b">
                                                            <input type="radio" name="user_type" id="radio2" value="business.user" />
                                                            <label value="2" for="radio2">Become a Premium Member</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <!-- Success message -->
                                    <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>
                                    
                                    <div class="form-group">
                                      <div class="col-md-8 col-md-offset-2">
                                        <div class="col-md-6">
                                          <input type="submit" class="btn button-submit btn-block col-md-12 pull-left" value="Register Now!">
                                        </div>


                                <!--added secondary button start------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


                                <div class="col-md-6">
                                  <a href="{{url('front/login')}}" class="btn button-other col-md-12">Login Here</a>
                                </div>


                                <!-- <div class="col-md-6">
                                  <a href="{{url('front/register')}}">
                                    <input type="button" id="register-btn" class="btn button-other btn-block col-md-12" value="Register Here">
                                  </a>
                                </div> -->


                                <!--added secondary button start------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>


                                <!-- <div class="col-md-6">
                                          <input type="reset" id="login_reset" class="btn button-reset col-md-12" value="reset">
                                      </div> -->
                                      </div>
                                        <!-- <div class="col-md-10 col-md-offset-4">
                                            <input type="reset" id="login_reset" class="btn button-reset col-md-12 btn-right pull-right" value="reset">
                                        </div> -->
                                    </div>                                    

                                    <!-- <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1">
                                            
                                            <input type="submit" class="btn button-submit btn-block col-md-10 pull-left" value="register your account">
                                          
                                            <input type="reset" id="contact_reset" name="contact_reset" class="btn button-reset col-md-12 text-right" value="reset">

                                        </div>
                                    </div> -->
                                    <!-- <div class="divider">
                                        <center>
                                            <h5>You already have an account?</h5>
                                        </center>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10 col-md-offset-1">
                                            <a href="{{url('front/login')}}">
                                              <input type="button" class="btn button-other btn-block col-md-10 pull-left" value="Login page"/>
                                            </a>
                                        </div>
                                    </div> -->
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
      // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
      feedbackIcons: {
          // valid: 'glyphicon glyphicon-ok',
          // invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        
          fname: {
              validators: {
                      stringLength: {
                      min: 2,
                      max: 40,
                  },
                      notEmpty: {
                      message: 'Please enter your First Name'
                  }
                  ,
                  regexp: {
                      // regexp: /^[a-zA-Z_. ]+$/,
                      regexp: /^^[a-zA-Z0-9!@#$&()\\-`.+,/\"]*$/,
                      message: 'The first name can only consist of alphabetical & underscore'
                  }
              }
          },
           lname: {
              validators: {
                   stringLength: {
                      min: 2,
                  },
                  notEmpty: {
                      message: 'Please enter your Last Name'
                  }
                  ,
                  regexp: {
                      // regexp: /^[a-zA-Z_. ]+$/,
                      regexp: /^^[a-zA-Z0-9!@#$&()\\-`.+,/\"]*$/,
                      message: 'The last name can only consist of alphabetical & underscore'
                  }
              }
          },
           user_name: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Username'
                  }
              }
          },
           password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  notEmpty: {
                      message: 'Please enter your Password'
                  }
              }
          },
          confirm_password: {
              validators: {
                   stringLength: {
                      min: 6,
                  },
                  identical: {
                      field: 'password',
                      message: 'The password and its confirm are not the same'
                  },
                  notEmpty: {
                      message: 'Please confirm your Password'
                  }
              }
          },
          email: {
              validators: {
                  notEmpty: {
                      message: 'Please enter your Email Address'
                  },
                  emailAddress: {
                      message: 'Please enter a valid Email Address'
                  }
              }
          }
        
          }
      })
    
      .on('success.form.bv', function(e) {

          $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
              $('#contact_form').data('bootstrapValidator').resetForm();
    
          // Prevent form submission
          e.preventDefault();
    
          // Get the form instance
          var $form = $(e.target);
    
          // Get the BootstrapValidator instance
          var bv = $form.data('bootstrapValidator');
    
          // Use Ajax to submit form data
          $.post($form.attr('action'), $form.serialize(), function(result) {
              console.log(result);
          }, 'json');
      });
    
       $("#contact_reset").click(function(){
         $('#contact_form').bootstrapValidator("resetForm",true);    
      });
       
    
    });
    
</script>
@stop
