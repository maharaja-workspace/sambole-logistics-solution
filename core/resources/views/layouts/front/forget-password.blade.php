<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Forget Password | Sambole.lk web portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">

    <link href="{{asset('assets/back/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{asset('assets/back/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->

    <link href="{{asset('assets/back/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/css/style.css')}}" rel="stylesheet">

</head>
<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            {{-- <h6 class="logo-name">&nbsp;</h6> --}}

        </div>
{{--        <h3>Welcome to Sambole.lk</h3>--}}
        <h3 class="text-center">Forget Password</h3>
        <form class="form-horizontal" method="POST" action="{{ url('forget-password') }}">
            <fieldset>
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-12 inputGroupContainer">
                        <h6 class="col-md-12 text-center" style="margin-bottom: 15px; margin-top: 10px;">
                            <strong>Send reset link to</strong>
                        </h6>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input name="email" placeholder="Email Address" class="form-control" type="email" required>
                        </div>
                    </div>
                </div>
                <div class="">
{{--                    <input type="submit" class="btn btn-primary block full-width m-b" value="Reset password">--}}
                    <button type="submit" class="btn btn-primary full-width m-b">Send Reset Link</button>
                </div>
            </fieldset>
        </form>
{{--        <p class="m-t"> <small>Framework based on Laraval 5.1</small> </p>--}}
    </div>
</div>

<script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('assets/back/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/back/js/plugins/toastr/toastr.min.js')}}"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif
</script>
</body>

</html>
