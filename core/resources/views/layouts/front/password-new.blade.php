<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Forget Password | Sambole.lk web portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">

    <link href="{{asset('assets/back/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{asset('assets/back/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <!-- Gritter -->

    <link href="{{asset('assets/back/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('assets/back/css/style.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />

</head>
<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            {{-- <h6 class="logo-name">&nbsp;</h6> --}}

        </div>
        {{--        <h3>Welcome to Sambole.lk</h3>--}}
        <h3 class="text-center">Reset Password</h3>
        <form class="form-horizontal" id="ps_form" method="POST" action="{{ url('password-reset/new') }}">
            <fieldset>
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-12 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input name="email" class="form-control" type="email" value="{{ $email }}" readonly readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input name="password" placeholder="New Password" class="form-control" type="password" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input name="password_confirmation" placeholder="Confirm Password" class="form-control" type="password" required>
                        </div>
                    </div>
                </div>
                <div class="">
                    <button type="submit" class="btn btn-primary full-width m-b">Reset password</button>
{{--                    <input type="submit" class="btn button-submit btn-block col-md-12 pull-left" value="Reset password">--}}
                </div>
            </fieldset>
        </form>
        {{--        <p class="m-t"> <small>Framework based on Laraval 5.1</small> </p>--}}
    </div>
</div>

<script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('assets/back/js/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js" type="text/javascript"></script>
<script src="{{asset('assets/back/js/plugins/toastr/toastr.min.js')}}"></script>

<script>
    $('#ps_form').bootstrapValidator({
        fields: {
            password: {
                validators: {
                    stringLength: {
                        min: 6,
                    },
                    notEmpty: {
                        message: 'Please enter your Password'
                    }
                }
            },
            password_confirmation: {
                validators: {
                    stringLength: {
                        min: 6,
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    },
                    notEmpty: {
                        message: 'Please confirm your Password'
                    }
                }
            }
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif
</script>

</body>

</html>
