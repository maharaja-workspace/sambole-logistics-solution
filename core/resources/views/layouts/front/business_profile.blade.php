@extends('layouts.front.master3')

<!-- CSS FOR THIS PAGE -->
@section('css')
<!-- style -->
<link rel='stylesheet' href="{{asset('assets/front/css/bootstrap-social.css')}}" type='text/css'/>
<link rel='stylesheet' href="{{asset('assets/front/css/slider/slider.css')}}" type='text/css'/>
<link  href="{{url('assets/front/js/fotoparma/fotorama.css')}}" rel="stylesheet"> <!-- 3 KB -->
<style type="text/css">
.media-left, .media > .pull-left {
    padding-right: 0px;
}
.fotorama__html img{
    max-height: 210px!important;
    width: auto!important;
}
.fotorama__wrap--css3 .fotorama__stage{
    margin-top: -1px;
    text-align: center;
    margin-bottom: -1px;
}

@media(max-width:480px){
    .white-block.ad-box-alt .media .pull-left {
        padding-left: 0px;
    }
    .fotorama__stage{
        height:100px;
    }
}
@media(max-width:980px){
    .col-md-3.text-right{
        margin-top:-18px!important;
    }
}
@media(min-width:320px) and (max-width:359px){
    .white-block-contenth5{
        font-size: 11px;
        margin-top: 10px;
    }
    .date{
        font-size: 11px;
    }
    .ct{
        font-size: 11px;
    }
    .pr{
        font-size: 11px;
    }
    .topic1{
        font-size: 18px;
    }
    .show1{
        font-size: 13px;
    }
}
@media(min-width:360px) and (max-width:767px){
    .white-block-contenth5{
        font-size: 13px;
        margin-top: 10px;
    }
}
@media(min-width:768px) and (max-width:979px){
    .col-md-3.text-right{
        font-size:13px;
    }
    .white-block-contenth5{
        font-size: 13px;
        margin-top: 10px;
    }
    .date{
        font-size: 13px;
    }
    .ct{
        font-size: 13px;
    }
    .pr{
        font-size: 13px;
    }
    .topic1{
        font-size: 23px;
    }
    .white-block-content{
        font-size: 20px;
    }
    li{
        font-size: 16px;
    }
    .sort{
        font-size: 16px;
    }

}
@media(min-width:980px) and (max-width:1279px){
    .col-md-3.text-right{
        font-size:13px;
    }
    .white-block-contenth5{
        font-size: 15px;
        margin-top: 10px;
    }
    .date{
        font-size: 15px;
    }
    .ct{
        font-size: 15px;
    }
    .pr{
        font-size: 15px;
    }
    .topic1{
        font-size: 25px;
    }
    .white-block-content{
        font-size: 22px;
    }
    li{
        font-size: 18px;
    }
    .sort{
        font-size: 18px;
    }
}
@media(min-width:1280px) and (max-width:1399px){
    .col-md-3.text-right{

        font-size:13px;
    }
    .white-block-contenth5 {
        font-size: 15px;
        margin-top: 10px;
        margin-bottom: 5px;
    }
    .date{
        font-size: 13px;
        margin-bottom: 0px;
    }
    .ct{
        font-size: 14px;
        margin-bottom: 5px;
    }
    .pr{
        font-size: 13px;
        margin-bottom: 5px;
    }
    .topic1{
        font-size: 20px;
    }
    .white-block-content{
        font-size: 25px;
    }
    li{
        font-size: 14px;
    }
    .sort{
        font-size: 18px;
    }

    p {
       margin-bottom: 5px;
   }
}


.sidebar .search-bar form h4{
    padding-bottom: 0!important;
}
.panel-group .panel-default .panel-heading .panel-title{
    font-weight: bold;
}
a[data-toggle="collapse"]:hover{
    text-decoration: none!important;;
}
.panel-group .panel-default .panel-heading, .panel-group .panel{
    border-radius: 0!important;
}
p.about{
    word-wrap: break-word;
}
.address, .open-hours, .about{
    margin-bottom: 0px!important;
}

/* Customzie Layout data pass to color appending */
.navigation{
    background-color: {{$header_colour}} !important; /* get color code from db */
}
.search-bar{
    background-color: {{$header_colour}}; /* get color code from db (do not flag as !important)*/
}

.navigation .navbar-default .navbar-collapse ul li a{
    mix-blend-mode: difference;
}
body, section{
    background-color: {{$background_colour}}; /* get background color code from db (do not flag as !important)*/

}
</style>
<style type="text/css">

/********************************/
/*          Main CSS     */
/********************************/
p .btn.btn-block.btn-social{
    color: #fff!important;
}
.telephone{
    font-size:13px!important;
}
.address.left{
    text-align: left!important;
}

#first-slider .main-container {
  padding: 0;

}


#first-slider .slide1 h3, #first-slider .slide2 h3, #first-slider .slide3 h3, #first-slider .slide4 h3{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
}

#first-slider .slide1 h4,#first-slider .slide2 h4,#first-slider .slide3 h4,#first-slider .slide4 h4{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
}
#first-slider .slide1 .text-left ,#first-slider .slide3 .text-left{
    padding-left: 40px;
}


#first-slider .carousel-indicators {
  bottom: 0;
}
#first-slider .carousel-control.right,
#first-slider .carousel-control.left {
  background-image: none;
}
#first-slider .carousel .item {
  min-height: 225px; 
  height: 100%;
  width:100%;
}

.carousel-inner .item .container {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 0;
    top: 0;
    left: 0;
    right: 0;
}


#first-slider h3{
  animation-delay: 1s;
}
#first-slider h4 {
  animation-delay: 2s;
}
#first-slider h2 {
  animation-delay: 3s;
}


#first-slider .carousel-control {
    width: 6%;
    text-shadow: none;
}


#first-slider h1 {
  text-align: center;  
  margin-bottom: 30px;
  font-size: 30px;
  font-weight: bold;
}

#first-slider .p {
  padding-top: 125px;
  text-align: center;
}

#first-slider .p a {
  text-decoration: underline;
}
#first-slider .carousel-indicators li {
    width: 14px;
    height: 14px;
    background-color: rgba(255,255,255,.4);
    border:none;
}
#first-slider .carousel-indicators .active{
    width: 16px;
    height: 16px;
    background-color: #fff;
    border:none;
}


.carousel-fade .carousel-inner .item {
  -webkit-transition-property: opacity;
  transition-property: opacity;
}
.carousel-fade .carousel-inner .item,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  opacity: 0;
}
.carousel-fade .carousel-inner .active,
.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {
  opacity: 1;
}
.carousel-fade .carousel-inner .next,
.carousel-fade .carousel-inner .prev,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  left: 0;
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}
.carousel-fade .carousel-control {
  z-index: 2;
}

.carousel-control .fa-angle-right, .carousel-control .fa-angle-left {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
}
.carousel-control .fa-angle-left{
    left: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
}
.carousel-control .fa-angle-right{
    right: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
}
.carousel-control {
    opacity: 1;
    filter: alpha(opacity=100);
}


/********************************/
/*       Slides backgrounds     */
/********************************/
#first-slider .slide1 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide2 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide3 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide4 {

  background-size: cover;
  background-repeat: no-repeat;
}
.navbar-brand img{
    max-height: 60px;
    width: auto!important;
}
.round{
    text-decoration:none;
    border-radius:50px;
    font-size:1.5rem;
    border:thin solid #000000;
    padding: 3px 10px;
    text-align:center;
    margin:0 2px 0 0;
}
.round:hover{
    background-color:#712b2f;
    color:#ffffff;
}
@media(max-width: 480px){
    .navbar-brand img{
        margin-top:-15px;
        max-height: 60px;
        width: auto!important;
    }
    .navigation .navbar-default .navbar-brand img{
       /* margin-left:5px!important; */
       max-height: 60px;
       width: auto!important;
   }
   .search-bar{
     padding-top: 30px;
 }
}
@media(max-width: 768px){
    .navbar-brand img{
        margin-top:-15px;        
        max-height: 60px;
        width: auto!important;
    }
    .navigation .navbar-default .navbar-brand img{
       /* margin-left:5px!important; */
       max-height: 60px;
       width: auto!important;
   }

   .search-bar{
    padding-top: 30px;
}
.navigation .navbar-default .navbar-brand.text-center{
    text-align:center!impotrant;
    margin-left:auto;
    margin-right:auto;
}
}

@media (min-width: 0px) and (max-width: 320px){
    .navigation .navbar-default .navbar-brand img {
        margin-left:auto!important;
        max-height: 60px;
        width: auto!important;
    }
}
.footer-copy{
    background-color: {{$header_colour}}; /* get color code from db (do not flag as !important)*/
}
/*About Description Limit CSS*/
.morecontent span {
    display: none;
    outline: none!important;
    border-bottom: none!important;
}
.morelink {
    display: block;
    outline: none!important;
    border-bottom: none!important;
    font-weight: 800;
    font-size: 12px!important;
}
.morecontent span:hover,.morelink:hover,.less,.less:hover{
    border-bottom: none!important;
}
.m-top-5{
    margin-top: -10px;
}

/*Slider Customize*/
/*.fotorama__wrap--css3 .fotorama__stage {
    max-height: 224px;
}*/
/* @media (max-width:780px){
.fotorama__wrap--css3 .fotorama__stage {
    height: 224px;
}
} */
.fotorama__wrap--css3 .fotorama__stage{
    width: auto!important;
    height: 340px!important;
    max-height: 340px!important;
}
</style>

@stop


<!-- BODY -->
@section('logo')
<section class="navigation">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                    <!-- <div class="navbar-header">
                        <a href="{{url('/ad/post-ad')}}"  class="btn submit-add visible-xs">Post Your Ad</a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div> -->


                    <div style="padding-bottom: 20px;" class="logo-business">
                       <?php if($logo_details[0]==1){ ?>
                       <a href="{{url("/" .$business_name)}}" class="navbar-brand" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="float: left;">
                    </a>
                    <?php }elseif($logo_details[0]==2){ ?>
                    <a href="{{url("/" .$business_name)}}" class="navbar-brand text-center" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="text-align:center;">
                    </a>
                    <?php }elseif($logo_details[0]==3){ ?>
                    <a href="{{url("/" .$business_name)}}" class="navbar-brand" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="float: right;">
                    </a>
                    <?php } ?>



                </div>

            </nav>
        </div>
    </section>
    @stop

    @section('search')

    @stop

    @section('content')
    <div class="markers hidden"></div>
    <section class="search-page style-left">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-4">


                    <div class="panel-group">
                      <div class="panel panel-default row">
                        <div class="panel-heading text-center" id="info-business" style="visibility:visible;display:block;">
                          <h3>{{$business_profiles->business_name}}</h3>
                          <!-- <h4 class="panel-title">
                            {{$business_profiles->getOwner->first_name}} {{$business_profiles->getOwner->last_name}}
                        </h4> -->

                         <p class="open-hours">
                            @if (isset($business_profiles->getOpeningTimes) && isset($business_profiles->getOpeningTimes->format))
                             @if ($business_profiles->getOpeningTimes->format == 'selected')
                                 <strong>Open Hours</strong>
                                 <table>
                                     <tr>
                                         <th>Day</th>
                                         <th>Open</th>
                                         <th>Close</th>
                                     </tr>
                                     @if (!is_null($business_profiles->getOpeningTimes->monday_start)|| !is_null($business_profiles->getOpeningTimes->monday_end) )
                                     <tr>
                                         <th>Monday</th>
                                        <td>{{$business_profiles->getOpeningTimes->monday_start}}</td>
                                        <td>{{$business_profiles->getOpeningTimes->monday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->tuesday_start)|| !is_null($business_profiles->getOpeningTimes->tuesday_end) )
                                     <tr>
                                         <th>Tuesday</th>
                                        <td>{{$business_profiles->getOpeningTimes->tuesday_start}}</td>
                                        <td>{{$business_profiles->getOpeningTimes->tuesday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->wednesday_start )|| !is_null($business_profiles->getOpeningTimes->wednesday_end) )
                                     <tr>
                                         <th>Wednesday</th>
                                        <td>{{$business_profiles->getOpeningTimes->wednesday_start }}</td>
                                        <td>{{$business_profiles->getOpeningTimes->wednesday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->thursday_start )|| !is_null($business_profiles->getOpeningTimes->thursday_end) )
                                     <tr>
                                         <th>Thursday</th>
                                        <td>{{$business_profiles->getOpeningTimes->thursday_start }}</td>
                                        <td>{{$business_profiles->getOpeningTimes->thursday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->friday_start )|| !is_null($business_profiles->getOpeningTimes->friday_end) )
                                     <tr>
                                         <th>Friday</th>
                                        <td>{{$business_profiles->getOpeningTimes->friday_start }}</td>
                                        <td>{{$business_profiles->getOpeningTimes->friday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->saturday_start )|| !is_null($business_profiles->getOpeningTimes->saturday_end) )
                                     <tr>
                                         <th>Saturday</th>
                                        <td>{{$business_profiles->getOpeningTimes->saturday_start }}</td>
                                        <td>{{$business_profiles->getOpeningTimes->saturday_end}}</td>
                                     </tr>
                                     @endif
                                     @if (!is_null($business_profiles->getOpeningTimes->sunday_start )|| !is_null($business_profiles->getOpeningTimes->sunday_end) )
                                     <tr>
                                         <th>Sunday</th>
                                        <td>{{$business_profiles->getOpeningTimes->sunday_start }}</td>
                                        <td>{{$business_profiles->getOpeningTimes->sunday_end}}</td>
                                     </tr>
                                     @endif
                                 </table>
                                     
                             @elseif ($business_profiles->getOpeningTimes->format == 'always')
                                  <strong>Open Hours</strong> : Always open
                             @elseif ($business_profiles->getOpeningTimes->format == 'unavailable')
                                  {{-- <strong>Open Hours</strong> : No hours available --}}
                             @elseif ($business_profiles->getOpeningTimes->format == 'p-closed')
                                  <strong>Open Hours</strong> : Permanantly closed
                             @endif
                            @else
                                 {{-- <strong>Open Hours</strong> : No hours available --}}
                            @endif
                         </p>


                        
                        <br/>
                             <!--<p>
                            
                                <br>
                                <a href="{{$business_profiles->facebook_url}}" class="btn btn-block btn-social btn-facebook">
                                    <span class="fa fa-facebook"></span> Visit my facebook page
                                </a>
                                <a href="{{$business_profiles->googleplus_url}}" class="btn btn-block btn-social btn-google">
                                    <span class="fa fa-google"></span> Visit my google+ page 
                                </a> 
                            </p>-->
                                <!-- <a class="btn btn-social-icon btn-facebook">
                                    <span class="fa fa-facebook"></span>
                                </a>
                                <a class="btn btn-social-icon btn-google">
                                    <span class="fa fa-google"></span> 
                                </a>
                                 <i class="fa fa-facebook"></i>
                                 <i class="fa fa-google"></i> -->

                                 <p class="address"><strong>{{$business_profiles->company_address}}</strong></p>
                                 <p class="about text-justify"><span class="more">{{$business_profiles->about}}</span></p>
                                 <br/>
                                 <p class="center">

                                    <!-- Facebook -->
                                    @if($business_profiles->facebook_url)
                                    <a class="round fa fa-facebook" href="//{{$business_profiles->facebook_url}}" target="_blank"></a>
                                    @endif

                                    <!-- Twitter -->
                                    @if($business_profiles->twitter_url)
                                    <a class="round fa fa-twitter" href="//{{$business_profiles->twitter_url}}" target="_blank"></a>
                                    @endif

                                    <!-- Instagram -->
                                    @if($business_profiles->instagram_url)
                                    <a class="round fa fa-instagram" href="//{{$business_profiles->instagram_url}}" target="_blank""></a>
                                    @endif

                                    <!-- Google-Plus -->
                                    @if($business_profiles->googleplus_url)
                                    <a class="round fa fa-google-plus" href="//{{$business_profiles->googleplus_url}}" target="_blank""></a>
                                    @endif

                                </p>   
                                <p class="center m-top-5">
                                    <!-- WebSite -->
                                    @if($business_profiles->website)
                                    <a class="round fa fa-globe" href="//{{$business_profiles->website}}" target="_blank""></a>
                                    @endif

                                    <!-- Email -->
                                    <a class="round fa fa-envelope" href="mailto:{{$business_profiles->company_email}}"></a>

                                    <!-- Telephone -->
                                    <a class="round fa fa-phone" href="tel:{{$business_profiles->contact_number}}"></a>
                                </p>
                                <p><a href="{{url('/')}}" class="round">Back to Sambole.lk</a></p>
                            <!-- <p class="address left"><a href="{{$business_profiles->website}}">Visit our website</a>
                            </p> -->

                            <!-- <p class="address left">
                                <a href="mailto:{{$business_profiles->company_email}}">{{$business_profiles->company_email}} Send us an email</a>
                            </p> -->

                            <!-- <p class="address left telephone">
                                <a href="tel:{{$business_profiles->contact_number}}"><strong>{{$business_profiles->contact_number}}</strong></a>
                            </p> -->

                        </div>

                    </div>

                    <!-- <a href="{{url('/')}}" class="btn submit-form">Home Page</a> -->
                </div>
                    <!-- <div class="search-bar clearfix">
                        <form method="get" class="search-form advanced-search" action="">
                           
                            <div class="filters-holder hidden"></div>
                            <li class="advanced-filter-wrap">
                                <a href="#filters" data-toggle="modal" class="btn disabled advanced-filters" data-filtered="Change Filters" data-default="Advanced Filters">
                                    More </a>
                            </li>
                        </form>
                    </div> -->
                </div>
                <div class="col-md-9 col-sm-8 map-left-content">
                    <!-- <h4 class="topic1">Products details listed on here</h4> -->
                    
                    <div class="col-md-14" style="margin-top: 0px; margin-bottom: 20px;text-align: center;">

                        <div class="fotorama" data-transition="dissolve" data-arrows="true" data-click="false" data-swipe="true" data-allowfullscreen="true" data-clicktransition="dissolve" data-fit="cover" data-maxheight="100%" data-width="100%" data-loop="true" data-autoplay="3000" data-nav="false">

                            <?php if (sizeof($slider)==0): ?>
                            
                            {{-- <img class="img-responsive" src="{{asset('/core/storage/uploads/no_image.png')}}" width="420" height="420" alt=""> --}}
                            
                            <?php else: ?>
                            <?php $i=1; foreach ($slider as $key => $value): ?>
                            
                            
                            <img class="img-responsive" src="{{Config('constants.bucket.url').''.$value->img_path.''.$value->img_name}}"  style="max-width:100%">
                            
                            <!-- <div class="single-ad-image item-<?php echo $i;?>">
                                <img src="{{Config('constants.bucket.url').''.$value->img_path.'/'.$value->img_name}}" width="420" height="420" alt="">
                            </div> -->
                            
                            
                            <?php $i++; endforeach ?>
                            <?php endif ?>

                            

                        </div>

                        {{-- <div class="sambole-content sambole-display-container" style="margin-bottom: 15px;">



                            <button class="sambole-button sambole-black sambole-display-left" onclick="plusDivs(-1)">&#10094;</button>
                            <button class="sambole-button sambole-black sambole-display-right" onclick="plusDivs(1)">&#10095;</button>
                        </div> --}}

                    </div> 


                    <div class="col-md-12 col-sm-12 map-left-content" style="padding-left: 0; padding-right: 0;">

                        <div class="white-block search-block">
                            <div class="white-block-content">
                                <div class="row">
                                    <div class="col-md-9">
                                        <p class="show1">
                                            Showing {{$products->firstItem()}} - {{$products->lastItem()}} of {{$products->total()}} total products found
                                        </p>
                                    </div>
                                    <div class="col-md-3 text-right">
                                        <span id="grid" class="change_view active">
                                            <i class="fa fa-th-large fa-fw"></i>
                                        </span>

                                        <span id="list" class="change_view">
                                            <i class="fa fa-th-list fa-fw"></i>
                                        </span>
                                    </div>
                              <!--   <div class="col-md-3 text-right">
                                <div class="btn-group">   -->
                                        <!-- <a href="#">
                                            <span class="fa fa-th-large fa-fw">
                                            </span>
                                        </a> -->
                                        <!-- <a href="#" id="list">
                                            <span class="fa fa-th-list fa-fw"></span>
                                        </a> -->
                                 <!--    </div>
                                 </div> -->
                             </div>
                         </div>

                     </div>

                     <div id="products" class="row list-group">
                        @foreach ($products as $product)
                        <div class="item  col-xs-12 col-sm-6 col-md-4">
                            <div class="white-block ad-box" style="height: 351px;">

                                <div class="white-block-media-grid">

                                    <!-- ## Please Remove after created Controllekr for above function -->
                                    <a href="{{url('/'.$business_name. '/product/'.$product->id)}}">
                                        @if ($product->getImages->first())

                                        @if ($product->getImages->first()->is_wp)
                                        <img src="{{Config('constants.image_server.url').''.$product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->first()['filename']}}" class="img-responsive">
                                        @else
                                        @if($product->getImages()->where('is_main', 1)->first())
                                        <img src="{{Config('constants.bucket.url').''.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['filename']}}" class="img-responsive"">
                                        @else
                                        <img src="{{Config('constants.bucket.url').''.$product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->first()['filename']}}" class="img-responsive">
                                        @endif
                                        @endif

                                        @else                
                                        <img src="{{asset('core/storage/uploads/no_image.png')}}">
                                        @endif
                                        
                                    </a>
                                    <!-- ## Please Remove after created Controllekr for above function -->
                                </div>
                                
                                <div class="white-block-content">                                  

                                    <a href="{{url('/'.$business_name. '/product/'.$product->id)}}">
                                        <h5>{{ $product->name }}</h5>
                                    </a>
                                    <p class="date">
                                        {{ $product->created_at->format('F d, Y') }}
                                    </p>

                                    {{-- <b class="ct">Piliyandala</b> --}}

                                    <p class="price-style">
                                      <?php if ($product->price>0): ?>
                                         Rs {{number_format((double)str_replace(',','',$product->price), 2, '.', ',')}}
                                      <?php else: ?>
                                        Call for Price
                                      <?php endif ?>
                                       
                                    </p>

                                </div>

                            </div>


                        </div>
                        @endforeach



                        <div class="col-xs-12 col-md-12 text-center">
                            {!! $products->render() !!}
                        </div>

                    </div>

                    <div class="search-results" id="resrult-list">
                        @foreach ($products as $product)

                        <div class="row">

                            <div class="col-md-12">
                                <div class="white-block ad-box-alt">
                                    <div class="media white-block-media-grid">
                                        <a class="pull-left" style="min-width: 250px" href="{{url('/'.$business_name. '/product/'.$product->id)}}" >

                                            @if ($product->getImages->first())
                                            <?php if ($product->getImages->first()->is_wp): ?>
                                            <img class="img-responsive" src="{{Config('constants.image_server.url').''.$product->getImages->first()->path.'/'.$product->getImages->first()->filename}}" style="object-fit: cover;"  alt=""/>
                                            <?php else: ?>
                                            <img class="img-responsive" src="{{Config('constants.bucket.url').''.$product->getImages->first()->path.'/'.$product->getImages->first()->filename}}" style="object-fit: cover;"  alt=""/>
                                            <?php endif ?>
                                            
                                            @else
                                            <img class="img-responsive" src="{{asset('/core/storage/uploads/no_image.png')}}" style="height: 180px; width: 240px; object-fit: cover;" alt=""/>
                                            @endif

                                        </a>

                                        <div class="media-body">
                                            <a href="{{url('/'.$business_name. '/product/'.$product->id)}}">
                                                <h5>{{ $product->name }}</h5>
                                                
                                                <p class="date">
                                                    {{ $product->created_at->format('F d, Y') }}
                                                </p>

                                                {{-- <b class="ct">Piliyandala</b> --}}

                                                <p class="price-style">
                                                    Rs {{number_format((double)str_replace(',','',$product->price), 2, '.', ',')}}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        @endforeach

                        <div class="col-xs-12 col-md-12 text-center">
                            {!! $products->render() !!}
                        </div>
                    </div>
                </section>

                @stop
                <!-- JS FOR THIS PAGE -->
                @section('js')
                {{-- <script src="{{asset('assets/front/js/cookie.js')}}"></script> --}}
                
                {{-- <script src="{{url('assets/front/js/fotoparma/jquery.min.js')}}"></script> <!-- 33 KB --> --}}
                <script src="{{url('assets/front/js/fotoparma/fotorama.js')}}"></script> <!-- 16 KB -->
                <script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
                <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js')}}"></script>
                <script src="{{asset('assets/front/js/select2/select2.full.min.js')}}"></script>
                {{-- <script src="{{asset('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js')}}"></script> --}}
                <script type="text/javascript">
                    $(function() {
                        var cc = $.cookie('list_grid');
                        if (cc == 'l') {
                            $("#list").addClass("active");
                            $("#grid").removeClass("active");
                            $("#products").addClass("vnone");
                            $("#resrult-list").removeClass("vnone");
                        } 
                        else {
                            $("#grid").addClass("active");
                            $("#list").removeClass("active");
                            $("#resrult-list").addClass("vnone");
                            $("#products").removeClass("vnone");
                        }
                    });
                    $(document).ready(function(){
                        $('#grid').click(function() {
                // console.log('g');
                $("#grid").addClass("active");
                $("#list").removeClass("active");
                $("#resrult-list").addClass("vnone");
                $("#products").removeClass("vnone");
                $.cookie('list_grid', null);
                return false;
            });

                        $('#list').click(function() {
                // console.log('l');
                $("#list").addClass("active");
                $("#grid").removeClass("active");
                $("#products").addClass("vnone");
                $("#resrult-list").removeClass("vnone");
                $.cookie('list_grid', 'l');
                return false;
            });
                    });

                </script>
                {{-- <script type="text/javascript">
                    (function( $ ) {

    //Function to animate slider captions 
    function doAnimations( elems ) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        
        elems.each(function () {
            var $this = $(this),
            $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }
    
    //Variables on page load 
    var $myCarousel = $('#carousel-example-generic'),
    $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");

    //Initialize carousel 
    $myCarousel.carousel();
    
    //Animate captions in first slide on page load 
    doAnimations($firstAnimatingElems);
    
    //Pause carousel  
    $myCarousel.carousel('pause');
    
    
    //Other slides to be animated on carousel slide event 
    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
    
})(jQuery); 

</script> --}}
{{-- <script>
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("businessSlider");
        if (n > x.length) {slideIndex = 1}    
            if (n < 1) {slideIndex = x.length}
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";  
                }
                x[slideIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script> --}}

<!-- About Description Limit JS -->
<script type="text/javascript">
    $(document).ready(function() {
    var showChar = 150;  // How many characters are shown by default
    var ellipsestext = " ...";
    var moretext = "<i class='round fa fa-angle-double-down' title='Show more'></i>";
    var lesstext = "<i class='round fa fa-angle-double-up' title='Show less'></i>";
    

    $('.more').each(function() {
        var content = $(this).html();
        
        if(content.length > showChar) {
           
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent text-center"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            
            $(this).html(html);
        }
        
    });
    
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
@stop
