@extends('layouts.front.master')

<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
/* Media screens lesser than 480px */
@media(max-width: 320px) and (max-height: 480px), (max-width: 320px){

  h1.sub{
    font-size: 20px;
    margin-bottom: -10px;
}
#heading {
    font-size: 14px;
    text-align: center;
    margin-bottom: -2px;
}
.checkbox label::before{
    left: 4px;
    margin-left: -18px;
}
.checkbox input[type="checkbox"]{
    margin-left: -10px;
}
.checkbox input[type="checkbox"]:checked + label::after{
    left: 11px;
}
.checkbox label{
    padding-left: 10px;
    line-height: 21px;
}
#btn .submit, button.reset{
    font-size: 12px;
    padding: 10px;
}
a.btn{
    margin-top: 5px!important;
}
}
/* Media screens lesser than 640px and larger than 360px */
@media(min-width: 360px) and (max-width: 640px){
  a.btn{
    margin-top: 5px!important;
}
}
/* Media screens lesser than 1024px and larger than 768px */
@media(min-width: 768px) and (max-width: 1024px){
  a.btn{
    margin-top: 5px!important;
}
}
/* Media screens lesser than 1280px and larger than 800px - Portaite Version */
@media(min-width: 800px) and (max-width: 1280px){

}
/* Media screens lesser than 1280px and larger than 980px - Portaite Version */
@media(min-width: 980px) and (max-width: 1280px){

}
/* Media screens lesser than 600px and larger than 1280px - Landscape Version */
@media(min-width: 1280px) and (max-width: 600px){

}

/* Media screens lesser than 900px and larger than 1920px - Landscape Version */
@media(min-width: 1920px) and (max-width: 900px){

}

.btn-right{
  float: right!important;
  background: none!important;
  border: none!important;
  margin-top: 5px!important;
  padding-left: 0px;
  font-weight: 600;
  /*color: #bdbdbd;*/
  color: blue;
  text-decoration: underline;
}
.btn-right:hover{
  float: right!important;
  background: none!important;
  border: none!important;
  margin-top: 5px!important;
  padding-left: 0px;
  font-weight: 600;
  color: blue;
  text-decoration: none;
}
a.btn{
  color: #fff;
}

#success_message{ display: none;}
</style>

@stop


<!-- BODY -->

@section('content')
<section class="martop30">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="white-block">
                    <div class="white-block-content">
                        <div class="page-content clearfix martop30">
                            <form class="well form-horizontal" method="POST" action="{{ route('front.email.resend.post') }}">
                                <fieldset>
                                {{ csrf_field() }}
                                <legend>
                                    <h2 class="text-center">Email Resend</h2>
                                    <small class="text-center">If you didn't receive an email, use this form to resend confirmation request! Otherwise ignore!</small>
                                </legend>                          
                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-1 inputGroupContainer">
                                      <h6 class="col-md-12 text-center" style="margin-bottom: 15px; margin-top: 10px;">
                                              <strong>Send confirmation link to</strong>
                                            </h6>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input name="email" placeholder="Email Address" class="form-control" type="email" required>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-md-6 col-md-offset-3">                                 
                                  <input type="submit" class="btn button-submit btn-block col-md-12 pull-left" value="Send Confirmation Email">
                                </div>    
                                </fieldset>   
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
