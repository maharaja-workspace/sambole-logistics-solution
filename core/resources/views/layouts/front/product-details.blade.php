@extends('layouts.front.master3')

@section('title'){{ $product->name . ' - Sambole.lk' }}@stop
@section('og_title'){{ $product->name . ' - Sambole.lk' }}@stop
@section('description'){!! str_limit(strip_tags($product->description), 160, '...') !!}@stop
@section('og_description'){!! str_limit(strip_tags($product->description), 160, '...') !!}@stop
@section('og_url'){{ url('/'.$business_profiles->business_page_name.'/product/'.$product->id) }}@stop
@section('og_image')
@if($product->getImages->first())

{{-- @foreach($product->getImages()->orderBy('id', 'desc')->get() as $adimage) --}}
@if($product->getImages()->orderBy('id', 'desc')->first()['is_wp'])
{{Config('constants.image_server.url').''.$product->getImages()->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->orderBy('id', 'desc')->first()['filename']}}
@else
{{Config('constants.bucket.url').''.$product->getImages()->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->orderBy('id', 'desc')->first()['filename']}}
@endif
{{-- @endforeach --}}

@else
{{asset('/core/storage/uploads/no_image.png')}}
@endif
@stop


<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
.widget-footer{
    background-color: #712b2f;
}
.fotorama__wrap--css3 .fotorama__stage{
    text-align: center;
}
.textwidget p, .textwidget a, .textwidget li{
    color: #ffffff!important;
}
.media-left, .media > .pull-left {
    padding-right: 0px;
}
@media(max-width:480px){
    .white-block.ad-box-alt .media .pull-left {
        padding-left: 0px;
    }
}
@media(max-width:980px){
    .col-md-3.text-right{
        margin-top:-18px!important;
    }
}
@media(min-width:320px) and (max-width:359px){
    .white-block-contenth5{
        font-size: 11px;
        margin-top: 10px;
    }
    .date{
        font-size: 11px;
    }
    .ct{
        font-size: 11px;
    }
    .pr{
        font-size: 11px;
    }
    .topic1{
        font-size: 18px;
    }
    .show1{
        font-size: 13px;
    }
}
@media(min-width:360px) and (max-width:767px){
    .white-block-contenth5{
        font-size: 13px;
        margin-top: 10px;
    }
}
@media(min-width:768px) and (max-width:979px){
    .col-md-3.text-right{
        font-size:13px;
    }
    .white-block-contenth5{
        font-size: 13px;
        margin-top: 10px;
    }
    .date{
        font-size: 13px;
    }
    .ct{
        font-size: 13px;
    }
    .pr{
        font-size: 13px;
    }
    .topic1{
        font-size: 23px;
    }
    .white-block-content{
        font-size: 20px;
    }
    li{
        font-size: 16px;
    }
    .sort{
        font-size: 16px;
    }

}
@media(min-width:980px) and (max-width:1279px){
    .col-md-3.text-right{
        font-size:13px;
    }
    .white-block-contenth5{
        font-size: 15px;
        margin-top: 10px;
    }
    .date{
        font-size: 15px;
    }
    .ct{
        font-size: 15px;
    }
    .pr{
        font-size: 15px;
    }
    .topic1{
        font-size: 25px;
    }
    .white-block-content{
        font-size: 22px;
    }
    li{
        font-size: 18px;
    }
    .sort{
        font-size: 18px;
    }
}
@media(min-width:1280px) and (max-width:1399px){
    .col-md-3.text-right{

        font-size:13px;
    }
    .white-block-contenth5 {
        font-size: 15px;
        margin-top: 10px;
        margin-bottom: 5px;
    }
    .date{
        font-size: 13px;
        margin-bottom: 0px;
    }
    .ct{
        font-size: 14px;
        margin-bottom: 5px;
    }
    .pr{
        font-size: 13px;
        margin-bottom: 5px;
    }
    .topic1{
        font-size: 20px;
    }
    .white-block-content{
        font-size: 25px;
    }
    li{
        font-size: 14px;
    }
    .sort{
        font-size: 18px;
    }

    p {
     margin-bottom: 5px;
 }
}

.sidebar .search-bar form h4{
    padding-bottom: 0!important;
}
.panel-group .panel-default .panel-heading .panel-title{
    font-weight: bold;
}
a[data-toggle="collapse"]:hover{
    text-decoration: none!important;;
}
.panel-group .panel-default .panel-heading, .panel-group .panel{
    border-radius: 0!important;
}
.address{
    margin-bottom: 0px!important;
}

/* Customzie Layout data pass to color appending */
.navigation{
    background-color: {{$header_colour}} !important; /* get color code from db */
}
.search-bar{
    background-color: {{$header_colour}}; /* get color code from db (do not flag as !important)*/
}
.navigation .navbar-default .navbar-collapse ul li a{
    mix-blend-mode: difference;
}
body, section{
    background-color: {{$background_colour}}; /* get background color code from db (do not flag as !important)*/

}


#fb-share-button {
    background: #3b5998;
    border-radius: 3px;
    font-weight: 600;
    padding: 5px 8px;
    display: inline-block;
    position: static;
}

#fb-share-button:hover {
    cursor: pointer;
    background: #213A6F
}

#fb-share-button svg {
    width: 18px;
    fill: white;
    vertical-align: middle;
    border-radius: 2px
}

#fb-share-button span {
    vertical-align: middle;
    color: white;
    font-size: 14px;
    padding: 0 3px
}
</style>
<style type="text/css">

/********************************/
/*          Main CSS     */
/********************************/
p .btn.btn-block.btn-social{
    color: #fff!important;
}
.telephone{
    font-size:13px!important;
}
.address.left{
    text-align: left!important;
}

#first-slider .main-container {
  padding: 0;

}


#first-slider .slide1 h3, #first-slider .slide2 h3, #first-slider .slide3 h3, #first-slider .slide4 h3{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
}

#first-slider .slide1 h4,#first-slider .slide2 h4,#first-slider .slide3 h4,#first-slider .slide4 h4{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
}
#first-slider .slide1 .text-left ,#first-slider .slide3 .text-left{
    padding-left: 40px;
}


#first-slider .carousel-indicators {
  bottom: 0;
}
#first-slider .carousel-control.right,
#first-slider .carousel-control.left {
  background-image: none;
}
#first-slider .carousel .item {
  min-height: 225px; 
  height: 100%;
  width:100%;
}

.carousel-inner .item .container {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 0;
    top: 0;
    left: 0;
    right: 0;
}


#first-slider h3{
  animation-delay: 1s;
}
#first-slider h4 {
  animation-delay: 2s;
}
#first-slider h2 {
  animation-delay: 3s;
}


#first-slider .carousel-control {
    width: 6%;
    text-shadow: none;
}


#first-slider h1 {
  text-align: center;  
  margin-bottom: 30px;
  font-size: 30px;
  font-weight: bold;
}

#first-slider .p {
  padding-top: 125px;
  text-align: center;
}

#first-slider .p a {
  text-decoration: underline;
}
#first-slider .carousel-indicators li {
    width: 14px;
    height: 14px;
    background-color: rgba(255,255,255,.4);
    border:none;
}
#first-slider .carousel-indicators .active{
    width: 16px;
    height: 16px;
    background-color: #fff;
    border:none;
}


.carousel-fade .carousel-inner .item {
  -webkit-transition-property: opacity;
  transition-property: opacity;
}
.carousel-fade .carousel-inner .item,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  opacity: 0;
}
.carousel-fade .carousel-inner .active,
.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {
  opacity: 1;
}
.carousel-fade .carousel-inner .next,
.carousel-fade .carousel-inner .prev,
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  left: 0;
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}
.carousel-fade .carousel-control {
  z-index: 2;
}

.carousel-control .fa-angle-right, .carousel-control .fa-angle-left {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
}
.carousel-control .fa-angle-left{
    left: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
}
.carousel-control .fa-angle-right{
    right: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
}
.carousel-control {
    opacity: 1;
    filter: alpha(opacity=100);
}


/********************************/
/*       Slides backgrounds     */
/********************************/
#first-slider .slide1 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide2 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide3 {

  background-size: cover;
  background-repeat: no-repeat;
}
#first-slider .slide4 {

  background-size: cover;
  background-repeat: no-repeat;
}
.navbar-brand img{
    max-height: 60px;
    width: auto!important;
}
.round{
    text-decoration:none;
    border-radius:50px;
    font-size:1.5rem;
    border:thin solid #000000;
    padding: 3px 10px;
    text-align:center;
    margin:0 2px 0 0;
}
.round:hover{
    background-color:#712b2f;
    color:#ffffff;
}
@media(max-width: 480px){
    .navbar-brand img{
        margin-top:-15px;
        width:100px!important;
        height: auto !important;
    }
    .navigation .navbar-default .navbar-brand img{
     margin-left:5px!important;
 }
 .search-bar{
   padding-top: 30px;
}
}
@media(max-width: 768px){
    .navbar-brand img{
        margin-top:-15px;
        max-height: 60px;
        width: auto!important;
    }
    .navigation .navbar-default .navbar-brand img{
     margin-left:5px!important;
     max-height: 60px;
     width: auto!important;
 }
 .search-bar{
    padding-top: 30px;
}

}
@media (min-width: 0px) and (max-width: 319px){
    .navigation .navbar-default .navbar-brand img {
        margin-left:auto!important;
        max-height: 60px;
        width: auto!important;
    }
}
.footer-copy{
    background-color: {{$header_colour}}; /* get color code from db (do not flag as !important)*/
}
</style>
@stop

<!-- BODY -->
<link href="{{asset('assets/front/css/lightbox/lightbox.min.css')}}" rel="stylesheet">
<link  href="{{url('assets/front/js/fotoparma/fotorama.css')}}" rel="stylesheet"> <!-- 3 KB -->


@section('logo')
<section class="navigation">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
                    <!-- <div class="navbar-header">
                        <a href="{{url('/ad/post-ad')}}"  class="btn submit-add visible-xs">Post Your Ad</a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div> -->


                    <div style="padding-bottom: 20px;" class="logo-business">
                     <?php if($logo_details[0]==1){ ?>
                     <a href="{{url("/" .$business_name)}}" class="navbar-brand" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="float: left;">
                    </a>
                    <?php }elseif($logo_details[0]==2){ ?>
                    <a href="{{url("/" .$business_name)}}" class="navbar-brand" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="">
                    </a>
                    <?php }elseif($logo_details[0]==3){ ?>
                    <a href="{{url("/" .$business_name)}}" class="navbar-brand" style="padding:0px; margin-top:-15px;">
                        <img src="{{$logo_details[1].$logo_details[2]}}" title="" alt="" style="float: right;">
                    </a>
                    <?php } ?>



                </div>

            </nav>
        </div>
    </section>
    @stop

   {{--  @section('search')

   @stop --}}
   @section('content')

   <div class="ad-template-default single single-cad postid-34141">
    <section class="page-title">
        <div class="container">
            <ul class="breadcrumb">
                   <!--  <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="">{{$product->name}}</a></li>
                    <li>Title 01</li> -->
                </ul>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="white-block">
                            <div class="white-block-content">
                                <h3 class="blog-title">{{$product->name}}</h3>
                                <div class="top-meta">
                                    For sale 
                                    by 
                                    <a href="#">{{$business_profiles->business_name}}</a>

                                    
                                    <!-- <span class="icon-verified"><img src="{{asset('assets/front/images/icon-verified.svg')}}" class="icon">Member</span> -->
                                    <!-- -
                                        <a href="">Galle</a> -->
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="fotorama" data-nav="thumbs" data-transition="dissolve" data-arrows="true" data-click="false" data-swipe="true" data-allowfullscreen="true" data-clicktransition="dissolve" data-height="400" data-maxheight="100%" data-width="100%" data-loop="true" data-autoplay="3000">

                                                @if($product->getImages->first())

                                                @if($product->getImages()->where('is_main', 1)->first())                                
                                                <img class="img-responsive" src="{{Config('constants.bucket.url').''.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['path'].'/'.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['filename']}}"  alt=""/>
                                                @endif

                                                @foreach($product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->get() as $adimage)
                                                @if($adimage->is_wp)
                                                <img class="img-responsive" src="{{Config('constants.image_server.url').''.$adimage->path.'/'.$adimage->filename}}"  alt=""/>
                                                @else
                                                <img class="img-responsive" src="{{Config('constants.bucket.url').''.$adimage->path.'/'.$adimage->filename}}"  alt=""/>
                                                @endif
                                                @endforeach

                                                @else
                                                <div class="single-ad-image item-0">
                                                    <img src="{{asset('/core/storage/uploads/no_image.png')}}" width="420" height="420" alt="">
                                                </div>
                                                @endif

                                               {{--  @if($product->getImages->first())

                                                @if($product->getImages()->where('is_main', 1)->first())                                
                                                <img class="img-responsive" src="{{Config('constants.bucket.url').''.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['img_path'].'/'.$product->getImages()->where('is_main', 1)->orderBy('id', 'desc')->first()['img_name']}}"  alt=""/>
                                                @endif

                                                @foreach($product->getImages()->where('is_main', 0)->orderBy('id', 'desc')->get() as $getImage)
                                                @if($getImage->is_wp)
                                                <img class="img-responsive" src="{{Config('constants.image_server.url').$getImage->img_path.'/'.$getImage->img_name}}"  alt=""/>
                                                @else
                                                <img class="img-responsive" src="{{Config('constants.bucket.url').''.$getImage->img_path.'/'.$getImage->img_name}}"  alt=""/>
                                                @endif
                                                @endforeach

                                                @else
                                                <div class="single-ad-image item-0">
                                                    <img src="{{asset('/core/storage/uploads/no_image.png')}}" width="420" height="420" alt="">
                                                </div>
                                                @endif --}}

                                                

                                            </div>
                                        </div>
                                    </div>
                                    <div class="ad-details clearfix">
                                        <div class="price-block pull-left">
                                            @if ($product->price_negotiable)
                                            <div class="ad-pricing">
                                                <!-- <span>Rs 8,000.00</span> -->

                                                (Price Negotiable)

                                            </div>

                                            @else
                                            <div class="ad-pricing">
                                                <!-- <span>Rs 8,000.00</span> -->
                                                <?php if ($product->price>0): ?>
                                                    Rs. {{ number_format($product->price,2) }}
                                                <?php else: ?>
                                                    Call for Price
                                                <?php endif ?>
                                                

                                            </div>
                                            @endif

                                        </div>
                                        <div class="ad-short-code pull-right">Short Code:
                                            <span style="color: #712b2f">
                                                {{strtoupper($product->short_code)}}</span>
                                            </div>
                                        </div>
                                        <div class="ad-details clearfix">
                                            <h3>Description</h3>
                                            <p><?php echo ($product->description);?></p>
                                            <div class="clearfix"></div>
                                                @include('includes.cat-questions-answers', ['answers' => $product->catgeoryAnswers])
                                            <div class="clearfix"></div>
                                            <hr>
                                            <a href="#!" style="font-size: 14px;" class="report-ad" data-toggle="modal" data-target="#report">Report Product</a>
                                        </div>
                                        <div class="ad-actions">
                                            <ul class="list-inline list-unstyled">

                                                <?php
                                                if ($business_profiles->user_id !=10107) {
                                                    ?>
                                                    <div id="contact-bottom">
                                                        <li class="phone has-icon box" style="cursor: pointer;" data-value="{{ $business_profiles->contact_number }}">
                                                            <div class="icon"><img src="{{asset('assets/front/images/icon-phone.svg')}}"></div>
                                                            <strong>{{ substr($business_profiles->contact_number, 0, 6) }}XXXX</strong>
                                                            <span>Click to show phone number</span>
                                                        </li>  
                                                    </div>
                                                    <?php
                                                } 

                                                ?>


                                                <li class="has-icon">
                                                    <a href="#!" class="ask-question" data-toggle="modal" data-target="#question">
                                                        <div class="icon"><img src="{{asset('assets/front/images/icon-email.svg')}}"></div>Reply by Email
                                                    </a>
                                                </li>
                                                <li>
                                                    <div id="fb-share-button">
                                                        <svg viewBox="0 0 12 12" preserveAspectRatio="xMidYMid meet">
                                                            <path class="svg-icon-path" d="M9.1,0.1V2H8C7.6,2,7.3,2.1,7.1,2.3C7,2.4,6.9,2.7,6.9,3v1.4H9L8.8,6.5H6.9V12H4.7V6.5H2.9V4.4h1.8V2.8 c0-0.9,0.3-1.6,0.7-2.1C6,0.2,6.6,0,7.5,0C8.2,0,8.7,0,9.1,0.1z"></path>
                                                        </svg>
                                                        <a style="color: white;text-decoration: none; 
                                                        " href="https://www.facebook.com/sharer/sharer.php?u={{ url('/'.$business_profiles->business_page_name.'/product/'.$product->id) }}&display=popup" target="_blank">
                                                    Share</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">

                            <div class="widget white-block">
                                <div class="ad-actions">
                                    <ul class="list-inline list-unstyled">
                                        <li class="profile">
                                            <a class="link-profile" href="{{ url('/'.$business_profiles->business_page_name) }}">{{$business_profiles->business_name}}</a>
                                            <!-- <span class="icon-verified"><img src="{{asset('assets/front/images/icon-verified.svg')}}" class="verified-icon" class="img-responsive">Member</span> -->
                                        </li>

                                        <div id="contact-top">
                                            <?php
                                            if ($business_profiles->user_id !=10107) { ?>
                                            <li class="phone has-icon box" style="cursor: pointer;" data-value="{{ $business_profiles->contact_number }}">
                                                <div class="icon"><img src="{{asset('assets/front/images/icon-phone.svg')}}"></div>
                                                <strong>{{ substr($business_profiles->contact_number, 0, 6) }}XXXX</strong>
                                                <span>Click to show phone number</span>
                                            </li> 
                                        <?php }
                                        ?>

                                    </div>

                                            {{-- <li class="phone has-icon">
                                                <div class="icon"><img src="{{asset('assets/front/images/icon-phone.svg')}}"></div>
                                                <strong>
                                                    <a href="javascript:;" data-last="{{$business_profiles->contact_number}}" data-index="i" class="phone-reveal-1" title="Click to reveal number">07XXXXXXXX</a>  
                                                </strong>
                                                <span class="click-ins">Click to show phone number</span>
                                            </li> --}}

                                            <li class="has-icon" style="font-weight: 600; padding: 15px 0 0 35px">
                                                <div class="icon"><i style="font-size: 1.5em; color: #17a186;" class="fa fa-shopping-cart"></i></div>
                                                @if($product->current_stock > 0)
                                                <p>
                                                	Available stock: 
                                                    @if($product->current_stock > 10)
                                                    <span>10+</span> 
                                                    @else
                                                    <span>{{$product->current_stock}}</span>           
                                                    @endif
                                                </p>
                                                @else
                                                <p style="color: #ff0000;">
                                                	<i>Out of stock</i>
                                                </p>
                                                @endif
                                            </li>
                                            
                                            @if($product->current_stock > 0)
                                            <li>
                                                <button class="btn btn-default btn-block" data-toggle="modal" data-target="#order">Order now</button>
                                            </li>
                                            @endif

                                            <li class="has-icon">
                                                <a href="#!" class="ask-question" data-toggle="modal" data-target="#question">
                                                    <div class="icon"><img src="{{asset('assets/front/images/icon-email.svg')}}"></div>Reply by Email
                                                </a>
                                            </li>
                                            <li>
                                                <div id="fb-share-button">
                                                    <svg viewBox="0 0 12 12" preserveAspectRatio="xMidYMid meet">
                                                        <path class="svg-icon-path" d="M9.1,0.1V2H8C7.6,2,7.3,2.1,7.1,2.3C7,2.4,6.9,2.7,6.9,3v1.4H9L8.8,6.5H6.9V12H4.7V6.5H2.9V4.4h1.8V2.8 c0-0.9,0.3-1.6,0.7-2.1C6,0.2,6.6,0,7.5,0C8.2,0,8.7,0,9.1,0.1z"></path>
                                                    </svg>
                                                    <a style="color: white;text-decoration: none; 
                                                    " href="https://www.facebook.com/sharer/sharer.php?u={{ url('/'.$business_profiles->business_page_name.'/product/'.$product->id) }}&display=popup" target="_blank">
                                                Share</a>
                                            </div>
                                        </li>

                                        <li class="text-center">
                                            <p>
                                                <a href="{{url('/'.$business_profiles->business_page_name)}}" class="round">Back to {{$business_profiles->business_name}}</a>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="widget white-block">
                                <h4>Similar Products</h4>
                                <ul class="list-unstyled similar-ads">
                                    @foreach ($similar_product as $similar_product)
                                    <li>
                                        <div class="white-block ad-box ad-box-alt">
                                            <div class="media">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div>
                                                            <a href="{{url('/'.$business_profiles->business_page_name.'/product/'.$similar_product->id)}}" class="pull-left">
                                                                @if($similar_product->getImages->first())


                                                                @if($similar_product->getImages()->orderBy('id', 'desc')->first()->is_wp)
                                                                <img style="width: 125px; height: 125px; object-fit: cover;" class="img-responsive" src="{{Config('constants.image_server.url').$similar_product->getImages()->orderBy('id', 'desc')->first()['path'].'/'.$similar_product->getImages()->orderBy('id', 'desc')->first()['filename']}}"  alt=""/>
                                                                @else
                                                                <img style="width: 125px; height: 125px; object-fit: cover;" class="img-responsive" src="{{Config('constants.bucket.url').''.$similar_product->getImages()->orderBy('id', 'desc')->first()['path'].'/'.$similar_product->getImages()->orderBy('id', 'desc')->first()['filename']}}"  alt=""/>
                                                                @endif

                                                                @else
                                                                <img class="img-responsive" style="width: 125px; height: 125px; object-fit: cover;" src="{{asset('/core/storage/uploads/no_image.png')}}">
                                                                @endif

                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <a href="{{url('/'.$business_profiles->business_page_name.'/product/'.$similar_product->id)}}">
                                                                <h5>{{$similar_product->name}}</h5>
                                                            </a>
                                                            <p></p>
                                                            <p class="price">
                                                                @if ($similar_product->price)
                                                                Rs {{number_format((double)$similar_product->price, 2, '.', ',')}}
                                                                @if ($similar_product->price_negotiable)
                                                                (Price Negotiable)
                                                                @endif
                                                                @else
                                                                Call for Price
                                                                @endif                             
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        </div>

        {{-- models --}}
        <div class="modal fade in" id="order" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>
                            Order this product
                            <br>
                            <small>({{ $product->current_stock }} Items Available)</small>
                        </h4>
                        <div class="ajax-response" style="color: #048e24;"></div>
                        <div class="errors" style="color: #8e0303;"></div>
                        <form class="form-order">
                            {{ csrf_field() }}
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" maxlength="100" class="form-control"/ required>
                            <br>
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" maxlength="64" class="form-control"/ required>
                            <br>
                            <label for="contact">Contact</label>
                            <input type="number" name="contact" id="contact" class="form-control"/ required>
                            <br>
                            <label for="qty">Quantity</label>
                            <input type="number" name="qty" id="qty" max="{{ $product->current_stock }}" class="form-control"/ required>
                            <br>
                            <label for="message">Message <small>(Optional)</small></label>
                            <p class="description">Message to the seller regarding this product.</p>
                            <textarea name="message" id="message" class="form-control"></textarea>


                            <input type="hidden" name="type" value="ask_question">
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <button type="submit" class="btn" id="order-submit" style="margin-top: 10px;">place the order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in" id="question" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" id="close-qsk" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>Ask Question</h4>
                        <div class="ajax-response" style="color: #048e24;"></div>
                        <form class="form-ask">
                            {{ csrf_field() }}
                            <label for="ask-name">Name</label>
                            <input type="text" name="name" id="ask-name" class="form-control"/ required>

                            <label for="ask-email">Email</label>
                            <input type="email" name="email" id="ask-email" maxlength="64" class="form-control"/ required>

                            <label for="ask-message">Message</label>
                            <p class="description">Ask question to seller regarding this product.</p>
                            <textarea name="message" id="ask-message" class="form-control" required></textarea>

                            <input type="hidden" name="item_type" value="product">

                            <input type="hidden" name="type" value="ask_question">
                            <input type="hidden" name="ad_id" value="{{ $product->id }}">
                            <button type="submit" class="btn" style="margin-top: 10px;">ASK QUESTION</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in" id="report" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" id="close-report" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        <h4>Report Ad</h4>
                        <div class="ajax-response" style="color: #048e24;"></div>
                        <form class="form-ask">
                            {{ csrf_field() }}
                            <label for="report-name">Name*</label>
                            <input type="text" name="name" id="report-name" class="form-control" required/>

                            <label for="report-email">Email*</label>
                            <input type="email" name="email" id="report-email" maxlength="64" class="form-control" required/>

                            <label for="report-message">Reason</label>

                            <div class="form-group">
                                <label for="report-reason">Your feedback is important to us. Please tell us why you want to report this ad.</label>
                                <select class="form-control" name="reason" id="report-reason" required>
                                    <option value="" selected>Select</option>
                                    <option value="incorrect">Incorrect Category</option>
                                    <option value="inappropiate">Inappropriate Content/Images</option>
                                    <option value="fraud">Fraud/Illegal Product or Service</option>
                                    <option value="duplicate">Duplicate Ad/Spammer</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>

                            <div style="display:none;" class="form-group" id="report-message">
                                <label for="exampleFormControlTextarea1">Description*</label>
                                <textarea name="message" id="txt-message" class="form-control" placeholder="Please specify your message" rows="3"></textarea>
                            </div>

                            <input type="hidden" name="item_type" value="product">

                            <div class="form-group">
                                <input type="hidden" name="type" value="report">
                                <input type="hidden" name="ad_id" value="{{ $product->id }}">
                                <button type="submit" class="btn report" style="margin-top: 10px;">REPORT AD</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @stop


        <!-- JS FOR THIS PAGE -->
        @section('js')
        {{-- <script src="{{url('assets/front/js/fotoparma/jquery.min.js')}}"></script> <!-- 33 KB --> --}}
        <script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js')}}"></script>
        <script src="{{asset('assets/front/js/select2/select2.full.min.js')}}"></script>
        <script src="{{url('assets/front/js/fotoparma/fotorama.js')}}"></script> <!-- 16 KB -->
        <script> 
            $('#report-reason').change(function(event) {
                if ($(this).val() == "other") {
                    $('#report-message').css("display","block");
                    $('#txt-message').attr("required","required");
                } else {
                    $('#report-message').css("display","none");
                    $('#txt-message').removeAttr("required");
                }
            });
            $('#report .form-ask').on('submit', function(e) {
                e.preventDefault();
                $('#report .form-ask .report').prop('disabled', true);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('front.response.new') }}',
                    dataType: 'json', 
                    data: $('#report .form-ask').serialize(),
                    success: function (data) {
                        $('#report-name').val('');
                        $('#report-email').val('');
                        $('#report-message').val(''); 
                        $('#report-reason').val('');   
                        $("#close-report").click();     
                        $('#report .form-ask button').prop('disabled', false);               
                        toastr.success('Thank you for your feedback.');
                    }
                });
            });

            $('#question .form-ask').on('submit', function(e) {
                e.preventDefault();
                $('#question .form-ask button').prop('disabled', true);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('front.response.new') }}',
                    dataType: 'json', 
                    data: $('#question .form-ask').serialize(),
                    success: function (data) {
                        $('#ask-name').val('');
                        $('#ask-email').val('');
                        $('#ask-message').val('');
                        $('#question .ajax-response').text("").fadeOut();
                        $('#question .ajax-response').text(data).fadeIn();
                        $('#question .form-ask button').prop('disabled', false);
                        $("#close-qsk").click();                    
                        toastr.success('Thank you for your feedback.');
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ($) {
                $('.phone-reveal-1').click(function(e){
                    var last = $(this).data('last');
                    $('.phone-reveal-1').text(last);
                    $('.click-ins').text('');
                });
            });

            $('#order .form-order').on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('product.order') }}',
                    dataType: 'json', 
                    data: $('#order .form-order').serialize(),
                    success: function (data) {
                        $('#name').val('');
                        $('#email').val('');
                        $('#contact').val('');
                        $('#qty').val('');
                        $('#message').val('');
                        $('#order .ajax-response').text("").fadeOut();
                        $('#order .ajax-response').text('Successfuly Ordered').fadeIn();
                        window.location.reload();
                    }
                });
            });

            $("#contact-top li").each(function(index, el) {
                $(this).on('click', function(event) {
                    $(this).children('span').fadeOut().text("");
                    $(this).children('strong').fadeOut().text("");
                    $(this).append('<strong><a href='+'"tel:' + $(this).attr('data-value') + '" class="phone-reveal-1">'+ $(this).attr('data-value') +'</a></strong>').fadeIn();
                // $.ajax({
                //     type: 'POST',
                {{-- //     url: "{{url('/ad/numberclickcount')}}", --}}
                //     data: {
                    {{-- //         "_token": "{{ csrf_token() }}", --}}
                    {{-- //         "id": {{$ad->id}} --}}
                //     }

                // });
            });
            });

            $("#contact-bottom li").each(function(index, el) {
                $(this).on('click', function(event) {
                    $(this).children('span').fadeOut().text("");
                    $(this).children('strong').fadeOut().text("");
                    $(this).append('<strong><a href='+'"tel:' + $(this).attr('data-value') + '" class="phone-reveal-1">'+ $(this).attr('data-value') +'</a></strong>').fadeIn();
                // $.ajax({
                //     type: 'POST',
                {{-- //     url: "{{url('/ad/numberclickcount')}}", --}}
                //     data: {
                    {{-- //         "_token": "{{ csrf_token() }}", --}}
                    {{-- //         "id": {{$ad->id}} --}}
                //     }

                // });
            });

            });
        </script>
        <script src="{{asset('assets/front/js/lightbox/lightbox.min.js')}}"></script>
        @stop