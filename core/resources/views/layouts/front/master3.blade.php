    <!DOCTYPE html>
    <html lang="en-US" prefix="og: https://ogp.me/ns#">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!--         <script src='../www.google.com/recaptcha/api.js'></script> -->
        <title>@yield('title', 'Sambole.lk - Classifieds in Sri Lanka')</title>
        <meta name="google" content="notranslate">
        <meta name="description" content="@yield('description', 'Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories.')"/>
        <link rel="canonical" href={{url('/')}}" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="@yield('og_title', 'Sambole.lk - Classifieds in Sri Lanka')" />
        <meta property="og:description" content="@yield('og_description', 'Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories.')" />
        <meta property="og:url" content="@yield('og_url', 'https://www.sambole.lk/')" />
        <meta property="og:site_name" content="Sambole.lk" />
        <meta property="og:image" content="@yield('og_image')" />
        <meta property="og:image:type" content="image/png/jpg">
        <meta property="og:image:width" content="600">
        <meta property="og:image:height" content="400">


        <meta name="twitter:card" content="summary" />
        <meta name="twitter:description" content="Sambole enables you to search or buy anything from used to new products as well as services offered. Advertising is completely FREE for all categories." />
        <meta name="twitter:title" content="Sambole.lk - Classifieds in Sri Lanka" />
        <meta name="twitter:creator" content="@com" />

        <!-- / Yoast SEO plugin. -->
        <link rel='dns-prefetch' href='https://maps.googleapis.com/' />
        <link rel='dns-prefetch' href="{{url('/')}}" />
        <link rel='dns-prefetch' href='https://cdnjs.cloudflare.com/' />
        <link rel='dns-prefetch' href='https://fonts.googleapis.com/' />
        <link rel='dns-prefetch' href='https://s.w.org/' />
        <link rel="alternate" type="application/rss+xml" title="Sambole &raquo; Home Page Comments Feed" href="home-page/feed/index.html" />
        <!-- <script src="{{asset('assets/front/js/emojiSettings.js')}}"></script> -->
        <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' href="{{asset('assets/front/css/style.css')}}" type='text/css'/>
    <link rel='stylesheet' id='classifieds-awesome-css'  href="{{asset('assets/front/css/font-awesome.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-bootstrap-css'  href="{{asset('assets/front/css/bootstrap.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-carousel-css'  href="{{asset('assets/front/css/owl.carousele100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-navigation-font-css'  href='https://fonts.googleapis.com/css?family=Muli%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic&amp;ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css'  href="{{asset('assets/front/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css'  href="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='imgareaselect-css'  href="{{asset('assets/front/js/imgareaselect/imgareaselect3bf4.css?ver=0.9.8')}}" type='text/css' media='all' />
    {{-- <link rel='stylesheet' id='classifieds-select2-css'  href="{{asset('assets/front/js/select2/select2e100.css?ver=4.7.2')}}" type='text/css' media='all' /> --}}
    <link rel='stylesheet' id='classifieds-magnific-popup-css'  href="{{asset('assets/front/css/magnific-popupe100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-navigation-font-css'  href='https://fonts.googleapis.com/css?family=Muli%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic&amp;ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css'  href="{{asset('assets/front/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css'  href="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='imgareaselect-css'  href="{{asset('assets/front/js/imgareaselect/imgareaselect3bf4.css?ver=0.9.8')}}" type='text/css' media='all' />
    {{-- <link rel='stylesheet' id='classifieds-select2-css'  href="{{asset('assets/front/js/select2/select2e100.css?ver=4.7.2')}}" type='text/css' media='all' /> --}}
    <link rel='stylesheet' id='classifieds-magnific-popup-css'  href="{{asset('assets/front/css/magnific-popupe100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-navigation-font-css'  href='https://fonts.googleapis.com/css?family=Muli%3A100%2C300%2C400%2C700%2C900%2C100italic%2C300italic%2C400italic%2C700italic%2C900italic&amp;ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-css'  href="{{asset('assets/front/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css'  href="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='imgareaselect-css'  href="{{asset('assets/front/js/imgareaselect/imgareaselect3bf4.css?ver=0.9.8')}}" type='text/css' media='all' />
    {{-- <link rel='stylesheet' id='classifieds-select2-css'  href="{{asset('assets/front/js/select2/select2e100.css?ver=4.7.2')}}" type='text/css' media='all' /> --}}
    <link rel='stylesheet' id='classifieds-magnific-popup-css'  href="{{asset('assets/front/css/magnific-popupe100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-style-css'  href="{{asset('assets/front/stylee100.css?ver=4.7.2')}}" type='text/css' media='all' />
    <link rel='stylesheet' id='classifieds-style-css'  href="{{asset('assets/front/css/classifieds-style-inline-css.css?ver=4.7.2')}}" type='text/css' media='all' />
    <script src="{{asset('assets/back/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('assets/front/js/select2/select2.full.min.js')}}"></script>
    <script type='text/javascript' src="{{asset('assets/front/js/jquery/jqueryb8ff.js?ver=1.12.4')}}"></script>
    {{-- <script type='text/javascript' src="{{asset('assets/front/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script> --}}
    {{-- <script type='text/javascript' src="{{asset('assets/front/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script> --}}
    {{-- <script type='text/javascript' src="{{asset('assets/front/js/jquery/jquery-migrate.min330a.js?ver=1.4.1')}}"></script> --}}
    <script type='text/javascript' src="{{asset('assets/front/js/utils.mine100.js?ver=4.7.2')}}"></script>
    <script type='text/javascript' src="{{asset('assets/front/js/plupload/plupload.full.mincc91.js?ver=2.1.8')}}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/front/css/select2.min.css')}}" rel="stylesheet">

    <link rel='shortlink' href="{{url('/')}}" />

    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <!-- Global site tag (gtag.js) - Google Analytics -->

    @yield('css')
</head>
<body class="page-template page-template-page-tpl_search_page page-template-page-tpl_search_page-php page page-id-342">
 @yield('logo')

 <!-- Search  -->
 @yield('search')
 <section class="search-bar clearfix">
    <div class="container">
        <form method="get" class="search-form advanced-search" action="{{url('/').'/'.$business_profiles->business_page_name}}">

            <ul class="list-unstyled list-inline">
                <li>
                    <input type="text" class="form-control keyword" name="keyword" value="{{$keyword}}" placeholder="Find by keyword or short code">
                    @if ($keyword)
                    <a id="clear-search"><i class="fa fa-remove"></i></li></a>
                    @else
                    <i class="fa fa-search"></i>
                    @endif
                </li>
                <li>
                    <select class="form-control select-simple" name="sortby" id="loc-simple" data-placeholder="sortby" >

                        @foreach ($sortType as  $value): ?>
                        @if ($sortby==$value->value)
                        <option value="{{$value->id}}" selected="">{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif


                        @endforeach


                    </select>
                    <i class="fa fa-bars"></i>
                </li>
                <li>
                    <select class="form-control location select-simple" name="category" data-placeholder="Category" id="s2">
                       <option value="">All</option>
                       <?php echo CategoryManage\Models\Category::getCategoryDropDown($category) ?>
                                {{-- @foreach ($categories as $option)
                                    @if ($option->main_category_id)
                                        <option value="{{$option->id}}" class="subitem" @if ($category == $option->id) selected="selected" @endif>&nbsp;&nbsp; {{$option->name}}</option>
                                    @else
                                        <option value="{{$option->id}}" @if ($category == $option->id) selected="selected" @endif> {{$option->name}}</option>
                                    @endif
                                    @endforeach --}}

                                </select>
                                <i class="fa fa-bars"></i>
                            </li>

                            <li>
                                <a href="javascript:;" class="btn submit-form">
                                Search </a>
                            </li>
                        </ul>
                        <div class="filters-holder hidden"></div>
                    </form>
                </div>
            </section>
            <!-- Search  -->


            <div class="modal fade in" id="filters" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                            <div class="filters-modal-holder">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fadein" id="payUAdditional" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content showCode-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                            <div class="payu-content-modal">
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <script type="text/html" id="tmpl-media-frame">
                <div class="media-frame-menu"></div>
                <div class="media-frame-title"></div>
                <div class="media-frame-router"></div>
                <div class="media-frame-content"></div>
                <div class="media-frame-toolbar"></div>
                <div class="media-frame-uploader"></div>
            </script>
            <script type="text/html" id="tmpl-media-modal">
                <div class="media-modal wp-core-ui">
                    <button type="button" class="button-link media-modal-close"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>
                    <div class="media-modal-content"></div>
                </div>
                <div class="media-modal-backdrop"></div>
            </script>
            <script type="text/html" id="tmpl-uploader-window">
                <div class="uploader-window-content">
                    <h1>Drop files to upload</h1>
                </div>
            </script>
            <script type="text/html" id="tmpl-uploader-editor">
                <div class="uploader-editor-content">
                    <div class="uploader-editor-title">Drop files to upload</div>
                </div>
            </script>


            <!-- header-two.php 541 - 570 -->


            <script type="text/html" id="tmpl-media-library-view-switcher">
                <a href="/?mode=list" class="view-list">
                    <span class="screen-reader-text">List View</span>
                </a>
                <a href="/?mode=grid" class="view-grid current">
                    <span class="screen-reader-text">Grid View</span>
                </a>
            </script>
            <script type="text/html" id="tmpl-uploader-status">
                <h2>Uploading</h2>
                <button type="button" class="button-link upload-dismiss-errors"><span class="screen-reader-text">Dismiss Errors</span></button>

                <div class="media-progress-bar"><div></div></div>
                <div class="upload-details">
                    <span class="upload-count">
                        <span class="upload-index"></span> / <span class="upload-total"></span>
                    </span>
                    <span class="upload-detail-separator">&ndash;</span>
                    <span class="upload-filename"></span>
                </div>
                <div class="upload-errors"></div>
            </script>



            <!-- header-two.php 593 - 596 -->


            <script type="text/html" id="tmpl-edit-attachment-frame">
                <div class="edit-media-header">
                    <button class="left dashicons <# if ( ! data.hasPrevious ) { #> disabled <# } #>"><span class="screen-reader-text">Edit previous media item</span></button>
                    <button class="right dashicons <# if ( ! data.hasNext ) { #> disabled <# } #>"><span class="screen-reader-text">Edit next media item</span></button>
                </div>
                <div class="media-frame-title"></div>
                <div class="media-frame-content"></div>
            </script>



            <!-- header-two.php 605 - 749 -->
            <!-- header-two.php 650 - 798 -->
            <!-- header-two.php 799 - 880 -->


            <script type="text/html" id="tmpl-media-selection">
                <div class="selection-info">
                    <span class="count"></span>
                    <# if ( data.editable ) { #>
                    <button type="button" class="button-link edit-selection">Edit Selection</button>
                    <# } #>
                    <# if ( data.clearable ) { #>
                    <button type="button" class="button-link clear-selection">Clear</button>
                    <# } #>
                </div>
                <div class="selection-view"></div>
            </script>



            <!-- header-two.php 893 - 997 -->


            <script type="text/html" id="tmpl-gallery-settings">
                <h2>Gallery Settings</h2>

                <label class="setting">
                    <span>Link To</span>
                    <select class="link-to"
                    data-setting="link"
                    <# if ( data.userSettings ) { #>
                    data-user-setting="urlbutton"
                    <# } #>>

                    <option value="post" <# if ( ! wp.media.galleryDefaults.link || 'post' == wp.media.galleryDefaults.link ) {
                    #>selected="selected"<# }
                    #>>
                Attachment Page             </option>
                <option value="file" <# if ( 'file' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
                Media File              </option>
                <option value="none" <# if ( 'none' == wp.media.galleryDefaults.link ) { #>selected="selected"<# } #>>
                None                </option>
            </select>
        </label>

        <label class="setting">
            <span>Columns</span>
            <select class="columns" name="columns"
            data-setting="columns">
            <option value="1" <#
            if ( 1 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
            #>>
        1                   </option>
        <option value="2" <#
        if ( 2 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
        #>>
    2                   </option>
    <option value="3" <#
    if ( 3 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
    #>>
3                   </option>
<option value="4" <#
if ( 4 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
4                   </option>
<option value="5" <#
if ( 5 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
5                   </option>
<option value="6" <#
if ( 6 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
6                   </option>
<option value="7" <#
if ( 7 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
7                   </option>
<option value="8" <#
if ( 8 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
8                   </option>
<option value="9" <#
if ( 9 == wp.media.galleryDefaults.columns ) { #>selected="selected"<# }
#>>
9                   </option>
</select>
</label>

<label class="setting">
    <span>Random Order</span>
    <input type="checkbox" data-setting="_orderbyRandom" />
</label>

<label class="setting size">
    <span>Size</span>
    <select class="size" name="size"
    data-setting="size"
    <# if ( data.userSettings ) { #>
    data-user-setting="imgsize"
    <# } #>
    >
    <option value="thumbnail">
    Thumbnail                   </option>
    <option value="medium">
    Medium                  </option>
    <option value="large">
    Large                   </option>
    <option value="full">
    Full Size                   </option>
</select>
</label>
</script>
<script type="text/html" id="tmpl-playlist-settings">
    <h2>Playlist Settings</h2>

    <# var emptyModel = _.isEmpty( data.model ),
    isVideo = 'video' === data.controller.get('library').props.get('type'); #>

    <label class="setting">
        <input type="checkbox" data-setting="tracklist" <# if ( emptyModel ) { #>
        checked="checked"
        <# } #> />
        <# if ( isVideo ) { #>
        <span>Show Video List</span>
        <# } else { #>
        <span>Show Tracklist</span>
        <# } #>
    </label>

    <# if ( ! isVideo ) { #>
    <label class="setting">
        <input type="checkbox" data-setting="artists" <# if ( emptyModel ) { #>
        checked="checked"
        <# } #> />
        <span>Show Artist Name in Tracklist</span>
    </label>
    <# } #>

    <label class="setting">
        <input type="checkbox" data-setting="images" <# if ( emptyModel ) { #>
        checked="checked"
        <# } #> />
        <span>Show Images</span>
    </label>
</script>
<script type="text/html" id="tmpl-embed-link-settings">
    <label class="setting link-text">
        <span>Link Text</span>
        <input type="text" class="alignment" data-setting="linkText" />
    </label>
    <div class="embed-container" style="display: none;">
        <div class="embed-preview"></div>
    </div>
</script>



<!-- header-two.php 1129 - 1170 -->
<!-- header-two.php 1171 - 1309 -->
<!-- header-two.php 1310 - 1313 -->
<!-- header-two.php 1314 - 1456 -->
<!-- header-two.php 1457 - 1697 -->
<!-- header-two.php 1698 - 1726 -->
<!-- header-two.php 1727 - 1730 -->
<!-- header-two.php 1731 - 1747 -->



<script type='text/javascript' src="{{asset('assets/front/js/underscore.min4511.js?ver=1.8.3')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/backbone.min9632.js?ver=1.2.3')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/wp-api.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDTkWWz97piXi43nvxfdFkdppZz3aeY7rc&amp;libraries=places&amp;ver=4.7.2'></script>
<script type='text/javascript' src="{{asset('assets/front/js/bootstrap.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/bootstrap-dropdown-multilevele100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/shortcode.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/wp-util.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/wp-backbone.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpMediaModelsL10n = {"settings":{"ajaxurl":"\/wp-admin\/admin-ajax.php","post":{"id":0}}};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/media-models.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var pluploadL10n = {"queue_limit_exceeded":"You have attempted to queue too many files.","file_exceeds_size_limit":"%s exceeds the maximum upload size for this site.","zero_byte_file":"This file is empty. Please try another.","invalid_filetype":"This file type is not allowed. Please try another.","not_an_image":"This file is not an image. Please try another.","image_memory_exceeded":"Memory exceeded. Please try another smaller file.","image_dimensions_exceeded":"This is larger than the maximum size. Please try another.","default_error":"An error occurred in the upload. Please try again later.","missing_upload_url":"There was a configuration error. Please contact the server administrator.","upload_limit_exceeded":"You may only upload 1 file.","http_error":"HTTP error.","upload_failed":"Upload failed.","big_upload_failed":"Please try uploading this file with the %1$sbrowser uploader%2$s.","big_upload_queued":"%s exceeds the maximum upload size for the multi-file uploader when used in your browser.","io_error":"IO error.","security_error":"Security error.","file_cancelled":"File canceled.","upload_stopped":"Upload stopped.","dismiss":"Dismiss","crunching":"Crunching\u2026","deleted":"moved to the trash.","error_uploading":"\u201c%s\u201d has failed to upload."};
    var _wpPluploadSettings = {"defaults":{"runtimes":"html5,flash,silverlight,html4","file_data_name":"async-upload","url":"\/wp-admin\/async-upload.php","flash_swf_url":"https:\/\/www.sambole.lk\/wp-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"https:\/\/www.sambole.lk\/wp-includes\/js\/plupload\/plupload.silverlight.xap","filters":{"max_file_size":"67108864b","mime_types":[{"extensions":"jpg,jpeg,jpe,gif,png,bmp,tiff,tif,ico,asf,asx,wmv,wmx,wm,avi,divx,flv,mov,qt,mpeg,mpg,mpe,mp4,m4v,ogv,webm,mkv,3gp,3gpp,3g2,3gp2,txt,asc,c,cc,h,srt,csv,tsv,ics,rtx,css,vtt,dfxp,mp3,m4a,m4b,ra,ram,wav,ogg,oga,mid,midi,wma,wax,mka,rtf,js,pdf,class,tar,zip,gz,gzip,rar,7z,psd,xcf,doc,pot,pps,ppt,wri,xla,xls,xlt,xlw,mdb,mpp,docx,docm,dotx,dotm,xlsx,xlsm,xlsb,xltx,xltm,xlam,pptx,pptm,ppsx,ppsm,potx,potm,ppam,sldx,sldm,onetoc,onetoc2,onetmp,onepkg,oxps,xps,odt,odp,ods,odg,odc,odb,odf,wp,wpd,key,numbers,pages,redux"}]},"multipart_params":{"action":"upload-attachment","_wpnonce":"002e5e128a"}},"browser":{"mobile":false,"supported":true},"limitExceeded":false};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/plupload/wp-plupload.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/core.mine899.js?ver=1.11.4')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/widget.mine899.js?ver=1.11.4')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/mouse.mine899.js?ver=1.11.4')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/jquery/ui/sortable.mine899.js?ver=1.11.4')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Download File":"Download File","Download Video":"Download Video","Play":"Play","Pause":"Pause","Captions\/Subtitles":"Captions\/Subtitles","None":"None","Time Slider":"Time Slider","Skip back %1 seconds":"Skip back %1 seconds","Video Player":"Video Player","Audio Player":"Audio Player","Volume Slider":"Volume Slider","Mute Toggle":"Mute Toggle","Unmute":"Unmute","Mute":"Mute","Use Up\/Down Arrow keys to increase or decrease volume.":"Use Up\/Down Arrow keys to increase or decrease volume.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
    var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/mediaelement/mediaelement-and-player.min51cd.js?ver=2.22.0')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/mediaelement/wp-mediaelement.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpMediaViewsL10n = {"url":"URL","addMedia":"Add Media","search":"Search","select":"Select","cancel":"Cancel","update":"Update","replace":"Replace","remove":"Remove","back":"Back","selected":"%d selected","dragInfo":"Drag and drop to reorder media files.","uploadFilesTitle":"Upload Files","uploadImagesTitle":"Upload Images","mediaLibraryTitle":"Media Library","insertMediaTitle":"Insert Media","createNewGallery":"Create a new gallery","createNewPlaylist":"Create a new playlist","createNewVideoPlaylist":"Create a new video playlist","returnToLibrary":"\u2190 Return to library","allMediaItems":"All media items","allDates":"All dates","noItemsFound":"No items found.","insertIntoPost":"Insert into post","unattached":"Unattached","trash":"Trash","uploadedToThisPost":"Uploaded to this post","warnDelete":"You are about to permanently delete this item.\n  'Cancel' to stop, 'OK' to delete.","warnBulkDelete":"You are about to permanently delete these items.\n  'Cancel' to stop, 'OK' to delete.","warnBulkTrash":"You are about to trash these items.\n  'Cancel' to stop, 'OK' to delete.","bulkSelect":"Bulk Select","cancelSelection":"Cancel Selection","trashSelected":"Trash Selected","untrashSelected":"Untrash Selected","deleteSelected":"Delete Selected","deletePermanently":"Delete Permanently","apply":"Apply","filterByDate":"Filter by date","filterByType":"Filter by type","searchMediaLabel":"Search Media","searchMediaPlaceholder":"Search media items...","noMedia":"No media files found.","attachmentDetails":"Attachment Details","insertFromUrlTitle":"Insert from URL","setFeaturedImageTitle":"Featured Image","setFeaturedImage":"Set featured image","createGalleryTitle":"Create Gallery","editGalleryTitle":"Edit Gallery","cancelGalleryTitle":"\u2190 Cancel Gallery","insertGallery":"Insert gallery","updateGallery":"Update gallery","addToGallery":"Add to gallery","addToGalleryTitle":"Add to Gallery","reverseOrder":"Reverse order","imageDetailsTitle":"Image Details","imageReplaceTitle":"Replace Image","imageDetailsCancel":"Cancel Edit","editImage":"Edit Image","chooseImage":"Choose Image","selectAndCrop":"Select and Crop","skipCropping":"Skip Cropping","cropImage":"Crop Image","cropYourImage":"Crop your image","cropping":"Cropping\u2026","suggestedDimensions":"Suggested image dimensions:","cropError":"There has been an error cropping your image.","audioDetailsTitle":"Audio Details","audioReplaceTitle":"Replace Audio","audioAddSourceTitle":"Add Audio Source","audioDetailsCancel":"Cancel Edit","videoDetailsTitle":"Video Details","videoReplaceTitle":"Replace Video","videoAddSourceTitle":"Add Video Source","videoDetailsCancel":"Cancel Edit","videoSelectPosterImageTitle":"Select Poster Image","videoAddTrackTitle":"Add Subtitles","playlistDragInfo":"Drag and drop to reorder tracks.","createPlaylistTitle":"Create Audio Playlist","editPlaylistTitle":"Edit Audio Playlist","cancelPlaylistTitle":"\u2190 Cancel Audio Playlist","insertPlaylist":"Insert audio playlist","updatePlaylist":"Update audio playlist","addToPlaylist":"Add to audio playlist","addToPlaylistTitle":"Add to Audio Playlist","videoPlaylistDragInfo":"Drag and drop to reorder videos.","createVideoPlaylistTitle":"Create Video Playlist","editVideoPlaylistTitle":"Edit Video Playlist","cancelVideoPlaylistTitle":"\u2190 Cancel Video Playlist","insertVideoPlaylist":"Insert video playlist","updateVideoPlaylist":"Update video playlist","addToVideoPlaylist":"Add to video playlist","addToVideoPlaylistTitle":"Add to Video Playlist","settings":{"tabs":[],"tabUrl":"https:\/\/www.sambole.lk\/wp-admin\/media-upload.php?chromeless=1","mimeTypes":{"image":"Images","audio":"Audio","video":"Video"},"captions":true,"nonce":{"sendToEditor":"0dc6239036"},"post":{"id":3274},"defaultProps":{"link":"none","align":"","size":""},"attachmentCounts":{"audio":0,"video":0},"embedExts":["mp3","ogg","wma","m4a","wav","mp4","m4v","webm","ogv","wmv","flv"],"embedMimes":{"mp3":"audio\/mpeg","ogg":"audio\/ogg","wma":"audio\/x-ms-wma","m4a":"audio\/mpeg","wav":"audio\/wav","mp4":"video\/mp4","m4v":"video\/mp4","webm":"video\/webm","ogv":"video\/ogg","wmv":"video\/x-ms-wmv","flv":"video\/x-flv"},"contentWidth":1920,"months":[{"year":"2018","month":"2","text":"February 2018"},{"year":"2018","month":"1","text":"January 2018"},{"year":"2017","month":"12","text":"December 2017"},{"year":"2017","month":"11","text":"November 2017"},{"year":"2017","month":"10","text":"October 2017"},{"year":"2017","month":"9","text":"September 2017"},{"year":"2017","month":"8","text":"August 2017"},{"year":"2017","month":"7","text":"July 2017"},{"year":"2017","month":"6","text":"June 2017"},{"year":"2017","month":"5","text":"May 2017"},{"year":"2017","month":"4","text":"April 2017"},{"year":"2017","month":"3","text":"March 2017"},{"year":"2017","month":"2","text":"February 2017"},{"year":"2017","month":"1","text":"January 2017"}],"mediaTrash":0}};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/media-views.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/media-editor.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/media-audiovideo.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/gmape100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/custom-fieldse100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/markerclusterer_compilede100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/comment-reply.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/owl.carousel.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/responsiveslides.mine100.js?ver=4.7.2')}}"></script>
{{-- <script type='text/javascript' src="{{asset('assets/front/js/select2/select2.mine100.js?ver=4.7.2')}}"></script> --}}
<script type='text/javascript' src="{{asset('assets/front/js/imagesloadede100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/masonrye100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/cachee100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/jquery.magnific-popup.mine100.js?ver=4.7.2')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var classifieds_data = {"url":"https:\/\/www.sambole.lk\/assets\/themes\/sambole","home_map_geolocation":"no","home_map_geo_zoom":"","contact_map_zoom":"5","restrict_country":"LK","home_map_zoom":"","map_price":"","empty_search_location":"","ads_max_videos":"10","ads_max_images":"10"};
    /* ]]> */
</script>
<script type='text/javascript' src="{{asset('assets/front/js/custome100.js?ver=4.7.2')}}"></script>
<script type='text/javascript' src="{{asset('assets/front/js/wp-embed.mine100.js?ver=4.7.2')}}"></script>
@yield('content')


<style type="text/css">
.topic_login{
    text-align: center;
}
.submit-form-ajax1{
    width: 90%;
    background-color: #712b2f;
    margin-left: 5%;
    margin-bottom: 30px;
}
.submit-form-ajax1:hover{
    background-color:#cccccc;
    color: #712b2f;
}
.register-close-login1{
    width: 100%;
    margin-left: 20px;
}
.or{
    font-style: italic;
    color: #b8b894;
    margin-left: 10px;
    margin-top: 20px;
    text-align: center;
}
.login-remember{
    color: #333333;
}
.login-remember:hover{
    color: #a6a6a6;
}
.forgot-password1 {
    color:  #333333;
}
.forgot-password1:hover{
    color: #a6a6a6;
}
.register-close-login2{
    background-color: #ffad33;
    color: white;
    width: 95%;
    margin-left: 2%;
    margin-top: 20px;
}
.register-close-login2:hover{
    background-color: #cccccc;
    color:  #ffad33;
}

#btn1
{
    text-align: center;
}

#submit
{

   background-color: #712b2f;
   margin-right: 10px;
}

#submit:hover
{
 background-color: #D2691E;

}

#reset:hover
{

    background-color: #D2691E;
}

.button-style1 button {
    font-size:20px;
    /*margin:0 20px 0 20px;*/
    width: 60%;
    border-radius: 5px;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px ;
    cursor: pointer;
}

.button-style1
{
    text-align: center;
}

.member-reg {
    background-color: #4CAF50;
}
.business-reg {
    background-color: #ff9933;

}
.member-reg:hover{
    background-color: #eaeae1;
    color:#4CAF50;
}
.business-reg:hover {
    background-color: #eaeae1;
    color:#ff9933;
}

#h5{
    color: #737373;
}
.modal-content{
    width: 100%;
}

@media(min-width: 0px) and (max-width: 319px){
    .select2-display-none, #select2-drop{
        width:100%!important;
        left:0!important;
    }
    .navigation .navbar-default .btn.submit-add{
        padding: 10px 12px;
        height: auto;
        font-size: 13px;
        background-color: #712b2f;
        box-shadow: 0.788px 0.616px 3px 0px rgba(0, 0, 0, 0.2);
        text-transform: none;
        color: #fff;
        position: absolute;
        right: 70px;
        top: 8px;
    }
    .navigation .navbar-default .navbar-brand img {
        /* display: inline-block; */
        width: 7.8rem;
        /* margin-left: -20px!important; */
    }
    .button-style1 button {
        font-size: 20px;
        margin: 0 20px 0 20px;
        width: 60%;
        border-radius: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        border-bottom-left-radius: 5px;
        border: none;
        color: white;
        padding: 15px 3px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 10px;
        margin: 4px 2px;
        cursor: pointer;
    }
}

@media(min-width: 320px) and (max-width: 359px){
    .select2-display-none, #select2-drop{
        width:100%!important;
        left:0!important;
    }
    /************* User Type *****************/

    .topic {
        font-size: 13px;
    }

    #h5 {
        font-size: 12px;
    }

    .button-style1 button {
        padding: 15px 10px;
        font-size: 11px;
        width: 100%;
    }

    .modal-content {
        padding: 20px 10px;
    }

    .member-reg {
        font-size: 12px;
    }

}

/************* Contact Popup Model *****************/

.contact {
    font-size: 13px;
}

.label1 {
    font-size: 11px;
}

#submit {
    font-size: 10px;
}

#reset {
    font-size: 10px;
}

}


@media(min-width: 360px) and (max-width: 767px){

    /************* User Type *****************/

    .topic {
        font-size: 18px;
    }

    #h5 {
        font-size: 16px;
    }

    .button-style1 button {
        padding: 17px 20px;
        font-size: 16px;}

        @media(max-width: 320px) and (max-height: 480px){
            .forgetPass-login {
                text-align: left;
            }

            .rememberMe-login {
                text-align: left;
            }

            /************* User Type *****************/

            .topic {
                font-size: 22px;
            }

            #h5 {
                font-size: 15px;
            }

            .button-style1 button {
                padding: 15px 10px;
                font-size: 15px;

                width: 100%;
            }

            .modal-content {
                padding: 30px 10px;
            }



            /************* Contact Popup Model *****************/

            .contact {
                font-size: 28px;
            }

            .label1 {
                font-size: 19px;
            }

            #submit {
                font-size: 17px;
            }

            #reset {
                font-size: 17px;
            }

        }

        @media (min-width: 768px) and (max-width: 979px){

        }

        @media(min-width: 360px) and (max-width: 640px){


            /************* User Type *****************/

            .topic {

                font-size: 22px;
            }

            #h5 {
                font-size: 20px;

                font-size: 24px;
            }

            #h5 {
                font-size: 17px;

            }

            .button-style1 button {
                padding: 17px 20px;

                font-size: 19px;

                font-size: 16px;

                width: 100%;
            }

            .modal-content {
                padding: 30px 10px;
            }




            /************* Contact Popup Model *****************/

            .contact {
                font-size: 22px;
            }

            .label1 {
                font-size: 20px;
            }

            #submit {
                font-size: 16px;
            }

            #reset {
                font-size: 16px;
            }

        }


        @media(min-width: 980px) and (max-width: 1279px){


            /************* Contact Popup Model *****************/

            .contact {
                font-size: 28px;
            }

            .label1 {
                font-size: 26px;
            }

            #submit {
                font-size: 22px;
            }

            #reset {
                font-size: 22px;
            }

            input .one {
                width: 100px;
            }



            /************* User Type *****************/

            .topic {
                font-size: 26px;
            }

            #h5 {
                font-size: 24px;
            }

            .button-style1 button {
                padding: 17px 20px !important;
                font-size: 28px;
                width: 100% !important;
            }

            .modal-content {
                padding: 30px 10px !important;
            }

        }

        @media(min-width: 768px) and (max-width: 980px){

            .model-dialog {
                width:700px;
                margin: 30px auto;
            }

            .button-style1 button {
                width: 80%;
            }
        }


    </style>








</style>




<div class="footer-copy">Powered by <strong>SAMBOLE.LK</strong> <span class="footer-version">CURRENT VERSION: {{ $config->version }}</span></div>




<!-- JS LINK -->
<!-- CODE HERE -->
<!-- ======= -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js" type="text/javascript"></script>
<link href="{{asset('assets/back/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<script src="{{asset('assets/back/js/plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('assets/front/js/select2/select2.full.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $(".select-simple").select2({
                    // theme: "flat",
                    minimumResultsForSearch: 5
                    // tags: true
                });



    //   jQuery("#s1").select2({
    //     allowClear: true
    // }).on("select2-removed", function(e){
    //             // var baseUrl = {!! json_encode(url('/')) !!}
    //             // window.location = baseUrl + "/ad/search";
    //         });
    // jQuery("#s2").select2({
    //     allowClear: true
    // }).on("select2-removed", function(e){
    //             // var baseUrl = {!! json_encode(url('/')) !!}
    //             // window.location = baseUrl + "/ad/search";
    //         });

    jQuery("#clear-search").click(function(){
        alert();
        jQuery(".keyword").val("");
        var baseUrl = {!! json_encode(url('/')) !!}
        window.location = baseUrl + "/ad/search";
    });

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
  }

  @if(session('success'))
  Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
  @elseif(session('error'))
  Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
  @elseif(session('warning'))
  Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
  @elseif(session('info'))
  Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
  @endif

});




</script>
@yield('js')
</body>

</html>
