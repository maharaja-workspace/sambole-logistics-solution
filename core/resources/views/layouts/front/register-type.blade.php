@extends('layouts.front.master')

<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
    /* Media screens lesser than 480px */
    @media(max-width: 320px) and (max-height: 480px), (max-width: 320px){

        h1.sub{
            font-size: 20px;
            margin-bottom: -10px;
        }
        #heading {
            font-size: 14px;
            text-align: center;
            margin-bottom: -2px;
        }
        .checkbox label::before{
            left: 4px;
            margin-left: -18px;
        }
        .checkbox input[type="checkbox"]{
            margin-left: -10px;
        }
        .checkbox input[type="checkbox"]:checked + label::after{
            left: 11px;
        }
        .checkbox label{
            padding-left: 10px;
            line-height: 21px;
        }
        #btn .submit, button.reset{
            font-size: 12px;
            padding: 10px;
        }
    }
    /* Media screens lesser than 640px and larger than 360px */
    @media(min-width: 360px) and (max-width: 640px){

    }
    /* Media screens lesser than 1024px and larger than 768px */
    @media(min-width: 768px) and (max-width: 1024px){

    }
    /* Media screens lesser than 1280px and larger than 800px - Portaite Version */
    @media(min-width: 800px) and (max-width: 1280px){

    }
    /* Media screens lesser than 1280px and larger than 980px - Portaite Version */
    @media(min-width: 980px) and (max-width: 1280px){

    }
    /* Media screens lesser than 600px and larger than 1280px - Landscape Version */
    @media(min-width: 1280px) and (max-width: 600px){

    }

    /* Media screens lesser than 900px and larger than 1920px - Landscape Version */
    @media(min-width: 1920px) and (max-width: 900px){

    }
</style>
<style type="text/css">
    #success_message{ display: none;}
</style>


@stop


<!-- BODY -->

@section('content')
<section class="martop30">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="white-block" id="company-reg">
                    <div class="white-block-content">
                        <div class="page-content clearfix martop30">
                              <form class="well form-horizontal" action="{{URL::to('front/register/type')}}" method="post" id="contact_form">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <!-- Form Name -->
                            <legend>
                                <center>
                                    <h2>USER TYPE</h2>
                                </center>
                            </legend>
                            
                        
                            <div class="button-style1">
                                <a href="{{url('/')}}" >
                                <button  class="member-reg" name="user_type" value="ad.user">
                                    <i class="fa fa-users"></i>  &nbsp; I want to add Advertisements
                                   
                                </button></a>
                                <a href="">
                                   <button class="business-reg" name="user_type" value="business.user">
                                    <i class="fa fa-user"></i> &nbsp; I want to add Products
                                    
                                  </button>
                                </a>
                               
                            </div>
                            <!-- Select Basic -->
                            
                        </fieldset>
                    </form>
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
   <script type="text/javascript">
    $(document).ready(function() {
    

    });
    
</script>
    
@stop
