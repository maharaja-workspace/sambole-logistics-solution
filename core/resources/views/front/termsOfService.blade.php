@extends('layouts.front.master')

<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
                .a1{
                    font-size: 30px;
                    text-align: center;
                }
                @media(min-width:320px) and (max-width:480px){
                    .hd{
                        font-size: 20px;
                    }
                    .breadcrumb{
                        font-size:20px;
                    }
                }

</style>
@stop


<!-- BODY -->

@section('content')
<section class="page-title">
    <div class="container">
        <div class="clearfix">

            <ul class="breadcrumb"><li><a href="{{url('/')}}">Home</a></li><li class="hd">Terms &#038; Conditions</li></ul>
            <p class="a1">
                Terms &#038; Conditions     
            </p>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="white-block">
                    
                    <div class="white-block-content">
                        <div class="page-content clearfix">
                            <h2>Terms of Use</h2>
<p class="pspace">&nbsp;</p>
<h3>Legal Notices</h3>
<p>We, the Operators of this Website, provide it as a public service to our users.

Please carefully review the following basic rules that govern your use of the Website. Please note that your use of the Website constitutes your unconditional agreement to follow and be bound by these Terms and Conditions of Use. If you (the “User”) do not agree to them, do not use the Website, provide any materials to the Website or download any materials from them.

The Operators reserve the right to update or modify these Terms and Conditions at any time without prior notice to User. Your use of the Website following any such change constitutes your unconditional agreement to follow and be bound by these Terms and Conditions as changed. For this reason, we encourage you to review these Terms and Conditions of Use whenever you use the Website.

These Terms and Conditions of Use apply to the use of the Website and do not extend to any linked third party sites. These Terms and Conditions and our Privacy Policy, which are hereby incorporated by reference, contain the entire agreement (the “Agreement”) between you and the Operators with respect to the Website. Any rights not expressly granted herein are reserved.</p>
<p class="pspace">&nbsp;</p>
                            <h2>Privacy Policy</h2>
<p class="pspace">&nbsp;</p>
<p>This privacy policy describes how we handle your personal information. We collect, use, and share personal information to help the SAMBOLE.lk website work and to keep it safe.
Collection: Information posted on SAMBOLE.lk is obviously publicly available. We collect and store the following personal information: email address, physical contact information.
</p>
<ul>
    <li>computer sign-on data, statistics on page views, traffic to and from SAMBOLE.lk (all through cookies – you can take steps to disable the cookies on your browser although this is likely to affect your ability to use the site).</li>
    <li>other information, including users IP address and standard web log information.</li>
</ul>
<p class="pspace">&nbsp;</p>
<h3>Use</h3>
<p>We use users’ personal information to:</p>
<ul>
    <li>provide our services;</li>
    <li>resolve disputes, and troubleshoot problems;</li>
    <li>encourage safe trading and enforce our policies;</li>
    <li>customize users’ experience, measure interest in our services, and inform users about services and updates;</li>
    <li>communicate marketing and promotional offers to you;</li>
    <li>Do other things for users as described when we collect the information.</li>
</ul>
<p class="pspace">&nbsp;</p>
<h3>Disclosure</h3>
<ul>
    <li>All information contained by SAMBOLE.lk is treated as strictly confidential and SAMBOLE.lk does not and will not disclose or share such confidential information to / with any external organization.</li>
    <li>The users’ privacy is very important to us. We do not and shall not at any point in time, either sell or rent users’ personal information to third parties without users’ explicit consent.</li>
    <li>We may be required from time to time to disclose users’ personal information to Governmental or law enforcing agencies or our regulators, but we will only do so under proper authority.</li>
    <li>SAMBOLE.lk also reserves the right to make use of the personal information in any investigation or judicial process relating to fraud on account of such transactions during the period SAMBOLE.lk retains such information.</li>
    <li>We may also disclose personal information to enforce our policies, respond to claims that a posting or other content violates other’s rights, or protects anyone’s rights, property or safety.</li>
    <li>We may also share personal information with: – Service providers who help with our business operations.</li>
    <li>Third party vendors, including Google, use cookies to serve ads based on a user’s prior visits to the website.</li>
</ul>

<p class="pspace">&nbsp;</p>
<h3>Using Information from SAMBOLE.lk</h3>
<p>You may use personal information gathered from SAMBOLE.lk only to follow up with another user about a specific posting, not to send spam/ phishing or collect personal information from someone who hasn’t agreed to that.</p>
<p style="color: #606060;"><strong>Access, Modification, and Deletion:</strong></p>
<p>You can see, modify or erase your personal information by reviewing your posting or account status page. C. We retain personal information as permitted by law to resolve disputes, enforce our policies; and prevent bad guys from coming back.</p>

<p class="pspace">&nbsp;</p>
<h3>Data Security</h3>
We use a number of mechanisms (encryption, passwords, physical security) to protect the security and integrity of your personal information against unauthorized access and disclosure. Unfortunately, no data transmission over the internet can be guaranteed to be completely secure. So while we strive to protect such information, we cannot ensure or warrant the security of any information you transmit to us and you do so at your own risk. Once any personal information comes into our possession, we will take reasonable steps to protect that information from misuse and loss and from unauthorized access, modification or disclosure.</p>

<p class="pspace">&nbsp;</p>
<h3>Confidentiality of Information:</h3>
<p>Users who use any of the features on SAMBOLE.lk agree and accept that they have been fully informed by SAMBOLE.lk that the use of features may lead to publication, to all users of sambole.lk, of any personal information posted by them while using any specific feature on SAMBOLE.lk. Users further agree that the authenticity of, and consequences from the posting by users of any personal information of themselves or any other person, are the sole responsibility of the user. Users further agree and accept that the terms of the Privacy Policy will be applicable to the use of all features, existing and new. However, the Users agree and accept that confidentiality of information posted on such features has been waived by the Users of such features themselves.</p>
<strong>General</strong>
<p>We may update, upgrade, modify (partially &/or fully) this policy at any time.
Send inquiries about this policy to info@sambole.lk
</p>
<p class="pspace">&nbsp;</p>
<h2>TERMS</h2>
<p><strong>PLEASE CAREFULLY READ THESE TERMS OF USE. BY USING THIS WEBSITE, YOU INDICATE YOUR UNDERSTANDING AND ACCEPTANCE OF THESE TERMS. IF YOU DO NOT AGREE TO THESE TERMS YOU MAY NOT USE THIS WEBSITE.</strong></p>
<p>Welcome to www.sambole.lk. These are the terms and conditions governing your use of the Site (“herein after referred to as Acceptable Use Policy “AUP”). By accessing SAMBOLE.lk either through the website or any other electronic device, you acknowledge, accept and agree to the following terms of the AUP, which are designed to make sure that SAMBOLE.lk works for everyone. This AUP is effective from the time you log in to SAMBOLE.lk. By accepting this AUP, you are also accepting and agreeing to be bound by the Privacy Policy and the Listing Policy.</p>
<p class="pspace">&nbsp;</p>


    <h3>1. Using SAMBOLE.lk</h3>
    <p>You agree and understand that www.sambole.lk is an internet enabled electronic platform that facilitates communication for the purposes of advertising and distributing information pertaining to goods and/ or services. You further agree and understand that we do not endorse, market or promote any of the listings, postings or information, nor do we at any point in time come into possession of or engage in the distribution of any of the goods and/ or services, you have posted, listed or provided information about on our site. While interacting with other users on our site, with respect to any listing, posting or information we strongly encourage you to exercise reasonable diligence as you would in traditional off line channels and practice judgment and common sense before committing to or complete intended sale, purchase of any goods or services or exchange of information.
    While making use of SAMBOLE.lk classifieds and other services, you will post in the appropriate category or area and you agree that your use of the Site shall be strictly governed by this AUP including the policy for listing of your Classified which shall not violate the prohibited and restricted items policy (herein after referred to as the Listing Policy.) The listing policy shall be read as part of this AUP and is incorporated in this AUP by way of reference:
</p>

<p><strong>a.</strong> “Your Information” is defined as any information you provide to us or other users of the Site during the registration, posting, listing or replying process of classifieds, in the feedback area (if any), through the discussion forums or in the course of using any other feature of the Services. You agree that you are the lawful owner having all rights, title and interest in your information, and further agree that you are solely responsible and accountable for Your Information and that we act as a mere platform for your online distribution and publication of Your Information.
</p> 
<p><strong>b.</strong> You agree that your listing, posting and / or Information:
            <ul style="list-style-type:disc;">
                <li>shall “not be fraudulent, misrepresent, mislead or pertain to the sale of any illegal, counterfeit, stolen goods and / or services</li>
                <li>shall not pertain to good, services of which you are not the lawful owner or you do not have the authority or consent to ‘list’ which do not belong to you or you do not have the authority for</li>
                <li>shall not infringe any intellectual property, trade secret, or other proprietary right or rights of publicity or privacy of any third party;</li>
                <li>shall not consist of material that is an expression of bigotry, racism or hatred based on age, gender, race, religion, caste, class, lifestyle preference, and nationality and / or is in the nature of being derogatory, slanderous to any third party;</li>
                <li>shall not be obscene, contain pornography;</li>
                <li>shall not distribute or contain spam, multiple / chain letters, or pyramid schemes in any of its forms;</li>
                <li>shall not distribute viruses or any other technologies that may harm SAMBOLE.lk or the interests or property of SAMBOLE.lk users or impose an unreasonable load on the infrastructure or interfere with the proper working of SAMBOLE.lk;</li>
                <li>shall not, directly or indirectly, offer, attempt to offer, trade or attempt to trade in any goods and services, the dealing of which is prohibited or restricted in any manner under the provisions of any applicable law, rule, regulation or guideline for the time being in force.</li>
                <li>shall not be placed in a wrong category or in an incorrect area of the site;</li>
                <li>shall not list or post or pertain to information that is either prohibited or restricted under the laws of the Democratic Socialist Republic of Sri Lanka and such listing, posting or information shall not violate SAMBOLE.lk’s Listing Policy</li>
            </ul>
</p>
    
    <h3>2. Abuse</h3>
    <p>You agree to inform us if you come across any listing or posting that is offensive or violates our listing policy or infringes any intellectual property rights by clicking on the following link info@sambole.lk to enable us to keep the site working efficiently and in a safe manner. We reserve the right to take down any posting, listing or information and or limit or terminate our services and further take all reasonable technical and legal steps to prevent the misuse of the Site in keeping with the letter and spirit of this AUP and the listing policy. In the event you encounter any problems with the use of our site or services you are requested to report the problem by clicking on this link info@sambole.lk</p>

    
    <h3>3. Violations by User</h3>
    <p>You agree that in the event your listing, posting or your information violates any provision of this AUP or the listing policy, we shall have the right to terminate and or suspend your membership of the Site and refuse to provide you or any person acting on your behalf, access to the Site.</p>

    <h3>4. Content</h3>
    <p>The Site contains content which includes Your Information, SAMBOLE information and information from other users. You agree not to copy, modify, or distribute such content (other than Your Information), SAMBOLE’s copyrights or trademarks. When you give us any content as part of Your Information, you are granting us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable right and license to use, reproduce, publish, translate, distribute, perform and display such content (in whole or part) worldwide through the Site as well as on any of our affiliates or partner’s websites, publications and mobile platform. We need these rights with respect to the content in Your Information in order to host and display your content. If you believe that there is a violation, please notify us by clicking here info@sambole.lk.</p>
    <p>We reserve the right to remove any such content where we have grounds for suspecting the violation of these terms and our Listing Policy or of any party’s rights.</p>

    <h3>5. Third Party Content and Services</h3>
    <p>SAMBOLE may provide, on its site, links to sites operated by other entities. If the user decides to view this site, they shall do so at their own risk, subject to that site’s terms and conditions of use and privacy policy that may be different from those of this site. It is the user’s responsibility to take all protective measures to guard against viruses or other destructive elements they may encounter on these sites. SAMBOLE.lk makes no warranty or representation regarding, and do not endorse any linked website or the information appearing thereon or any of the products or services described thereon.
    Further, user’s interactions with organizations and/or individuals found on or through the service, including payment and delivery of goods and services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between the user and such organization and/or individual. The user should make whatever investigation they feel necessary or appropriate before proceeding with any offline or online transaction with any of these third parties.
</p>



                        </div>
                    </div>

                </div>

                
            </div>

        </div>
    </div>
</section>

<!-- <!-- <div class="footer-social">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                                        <a href="../index.html" class="logo">
                            <img src="../assets/uploads/2017/07/logo-1.png" title="" alt=""
                                 width="488"
                                 height="488">
                        </a>
                                </div>
            <div class="col-md-6">
                <ul class="social-links list-inline">
                    <li><a href="https://www.facebook.com/sambole.lk/" target="_blank"><i class="fa fa-facebook"></i></a></li> -->
                    <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                <!-- </ul>
            </div>
        </div>
    </div>
</div> -->

@stop
<!-- JS FOR THIS PAGE -->
@section('js')

@stop
