@extends('layouts.front.master')

<!-- CSS FOR THIS PAGE -->
@section('css')
<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="admin/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<!-- Color Picker -->
<link href="{{asset('assets/front/css/colorpicker/bootstrap-colorpicker.css')}}" rel="stylesheet">
<script src="{{asset('assets/front/js/colorpicker/jquery-2.2.2.min.js')}}"></script>
<script src="{{asset('assets/front/js/colorpicker/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/front/js/colorpicker/bootstrap-colorpicker.js')}}"></script>
<style type="text/css">
    .martop30 {
    margin-top:30px;
    }
    #para
    {
    text-align:justify;
    }
    #table-data
    {
    width:95%;
    padding-left: 5px;
    }
    .img-valign
    {
    vertical-align: middle;
    horizontal-align:middle;
    margin-bottom: 1.5em;
    margin-right: 1.5em;
    width:30px;
    height:30px; 
    }
    #company-reg
    {
    border: 2px solid Maroon;
    border-radius: 4px;
    }
    #dropdown-industry
    {
    padding-bottom:20px;
    }
    .submit
    {
    border-radius: 5px;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 10px 2px ;
    cursor: pointer;
    font-weight: bold;
    padding: 10px 25px;
    background-color: #712b2f;
    }
    .submit:hover
    {
    background-color: #D2691E;
    }
    .check {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }
    .advanceC{
    position: relative;
    vertical-align: top;
    padding-top: 5px;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }
    .check input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    }
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #eee;
    }
    .check input:checked ~ .checkmark {
    background-color: #4CAF50;
    }
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }
    .check input:checked ~ .checkmark:after {
    display: block;
    }
    .check .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    }
    .btn{
    text-align: center;
    background-color: #712b2f;
    }
    .btn:hover{
    transition: all .5s;
    background-color: #D2691E;
    }
    .dropzone, .colorpicker-component{
    margin-bottom: 20px;
    }
    .container .jumbotron{
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 10px;
    background-color: #ffec91;
    }
    .jumbotron p{
    padding: 2px;
    font-size: 16px;
    text-align: center;
    margin-left: 0!important;
    margin-right: 0!important;
    }
    p.social{
    margin-top: -20px;
    font-size: 16px;
    text-align: center;
    }
    p.tel{
    margin-top: 10px;
    font-size: 16px;
    }
    .social a{
    text-decoration: none!important;
    }
    p a {
    border-bottom: none;
    text-decoration: none!important;
    }
    p a:hover {
    border-bottom: none;
    text-decoration: none!important;
    }
    a{
    color: #fff;
    }
    .keywords{
    list-style:none;
    text-align: center;
    margin: 0 auto;
    padding: 0px;
    display:table;
    overflow: hidden;
    color: #ffffff;
    }
    .filter-button span{
    color: #ffffff;
    font-size: 10px;
    }
    .keywords li{
    vertical-align: bottom;
    float: left;
    padding: 8px 8px 8px 8px;
    margin-left: 3px;
    margin-right: 3px;
    color: #ffffff;
    background-color: #712b2f;
    font-size: 13px;
    border-radius: 5px;
    }
    .keywords li:hover{
    vertical-align: bottom;
    float: left;
    padding: 8px 8px 8px 8px;
    margin-left: 3px;
    margin-right: 3px;
    color: #ffffff;
    background-color: #D2691E;
    font-size: 13px;
    border-radius: 5px;
    cursor: pointer;
    border: no;
    -webkit-transition: all 0.4s ease-in-out;
    }
    .keywords li:hover a{
    color : #fff!important;
    -webkit-transition: all 0.3s ease-in-out;
    }
    .imgbg{
    padding: 10px 10px 20px 10px;
    background-color: #eee;
    margin-top: 10px;
    margin-left: 2px;
    margin-right: 2px;
    border-radius: 5px;
    }
    .imgbg label{
    text-align: center!important;
    margin-top: 10px;
    margin-left: 35px;
    font-weight: 400;
    }
    .col-md-4.imgbg{
    width: 175px;
    }
</style>
<style type="text/css">
    /********************************/
    /*          Main CSS     */
    /********************************/
    #first-slider .main-container {
    padding: 0;
    }
    #first-slider .slide1 h3, #first-slider .slide2 h3, #first-slider .slide3 h3, #first-slider .slide4 h3{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
    }
    #first-slider .slide1 h4,#first-slider .slide2 h4,#first-slider .slide3 h4,#first-slider .slide4 h4{
    color: #fff;
    font-size: 30px;
    text-transform: uppercase;
    font-weight:700;
    }
    #first-slider .slide1 .text-left ,#first-slider .slide3 .text-left{
    padding-left: 40px;
    }
    #first-slider .carousel-indicators {
    bottom: 0;
    }
    #first-slider .carousel-control.right,
    #first-slider .carousel-control.left {
    background-image: none;
    }
    #first-slider .carousel .item {
    min-height: 225px; 
    height: 100%;
    width:100%;
    }
    .carousel-inner .item .container {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 0;
    top: 0;
    left: 0;
    right: 0;
    }
    #first-slider h3{
    animation-delay: 1s;
    }
    #first-slider h4 {
    animation-delay: 2s;
    }
    #first-slider h2 {
    animation-delay: 3s;
    }
    #first-slider .carousel-control {
    width: 6%;
    text-shadow: none;
    }
    #first-slider h1 {
    text-align: center;  
    margin-bottom: 30px;
    font-size: 30px;
    font-weight: bold;
    }
    #first-slider .p {
    padding-top: 125px;
    text-align: center;
    }
    #first-slider .p a {
    text-decoration: underline;
    }
    #first-slider .carousel-indicators li {
    width: 14px;
    height: 14px;
    background-color: rgba(255,255,255,.4);
    border:none;
    }
    #first-slider .carousel-indicators .active{
    width: 16px;
    height: 16px;
    background-color: #fff;
    border:none;
    }
    .carousel-fade .carousel-inner .item {
    -webkit-transition-property: opacity;
    transition-property: opacity;
    }
    .carousel-fade .carousel-inner .item,
    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
    opacity: 0;
    }
    .carousel-fade .carousel-inner .active,
    .carousel-fade .carousel-inner .next.left,
    .carousel-fade .carousel-inner .prev.right {
    opacity: 1;
    }
    .carousel-fade .carousel-inner .next,
    .carousel-fade .carousel-inner .prev,
    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
    left: 0;
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    }
    .carousel-fade .carousel-control {
    z-index: 2;
    }
    .carousel-control .fa-angle-right, .carousel-control .fa-angle-left {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
    }
    .carousel-control .fa-angle-left{
    left: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
    }
    .carousel-control .fa-angle-right{
    right: 50%;
    width: 38px;
    height: 38px;
    margin-top: -15px;
    font-size: 30px;
    color: #fff;
    border: 3px solid #ffffff;
    -webkit-border-radius: 23px;
    -moz-border-radius: 23px;
    border-radius: 53px;
    }
    .carousel-control {
    opacity: 1;
    filter: alpha(opacity=100);
    }
    /********************************/
    /*       Slides backgrounds     */
    /********************************/
    #first-slider .slide1 {
    background-size: cover;
    background-repeat: no-repeat;
    }
    #first-slider .slide2 {
    background-size: cover;
    background-repeat: no-repeat;
    }
    #first-slider .slide3 {
    background-size: cover;
    background-repeat: no-repeat;
    }
    #first-slider .slide4 {
    background-size: cover;
    background-repeat: no-repeat;
    }
</style>
@stop


<!-- BODY -->

@section('content')
<section class="martop30">
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="white-block" id="company-reg">
                <div class="white-block-content">
                    <div class="page-content clearfix">
                        <div class="ajax-response"></div>
                        <form class="form-register">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="pull-left" src="{{asset('assets/front/images/logo.jpg')}}" alt="logo" />
                                </div>
                                <div class="col-md-12" style="margin-top: 20px; margin-bottom: 20px;">
                                    <div id="first-slider">
                                        <div id="carousel-example-generic" class="carousel slide carousel-fade">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                            </ol>
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                <!-- Item 1 -->
                                                <div class="item active slide1" style="background-image: url(assets/front/images/slider/01.jpg);">
                                                </div>
                                                <!-- Item 2 -->
                                                <div class="item slide2" style="background-image: url(assets/front/images/slider/02.jpg);">
                                                </div>
                                                <!-- Item 3 -->
                                                <div class="item slide3" style="background-image: url(assets/front/images/slider/03.jpg);">
                                                </div>
                                                <!-- Item 4 -->
                                                <div class="item slide4" style="background-image: url(assets/front/images/slider/04.jpg);">
                                                </div>
                                                <!-- End Item 4 -->
                                            </div>
                                            <!-- End Wrapper for slides-->
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="jumbotron">
                                                <p><strong>Company Name (pvt) Ltd.</strong></p>
                                                <p class="social text-center">
                                                    <a href=""><i class="fa fa-facebook"></i></a>
                                                    <a href=""><i class="fa fa-twitter"></i></a>
                                                    <a href=""><i class="fa fa-share"></i></a>
                                                    <a href=""><i class="fa fa-gplus"></i></a>
                                                    <a href=""><i class="fa fa-skype"></i></a>
                                                </p>
                                                <p class="tel">+94 000 000 000</p>
                                                <p class="address">#222/A, <br/> ABC Road, <br/> Colombo 10.</p>
                                            </div>
                                            <div>
                                                <h4>About Company Name (pvt) Ltd.</h4>
                                                <p class="text-justify" style="font-size: 13px; line-height: 23px;">MirrorMirror.lk was launched in August 2013 which revolutionized the Fashion landscape in Sri Lanka by introducing Online Fashion Retailing. MirrorMirror.lk brings the most fashionable clothing accessories and footwear from around the world right to your fingertips without limits and boundaries. MirrorMirror.lk is currently Sri Lanka’s largest online fashion store with over 5000 products and we are committed to delivering the most personalized fashion experience right to your favorite device. We continuously strive to ensure the highest degree of customer satisfaction by providing our customers exclusive yet affordable clothing, accessories & lifestyle products at convenience whilst ensuring accessibility to all consumers across the island.</p>

                                                <p>MirrorMirror.lk aims to give you everything that you need to make you look & feel good. Let us be your new online shopping addiction!</p>

                                                <p>Open hours:
                                                24 hours</p>
                                            </div>
                                        </div>
                                        <div class="col-md-8 bg">
                                            
                                        <div class="row">
                                        <ul class="keywords keywords-hover-new">
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Category 1<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Category 2<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Category 3<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i> --> Category 4<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i>--> Category 5<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        <li>
                                        <a class="filter-button"><!-- <i class="fa fa-tags" aria-hidden="true"></i>--> Category 6<br/><!--  <span>Test category</span> --></a>
                                        </li>
                                        </ul>
                                        </div>
                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px; width: 100%;">
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px; width: 100%;">
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px; margin-bottom: 10px; width: 100%;">
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        <div class="col-md-4 imgbg">
                                        <img src="{{asset('assets/front/images/uploads/01.jpg')}}"/>
                                        <label>Product Name</label>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
<script type="text/javascript">
    (function( $ ) {
    
    //Function to animate slider captions 
    function doAnimations( elems ) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        
        elems.each(function () {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }
    
    //Variables on page load 
    var $myCarousel = $('#carousel-example-generic'),
        $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
        
    //Initialize carousel 
    $myCarousel.carousel();
    
    //Animate captions in first slide on page load 
    doAnimations($firstAnimatingElems);
    
    //Pause carousel  
    $myCarousel.carousel('pause');
    
    
    //Other slides to be animated on carousel slide event 
    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });  
    $('#carousel-example-generic').carousel({
        interval:3000,
        pause: "false"
    });
    
    })(jQuery); 
    
</script>
<!-- DROPZONE -->
<script src="js/plugins/dropzone/dropzone.js"></script>
<!-- Jasny -->
<script src="admin/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script type="text/javascript">
    $('#cp2').colorpicker();
    $('#cp3').colorpicker();
</script>
<script>
    Dropzone.options.dropzoneForm = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 5, // MB
        dictDefaultMessage: "<strong>Drop slider images here or click to upload. </strong></br> (Maximum file size is 5MB and 3 files are allowed)"
    };
    
    $(document).ready(function(){
    
        var editor_one = CodeMirror.fromTextArea(document.getElementById("code1"), {
            lineNumbers: true,
            matchBrackets: true
        });
    
        var editor_two = CodeMirror.fromTextArea(document.getElementById("code2"), {
            lineNumbers: true,
            matchBrackets: true
        });
    
        var editor_two = CodeMirror.fromTextArea(document.getElementById("code3"), {
            lineNumbers: true,
            matchBrackets: true
        });
    
    });
</script>
@stop
