@extends('layouts.front.master')
<!-- CSS FOR THIS PAGE -->
@section('css')
<!-- style -->
<link href="{{asset('assets/front/css/select2.min.css')}}" rel="stylesheet">

<link href="{{asset('assets/front//css/datepicker/cus_bootstrap.min.css')}}" rel="stylesheet">
<style type="text/css">
.bootstrap-datetimepicker-widget a[data-action]{
    background:none!important;
    display: inline-block!important;
    width: 54px;
    height: 54px;
    color: #337ab7;
}
.alert{
    margin-left: 15px !important;
}
.martop30 {
    margin-top:30px;
}
#para
{
    text-align:justify;
}
#table-data
{
    width:95%;
    padding-left: 5px;
}
.img-valign
{
    vertical-align: middle;
    horizontal-align:middle;
    margin-bottom: 0em;
    margin-right: 1.5em;
    width:30px;
    height:30px;
}
#company-reg
{
    /*border: 2px solid Maroon;*/
    border-radius: 4px;
}
#dropdown-industry
{
    padding-bottom:20px;
}
a.submit{
    width: 225px;
}
.submit, a.submit
{
    border-radius: 5px;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 10px 2px ;
    cursor: pointer;
    font-weight: bold;
    padding: 10px 25px;
    background-color: #712b2f;
}
.submit:hover, a.submit:hover
{
    background-color: #D2691E;
}
.check {
    display: block;
    position: relative;
    padding-left: 30px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 13px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.check input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
.checkmark{
 /* margin-left: 15px; */
 position: absolute;
 top: 0;
 left: 0;
 height: 20px;
 width: 20px;
 background-color: #eee;
}
.check input:checked ~ .checkmark {
    background-color: #4CAF50;
}
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}
.check input:checked ~ .checkmark:after {
    display: block;
}
.check .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
.btn-right{
    float: right!important;
    background: none!important;
    border: none!important;
    margin-top: 5px!important;
    padding-right: 0px;
    font-weight: 600;
    /*color: #bdbdbd;*/
    color: blue;
    text-decoration: underline;
}
.btn-right:hover{
    float: right!important;
    background: none!important;
    border: none!important;
    margin-top: 5px!important;
    padding-right: 0px;
    font-weight: 600;
    color: blue;
    text-decoration: none;
}
@media (min-width: 320px) and (min-width: 359px){
    .img-valign {
        /*height: 20px;*/
    }
    .form-register p {
        font-size: 11px;
    }
    .check {
        font-size: 13px!important;
        padding-left: 20px!important;
    }
    .checkmark {
        margin-left: 0px!important;
        left: -8px !important;
    }
    .submit, a.submit {
        font-size: 12px;
        padding: 5px 15px;
    }
    .form-control {
        font-size: 11px;
    }
    .topic {
        font-size: 14px;
    }
    .para {
        font-size: 10px!important;
    }
    .submit, a.submit {
        font-size: 11px;
        padding: 9px 20px;
    }
    .btn-right{
        margin-right: 0px !important;
        text-align: right;
    }
    .btn-right:hover{
      margin-right: 0px!important;
      text-align: right;
  }
}
@media (min-width: 360px) and (max-width: 767px) {
    .topic {
        font-size: 16px;
    }
    .form-control {
        font-size: 15px;
    }
    .select {
        font-size: 12px;
    }
    .form-register p {
        font-size: 15px;
    }
    .check {
        font-size: 13px;
        padding-left: 20px;
    }
    .checkmark {
        left: -20px;
    }
    .submit, a.submit {
        font-size: 14px;
        padding: 9px 20px;
    }
}
@media (min-width: 768px) and (max-width: 979px) {
    .topic {
        font-size: 16px;
    }
    .form-control {
        font-size: 16px;
    }
    .select {
        font-size: 16px;
    }
    .form-register p {
        font-size: 16px;
    }
    .check {
        font-size: 14px;
        padding-left:8px;
    }
    .checkmark {
        left: -20px;
    }
    .submit, a.submit {
        font-size: 16px;
        padding: 9px 20px;
    }
}
@media (min-width: 980px) and (max-width: 1279px) {
    .topic {
        font-size: 16px;
    }
    .form-control {
        font-size: 18px;
    }
    .select {
        font-size: 18px;
    }
    .form-register p {
        font-size: 18px;
    }
    .check {
        font-size: 13px;
        padding-left: 20px;
    }
    .checkmark {
        left: -20px;
    }
    .submit, a.submit {
        font-size: 18px;
        padding: 9px 20px;
    }
    .para {
        font-size: 18px;
    }
}
.form-register input, .form-register textarea
{
    margin-bottom: 0;
}
input["select"] .option-css
{
    min-height: 35px;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
@stop
<!-- BODY -->
@section('content')
<section class="martop30">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="white-block" id="company-reg">

                    <ul class="breadcrumb">
                        <li>
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li>
                            Business Register
                        </li>
                    </ul>

                    <h1>Business Register</h1>

                    <div class="white-block-content">
                        <div class="page-content clearfix">                            
                            <!-- <h3>Sign up as a member to list your Products &amp; Services with a Sambole Business Member Page!</h3> -->
                            <p class="lead">Register as a member and get a dedicated web space for your shop on sambole.lk. A member page enables you to maintain and view your listings in one place.</p>
                            <div class="ajax-response"></div>
                            <form class="form-register" method="POST" action="{{url('/businessReg')}}" id="businessRegForm">
                                {!!Form::token()!!}
                                <div class="row">
                                    @if ($business)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Business Name</label>
                                            <input type="text" name="business_name" id="business_name" placeholder="Business Name" class="form-control" value="{{$business->business_name}}" />
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Business Name</label>
                                            <input type="text" name="business_name" id="business_name" placeholder="Business Name" class="form-control" value="{{Input::old('business_name')}}" />
                                        </div>
                                    </div>
                                    @endif
                                    
                                    @if ($business)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">About Business</label>
                                            <textarea type="text" name="about" id="about" placeholder="About Business" class="form-control" >{{$business->about}}</textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">About Business</label>
                                            <textarea type="text" name="about" id="about" placeholder="About Business" class="form-control">{{Input::old('about')}}</textarea>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($business)

                                        <div class="col-md-12">
                                            <label class="control-label">Opening Hours</label>
                                            <div class="row">                                             
                                               <div class="col-sm-6">
                                                    <div class="form-group"> 
                                                      <input name="open"
                                                      type="text"
                                                      class="form-control datepicker"
                                                      autocomplete="off" 
                                                      value="{{$business->open or ""}}">
                                                      <p class="help-block errors" id="business_open_error"></p>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                      <input name="close"
                                                      type="text"
                                                      class="form-control datepicker"
                                                      autocomplete="off" 
                                                      value="{{$business->close or ""}}">
                                                      <p class="help-block errors" id="business_close_error"></p>
                                                   </div>
                                                </div>
                                            </div> 
                                      </div>
                                 
                              @endif

                              @if ($business)
                              <div class="col-md-12" id="dropdown-industry">
                                <div class="form-group">
                                    <label>Industry</label>
                                    <select id="industry" class="form-control select-simple" name="industry">
                                        <option hidden >Select Industry</option>
                                        @foreach ($industries as $industry)
                                        <option value="{{$industry->id}}" class="select option-css" @if ($business->industry_id == $industry->id) selected="selected" @endif>{{$industry->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @else
                            <div class="col-md-12" id="dropdown-industry">
                                <div class="form-group">
                                    <label>Industry</label>
                                    <select id="industry" class="form-control select-simple" name="industry">
                                        <option hidden value="">Select Industry</option>
                                        @foreach ($industries as $industry)
                                        <option value="{{$industry->id}}" class="select option-css" >{{$industry->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            @if ($business)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Company Address</label>
                                    <textarea name="company_address" id="company_address" class="form-control" rows="4" placeholder="Company Address">{{$business->company_address}}</textarea>
                                </div>
                            </div>
                            @else
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Company Address</label>
                                    <textarea name="company_address" id="company_address" class="form-control" rows="4" placeholder="Company Address">{{Input::old('company_address')}}</textarea>
                                </div>
                            </div>
                            @endif
                            @if ($business)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Contact Number</label>
                                    <input type="text"  name="contactNo" id="contactNo" placeholder="Contact Number (94xxxxxxxxx or 07xxxxxxxx)" class="form-control bfh-phone" value="{{$business->contact_number}}" />
                                </div>
                            </div>
                            @else
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Contact Number</label>
                                    <input type="text"  name="contactNo" id="contactNo" placeholder="Contact Number (94xxxxxxxxx or 07xxxxxxxx)" class="form-control bfh-phone" value="{{ Input::old('contactNo') }}" />
                                </div>
                            </div>
                            @endif
                            @if ($business)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Profile Url</label>
                                    <div class="row">
                                        <div class="col-md-3" style="padding-top: 6px;">
                                            www.sambole.lk/
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" name="business_page_name" id="business_page_name" placeholder="Profile Url" class="form-control" value="{{$business->business_page_name}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Profile Url</label>
                                    <div class="row">
                                        <div class="col-md-3" style="padding-top: 6px;">
                                            www.sambole.lk/
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" name="business_page_name" id="business_page_name" placeholder="Profile Url" class="form-control" value="{{Input::old('business_page_name')}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- <div class="col-md-12">

                                <label class="control-label">Opening Hours</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group date" id="from">
                                                <input type="text" class="form-control" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">                                        
                                            <div class="input-group date" id="to">
                                                <input type="text" class="form-control" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-clock-o"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div> -->
                                   <!--  @if ($business)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nominal Capital</label>
                                            <input type="text" name="nominal_capital" id="nominal_capital" placeholder="Nominal Capital" autocomplete="off" class="form-control" value="{{$business->nominal_capital}}"/>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Nominal Capital</label>
                                            <input type="text" name="nominal_capital" id="nominal_capital" placeholder="Nominal Capital" autocomplete="off" class="form-control" value="{{Input::old('nominal_capital')}}"/>
                                        </div>
                                    </div>
                                    @endif -->
                                    @if ($business)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Website</label>
                                            <input type="text" name="website" id="website" placeholder="Website" class="form-control" value="{{$business->website}}"/>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Website</label>
                                            <input type="text" name="website" id="website" placeholder="Website" class="form-control" value="{{Input::old('website')}}"/>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Facebook URL</label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img class="img-valign" src="{{asset('assets/front/images/fb.png')}}"/>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td id="table-data">
                                                        <span>
                                                            @if ($business)
                                                            <input type="text" name="facebook" id="facebook" placeholder="Enter Facebook URL" class="form-control" value="{{$business->facebook_url}}"/>
                                                            @else
                                                            <input type="text" name="facebook" id="facebook" placeholder="Enter Facebook URL" class="form-control" value="{{Input::old('facebook')}}"/>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Google plus URL</label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img class="img-valign" src="{{asset('assets/front/images/google.png')}}" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td id="table-data">
                                                        <span>
                                                            @if ($business)
                                                            <input type="text" name="googleplus" id="googleplus" placeholder="Enter Google plus URL" class="form-control" value="{{$business->googleplus_url}}"/>
                                                            @else
                                                            <input type="text" name="googleplus" id="googleplus" placeholder="Enter Google plus URL" class="form-control" value="{{Input::old('googleplus')}}"/>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Instagram URL</label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img class="img-valign" src="{{asset('assets/front/images/instagram.png')}}" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td id="table-data">
                                                        <span>
                                                            @if ($business)
                                                            <input type="text" name="instagram_url" id="instagram_url" placeholder="Enter Instagram URL" class="form-control" value="{{$business->instagram_url}}"/>
                                                            @else
                                                            <input type="text" name="instagram_url" id="instagram_url" placeholder="Enter Instagram URL" class="form-control" value="{{Input::old('instagram_url')}}"/>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Twitter URL</label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img class="img-valign" src="{{asset('assets/front/images/twitter.png')}}" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td id="table-data">
                                                        <span>
                                                            @if ($business)
                                                            <input type="text" name="twitter_url" id="twitter_url" placeholder="Enter Twitter URL" class="form-control" value="{{$business->twitter_url}}"/>
                                                            @else
                                                            <input type="text" name="twitter_url" id="twitter_url" placeholder="Enter Twitter URL" class="form-control" value="{{Input::old('twitter_url')}}"/>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">LinkedIn URL</label>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img class="img-valign" src="{{asset('assets/front/images/linkedIn.png')}}" />
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td id="table-data">
                                                        <span>
                                                            @if ($business)
                                                            <input type="text" name="linkedin_url" id="linkedin_url" placeholder="Enter LinkedIn URL" class="form-control" value="{{$business->linkedin_url}}"/>
                                                            @else
                                                            <input type="text" name="linkedin_url" id="linkedin_url" placeholder="Enter LinkedIn URL" class="form-control" value="{{Input::old('linkedin_url')}}"/>
                                                            @endif
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    @if ($business)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Electronic Signature</label>
                                            <textarea name="electronic_signature" id="electronic_signature" class="form-control" rows="4" placeholder="Electronic Signature">{{$business->electronic_signature}}</textarea>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="electronic_signature" id="electronic_signature" class="form-control" rows="4" placeholder="Electronic Signature"></textarea>
                                        </div>
                                    </div>
                                    @endif
                                    <p class="col-md-12 para" id="para">You agree to keep the full responsibility of the provided information and supply with any additional documents upon request. </p>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="check control-label">
                                            @if ($business)
                                            <input type="checkbox" name="agreement" checked="checked" class="form-control"> Agree to the Terms and Conditions
                                            @else
                                            <input type="checkbox" name="agreement" class="form-control"> Agree to the Terms and Conditions
                                            @endif
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <!-- <a class="submit" href="#">
                                            REGISTER COMPANY
                                        </a> -->
                                        <button class="btn button-submit btn-block col-md-12 pull-left" type="submit">
                                            @if ($business)
                                            UPDATE COMPANY DETAILS
                                            @else
                                            REGISTER COMPANY
                                            @endif
                                        </button>
                                    </div>
                                    <div class="col-md-8 col-md-offset-4">
                                        <input type="reset" id="login_reset" class="btn button-reset col-md-8 btn-right pull-right" value="reset">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
{{-- <script src="http://localhost/sambole-web-portal/assets/front/js/datepicker/bootstrap-datepicker.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{asset('assets/front/js/select2/select2.full.min.js')}}"></script>
<!-- jQuery Bootstrap Form Validation -->
<script src="{{asset('assets/back/jquery-validation/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function($) {
        $('.datepicker').datetimepicker({
          format: 'LT'
      });
    });
    // $.validator.addMethod('customphone', function (value, element) {
    //     return this.optional(element) || /^\d{3}-\d{3}-\d{4}$/.test(value);
    // }, "Please enter a valid phone number");
    jQuery(document).ready(function ($) {
        "use strict";

        $(".select-simple").select2();

        jQuery("#businessRegForm").bootstrapValidator({
            fields: {
                business_name: {
                    validators: {
                        notEmpty: {
                            message: "Please enter the Business Name"
                        },
                            // stringLength: {
                            //     min: 5,
                            //     message: "The Business Name should be minimun 5 characters long"
                            // },
                            stringLength: {

                                min: 5,
                                max: 70,
                                message: "The Business Name should be between minimun 5 characters and maximum 70 characters"

                            }
                        }
                    },
                    industry: {
                        validators: {
                            notEmpty: {
                                message: "Please select an Industry"
                            }
                        }
                    },
                    company_address: {
                        validators: {
                            notEmpty: {
                                message: "Please enter the Company Address"
                            }
                        }
                    },
                    contactNo: {
                        // enabled: flase,
                        validators: {
                            notEmpty: {
                                message: "Please enter a Contact Number"
                            },

                            
                            stringLength: {
                                max: 17,
                                min: 10,
                                message: "The Contact Number should be 10 digits long<br/>Ex: 0712345678 or +94712345678"
                            },

                            regexp:{
                             regexp: "^(94)[0-9]{9}$|^(07)[0-9]{8}$",

                             message: 'Contact Number syntax sholud be 94xxxxxxxxx or 07xxxxxxxx.'
                         }

                     }
                 },
                 nominal_capital: {
                    validators: {
                        notEmpty: {
                            message: "Please enter Nominal Capital"
                        }
                    }
                },
                website: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Please enter a valid URL."
                        },
                    }
                },
                facebook: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Please enter a valid URL."
                        },
                    }
                },
                googleplus: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Please enter a valid URL."
                        },
                    }
                },
                twitter_url: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Please enter a valid URL."
                        },
                    }
                },
                instagram_url: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Not a valid url"
                        },
                    }
                },
                linkedin_url: {
                    validators: {
                        regexp:{
                            regexp: "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$",
                            message: "Please enter a valid URL."
                        },
                    }
                },
                electronic_signature: {
                    validators: {
                        notEmpty: {
                            message: "Please enter the Electronic Signature"
                        }
                    }
                },
                agreement: {
                    validators: {
                        notEmpty: {
                            message: "Please agree to the Terms and Conditions"
                        }
                    }
                },
                business_page_name: {
                    validators: {
                        remote: {
                            url: '{{url('/checkPageName')}}',
                            data: function(validator) {
                                return {
                                    business_page_name: validator.getFieldElements('business_page_name').val(),
                                    _token: '{{csrf_token()}}'
                                }
                            },
                            message: 'The page Url is not available'
                        },
                        stringLength: {
                            max: 70,
                            message: "The Page Url should be maximum 70 characters long"
                        }
                    }
                }
            }

        })

.on('success.form.bv', function(e) {
                  jQuery('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                  jQuery('#businessRegForm').data('bootstrapValidator').resetForm();

                  // Prevent form submission
                  e.preventDefault();

                  // Get the form instance
                  var $form = jQuery(e.target);

                  // Get the BootstrapValidator instance
                  var bv = $form.data('bootstrapValidator');
                  // Use Ajax to submit form data
                  jQuery.post($form.attr('action'), $form.serialize(), function(result) {
                      console.log(result);
                  }, 'json');
              });

});

</script>
<script type="text/javascript">
    $(function () {
        $('#from').datetimepicker({
            format: 'LT',
            defaultDate: "11/1/2013 00:00",
        });
        $('#to').datetimepicker({
            format: 'LT',
            defaultDate: "11/1/2013 23:59",

        });
    });
</script>
@stop
