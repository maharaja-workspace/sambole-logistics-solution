@extends('layouts.front.master2')

<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
@media screen and (max-width: 320px) {
    .home-welcome {
        height: 15rem;
        width: 100%;
        background-size: 150%;
    }
    .navigation .navbar-default .navbar-brand img {
        margin-left: -10px !important;
    }
}

@media screen and (min-width: 321px) and (max-width: 360px) {
    .home-welcome {
        height: 17rem;
        width: 100%;
        background-size: 170%;
    }
}

@media screen and (min-width: 361px) and (max-width: 768px) {

}

@media screen and (min-width: 769px) and (max-width: 800px) {

}

@media screen and (min-width: 801px) and (max-width: 980px) {

}

@media screen and (min-width: 981px) and (max-width: 1280px) {

}

@media screen and (min-width: 1281px) {

}
</style>
@stop


<!-- BODY -->

@section('content')

         <section class="home-welcome"></section>
        <section class="search-bar clearfix">
            <div class="container">
                <form method="get" class="search-form " action="{{url('ad/search')}}">
                    <input type="hidden" class="view" name="view" value="grid">
                    <input type="hidden" class="sortby" name="sortby" value="">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <input type="text" class="form-control keyword" name="keyword" value="" placeholder="Find by keyword or short code">
                            <i class="fa fa-search"></i>
                        </li>
                        <li>
                            <select class="form-control select-simple location" name="location" data-placeholder="Location">
                                <option value="">&nbsp;</option>
                                @foreach ($location_select_options as $option)
                                    @if ($option->district_id)
                                        <option value="{{$option->id}}" class="subitem">&nbsp;&nbsp; {{$option->name}}</option>
                                    @else
                                        <option value="{{$option->id}}"> {{$option->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <i class="fa fa-bars"></i>
                        </li>
                        <li>
                            <select class="form-control select-simple category" name="category" data-placeholder="Category">
                                <option value="">&nbsp;</option>
                                <?php echo CategoryManage\Models\Category::getCategoryDropDown(Input::old('ad_category_id')) ?>
                                {{-- @foreach ($category_select_options as $option)
                                    @if ($option->main_category_id)
                                        <option value="{{$option->id}}" class="subitem">&nbsp;&nbsp; {{$option->name}}</option>
                                    @else
                                        <option value="{{$option->id}}"> {{$option->name}}</option>
                                    @endif
                                @endforeach --}}
                            </select>
                            <i class="fa fa-bars"></i>
                        </li>
                        <!-- <li>
                            <select class="form-control select2 radius" name="radius" data-placeholder="Radius (300km)">
                                <option value="">&nbsp;</option>
                                                        </select>
                            <i class="fa fa-bullseye"></i>
                            </li> -->
                        <li>
                            <a href="javascript:;" class="btn submit-form">
                            Search                        </a>
                        </li>
                    </ul>
                    <div class="filters-holder hidden"></div>
                </form>
            </div>
        </section>
        <section class="section RDErUScruz">
            <style scoped>
                .RDErUScruz{
                padding: 60px 0px;
                }
            </style>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        </p>
                        <h3 style="text-align: center;">Browse by category</h3>
                        <p>
                    </div>
                </div>
                </p>
                <p>
                <div class="row">
                    <!--   <div class="col-sm-6 col-md-2">
                        <a href="http://www.sambole.lk/mart">
                            <div class="category-item">
                                <div clas="clearfix">                                   
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/gift-box-icon.jpg')}}" class="attachment-full size-full" alt="" />                                    
                                    <div class="category-item-content">
                                        <h4>                                           
                                            Seasonal Offers                                            
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=39')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=35')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/6.png')}}" class="attachment-full size-full" alt="" />
                                   <!--  </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=35')}}"> -->
                                            Fashion, Health &amp; Beauty
                                           <!--  </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=2')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=21')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/1.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=21')}}"> -->
                                            Electronics
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=31')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=59')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/4.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=59')}}"> -->
                                            Home, Kitchen &amp; Garden
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=16')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=68')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/3.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=68')}}"> -->
                                            Motor Vehicles
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=26')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=78')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/7.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=78')}}"> -->
                                            Property
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=65')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=83')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/05/marketer.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=83')}}"> -->
                                            Services
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=49')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=50')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/05/ball.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=50')}}"> -->
                                            Hobby, Sports &amp; Kids
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=58')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=8')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/8.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=8')}}"> -->
                                            Business &amp; Industry
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=72')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=15')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/9.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=15')}}"> -->
                                            Education
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=85')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=45')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/11.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=45')}}"> -->
                                            Food &amp; Agriculture
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=78')}}">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=1')}}"> -->
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/10.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=1')}}"> -->
                                            Animals
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                  <div class="col-sm-6 col-md-2">
                        <a href="http://www.sambole.lk/mart">
                            <div class="category-item">
                                <div clas="clearfix">
                                    <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=89')}}">
                                    <img width="84" height="83" src="{{asset('assets/front/uploads/2017/03/12.png')}}" class="attachment-full size-full" alt="" />
                                    <!-- </a> -->
                                    <div class="category-item-content">
                                        <h4>
                                            <!-- <a href="{{url('ad/search/?view=grid&sortby=&keyword=&location=&category=67')}}"> -->
                                            Jobs & Work Overseas
                                            <!-- </a> -->
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                </p>
                <p>
            </div>
        </section>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108362078-1"></script>

<script type="text/javascript">
/*Analatic*/
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
             
            gtag('config', 'UA-108362078-1');
            
</script>
<!-- Hotjar Tracking Code for https://www.sambole.lk/ -->

<script>

    (function(h,o,t,j,a,r){

        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};

        h._hjSettings={hjid:1209633,hjsv:6};

        a=o.getElementsByTagName('head')[0];

        r=o.createElement('script');r.async=1;

        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;

        a.appendChild(r);

    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

</script>


@stop
