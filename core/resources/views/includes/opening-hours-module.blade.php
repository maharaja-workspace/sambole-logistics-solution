<div class="{{isset($openHourDisbale) ? 'openHousHoursDisabled' : ''}}">
<div class="radio">
  <label><input type="radio" {{(!isset($openHourDisbale)) ? 'name=opening-hours-format' : ''}} class="opening-hours-format" value="selected" {{ ( isset($openingData) && $openingData->format == 'selected') ? 'checked' : ''}}>Open on selected hours</label>
</div>
<div class="radio">
  <label><input type="radio" {{(!isset($openHourDisbale)) ? 'name=opening-hours-format' : ''}} class="opening-hours-format" value="always" {{(!isset($openingData) ||(isset($openingData) && $openingData->format == 'always')) ? 'checked' : ''}}>Always open</label>
</div>
<div class="radio">
  <label><input type="radio" {{(!isset($openHourDisbale)) ? 'name=opening-hours-format' : ''}} class="opening-hours-format" value="unavailable" {{(isset($openingData) && $openingData->format == 'unavailable') ? 'checked' : ''}}>No hours available</label>
</div>
<div class="radio">
  <label><input type="radio" {{(!isset($openHourDisbale)) ? 'name=opening-hours-format' : ''}} class="opening-hours-format" value="p-closed" {{(isset($openingData) && $openingData->format == 'p-closed') ? 'checked' : ''}}>Permanantly closed</label>
</div>

<div id="openHousHourSelect" class="">
<?php 
    $mondayChecked = isset($openingData) && (!empty($openingData->monday_start) || !empty($openingData->monday_end));
    $tuesdayChecked = isset($openingData) && (!empty($openingData->tuesday_start) || !empty($openingData->tuesday_end));
    $wednesdayChecked = isset($openingData) && (!empty($openingData->wednesday_start) || !empty($openingData->wednesday_end));
    $thursdayChecked = isset($openingData) && (!empty($openingData->thursday_start) || !empty($openingData->thursday_end));
    $fridayChecked = isset($openingData) && (!empty($openingData->friday_start) || !empty($openingData->friday_end));
    $saturdayChecked = isset($openingData) && (!empty($openingData->saturday_start) || !empty($openingData->saturday_end));
    $sundayChecked = isset($openingData) && (!empty($openingData->sunday_start) || !empty($openingData->sunday_end));
?>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[monday]" class="opening-hours-days" id="opening-hours-monday" {{$mondayChecked ? 'checked' : '' }}> Monday</span>
    <input type="text" id="opening-hours-monday-start" name="opening-hours[monday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$mondayChecked ? $openingData->monday_start : ''}}" {{$mondayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-monday-end" name="opening-hours[monday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$mondayChecked ? $openingData->monday_end : ''}}" {{$mondayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[tuesday]" class="opening-hours-days" id="opening-hours-tuesday" {{$tuesdayChecked ? 'checked' : '' }}> Tuesday</span>
    <input type="text" id="opening-hours-tuesday-start" name="opening-hours[tuesday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$tuesdayChecked ? $openingData->tuesday_start : ''}}" {{$tuesdayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-tuesday-end" name="opening-hours[tuesday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$tuesdayChecked ? $openingData->tuesday_end : ''}}" {{$tuesdayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[wednesday]" class="opening-hours-days" id="opening-hours-wednesday" {{$wednesdayChecked ? 'checked' : '' }}> Wednesday</span>
    <input type="text" id="opening-hours-wednesday-start" name="opening-hours[wednesday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$wednesdayChecked ? $openingData->wednesday_start : ''}}" {{$wednesdayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-wednesday-end" name="opening-hours[wednesday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$wednesdayChecked ? $openingData->wednesday_end : ''}}" {{$wednesdayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[thursday]" class="opening-hours-days" id="opening-hours-thursday" {{$thursdayChecked ? 'checked' : '' }}> Thursday</span>
    <input type="text" id="opening-hours-thursday-start" name="opening-hours[thursday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$thursdayChecked ? $openingData->thursday_start : ''}}" {{$thursdayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-thursday-end" name="opening-hours[thursday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$thursdayChecked ? $openingData->thursday_end : ''}}" {{$thursdayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[friday]" class="opening-hours-days" id="opening-hours-friday" {{$fridayChecked ? 'checked' : '' }}> Friday</span>
    <input type="text" id="opening-hours-friday-start" name="opening-hours[friday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$fridayChecked ? $openingData->friday_start : ''}}" {{$fridayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-friday-end" name="opening-hours[friday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$fridayChecked ? $openingData->friday_end : ''}}" {{$fridayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[saturday]" class="opening-hours-days" id="opening-hours-saturday" {{$saturdayChecked ? 'checked' : '' }}> Saturday</span>
    <input type="text" id="opening-hours-saturday-start" name="opening-hours[saturday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$saturdayChecked ? $openingData->saturday_start : ''}}" {{$saturdayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-saturday-end" name="opening-hours[saturday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$saturdayChecked ? $openingData->saturday_end : ''}}" {{$saturdayChecked ? '': 'disabled' }}>
    
</div>
<div class="input-group" >
    <span class="input-group-addon" style="width: 150px; text-align: left;"><input type="checkbox" name="opening-hours[sunday]" class="opening-hours-days" id="opening-hours-sunday" {{$sundayChecked ? 'checked' : '' }}> Sunday</span>
    <input type="text" id="opening-hours-sunday-start" name="opening-hours[sunday][start]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="Start Time" value="{{$sundayChecked ? $openingData->sunday_start : ''}}" {{$sundayChecked ? '': 'disabled' }}>
    <input type="text" id="opening-hours-sunday-end" name="opening-hours[sunday][end]" class=" input-group-addon opening-hours-input" style="width: auto;"  placeholder="End Time" value="{{$sundayChecked ? $openingData->sunday_end : ''}}" {{$sundayChecked ? '': 'disabled' }}>
    
</div>

</div>
</div>