@if ($answers && $answers->count() > 0)
<ol>
    @foreach ($answers->groupBy('category_question_id') as $quesAnswers)
    <li>
        <b>{{$quesAnswers->first()->question->question}}</b> <br>
        <ul style="margin-top : 0px; margin-bottom : 0px;">
            @foreach ($quesAnswers as $quesAnswer)
                <li style="padding : 0px;">
                    @if ($quesAnswer->answerOption)
                        {{$quesAnswer->answerOption->option}}
                    @else
                        {{$quesAnswer->answer}}
                    @endif
                </li>
            @endforeach        
        </ul>
    </li>
    @endforeach
</ol>
@endif