<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Date', 'Ads'],
            @foreach($chart_ads as $key => $chart_ad)
            [{!! $key !!}, {!! (int) $chart_ad !!}],
            @endforeach
            ]);

        var options = {
            hAxis: {
                title: 'Days',
                titleTextStyle: {
                    color: '#fff', 
                    fontSize: 16,
                    italic: false,
                    bold: true
                },
                textStyle: {
                    color: '#fff', 
                    bold: true
                },
                gridlines: {
                    color: 'transparent'
                }
            },
            vAxis: {
                title: 'Number of Ads',
                titleTextStyle: {
                    color: '#fff', 
                    fontSize: 16,
                    italic: false,
                    bold: true
                },
                textStyle: {
                    color: '#fff', 
                    bold: true
                },
                gridlines: {
                    color: '#fff'
                },
                baselineColor: {
                    color: '#fff'
                },
                minValue: 0
            },
            legend: {
                position: 'right', 
                textStyle: {
                    color: 'fff', 
                    fontSize: 16,
                    italic: false,
                    bold: true
                }
            },
            colors: ['#fff'],
            sliceVisibilityThreshold :1,
            backgroundColor: {
                fill: 'transparent'
            },
            chartArea: {
                backgroundColor: 'transparent'
            }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('columnchart_material'));

        chart.draw(data, options);
    }

    $(window).resize(function(){
        drawChart();
    });
</script>