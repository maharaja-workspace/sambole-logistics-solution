<ol class="breadcrumb dmtop">
    <li>
        Total number of
    </li>
    <li class="content_none">
        <a href="{{ url('user/list/advertiser') }}">
            Ad users <span>(<strong>
                @foreach($role_wise_count as $key => $value)
                @if($value['name'] == 'Advertiser')
                {{ $value['count'] }}
                @endif
                @endforeach
            </strong>)</span>
        </a>
    </li>
    <li>
        <a href="{{ url('user/list/merchant-user') }}">
            Merchant users <span>(<strong>
                @foreach($role_wise_count as $key => $value)
                @if($value['name'] == 'Merchant User')
                {{ $value['count'] }}
                @endif
                @endforeach
            </strong>)</span>
        </a>
    </li>
    <li>
        <a href="{{ url('user/list/system-admin') }}">
            Admin users <span>(<strong>
                @foreach($role_wise_count as $key => $value)
                @if($value['name'] == 'System admin')
                {{ $value['count'] }}
                @endif
                @endforeach
            </strong>)</span>
        </a>
    </li>
    <li>
        <a href="{{ url('user/list/agent-admin') }}">
            Agent users <span>(<strong>
                @foreach($role_wise_count as $key => $value)
                @if($value['name'] == 'Agent admin')
                {{ $value['count'] }}
                @endif
                @endforeach
            </strong>)</span>
        </a>
    </li>
</ol>