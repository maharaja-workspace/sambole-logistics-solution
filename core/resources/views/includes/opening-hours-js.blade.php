<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script>
$(".openHousHoursDisabled :input").prop("disabled", true);
    $('.opening-hours-input').each(function(){
        $(this).datetimepicker({
                    format: 'HH:mm'
                });
    });
$('.opening-hours-input').click(function(){
    $(this).datetimepicker('show');
});

$(document).ready(function(){
    toggleOpenHours();
});

$('.opening-hours-format').click(function(){
    toggleOpenHours();
});

function toggleOpenHours(){
    if($(".opening-hours-format:checked").val() == 'selected'){
        $('#openHousHourSelect').show();
    }else{
        $('#openHousHourSelect').hide();
    }
}

$('.opening-hours-days').each(function(){
    let eleId = $(this).attr('id');
    $(this).change(function(){
        if($(this).prop("checked")){
            $('#'+eleId+'-start').prop('disabled', false);
            $('#'+eleId+'-end').prop('disabled', false);
        }else{
            $('#'+eleId+'-start').prop('disabled', true);
            $('#'+eleId+'-end').prop('disabled', true);
        }
    });
});
</script>