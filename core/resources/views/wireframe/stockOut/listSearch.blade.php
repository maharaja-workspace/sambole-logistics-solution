@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}

.aligncenter{
  text-align:center;
}

.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
.align-middle{
  padding-top: 6px;
  width: 80px;
}
</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>User</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>User List</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('user/add')}}';">
  <p class="plus">+</p>   
</div>

<div class="row">
  <div class="col-lg-12 margins">
    <div class="ibox-content">
 
  <div class="row">
 
   <div class="col-sm-1 align-middle">
          <h3><b>Filter: </b></h3>
   </div>
  <div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">Active</option>
      <option value="pending">Pending</option>
      <option value="deleted">Deleted</option>
    </select>
  </div>
  
</div>

</div>
<h2>Stock Out Management</h2>
</div>
</div>
</div>


 <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}
<div class="row">
  <div class="col-lg-12 margins">

    <div class="ibox-content">

    <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}

                <div class="form-group"><label class="col-sm-3 control-label">Vehicle Number <span class="required">*</span></label>
                   <div class="col-sm-4">
                       <select data-placeholder="Choose" class="js-source-states" style="width: 100%"  name="vehicle_number">
                        <option value="">Choose</option>
                        <option value="colombo7">BCU-8946</option>
                        <option value="Galle">CAD-1996</option>
                        </select>
                   </div>
               </div>

        <div class="form-group"><label class="col-sm-3 control-label">Rider Name <!--<span class="required">*</span>--></label>
            <div class="col-sm-4"><input type="text" name="rider_name" class="form-control" value="{{old('rider_name')}}"></div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Vehicle Type <!--<span class="required">*</span>--></label>
            <div class="col-sm-4"><input type="text" name="vehicle_type" class="form-control" value="{{old('vehicle_type')}}"></div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Vehicle Model <!--<span class="required">*</span>--></label>
            <div class="col-sm-4"><input type="text" name="vehicle_model" class="form-control" value="{{old('vehicle_model')}}"></div>
        </div>
        <div class="form-group"><label class="col-sm-3 control-label">Vehicle Color <!--<span class="required">*</span>--></label>
            <div class="col-sm-4"><input type="text" name="vehicle_color" class="form-control" value="{{old('vehicle_color')}}"></div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 col-sm-offset-8">
                <button class="btn btn-danger" style="float: right;" type="button" onclick="location.reload();">CANCEL</button>
            </div><div class="col-sm-2">
                {{--<button class="btn btn-primary" type="submit">Transfer All</button>--}}
                <button class="btn btn-info" type="button" onclick="location.href = '/sambole-logistics-solution/sPickUp/list';" >SELECT</button>
            </div>
        </div>
        <hr>
               <div class="form-group"><label class="col-sm-3 control-label">BIN SELECTION <span class="required">*</span></label>
                   <div class="col-sm-4">

                        <select class="form-control" name="bin_selection">
                        <option value="">Choose</option>
                        <option value="colombo7">bin 1</option>
                        <option value="Galle">bin 2</option>
                        </select>
                   </div>
               </div>

               <div class="form-group"><label class="col-sm-3 control-label">SEARCH <span class="required">*</span></label>
                   <div class="col-sm-4"><input type="text" name="search" class="form-control" value="{{old('search')}}"></div>

               </div>  
             
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                      <!--  <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>-->
                        <button class="btn btn-primary" type="submit">Transfer All</button>
                      <!--  <button class="btn btn-primary" type="button" onclick="location.href = '/sambole-logistics-solution/sPickUp/list';" >Reject</button>-->
                    </div>
                </div>

            </form>
              <div class="hr-line-dashed"></div>
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th><input type="checkbox"></th>
            <th>Ref No</th>              
            <th>Merchant Name</th>
            <th>Merchant Mobile Number</th>   
            <th>Buyer Name</th>
            <th>Mobile Number</th>
            <th class="aligncenter">Stock Transfer</th>                        
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td><input type="checkbox"></td>
                  <td>xxxx</td>
                  <td>Name01</td>
                  <td>077XXXXXXX</td>
                  <td>BName</td>
                  <td>077XXXXXXX</td>
                  <td class="aligncenter">
                  <button class="btn  btn-info" type="button">
                    <i class="fa fa-exchange"></i>
                  </button>
                  <!--<button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>-->
                  </td>
              </tr>
              <tr>
              <td><input type="checkbox"></td>
              <td>xxxx</td>
              <td>Name02</td>
                  <td>077XXXXXXX</td>
                  <td>BName</td>
                  <td>077XXXXXXX</td>
                  <td class="aligncenter">
                  <button class="btn  btn-info" type="button">
                    <i class="fa fa-exchange"></i>
                  </button>
                  <!--<button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>-->
                  </td>
              </tr>
              <tr>
                  <td><input type="checkbox"></td>
                  <td>xxxx</td>
              <td>Name03</td>
                  <td>077XXXXXXX</td>
                  <td>BName</td>
                  <td>077XXXXXXX</td>
                  <td class="aligncenter">
                  <button class="btn  btn-info" type="button">
                    <i class="fa fa-exchange"></i>
                  </button>
                  <!--<button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>-->
                  </td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>
</div>


@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                search: {
                    required: true,
                    lettersonly: true,
                    maxlength: 10             
                },
                bin_selection:{
                    required: true
                },
                vehicle_selection:{
                    required: true,
                   // lettersonly: false,
                   // maxlength: 200
                },
              /*  username:{
                    required: true,
                    maxlength: 20,
                },*/
               /* password:{
                    required: true
                },*/
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop