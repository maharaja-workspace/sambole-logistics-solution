@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">STORE NAME <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="store_name" class="form-control" value="{{old('store_name')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">STORE ID <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="store_id_name" class="form-control" value="{{old('store_id_name')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">WAREHOUSE<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select class="form-control m-b" name="warehouse_input">
                                        <option>WH DATA  1</option>
                                        <option>WH DATA  2</option>
                                        <option>WH DATA  3</option>
                                        <option>WH DATA  4</option>
                                    </select>
                   </div>
               </div>

                <div class="form-group"><label class="col-sm-2 control-label">STORE TYPE<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select  onchange="yesnoCheck(this);" class="form-control m-b" name="store_type">
                                        <option value="main">Main Stores</option>
                                        <option value="mobile">Mobile Stores</option>
                                    </select>
                   </div>
               </div>

               <div id="ifMobile" style="display: none;">
               <div class="form-group"><label class="col-sm-2 control-label">Vehicle Number <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text"  name="vh_num" class="form-control textnosize" value="{{old('vh_num')}}"></div>
               </div> 
               <div class="form-group"><label class="col-sm-2 control-label">Vehicle Type<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select class="form-control m-b" name="vehicle_input">
                                        <option>VHECILE TYPE 1</option>
                                        <option>VHECILE TYPE  2</option>
                                        <option>VHECILE TYPE  3</option>
                                        <option>VHECILE TYPE  4</option>
                                    </select>
                   </div>
              </div>
              </div>


             <div class="form-group"><label class="col-sm-2 control-label">BIN<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select class="form-control m-b" name="bin_input">
                                        <option>BIN 1</option>
                                        <option>BIN  2</option>
                                        <option>BIN  3</option>
                                        <option>BIN  4</option>
                                    </select>
                   </div>
              </div>
             

               <!-- <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password" id="password" class="form-control"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                </div>-->

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                store_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 20             
                },
                store_id_name:{
                    required: true,
                    lettersonly: false,
                    maxlength: 200
                },
                warehouse_input:{
                    required: true
                },
                price_tag:{
                    required: true,
                    maxlength: 20,
                },
                store_type:{
                    required: true,
                },
                vehicle_input:{
                    required: true,
                },
                vh_num:{
                    required: true,
                },
                bin_input:{
                    required: true,
                }
              /*  branch:{
                    required: true
                },
                username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });

        //function show mobile input type and vehicles
        function yesnoCheck(that) {
            if (that.value == "mobile") {
        alert("Please Enter");
                document.getElementById("ifMobile").style.display = "block";
            } else {
                document.getElementById("ifMobile").style.display = "none";
            }
        }

 </script>
 @stop