@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">

    <div class="col-lg-12 margins">
        <div class="ibox-content">
        <h2>Pending Delivery Ordres Management</h2>
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">REFERENCE NUMBER <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">0852764</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">BUYER NAME <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">Saman Perera</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">ADDRESS <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">123, ABC Road, Colombo 3.</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">MOBILE NUMBER <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">0987645782</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE AMOUNT <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">4524</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE SIZE <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">993</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE WEIGHT <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">13g</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">DATE TIME <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">01-14-2019 11:37:00:000000 A.M</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">REMARKS <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">remarks goes here</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">RIDER SELECTION <span class="required">*</span></label>
                   <div class="col-sm-10">
                        <select class="form-control" name="rider_selection">
                        <option value="">Choose</option>
                        <option value="colombo7">Bike</option>
                        <option value="Galle">Lorry</option>
                        </select>
                   </div>
               </div>    
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                        <button class="btn btn-primary" type="button" onclick="location.href = '/sambole-logistics-solution/pendingDelivery/list';" >Reject</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            /*rules: {
                rider_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 10             
                },
                rider_type:{
                    required: true
                },
                email:{
                    required: true,
                   // lettersonly: false,
                    maxlength: 200
                },*/
               /* username:{
                    required: true,
                    maxlength: 20,
                },*/
                rider_selection:{
                    required: true
                },
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop