@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}

.aligncenter{
  text-align:center;
}

.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
.align-middle{
  padding-top: 6px;
  width: 80px;
}
</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>User</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>User List</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('delivery_type/add')}}';">
  <p class="plus">+</p>   
</div>

<div class="row">
  <div class="col-lg-12 margins">
    <h2>Delivery Type Management</h2>
    <div class="ibox-content">
 
  <div class="row">
 
   <div class="col-sm-1 align-middle">
          <h3><b>Filter: </b></h3>
   </div>
  <div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">Active</option>
      <option value="pending">Pending</option>
      <option value="deleted">Deleted</option>
    </select>
  </div>
</div>
</div>
</div>
</div>
</div>

 <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}
<div class="row">
  <div class="col-lg-12 margins">

    <div class="ibox-content">
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th>Delivery Type Name</th>                  
            <th>Discription</th>
            <th>Price</th>                        
            <th class="aligncenter">Action</th>                        
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td>Package Name </td>
                  <td>Product Discription Goes Here</td>
                  <td>Rs.20 000</td>
                  <td class="aligncenter">
                  <button class="btn  btn-info">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>
                  </td>
              </tr>
              <tr>
                  <td>Package Name</td>
                  <td>Product Discription Goes Here</td>
                  <td>Rs.129 000</td>
                  <td class="aligncenter">
                  <button class="btn btn-info">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>
                  </td>
              </tr>
              <tr>
                  <td>Package Name</td>
                  <td>Product Discription Goes Here</td>
                  <td>Rs.34 000</td>
                  <td class="aligncenter">
                  <button class="btn btn-info">
                    <i class="fa fa-edit"></i>
                  </button>
                  <button class="btn btn-danger">
                    <i class="fa fa-trash"></i>
                  </button>
                  </td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>
</div>

@stop
@section('js')




@stop