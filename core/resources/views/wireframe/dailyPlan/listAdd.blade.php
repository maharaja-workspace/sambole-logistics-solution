@extends('layouts.back.master') @section('current_title','All User')
@section('css')

    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .btn.btn-secondary {
            margin: 0 2px 0 2px;
        }

        /*tr:hover .fixed_float{
          width: 45px;
          padding-left:0px;
        }*/

        /* tr:hover .fixed_float{
          width: 50px;
          padding:  5px 0 0 0px;
        } */
        .fixed_float {
            left: 85%;
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .btn.btn-primary.btn-sm.ad-view {
            font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 600;
            text-shadow: none;
            font-size: 13px;
        }

        .row-highlight-clr {
            /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
            background-color: rgba(0, 0, 0, 0.5) !important;
            color: #fff !important;
        }

        #checkAll {
            margin-right: 10px;
        }

        .align-middle {
            padding-top: 6px;
            width: 80px;
        }
    </style>

@stop
@section('page_header')
    <div class="col-lg-5">
        <h2>User</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>User List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-7">
        <h2>
            <small>&nbsp;</small>
        </h2>
        <ol class="breadcrumb text-right">


        </ol>
    </div>

@stop

@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
         onclick="location.href = '{{url('zone/add')}}';">
        <p class="plus">+</p>
    </div>

    <div class="row">
        <div class="col-lg-12 margins">
            <h2>Daily Plan</h2>
            <div class="ibox-content">

                <div class="row">


                    <form method="POST" class="form-horizontal" id="form2">
                        {!!Form::token()!!}
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Rider <span class="required">*</span></label>
                            <div class="col-sm-3">
                            <!--<input type="text" name="zone_name" class="form-control" value="{{old('zone_name')}}">-->
                                <select class="form-control" name="rider">
                                    <option value="all">All</option>
                                    <option value="active">Rider 1</option>
                                    <option value="pending">Rider 2</option>
                                    <option value="deleted">Rider 3</option>
                                </select>
                            </div>
                            <label class="col-sm-1 control-label">Route <span class="required">*</span></label>
                            <div class="col-sm-3">
                            <!--<input type="text" name="zone_code" disabled class="form-control" value="{{old('zone_code')}}">-->
                                <select class="form-control" name="route">
                                    <option value="all">All</option>
                                    <option value="active">Route 1</option>
                                    <option value="pending">Route 2</option>
                                    <option value="deleted">Route 3</option>
                                </select>
                            </div>
                            <label class="col-sm-1 control-label">Vehicle <span class="required">*</span></label>
                            <div class="col-sm-3">
                            <!--<input type="text" name="zone_code" disabled class="form-control" value="{{old('zone_code')}}">-->
                                <select class="form-control" name="vehicle">
                                    <option value="all">All</option>
                                    <option value="active">vehicle 1</option>
                                    <option value="pending">vehicle 2</option>
                                    <option value="deleted">vehicle 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1 align-middle">
                            <button class="btn btn-primary" type="submit">Select</button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>
    </div>

    <!--<form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}-->
    <div class="row">
        <div class="col-lg-12 margins">


            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Pending Pick Ups</h4>
                    </div>
                    <div class="col-sm-1 col-sm-offset-7">
                        <button class="btn btn-info" type="button" onclick="assign()">Assign</button>
                    </div>
                </div>
                <br>

                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox"></th>
                        <th>Reference Number</th>
                        <th>Merchant Name</th>
                        <th>Address</th>
                        <th>Mobile Number</th>
                        <th>Destination Address</th>
                        <th>Buyer Name</th>
                        <!-- <th>Action</th>     -->
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Pending Deliveries</h4>
                    </div>
                    <div class="col-sm-1 col-sm-offset-7">
                        <button class="btn btn-info" type="button" onclick="assign()">Assign</button>
                    </div>
                </div>
                <br>

                <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th><input type="checkbox"></th>
                        <th>Reference Number</th>
                        <th>Merchant Name</th>
                        <th>Address</th>
                        <th>Mobile Number</th>
                        <th>Destination Address</th>
                        <th>Buyer Name</th>
                        <!-- <th>Action</th>     -->
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>Reference number goes here</td>
                        <td>name goes here</td>
                        <td>address goes here</td>
                        <td>mobile number goes here</td>
                        <td>address goes here</td>
                        <td>name goes here</td>
                        <!-- <td>
                         <button>
                           <i class="fa fa-edit"></i>
                         </button>
                         <button>
                           <i class="fa fa-trash"></i>
                         </button>
                         </td>-->
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 margins">
            <div class="ibox-content">

                <!-- <div class="form-group"> -->
                <!--<label class="col-sm-1 control-label">Designations</label>-->
                <!-- <div class="col-sm-1 align-middle">-->
                <!--<input type="checkbox" name="checkAll" id="checkAll"> All- -->
                <!--</div>-->
                <!--<div class="col-sm-3">-->
                <!-- <select class="form-control"  name="role">
                  <
                </select>-->
                <!-- </div> -->
                <div class="col-sm-2">
                    <label>&nbsp</label>
                </div>
            </div>
            <!-- </form>-->
        </div>
    </div>


    </div>
@stop
@section('js')

    <script type="text/javascript">
        function assign() {
            var txt;
            var person = prompt("Assign Rider:", "");
            if (person == null || person == "") {
                txt = "User cancelled the prompt.";
            } else {
                txt = "Hello " + person + "! How are you today?";
            }
            document.getElementById("demo").innerHTML = txt;
        }

        $("#form2").validate({
            rules: {
                rider: {
                    required: true,
                    //lettersonly: true,
                    //maxlength: 20             
                },
                route: {
                    required: true,
                },
                vehicle: {
                    required: true
                },

            },
            submitHandler: function (form) {
                form.submit();
            }
        });
        })
        ;


    </script>
@stop