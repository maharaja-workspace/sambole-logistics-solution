@extends('layouts.back.master') @section('current_title','Edit User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}
.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
.align-middle{
  padding-top: 6px;
  width: 80px;
}
</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>User</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>User List</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



   </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('zone/add')}}';">
  <p class="plus">+</p>   
</div>


<!-- <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}-->
<div class="row">
  <div class="col-lg-12 margins">

    <div class="ibox-content">
      <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}

                <div class="form-group"><label class="col-sm-2 control-label">PICKUP REQUESTER'S NAME</label>
                   <div class="col-sm-10"><input type="text" name="reqName" class="form-control" value="{{old('req_name')}}"></div>
               </div>

               <div class="form-group"><label class="col-sm-2 control-label">PICKUP REQUESTER'S ADDRESS</label>
                   <div class="col-sm-10"><input type="text" name="reqAddress" class="form-control" value="{{old('req_address')}}"></div>
               </div>

                <div class="form-group"><label class="col-sm-2 control-label">PICKUP CITY</label>
                   <div class="col-sm-10">
                        <select class="form-control" name="pickup_city">
                        <option value="">Choose</option>
                        <option value="colombo7">Colombo - 7</option>
                        <option value="Galle">Galle</option>
                        <option value="kaluthara">Kaluthara<option>
                        </select>
                   </div>
               </div><br>

               <div class="form-group"><label class="col-sm-2 control-label">CUSTOMER'S NAME</label>
                   <div class="col-sm-10"><input type="text" name="cusName" class="form-control" value="{{old('cus_name')}}"></div>
               </div>

               <div class="form-group"><label class="col-sm-2 control-label">DELIVERY ADDRESS</label>
                   <div class="col-sm-10"><input type="text" name="cusAddress" class="form-control" value="{{old('del_address')}}"></div>
               </div>

                <div class="form-group"><label class="col-sm-2 control-label">DELIVERY CITY</label>
                   <div class="col-sm-10">
                        <select class="form-control" name="del_city">
                        <option value="">Choose</option>
                        <option value="colombo7">Colombo - 7</option>
                        <option value="Galle">Galle</option>
                        <option value="kaluthara">Kaluthara<option>
                        </select>
                   </div>
               </div><br>
                

                <div class="form-group"><label class="col-sm-2 control-label">PACKAGE SIZE</label>
                   <div class="col-sm-10"><input type="text" name="pk_size" class="form-control" value="{{old('pk_size')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE WEIGHT</label>
                   <div class="col-sm-10"><input type="text" name="pk_weight" class="form-control" value="{{old('pk_weight')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE TYPE</label>
                   <div class="col-sm-10">
                        <select class="form-control" name="pk_type">
                        <option value="">Choose</option>
                        <option value="type01">Type 01</option>
                        <option value="type02">Type 02</option>
                        <option value="type03">Type 03<option>
                        </select>
                   </div>
               </div>

               <div class="form-group"><label class="col-sm-2 control-label">CITY CODE <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="city_code" class="form-control" value="{{old('city_code')}}"></div>
               </div> 

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>
    </div>
  </div>
</div> 
@stop
@section('js')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
    rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    $(function () {
        $("#dialog").dialog({
            modal: true,
            autoOpen: false,
            title: "Assign Rider",
            width: 300,
            height: 150
        });
        $("#btnShow").click(function () {
            $('#dialog').dialog('open');
        });
    });
</script>


@stop