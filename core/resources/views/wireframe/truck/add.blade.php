@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">VEHICLE NUMBER <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="number" name="vehicle_number" class="form-control" value="{{old('vehicle_number')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">TRANSPORTER NAME <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="transporter_name" class="form-control" value="{{old('transporter_name')}}"></div>
               </div> 
               <div class="form-group"><label class="col-sm-2 control-label">USERNAME <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="email" name="username" class="form-control" value="{{old('username')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="password" name="password" class="form-control" value="{{old('password')}}"></div>
               </div>

               <div class="form-group"><label class="col-sm-2 control-label">SELECT USER <!--<span class="required">*</span>--></label>
                   <div class="col-sm-10">
                        <select class="form-control" name="city_selection">
                        <option value="">Choose</option>
                        <option value="colombo7">User 1</option>
                        <option value="Galle">User 2</option>
                        <option value="kaluthara">User 3<option>
                        </select>
                   </div>
               </div> 
               <!-- <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password" id="password" class="form-control"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                </div>-->

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                vehicle_number: {
                    required: true,
                    //lettersonly: true,
                    maxlength: 10             
                },
                transporter_name:{
                    required: true,
                    lettersonly: false,
                    maxlength: 200
                },
                username:{
                    required: true,
                    maxlength: 20,
                },
                password:{
                    required: true
                },
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop