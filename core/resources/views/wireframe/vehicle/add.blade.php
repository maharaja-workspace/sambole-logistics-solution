@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<style>
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  margin-top: 5%;
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a green background */
.container input:checked ~ .checkmark {
  background-color: #18a689;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container .checkmark:after {
  left: 9px;
  top: 7px;
  width: 7px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 2px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">
    <h2>Vehicle Creation</h2>
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">Vehicle Number <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="vehicle_number" class="form-control" value="{{old('vehicle_number')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Type <span class="required">*</span></label>
                   <div class="col-sm-3">
                        <select class="form-control" name="type">
                        <option value=""></option>
                        <option value="lorry">Lorry</option>
                        <option value="bikes">Bikes</option>
                        <option value="transport trucks">Transport Trucks<option>
                        </select>
                   </div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Customer Pick up Truck <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><label class="container">
                <input type="checkbox" id="vehicle1" value="pickUpTruck"><span class="checkmark"></span></label></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Color <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="color" class="form-control" value="{{old('color')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Model <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="model" class="form-control" value="{{old('model')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">Make <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="make" class="form-control" value="{{old('make')}}"></div>
                </div>
                <div id="addLocation" style="display: none" class="form-group"><label class="col-sm-2 control-label">Rack Locations </label>
                   <div class="col-sm-3"><input type="text" id="location" class="form-control" ></div>
                </div>
             
                <!--<div class="form-group"><label class="col-sm-7 control-label">ZONE CODE <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="zone_code" class="form-control" value="{{old('zone_code')}}"></div>
                </div>-->
               <!-- <div class="form-group"><label class="col-sm-2 control-label">AREA SELECTION<span class="required">*</span></label>
                   <div class="col-sm-10">
                        <select class="form-control" name="area_selection">
                        <option value="">Choose</option>
                        <option value="colombo7">Colombo - 7</option>
                        <option value="Galle">Galle</option>
                        <option value="kaluthara">Kaluthara<option>
                        </select>
                   </div>
               </div> -->
               
               
               
                <!--<div class="hr-line-dashed"></div>-->
                <!--<div class="form-group">
                   <div class="col-sm-10 col-sm-offset-3">
                        <button  class="btn btn-info" type="submit">Done</button>
                        <button  class="btn btn-danger" onclick="location.reload();">Cancel</button>
                    </div>
                </div>-->
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The area name can only consist of alphabetical & underscore"); 

        $(function () {
        $("#vehicle1").click(function () {
            if ($(this).is(":checked")) {
                $("#addLocation").show();
                //$("#noLocation").hide();
            } else {
                $("#addLocation").hide();
                //$("#noLocation").show();
            }
        });
    });

         $("#form").validate({
            rules: {
                vehicle_number: {
                    required: true,
                   // lettersonly: true,
                   // maxlength: 20             
                },
                type:{
                    required: true,
                    //lettersonly: false,
                    //maxlength: 20
                },
                color:{
                    required: true
                },
                model:{
                    required: true
                },
                make:{
                    required: true
                },
               /* username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop