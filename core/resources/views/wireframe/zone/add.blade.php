@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('page_header')
    <div class="col-lg-9">
        <h2>User</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">
                <strong>New User </strong>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 margins">
            <div class="ibox-content">
                <h2>Zone Creation</h2>

                <form method="POST" class="form-horizontal" id="form">
                    {!!Form::token()!!}


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Zone Name <span class="required">*</span></label>
                        <div class="col-sm-3">
                            <input type="text" name="zone_name" class="form-control" value="{{old('zone_name')}}">
                        </div>
                        <label class="col-sm-2 control-label">Zone Code</label>
                        <div class="col-sm-3"><input type="text" name="zone_code" disabled class="form-control"
                                                     value="{{old('zone_code')}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-3"><input type="text" name="description" class="form-control"
                                                     value="{{old('description')}}"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Zone Manager <span
                                    class="required">*</span></label>
                        <div class="col-sm-3">
                            <select class="form-control" name="zone_manager">
                                <option value=""></option>
                                <option value="colombo7">A</option>
                                <option value="Galle">B</option>
                                <option value="kaluthara">C
                                <option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Cities <span class="required">*</span></label>
                        <div class="col-sm-3">

                            <select class="js-source-states" style="width: 100%" name="cities[]" multiple="multiple"
                                    data-placeholder="Choose">
                                <option value="" disabled>Choose</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="D">D</option>
                                <option value="C">C</option>
                            </select>


                        <!-- <select id='cities_multiselect' data-placeholder="Choose" class="js-source-states" style="width: 100%" name="cities[]" multiple="multiple" name="cities">
                    		 	 <?php //foreach ($permissionArr as $key => $value): 
                        //echo $value;
                        //endforeach ?>
                                <option value="" disabled>Choose</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="D">D</option>
                                <option value="C">C</option>
		                    </select> -->
                        </div>
                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">Area <span
                                    class="required">*</span></label>
                        <div class="col-sm-3">
                            <select class="form-control" name="area">
                                <option value=""></option>
                                <option value="colombo7">A</option>
                                <option value="Galle">B</option>
                                <option value="kaluthara">C
                                <option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Price <span class="required">*</span></label>
                        <div class="col-sm-3"><input type="text" name="price" class="form-control"
                                                     value="{{old('price')}}"></div>
                    </div>
                <!--<div class="form-group"><label class="col-sm-7 control-label">ZONE CODE <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="zone_code" class="form-control" value="{{old('zone_code')}}"></div>
                </div>-->
                    <!-- <div class="form-group"><label class="col-sm-2 control-label">AREA SELECTION<span class="required">*</span></label>
                        <div class="col-sm-10">
                             <select class="form-control" name="area_selection">
                             <option value="">Choose</option>
                             <option value="colombo7">Colombo - 7</option>
                             <option value="Galle">Galle</option>
                             <option value="kaluthara">Kaluthara<option>
                             </select>
                        </div>
                    </div> -->


                    <!--<div class="hr-line-dashed"></div>-->
                    <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-3">
                            <button class="btn btn-info" type="submit">Done</button>
                            <button type="button" class="btn btn-danger" style="float: right;"
                                    onclick="location.reload();">Cancel
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


        $(document).ready(function () {
            $(".js-source-states").select2();

            jQuery.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
            }, "The area name can only consist of alphabetical & underscore");


            $("#form").validate({
                rules: {
                    zone_name: {
                        required: true,
                        lettersonly: true,
                        maxlength: 20
                    },
                    cities: {
                        required: true,
                        //lettersonly: false,
                        //maxlength: 20
                    },
                    zone_manager: {
                        required: true
                    },
                    area: {
                        required: true
                    },
                    price: {
                        required: true
                    },
                    /* username:{
                         required: true,
                         email: true
                     },
                     password:{
                         required: true,
                         minlength: 6
                     },
                     password_confirmation:{
                         required: true,
                         minlength: 6,
                         equalTo: '#password'
                     },
                     "roles[]":{
                         required: true
                     }*/
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop