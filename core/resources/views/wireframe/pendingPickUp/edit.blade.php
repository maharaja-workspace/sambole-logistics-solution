@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}
.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
#Wheader{
  margin-left: 10px;
}

.align-middle{
    margin-top:10px ;
}
.h4 {
    font-size: 14px;
}
.balance {
    margin-top: 2%;
    width:50%;
}



.clickable{
    cursor: pointer;   
}

.switch {
  position: relative;
  display: inline-block;
  width: 40px;
  height: 10px;
  bottom: -4px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: -2px;
  background-color: #0286f9;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #52abf9;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(18px);
  -ms-transform: translateX(18px);
  transform: translateX(18px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
  bottom: -3px;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 40%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>User</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>User List</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')

<div class="row">
  <div class="col-lg-12 margins">

    <div class="ibox-content">


  <div class="row">
    

    <div class="col-sm-5" align="right">

      <div class="row">
        <div class="col-xs-6">
          <label>Pickup Requester's Name</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Pickup Address</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>&nbsp</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Pickup City</label>          
        </div>
        <div class="col-xs-6">
          <!-- <input type="text" class="form-control" name="packType"> -->
          <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Kandy</option>
                        <option value="colombo7">Colombo</option>
                        <option value="Galle">Galle</option>
                        </select>
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Customer Name</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Customer Address</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>&nbsp</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Customer's City</label>          
        </div>
        <div class="col-xs-6">
          <!-- <input type="text" class="form-control" name="packType"> -->
          <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="cusCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Kandy</option>
                        <option value="colombo7">Colombo</option>
                        <option value="Galle">Galle</option>
                        </select>
        </div>
      </div><br><br>

      <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-4" align="right">
          <label>Cash on Delivery</label>          
        </div>
        <div class="col-xs-2" align="center">
          <label class="switch">
            <input type="checkbox">
            <span class="slider round"></span>
          </label>
        </div>
        <div class="col-xs-4" align="left">
          <label>Paid</label>          
        </div>
      </div><br><br>

      <div class="row">
        <div class="col-xs-6" align="right">
          <input type="radio" name="pick" value=""><br>
          <input type="radio" name="pick" value=""><br>
          <input type="radio" name="pick" value="">          
        </div>
        <div class="col-xs-6" align="left">
          <label>Pickup from Store</label><br> 
          <label>Pickup from Mobile Store</label><br> 
          <label>Door Step Delivery</label> 
        </div>
      </div><br>

    </div>

    <div class="col-sm-5" align="right">
      <div class="row">
        <div class="col-xs-6">
          <label>Package Type</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Package Size</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>

      <div class="row">
        <div class="col-xs-6">
          <label>Package Weight</label>          
        </div>
        <div class="col-xs-6">
          <input type="text" class="form-control" name="packType">
        </div>
      </div><br>
    </div>
    <div class="col-sm-2"></div>
  </div><br>
  <div class="row">
    <div class="col-sm-8">&nbsp</div>
    <div class="col-sm-4">
      <div class="row">
      <div class="col-xs-6" align="right"><button class="btn btn-info">SUBMIT</button></div>
      <div class="col-xs-6"><button class="btn btn-danger">CANCEL</button></div>
    </div>
    </div>
  </div>


    </div>
  </div>
</div>

<!-- <div class="row">
  <div class="col-lg-12 margins">
    <div class="ibox-content">
     
       <div class="form-group"> -->
       <!--<label class="col-sm-1 control-label">Designations</label>-->
       <!-- <div class="col-sm-1 align-middle">-->
          <!--<input type="checkbox" name="checkAll" id="checkAll"> All- -->
        <!--</div>-->
        <!--<div class="col-sm-3">-->
        <!-- <select class="form-control"  name="role">
          <
        </select>-->
     <!-- </div> -->
      <!-- <div class="col-sm-2">
        <button class="btn btn-primary" type="button" onclick="confirmation_role_change()">Change</button>
      </div>
    </div>

  </div>
</div>
</div> -->
  </form>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <h1 align="center" style="font-weight:bold;">Sambole Logistics</h1>
    <h2><span class="glyphicon glyphicon-print"></span></h2>
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Reference Number</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="refno"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Customer Name</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="cusName"></div></div></h3>  
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Mobile Number</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="MobiNum"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Package Type</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="PackType"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Package Size</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="PackSize"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Package Weight</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="PackWeight"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Delivery Type</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="DelType"></div></div></h3>   
    <h3><div class="row"><div class="col-sm-6" align="right"><label>Payment type</label></div><div class="col-sm-6"><input type="text" style="width:70%;" class="form-control" name="PayType"></div></div></h3>
    <br>
    <div class="row">
      <div class="col-sm-8" align="right"><button class="btn btn-info">DELIVER</button></div>
      <div class="col-sm-4"><button class="btn btn-danger">CANCEL</button></div>
    </div>   
       
  </div>

</div>

@stop
@section('js')


<script type="text/javascript">
$(document).ready(function() {
  $('.toggle').click(function(){
    $(this).find('span').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
  });
});

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("popup");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>




@stop