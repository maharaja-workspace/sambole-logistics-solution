@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">
<h3>Zone to Zone Price</h3>
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
       
                <div class="form-group">
                <label class="col-sm-2 control-label">Zone Name </label>
                   <div class="col-sm-3">
                        <select class="form-control m-b" name="zone">
                            <option>zone 1</option>
                            <option>zone 2</option>
                        </select>
                   </div>
                   <button class="btn btn-info" type="submit">Select</button>
               </div>   
              <!--  <div class="hr-line-dashed"></div>-->
            </form>

<form method="POST" class="form-horizontal" id="form2">
    {!!Form::token()!!}

   <!-- <div class="form-group"><label class="col-sm-2 control-label">Main Bin <span class="required">*</span></label>
        <div class="col-sm-3">
            <select class="form-control m-b" name="main_bin">
                <option value="">choose</option>
                <option value="main bin 1">main bin 1</option>
                <option value="main bin 2">main bin 2</option>
                           
            </select>
        </div>
    </div>-->
    <div class="form-group">
        <label class="col-sm-3 control-label">Zone <!--<span class="required">*</span>--></label>
        <label class="col-sm-3 control-label">Price <!--<span class="required">*</span>--></label>
    </div>
    <div class="hr-line-dashed"></div>
        <label class="col-sm-3 control-label">Zone A </label>
    <div class="form-group"><label class="col-sm-2 control-label"> <span class="required">*</span></label>
        <div class="col-sm-3"><input type="text" name="price1" class="form-control textnosize" value="{{old('price1')}}"></div>
    </div>
    <div class="hr-line-dashed"></div>
        <label class="col-sm-3 control-label">Zone B </label>
    <div class="form-group"><label class="col-sm-2 control-label"> <span class="required">*</span></label>
        <div class="col-sm-3"><input type="text" name="price2" class="form-control textnosize" value="{{old('price2')}}"></div>
    </div>
    <div class="hr-line-dashed"></div>
        <label class="col-sm-3 control-label">Zone C </label>
    <div class="form-group"><label class="col-sm-2 control-label"> <span class="required">*</span></label>
        <div class="col-sm-3"><input type="text" name="price3" class="form-control textnosize" value="{{old('price3')}}"></div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-8 col-sm-offset-2">
            <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
            <button class="btn btn-primary" type="submit">Done</button>
        </div>
    </div>
</form>
        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


       /*  $("#form").validate({
            rules: {
                code: {
                    required: true,
                  //  maxlength: 20             
                },
                
                description:{
                    required: true,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });*/

     $("#form2").validate({
            rules: {
                price1: {
                    required: true,       
                },
                price2:{
                    required: true,       
                },
                price3:{
                    required: true,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });

        //function show mobile input type and vehicles
        function yesnoCheck(that) {
            if (that.value == "mobile") {
        alert("Please Enter");
                document.getElementById("ifMobile").style.display = "block";
            } else {
                document.getElementById("ifMobile").style.display = "none";
            }
        }

 </script>
 @stop