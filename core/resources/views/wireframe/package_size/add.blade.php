@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">PACKAGE SIZE NAME <span class="required">*</span></label>
                   <div class="col-sm-10">
                   <input type="text" name="pkg_size_name" class="form-control" value="{{old('pkg_size_name')}}">
                   <!-- <select class="form-control" name="pkg_size_name">
                        <option value="">Choose</option>
                        <option value="colombo7">package size 1</option>
                        <option value="Galle">package size 2</option>
                        <option value="kaluthara">package size 3<option>
                        </select> -->
                   </div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">DISCRIPTION <span class="required">*</span></label>
                   <div class="col-sm-10"><textarea name="pkg_disc" class="form-control textnosize" value="{{old('pkg_disc')}}"></textarea></div>
               </div> 
             <!--  <div class="form-group"><label class="col-sm-2 control-label">PACKAGE TYPE <span class="required">*</span></label>
                   <div class="col-sm-10">
                        <select class="form-control" name="package_type">
                        <option value="">Choose</option>
                        <option value="colombo7">Type 1</option>
                        <option value="Galle">Type 2</option>
                        <option value="kaluthara">Type 3<option>
                        </select>
                   </div>
               </div> -->
              <!--  <div class="form-group"><label class="col-sm-2 control-label">PRICE <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="number" name="pkg_price_tag" class="form-control" value="{{old('pkg_price_tag')}}"></div>
                </div>-->

               <!-- <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password" id="password" class="form-control"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                </div>-->

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                pkg_size_name: {
                    required: true,
                   // lettersonly: true,
                   // maxlength: 20             
                },
                pkg_disc:{
                    required: true,
                    lettersonly: false,
                    maxlength: 200
                },
                pkg_price_tag:{
                    required: true,
                    maxlength: 20,
                },
                package_type:{
                    required: true
                },
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop