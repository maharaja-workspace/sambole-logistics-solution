@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}
.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
.align-middle{
  padding-top: 6px;
  width: 80px;
}
</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>User</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>User List</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('stockIn/add')}}';">
  <p class="plus">+</p>   
</div>

<!--<div class="row">
  <div class="col-lg-12 margins">
    <div class="ibox-content">-->
 
 <!-- <div class="row">-->
 <!--<h3>Warehouse View</h3>
   <div class="col-sm-1 align-middle">
          <h3><b>Zone: </b></h3>
   </div>-->
  <!--<div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">Zone 1</option>
      <option value="pending">Zone 2</option>
      <option value="deleted">Zone 3</option>
    </select>
  </div>-->

<!--<div class="col-sm-1 align-middle">
          <h4><b>StoreType: </b></h4>
   </div>-->
 <!-- <div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">store type 1</option>
      <option value="pending">store type 2</option>
      <option value="deleted">store type 3</option>
    </select>
  </div>-->

<!--</div>-->
<!--</div>
</div>
</div>
</div>-->

 <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}

<div class="row">

  <div class="col-lg-12 margins">
    <h2>Stock In Management</h2>

    <div class="ibox-content">
       <div class="form-group"><label class="col-sm-2 control-label">Vehicle <!--<span class="required">*</span>--></label>
           <div class="col-sm-3"><select data-placeholder="Choose" class="js-source-states" style="width: 100%"  name="vehicle_number">
                        <option value="">Choose</option>
                        <option value="colombo7">Motorcycle</option>
                        <option value="Galle">Lorry</option>
                        <option value="Galle">Car</option>
                        </select>
                        <!-- <input type="text" name="search" class="form-control" value="{{old('search')}}"> --></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Rider Name <!--<span class="required">*</span>--></label>

                   <div class="col-sm-3"><label name="rider_name">{{old('rider_name')}}</label>{{--<input type="text" name="rider_name" class="form-control" value="{{old('rider_name')}}">--}}</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Color <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><label name="vehicle_details">{{old('vehicle_details')}}</label>{{--<input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}">--}}</div>

               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Make<!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><label name="vehicle_details">{{old('vehicle_details')}}</label>{{--<input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}">--}}</div>

               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Model <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><label name="vehicle_details">{{old('vehicle_details')}}</label>{{--<input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}">--}}</div>

               </div>
               <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <!--<button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>-->
                        <button class="btn btn-primary" type="submit">Select</button>
                    </div>
                </div>
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th><input type="checkbox"></th>
            <th>Reference Number</th>                  
            <th>Merchant Name</th>
            <th>Merchant Mobile Number</th>  
            <th>Buyer Name</th>   
            <th>Buyer Mobile Number</th>                      
            <th>Root Bin</th>                      
            <th>Transfer</th>                        
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>mobile goes here</td>
                  <td>name goes here</td>
                  <td>mobile goes here</td>
                  <td>
                  <select>
                    <option>A</option>
                  </select>
                  </td>
                  <td>
                  <button  data-toggle="modal" data-target="#myModal" type="reset">
                    <i class="fa fa-exchange"></i>
                  </button>
                  </td>
              </tr>
              <tr>
              <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>mobile goes here</td>
                  <td>name goes here</td>
                  <td>mobile goes here</td>
                  <td>
                  <select>
                    <option>A</option>
                  </select>
                  </td>
                  <td>
                  <button  data-toggle="modal" data-target="#myModal" type="reset">
                    <i class="fa fa-exchange"></i>
                  </button>
                  </td>
              </tr>
              <tr>
              <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>mobile goes here</td>
                  <td>name goes here</td>
                  <td>mobile goes here</td>
                  <td>
                  <select>
                    <option>A</option>
                  </select>
                  </td>
                  <td>
                  <button  data-toggle="modal" data-target="#myModal" type="reset">
                    <i class="fa fa-exchange"></i>
                  </button>
                  </td>
              </tr>
          </tbody>
      </table>
    </div>
  </div>
</div>
<!-- The Modal -->
<div class="modal" id="myModal" align="center">
    <div class="modal-dialog">
      <div class="modal-content" style="width: 50%; align-content: center;">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <span class="modal-title">Scan QR Code</span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" align="center">
          <img alt="image" style="width: 100px;" src="http://localhost/tharushi-sambole-logistics-solution/assets/front/images/qr-code.png">
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

<div class="row">
  <div class="col-lg-12 margins">
    <div class="ibox-content">
     
       <div class="form-group">
       <!--<label class="col-sm-1 control-label">Designations</label>-->
       <!-- <div class="col-sm-1 align-middle">-->
          <!--<input type="checkbox" name="checkAll" id="checkAll"> All- -->
        <!--</div>-->
        <!--<div class="col-sm-3">-->
        <!-- <select class="form-control"  name="role">
          <
        </select>-->
     <!-- </div> -->
      <div class="col-sm-2">
      <!--  <button class="btn btn-primary" type="button" onclick="confirmation_role_change()">Change</button>-->
      </div>
    </div>
  </form>
</div>
</div>
</div>
@stop
@section('js')
<script type="text/javascript">
   $(document).ready(function(){
         $(".js-source-states").select2();

         
       });
</script>



@stop