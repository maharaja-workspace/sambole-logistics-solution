@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">
        <h2>Stock In Management</h2>
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">Vehicle <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><input type="text" name="search" class="form-control" value="{{old('search')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Rider Name <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><input type="text" name="rider_name" class="form-control" value="{{old('rider_name')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Color <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Make <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Model <!--<span class="required">*</span>--></label>
                   <div class="col-sm-3"><input type="text" name="vehicle_details" class="form-control" value="{{old('vehicle_details')}}"></div>
               </div>
               <!--<div class="form-group"><label class="col-sm-2 control-label">ROUTE <span class="required">*</span></label>
                   <div class="col-sm-10">
                        <select class="form-control" name="rider_type">
                        <option value="">Choose</option>
                        <option value="colombo7">route 1 - sub bin 2</option>
                        <option value="Galle">route 2 - sub bin 4</option>
                        </select>
                   </div>
               </div>-->
            <!--   <div class="form-group"><label class="col-sm-2 control-label">PICK UP ID </label>
                   <div class="col-sm-10">15</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Rider Name </label>
                   <div class="col-sm-10">Rider 1</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE SIZE </label>
                   <div class="col-sm-10">243</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE WEIGHT </label>
                   <div class="col-sm-10">19g</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE TYPE </label>
                   <div class="col-sm-10">type 1</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">PACKAGE AMOUNT </label>
                   <div class="col-sm-10">6</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">ZONE </label>
                   <div class="col-sm-10">zone 1</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">BUYER NAME </label>
                   <div class="col-sm-10">Saman Perera</div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">BUYER CONTACT NUMBER </label>
                   <div class="col-sm-10">22343534645</div>
               </div>-->
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <!--<button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>-->
                        <button class="btn btn-primary" type="submit">Select</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                /*rider_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 10             
                },*/
                search:{
                    required: true
                },
               /* email:{
                    required: true,
                   // lettersonly: false,
                    maxlength: 200
                },*/
               /* username:{
                    required: true,
                    maxlength: 20,
                },
                password:{
                    required: true
                },*/
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop