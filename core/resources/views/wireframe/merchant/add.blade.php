@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/css/bootstrap-chosen.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}

                <div class="form-group"><label class="col-sm-2 control-label">COMPANY NAME <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="company_name" class="form-control" value="{{old('company_name')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">MERCHANT CODE <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="merchant_code" value="123" class="form-control" value="{{old('merchant_code')}}" disabled="true"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">BR NUMBER<span class="required">*</span></label>
                   <div class="col-sm-10"><input type="number" name="br_number" class="form-control" value="{{old('br_number')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">NIC NUMBER<span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="nic_number" class="form-control" value="{{old('nic_number')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONTACT PERSON NAME<span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="contact_person_name" class="form-control" value="{{old('contact_person_name')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONTACT NUMBER<span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="contact_number" class="form-control" value="{{old('contact_number')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">ADDRESS<span class="required">*</span></label>
                   <div class="col-sm-10"><textarea type="text" name="address" class="form-control" value="{{old('address')}}"></textarea>
                   </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/chosen.jquery.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The area name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                company_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 20             
                },
                merchant_code:{
                    required: true,
                    lettersonly: false,
                    maxlength: 20
                },
                br_number:{
                    required: true
                },
                nic_number:{
                    required: true
                },
                contact_person_name:{
                    required: true,
                    lettersonly: true,
                    maxlength: 20
                },
                contact_number:{
                    required: true,
                    lettersonly: false,
                    maxlength: 10
                },
                address:{
                    required: true,
                    lettersonly: false,
                    maxlength: 40
                },
              /*  password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop