@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">
<h3>Bin Creation</h3>
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
       
                <div class="form-group"><label class="col-sm-2 control-label">Code <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="code" class="form-control textnosize" value="{{old('code')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Description <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="description" class="form-control textnosize" value="{{old('description')}}"></div>
               </div>
              <!-- <div class="form-group"><label class="col-sm-2 control-label">BIN TYPE<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select class="form-control m-b" name="bin_sec">
                                        <option>RACK</option>
                                        <option>BIN</option>
                           
                                    </select>
                   </div>
               </div>   
                <div class="form-group"><label class="col-sm-2 control-label">BIN ID <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="bin_id" class="form-control textnosize" value="{{old('bin_id')}}"></div>
               </div> 
               
                <div class="form-group"><label class="col-sm-2 control-label">CITY<span class="required">*</span></label>
                   <div class="col-sm-10">
                                <select class="form-control m-b" name="city_input">
                                        <option value="main">Colombo</option>
                                        <option value="mobile">Katunayake</option>
                                    </select>
                   </div>
               </div>-->

            <!--
             <div class="form-group"><label class="col-sm-2 control-label">SUB BINS<span class="required">*</span></label>
                   <div class="col-sm-10">
                    <button class="btn btn-warning" type="button" onclick="location.reload();">Remove Sub Bin</button>
                   </div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">SUB BIN ID <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="bin_id" class="form-control textnosize" value="{{old('bin_id')}}"></div>
               </div> 
               
               <div class="form-group"><label class="col-sm-2 control-label">SUB BIN TYPE <span class="required">*</span></label>
               <div class="col-sm-10">
                                <select class="form-control m-b" name="sub_in_sec">
                                        <option>RACK</option>
                                        <option>BIN</option>
                           
                                    </select>
                   </div>
               </div> 

   <div class="form-group"><label class="col-sm-2 control-label">SUB BINS<span class="required">*</span></label>
                   <div class="col-sm-10">
                    <button class="btn btn-warning" type="button" onclick="location.reload();">Remove Sub Bin</button>
                   </div>
              </div>
              <div class="form-group"><label class="col-sm-2 control-label">SUB BIN ID <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="bin_id2" class="form-control textnosize" value="{{old('bin_id')}}"></div>
               </div> 
               
               <div class="form-group"><label class="col-sm-2 control-label">SUB BIN TYPE <span class="required">*</span></label>
               <div class="col-sm-10">
                                <select class="form-control m-b" name="sub_in_sec2">
                                        <option>RACK</option>
                                        <option>BIN</option>
                           
                                    </select>
                   </div>
               </div> -->
               <!-- <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password" id="password" class="form-control"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                </div>-->

               
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
            </form>
<h3>Sub Bin Creation</h3>

<form method="POST" class="form-horizontal" id="form2">
    {!!Form::token()!!}

    <div class="form-group"><label class="col-sm-2 control-label">Main Bin <span class="required">*</span></label>
        <div class="col-sm-3">
            <select class="form-control m-b" name="main_bin">
                <option value="">choose</option>
                <option value="main bin 1">main bin 1</option>
                <option value="main bin 2">main bin 2</option>
                           
            </select>
        </div>
    </div>
    <div class="form-group"><label class="col-sm-2 control-label">Code <span class="required">*</span></label>
        <div class="col-sm-3"><input type="text" name="code2" class="form-control textnosize" value="{{old('code')}}"></div>
    </div>
    <div class="form-group"><label class="col-sm-2 control-label">Description <span class="required">*</span></label>
        <div class="col-sm-3"><input type="text" name="description2" class="form-control textnosize" value="{{old('description')}}"></div>
    </div>
    
   
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
</form>
        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">


       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                code: {
                    required: true,
                  //  maxlength: 20             
                },
                
                description:{
                    required: true,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });

     $("#form2").validate({
            rules: {
                code2: {
                    required: true,       
                },
                main_bin:{
                    required: true,       
                },
                description2:{
                    required: true,
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });

        //function show mobile input type and vehicles
        function yesnoCheck(that) {
            if (that.value == "mobile") {
        alert("Please Enter");
                document.getElementById("ifMobile").style.display = "block";
            } else {
                document.getElementById("ifMobile").style.display = "none";
            }
        }

 </script>
 @stop