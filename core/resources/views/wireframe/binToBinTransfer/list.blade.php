@extends('layouts.back.masterWithoutFtr') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}
.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
.align-middle{
  padding-top: 6px;
  width: 80px;
}
</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>Bin to Bin Transfer</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>New Transfer</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('binToBinTransfer/add')}}';">
  <p class="plus">+</p>   
</div>

<div class="row">
    <div class="col-lg-12 margins">
      <!-- h2>Bin 2 Bin Transfer</h2> -->
        <div class="ibox-content">
            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                 <div class="form-group">
                <label class="col-sm-2 control-label">Source Bin</label>
                   <div class="col-sm-3">
                        <select class="form-control" name="parent_city">
                        <option value="">Choose</option>
                        <option value="colombo7">Bin 1</option>
                        <option value="Galle">Bin 2</option>
                        <option value="kaluthara">Bin 3</option>
                        </select>
                   </div>
                  </div>
                   
                

                <div class="form-group"><label class="col-sm-2 control-label">Destination Bin</label>
                <div class="col-sm-3">
                        <select class="form-control" name="area">
                        <option value=""></option>
                        <option value="colombo7">Bin 1</option>
                        <option value="Galle">Bin 2</option>
                        <option value="kaluthara">Bin 3</option>
                        </select>
                   </div>
                </div>

            </form>
</div>


<!--<div class="row">
  <div class="col-lg-12 margins">
    <div class="ibox-content">-->
 
 <!-- <div class="row">-->
 <!--<h3>Warehouse View</h3>
   <div class="col-sm-1 align-middle">
          <h3><b>Zone: </b></h3>
   </div>-->
  <!--<div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">Zone 1</option>
      <option value="pending">Zone 2</option>
      <option value="deleted">Zone 3</option>
    </select>
  </div>-->

<!--<div class="col-sm-1 align-middle">
          <h4><b>StoreType: </b></h4>
   </div>-->
 <!-- <div class="col-md-3">
    <select class="form-control" id="status">
      <option value="all">All</option>
      <option value="active">store type 1</option>
      <option value="pending">store type 2</option>
      <option value="deleted">store type 3</option>
    </select>
  </div>-->

<!--</div>-->
<!--</div>
</div>
</div>
</div>-->

 <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}
<div class="row">
  <div class="col-lg-12 margins">


    <div class="ibox-content">
      <div class="col-sm-2 col-sm-offset-10" align="right">
                        <button  class="btn btn-info" type="submit">Transfer All</button>
                       <!-- <button type="button" class="btn btn-danger" onclick="location.reload();">Cancel</button>-->
                    </div><br><br><br>
      <div class="row">
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th><input type="checkbox"></th>
            <th>Reference Number</th>                  
            <th>Merchant Name</th>
            <th>Address</th>
            <th>Merchant Mobile Number</th>  
            <th>Destination</th>   
            <th>Sub Bin</th>                                                                 
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>address goes here</td>
                  <td>mobile goes here</td>
                  <td>destination goes here</td>
                  <td>
                  <select>
                    <option>Sub Bin 1</option>
                    <option>Sub Bin 2</option>
                    <option>Sub Bin 3</option>
                  </select>
                  </td>
              </tr>
              <tr>
              <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>address goes here</td>
                  <td>mobile goes here</td>
                  <td>destination goes here</td>
                  <td>
                  <select>
                    <option>Sub Bin 1</option>
                    <option>Sub Bin 2</option>
                    <option>Sub Bin 3</option>
                  </select>
                  </td>
              </tr>
              <tr>
              <td><input type="checkbox"></td>
                  <td>ref no. goes here</td>
                  <td>Name goes here</td>
                  <td>address goes here</td>
                  <td>mobile goes here</td>
                  <td>destination goes here</td>
                  <td>
                  <select>
                    <option>Sub Bin 1</option>
                    <option>Sub Bin 2</option>
                    <option>Sub Bin 3</option>
                  </select>
                  </td>
              </tr>
          </tbody>
      </table>
      </div>
    </div>
  </div>
</div>

  </form>
</div>
</div>
</div>
@stop
@section('js')




@stop