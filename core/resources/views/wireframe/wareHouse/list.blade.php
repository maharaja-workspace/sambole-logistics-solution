@extends('layouts.back.master') @section('current_title','All User')
@section('css')

<style type="text/css">
#floating-button{
  width: 55px;
  height: 55px;
  border-radius: 50%;
  background: #db4437;
  position: fixed;
  bottom: 50px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px #666;
  z-index:2
}
.btn.btn-secondary{
  margin: 0 2px 0 2px;
}
/*tr:hover .fixed_float{
  width: 45px;
  padding-left:0px;
}*/

/* tr:hover .fixed_float{
  width: 50px;
  padding:  5px 0 0 0px;
} */
.fixed_float{
  left: 85%;
}
.plus{
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}
.btn.btn-primary.btn-sm.ad-view{
  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-weight: 600;
  text-shadow: none;
  font-size: 13px;
}

.row-highlight-clr{
  /*background-color: rgba(244, 67, 54, 0.1)  !important;*/
  background-color: rgba(0, 0, 0, 0.5) !important;
  color: #fff !important;
}
#checkAll{
  margin-right: 10px;
}
#Wheader{
  margin-left: 10px;
}

.align-middle{
    margin-top:3% ;
}
.h4 {
    font-size: 14px;
}
.balance {
    margin-top: 2%;
    width:50%;
}

.Fill {
  padding-left: 2px;
  padding-right: 2px;
}


.clickable{
    cursor: pointer;   
}

</style>

@stop
@section('page_header')
<div class="col-lg-5">
  <h2>Warehouse Management</h2>
  <ol class="breadcrumb">
    <li>
      <a href="{{url('/')}}">Home</a>
    </li>
    <li class="active">
      <strong>Warehouse View</strong>
    </li>
  </ol>
</div>  
<div class="col-lg-7">
  <h2><small>&nbsp;</small></h2>
  <ol class="breadcrumb text-right">



  </ol>
</div>

@stop

@section('content')
<div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create" onclick="location.href = '{{url('wareHouse/add')}}';">
  <p class="plus">+</p>   
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="ibox-content">
 
  <!-- <div class="row">
 <h4 id="Wheader">Warehouse View</h4>
  </div> -->

        <div class="row">
            <div class="col-sm-1">
          <label class="h4">Zone: </label></div>
          <div class="col-sm-4">
    <select class="js-source-states form-control" id="status">

      <option value="all">All</option>
      <option value="active">Zone 1</option>
      <option value="pending">Zone 2</option>
      <option value="deleted">Zone 3</option>
    </select></div>

        <!-- <div class="col-md-4">
          <label class="h4 col-sm-4">StoreType: </label>
    <select class=" col-sm-8 align-middle" id="status">

      <option value="all">All</option>
      <option value="active">store type 1</option>
      <option value="pending">store type 2</option>
      <option value="deleted">store type 3</option>
    </select>
  </div></div>
  <div class="col-md-4">&nbsp</div> -->

</div>
</div>
</div>

</div>


 <form method="POST" class="form-horizontal" id="form">
       {!!Form::token()!!}
<div class="row">
  <div class="col-lg-12"><div class="ibox-content">
    <div class=" clickable">
      <h4 class="toggle" type="button" id="Ms" data-toggle="collapse" data-target="#MainStore"><span class="glyphicon glyphicon-plus"></span> &nbsp Main Store</h4>
    </div>
    
    <div id="MainStore" class=" collapse">
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th>Package ID</th>                  
            <th>Package Type</th>
            <th>Package Size</th>  
            <th>Bin</th>  
            <th>Delivery Type</th>  
            <th>Buyers Name</th>                      
            <th>Address</th>                        
            <th>Mobile Number</th>                       
            <th>Action</th>                       
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
                  <td align="center"><span data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-shopping-cart"></span></td>
              </tr>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
                  <td align="center"><span data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-shopping-cart"></span></td>
              </tr>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
                  <td align="center"><span data-toggle="modal" data-target="#myModal" class="glyphicon glyphicon-shopping-cart"></span></td>
              </tr>
          </tbody>
      </table>
    </div>


<!--   </div>
</div>

<div class="row">
  <div class="col-lg-12 margins"> -->
    <div class="clickable">
      <h4 class="toggle" type="button" id="bs" data-toggle="collapse" data-target="#BikeStore"><span class="glyphicon glyphicon-plus"></span> &nbsp Bike Store</h4>
    </div>
    
    <div id="BikeStore" class=" collapse">
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th>Package ID</th>                  
            <th>Package Type</th>
            <th>Package Size</th>
            <th>Bin</th>  
            <th>Delivery Type</th>  
            <th>Buyers Name</th>                      
            <th>Address</th>                        
            <th>Mobile Number</th>                        
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
          </tbody>
      </table>
    </div>


<!--   </div>
</div>

<div class="row">
  <div class="col-lg-12 margins"> -->
    <div class="clickable">
      <h4 class="toggle" type="button" id="mps" data-toggle="collapse" data-target="#MobilePickup"><span class="glyphicon glyphicon-plus"></span> &nbsp Mobile Pick-up Store</h4>
    </div>
    
    <div id="MobilePickup" class=" collapse">
      <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
        <thead>
          <tr>
            <th>Package ID</th>                  
            <th>Package Type</th>
            <th>Package Size</th>
            <th>Bin</th>  
            <th>Delivery Type</th>  
            <th>Buyers Name</th>                      
            <th>Address</th>                        
            <th>Mobile Number</th>                       
          </tr>
        </thead>
        <tbody>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
              <tr>                  
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>  
                  <td>Bin</td>  
                  <td>Delivery Type</td>
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
              <tr>
                  <td>Package ID</td>                  
                  <td>Package Type</td>
                  <td>Package Size</td>
                  <td>Bin</td>  
                  <td>Delivery Type</td>  
                  <td>Buyers Name</td>                      
                  <td>Address</td>                        
                  <td>Mobile Number</td>
              </tr>
          </tbody>
      </table>
    </div>

    </div>
  </div>
</div>
</form>

<!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <span style="font-weight: 600;" class="modal-title">Sambole Logistics</span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Reference Number</label>
            </div>
            <div class="col-xs-5 Fill">
              <input type="text" class="form-control" name="RefNum">
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Customer Name</label>
            </div>
            <div class="col-xs-5 Fill">
              <input type="text" class="form-control" name="RefNum">
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Mobile Number</label>
            </div>
            <div class="col-xs-5 Fill">
              <input type="text" class="form-control" name="RefNum">
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Package Type</label>
            </div>
            <div class="col-xs-5 Fill">
              <!-- <input type="text" class="form-control" name="RefNum"> -->
              <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Type1</option>
                        <option value="colombo7">Type2</option>
                        <option value="Galle">Type3</option>
              </select>
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Package Size</label>
            </div>
            <div class="col-xs-5 Fill">
              <!-- <input type="text" class="form-control" name="RefNum"> -->
              <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Size1</option>
                        <option value="colombo7">Size2</option>
                        <option value="Galle">Size3</option>
              </select>
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Package Weight</label>
            </div>
            <div class="col-xs-5 Fill">
              <!-- <input type="text" class="form-control" name="RefNum"> -->
              <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Weight1</option>
                        <option value="colombo7">Weight2</option>
                        <option value="Galle">Weight3</option>
              </select>
            </div>
          </div>

          <div class="row" style="margin-bottom: 3px;">
            <div class="col-xs-5  col-sm-offset-1 Fill">
              <label class="align-middle">Delivery Type</label>
            </div>
            <div class="col-xs-5 Fill">
              <!-- <input type="text" class="form-control" name="RefNum"> -->
              <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Type1</option>
                        <option value="colombo7">Type2</option>
                        <option value="Galle">Type3</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-5 col-sm-offset-1 Fill">
              <label class="align-middle">Payment Type</label>
            </div>
            <div class="col-xs-5 Fill">
              <!-- <input type="text" class="form-control" name="RefNum"> -->
              <select data-placeholder="Choose" class="js-source-states form-control" style="width: 100%" name="pickCity" tabindex="-1" title="">
                        <option value="">Choose</option>
                        <option value="">Type1</option>
                        <option value="colombo7">Type2</option>
                        <option value="Galle">Type3</option>
              </select>
            </div>
          </div>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">DELIVER</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL</button>
        </div>
        
      </div>
    </div>
  </div>


@stop
@section('js')


<script type="text/javascript">
$(document).ready(function() {
  $('.toggle').click(function(){
    $(this).find('span').toggleClass('glyphicon-plus').toggleClass('glyphicon-minus');
  });
});
</script>



@stop