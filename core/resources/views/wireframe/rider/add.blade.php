@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}
                <div class="form-group"><label class="col-sm-2 control-label">Rider Name <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="rider_name" class="form-control" value="{{old('rider_name')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">NIC <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="nic" class="form-control" value="{{old('nic')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">License number <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="lno" class="form-control" value="{{old('lno')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Mobile number <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="mobile" class="form-control" value="{{old('mobile')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Zone <span class="required">*</span></label>
                   <div class="col-sm-3">
                        <select class="form-control" name="zone">
                        <option value="">Choose</option>
                        <option value="colombo7">zone 1</option>
                        <option value="Galle">zone 2</option>
                        </select>
                   </div>
               </div>

              <!-- <div class="form-group"><label class="col-sm-2 control-label">MULTIPLE ROUTE SELECTION </label>
                    	<div class="col-sm-10">
                    		 <select data-placeholder="Choose" class="js-source-states" style="width: 100%" name="rotes[]" multiple="multiple" name="multiple_route_selection">
                    		 	 <?php //foreach ($permissionArr as $key => $value): 
                                	//echo $value;                                    
                                //endforeach ?>
                                <option value="" disabled>Choose</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="D">D</option>
                                <option value="C">C</option>
		                    </select>
                    	</div>
                </div>-->
              <!-- <div class="form-group"><label class="col-sm-2 control-label">EMAIL <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="email" name="email" class="form-control" value="{{old('email')}}"></div>
               </div>-->
               <div class="form-group"><label class="col-sm-2 control-label">Username <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="text" name="username" class="form-control" value="{{old('username')}}"></div>
               </div>
               <div class="form-group"><label class="col-sm-2 control-label">Password <span class="required">*</span></label>
                   <div class="col-sm-3"><input type="password" name="password" class="form-control" value="{{old('password')}}"></div>
               </div>

                
               <!-- <div class="form-group"><label class="col-sm-2 control-label">PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password" id="password" class="form-control"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CONFIRM PASSWORD <span class="required">*</span></label>
                    <div class="col-sm-10"><input type="password" name="password_confirmation" class="form-control"></div>
                </div>-->

               <!-- <div class="hr-line-dashed"></div>-->
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-3">
                        <button class="btn btn-info" type="submit">Done</button>
                        <button class="btn btn-danger" type="button" onclick="location.reload();">Cancel</button>
                    </div>
                </div>


              <!--  <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-3">
                        <button  class="btn btn-info" type="submit">Done</button>
                        <button type="button" class="btn btn-danger" onclick="location.reload();">Cancel</button>
                    </div>
                </div>-->


            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The first name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                rider_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 10             
                },
                nic:{
                    required: true
                },
                lno:{
                    required: true,
                   // lettersonly: false,
                   // maxlength: 200
                },
                mobile:{
                    required: true,
                },
                zone:{
                    required: true,
                },
                username:{
                    required: true,
                    maxlength: 20,
                },
                password:{
                    required: true
                },
                /*username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop