@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />

<style type="text/css">
  .map-container-6{
  overflow:hidden;
  padding-bottom:5%;
  margin-bottom: -10%; 
  position:relative;
  height:0;
  }
  .map-container-6 iframe{
  left:2%;
  top:2%;
  height:75%;
  width:50%;
  position:absolute;
  }
</style>

@stop
@section('page_header')
<div class="col-lg-9">
    <h2>User</h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{url('/')}}">Home</a>
        </li>
        <li class="active">
            <strong>New User </strong>
        </li>
    </ol>
</div>                  
@stop
@section('content')
<div class="row">
    <div class="col-lg-12 margins">
        <div class="ibox-content">

            <form method="POST" class="form-horizontal" id="form">
                {!!Form::token()!!}

               
                

                <div class="form-group"><label class="col-sm-2 control-label">ROUTE NAME <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="route_name" class="form-control" value="{{old('route_name')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">ROUTE CODE <span class="required">*</span></label>
                   <div class="col-sm-10"><input type="text" name="route_code" class="form-control" value="{{old('route_code')}}"></div>
                </div>
                <div class="form-group"><label class="col-sm-2 control-label">CITY SELECTION<span class="required">*</span></label>
                   <div class="col-sm-10">
                   <select data-placeholder="Choose" class="js-source-states" style="width: 100%" name="cities[]" multiple="multiple" id="multiple_city_selection">
                    		 	 <?php //foreach ($permissionArr as $key => $value): 
                                	//echo $value;                                    
                                //endforeach ?>
                                <option value="" disabled>Choose</option>
                                <option value="A">Matara</option>
                                <option value="B">Galle</option>
                                <option value="D">Hambanthota</option>
                                <option value="C">Colombo</option>
		                    </select>
                   </div>
               </div>
               
               <div class="form-group"><label class="col-sm-2 control-label">ROUTE PICK-UP LOCATION<span class="required">*</span></label>
                <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
                  <iframe src="https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <!-- <section id="location">
                   <div class="col-sm-10"><input type="text" name="pickup_location" id="pickup_location" class="form-control" value="{{old('pickup_location')}}"></div>
                </div></section> --> 
    
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    @stop
    @section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    {{-- <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script> --}}
    <script type="text/javascript">
        function locate() {

  var location = document.getElementById("location");
  var apiKey = 'f536d4c3330c0a1391370d1443cee848';
  var url = 'https://api.forecast.io/forecast/';

  navigator.geolocation.getCurrentPosition(success, error);

  function success(position) {
      latitude = position.coords.latitude;
      longitude = position.coords.longitude;

      $.getJSON("http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude + "," + longitude + "&language=en", function(data) {
          var fulladd = data.results[0].formatted_address.split(",");
          var count= fulladd.length;

          $('#pickup_location').val(fulladd[count-1]);
      });
  }

  function error() {
      location.innerHTML = "Unable to retrieve your location";
  }
}

locate();
    </script>

    <script type="text/javascript">
       $(document).ready(function(){
         $(".js-source-states").select2();

         jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z_. ]+$/i.test(value);
        }, "The area name can only consist of alphabetical & underscore"); 


         $("#form").validate({
            rules: {
                route_name: {
                    required: true,
                    lettersonly: true,
                    maxlength: 20             
                },
                route_code:{
                    required: true,
                    lettersonly: false,
                    maxlength: 20
                },
                multiple_city_selection:{
                    required: true
                },
               /* username:{
                    required: true,
                    email: true
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    minlength: 6,
                    equalTo: '#password'
                }, 
                "roles[]":{
                    required: true
                }*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     });


 </script>
 @stop