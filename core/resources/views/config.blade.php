@extends('layouts.back.master') @section('current_title','Site Configurations')
@section('css')

<style type="text/css">
.check-list{
	list-style-type: none;
}
.check-list li{
	float: left;
	margin-right: 8px;
	vertical-align: bottom;
}
.check-list li label{
	margin-left: 5px;
	vertical-align: middle;
}

</style>

@stop
@section('page_header')
<div class="col-lg-9">
	<h2>Site Configurations</h2>
	<ol class="breadcrumb">
		<li>
			<a href="{{url('/')}}">Home</a>
		</li>
		<li class="active">
			<strong>Config</strong>
		</li>
	</ol>
</div>           
@stop
@section('content')

<div class="row">
	<div class="col-lg-12 margins">
		<div class="ibox-content">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					{!! Form::model($config, array('route' => 'config', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}

					{!!Form::token()!!}
					
					{!!Form::hidden('id')!!}
					
					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('version', 'System Version', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('version', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('days_count', 'Ads Date Count', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::number('days_count', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('logo', 'Logo', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							<img src="{{Config('constants.bucket.url') . $config->logo }}" alt="Logo" style="max-width: 100px; padding-bottom: 10px;">
							{!! Form::file('logo', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('logo_width', 'Logo Width', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('logo_width', null, array('class' => 'form-control', 'max' => 1000)) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('logo_height', 'Logo Height', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('logo_height', null, array('class' => 'form-control', 'max' => 1000)) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('watermark', 'Watermark', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							@if($config->watermark)
							<img src="{{Config('constants.bucket.url') . $config->watermark }}" alt="Logo" style="max-width: 100px; padding-bottom: 10px;">
							@endif
							{!! Form::file('watermark', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('wt_width', 'Watermark Width', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('wt_width', null, array('class' => 'form-control', 'max' => 1000)) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('wt_height', 'Watermark Height', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('wt_height', null, array('class' => 'form-control', 'max' => 1000)) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('contact', 'Contact Number', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::text('contact', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('email', 'Email Address', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::email('email', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('facebook', 'Facebook Address', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::url('facebook', null, array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">{!! Form::label('instagram', 'Instagram Address', array('class' => 'control-label')) !!}</div>
						<div class="col-sm-9">
							{!! Form::url('instagram', null, array('class' => 'form-control')) !!}
						</div>
					</div>

					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<div class="col-sm-8">
							{!! Form::submit('Save Changes', array('class' => 'btn btn-primary btn-')) !!}
						</div>
					</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
{{-- {{dd($errors)}} --}}
@stop
@section('js')
<script>
	@if($errors->any())
	@foreach($errors->all() as $error)
	toastr.error("{!! $error !!}");
	@endforeach
	@endif
</script>
@stop